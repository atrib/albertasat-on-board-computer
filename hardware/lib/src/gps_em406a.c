/*
 * gps_em406a.c
 *
 *
 * THIS IS AN INCOMPLETE DRIVER USED ONLY TO HELP CONFIRM BUILD SETTINGS ARE WORKING.
 * The intention is that this file will be deleted once other production code is in place.
 * In particular, this is not the GPS device we will be using for Albertasat.
 */
#include "gps_em406a.h"

static size_t GpsDriver_getCharsImpl(GpsDriver* gpsDriver, char* buffer){
	return 0;
}

size_t (*GpsDriver_getChars)(GpsDriver* gpsDriver, char* buffer) = GpsDriver_getCharsImpl;

static void GpsDriver_putCharsImpl(GpsDriver* gpsDriver, char* msg, size_t len){
}
void (*GpsDriver_putChars)(GpsDriver* gpsDriver, char* msg, size_t len) = GpsDriver_putCharsImpl;

