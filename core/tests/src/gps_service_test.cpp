extern "C" {
#include "gps_service.h"
}
#include <CppUTest/TestHarness.h>
#include <string.h>

TEST_GROUP(GpsServiceTest){
};


static void infoEqual(const GpsInfo& a, const GpsInfo& b){
	STRCMP_EQUAL(a.lat, b.lat);
	STRCMP_EQUAL(a.lon, b.lon);
	STRCMP_EQUAL(a.utc, b.utc);
}

// Should be able to set/get a fix

TEST(GpsServiceTest, SetGetFix){
    GpsInfo expected;
    strcpy(expected.lat,"1235.5556");
    strcpy(expected.lon,"16755.4444");
    strcpy(expected.utc,"175677.234");
    GpsService_setFix(&expected);
    GpsInfo actual;
    GpsService_fix(&actual);
    infoEqual(expected, actual);
}

