/*
 * nmea_serde_test.cpp
 *
 *  Created on: Nov 24, 2014
 *      Author: albertasat
 */
extern "C" {
#include "nmea_serde.h"
}
#include <CppUTest/TestHarness.h>
#include <string.h>
#include <string>
using namespace std;

TEST_GROUP(NmeaSerdeTest){
};

// Should be able to parse $GPGGA string
// $GPGGA,utc(hhmmss.sss),lat(ddmm.mmmm),N/Sind(N or S),lon(dddmm.mmmm),
// E/W ind (E or W),Position Fix Indicator(0-3), Satellites used(0-12),
// HDOP(n.n?), alt, units(M), Geoid separation (blank?), units(M),
// age of diff corr, diff ref station id, checksum, <CR><LF>
// Should be able to parse out lat, lon, utc
TEST(NmeaSerdeTest, SerUtcLatLon){
	GpsInfo exp;
	strcpy(exp.utc,"164322.124");
	strcpy(exp.lat, "4323.5553");
	strcpy(exp.ns,"N");
	strcpy(exp.lon,"12345.5456");
	strcpy(exp.ew,"E");

	char serialized[NMEA_GGA_MAX_SIZE];
	NmeaSerde_serGga(serialized, &exp);
	STRCMP_EQUAL("$GPGGA,164322.124,4323.5553,N,12345.5456,E", serialized);
}


// a NULL should be deserialized as all empty strings
TEST(NmeaSerdeTest, DeserGgaNull){
	GpsInfo act;
    NmeaSerde_deserGga(&act, NULL);
	STRCMP_EQUAL("", act.utc);
	STRCMP_EQUAL("", act.lat);
	STRCMP_EQUAL("",  act.ns);
	STRCMP_EQUAL("", act.lon);
	STRCMP_EQUAL("", act.ew);
}



// Copy strings from strarray into dest, selecting elements indexed by select[] array.
// dest[] and select[] should be at least destSize long and strarray[] should be at least size long.
// If an index in select >= size, it is skipped
static void strcpyFromArray(char* dest[], size_t destSize, string strarray[], size_t size, const size_t select[]){
	for (size_t i = 0; i < destSize; i++){
		if (select[i] < size){
			strcpy(dest[i], strarray[select[i]].c_str());
		}
	}
}

static const size_t GGA_FIELD_COUNT = 9;
static const size_t GGA_FIELD_SELECT[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

static void ggaFieldList(char* fields[], GpsInfo& info){
	char* list[] = {
        info.utc,
        info.lat,
        info.ns,
        info.lon,
        info.ew,
        info.posFixInd,
        info.satsUsed,
        info.hdop,
        info.alt
	};
	for (size_t i = 0; i < GGA_FIELD_COUNT; i++){
		fields[i] = list[i];
	}
}

static void ggaStrCmpEqual(GpsInfo& exp, GpsInfo& act){
	char* expFields[GGA_FIELD_COUNT];
	char* actFields[GGA_FIELD_COUNT];
	ggaFieldList(expFields, exp);
	ggaFieldList(actFields, act);
	for (size_t i = 0; i < GGA_FIELD_COUNT; i++){
		STRCMP_EQUAL(expFields[i], actFields[i]);
	}
}

//sets up a test with values in an array
// exp is an array of char*, starting the command, utc, etc as would be in a $GPGGA command string
// without the commas.
static void deserTest(string exp[], size_t size){
	GpsInfo expInfo;
	GpsInfo_init(&expInfo);
	// calculate the length of the serialized command
	size_t len = 0;
	for (size_t i = 0; i < size; i++ ){
		len += exp[i].length() + 1; // add 1 for a comma
	}
	// build the serialized string
	char serialized[len];
	strcpy(serialized, "");
	for (size_t i = 0; i < size; i++){
		strcat(serialized, exp[i].c_str());
		if (i < size - 1){
			strcat(serialized, ",");
		}
	}
	char* fields[GGA_FIELD_COUNT];
	ggaFieldList(fields, expInfo);
	//exp[0] is the command and is not in the struct
	strcpyFromArray(fields, GGA_FIELD_COUNT, exp, size, GGA_FIELD_SELECT);
	GpsInfo act;
    NmeaSerde_deserGga(&act, serialized);
    ggaStrCmpEqual(expInfo, act);
}

TEST(NmeaSerdeTest, DeserGpgga){
	string exp[] = { "$GPGGA", "164322.124", "4323.5553", "N", "12345.5456", "E", "0", "07", "1.0", "987.5", "M", "", "M", "", "0000" };
	deserTest(exp, 15);
}

//A $GPGGA string should accept all max values
TEST(NmeaSerdeTest, DeserGgaMax){
	string exp[] = {"$GPGGA", "235959.999", "9000.0000", "N", "18000.0000", "E", "3", "12", "999.9", "99999.9", "M","","M","","0000"};
	deserTest(exp, 15);
}

//A $GPGGA string should accept all middle values
TEST(NmeaSerdeTest, DeserGgaMid){
	string exp[] = {"$GPGGA", "120000.000", "0.0000", "N", "0.0000", "E", "2", "06", "500.0", "50000.0", "M","","M","","0000"};
	deserTest(exp, 15);
}

//A $GPGGA string should accept all min values
TEST(NmeaSerdeTest, DeserGgaMin){
	string exp[] = {"$GPGGA", "000000.000", "90.0000", "s", "180.0000", "W", "0", "00", "0.0", "0.0", "M","","M","","0000"};
	deserTest(exp, 15);
}

//A $GPGGA string that ends before all fields should place empty strings in missing fields
TEST(NmeaSerdeTest, DeserGgaSomeMissing){
	string exp[] = { "$GPGGA", "165544.323", "S" };
	deserTest(exp, 3);
}

// empty values between commas should result in empty strings
TEST(NmeaSerdeTest, EmptyValues){
	string exp[] = { "$GPGGA", "165544.323", "5355.6753", "", "", "E" };
	deserTest(exp, 6);
}

static size_t ser100Test(const char* exp, NmeaBaud baud, NmeaDataBits dataBits, NmeaStopBits stopBits,
		NmeaParity parity){
	NmeaSerialPortSettings settings;
	settings.baud = baud;
	settings.dataBits = dataBits;
	settings.stopBits = stopBits;
	settings.parity = parity;
	char act[NMEA_100_MAX_SIZE];
    NmeaSerde_ser100(act, &settings);
    STRCMP_EQUAL(exp, act);
    return strlen(act);
}
// Should be able to serialize $PSRF100 command
TEST(NmeaSerdeTest, Ser100){
	ser100Test("$PSRF100,1,4800,8,1,0",
	        NMEA_BAUD_4800, NMEA_DATA_BITS_8, NMEA_STOP_BITS_1, NMEA_PARITY_NONE);
}

// Should be able to serialize $PSRF100 command (diff settings)
TEST(NmeaSerdeTest, Ser100_2){
	ser100Test("$PSRF100,1,2400,7,0,1",
	        NMEA_BAUD_2400, NMEA_DATA_BITS_7, NMEA_STOP_BITS_0, NMEA_PARITY_ODD);
}
// Should be able to serialize the $PSRF100 command (more settings)
TEST(NmeaSerdeTest, Ser100_3){
	ser100Test("$PSRF100,1,1200,8,1,2",
	        NMEA_BAUD_1200, NMEA_DATA_BITS_8, NMEA_STOP_BITS_1, NMEA_PARITY_EVEN);
}
// Should be able to serialize the $PSRF100 command (more settings)
TEST(NmeaSerdeTest, Ser100_4){
	ser100Test("$PSRF100,1,38400,7,1,0",
	        NMEA_BAUD_38400, NMEA_DATA_BITS_7, NMEA_STOP_BITS_1, NMEA_PARITY_NONE);
}
//Serialized $PSRF100 should not be longer than NMEA_100_MAX_SIZE - 1
TEST(NmeaSerdeTest, Ser100_5){
	size_t len = ser100Test("$PSRF100,1,19200,7,1,0",
	        NMEA_BAUD_19200, NMEA_DATA_BITS_7, NMEA_STOP_BITS_1, NMEA_PARITY_NONE);
	CHECK_EQUAL(NMEA_100_MAX_SIZE - 1, len);
}
