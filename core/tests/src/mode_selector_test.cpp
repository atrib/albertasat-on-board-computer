// mode_selector_test.cpp
#include <CppUTest/TestHarness.h>

TEST_GROUP(ModeSelectorTest)
{
};
// Can delete the boot time record
/*
TEST(ModeSelectorTest, deleteBootTime){
	ModeSelector_deleteBootTimeRecord();
	uint32_t actual = ModeSelector_getBootTime();
	CHECK_EQUAL(0, actual);
}
*/
// When starting for the first time, store the initial time.
/*
TEST(ModeSelectorTest, storeFirstBootTime)
{
	ModeSelector_deleteBootTimeFile();
	uint32_t expected = 12345;
	TimeService_setTimestamp(expected);
	ModeSelector_recordBootTime();
	uint32_t actual = ModeSelector_getBootTime();
	CHECK_EQUAL(expected, actual);
};
*/
// mode_selector
// When starting afterwards, check if 30 minutes have passed
// Get the battery level
// If  battery level less than vCrit, power off
// Start the initialize task
// Determine initialization is done (callback?) and go to standby if < 30 minutes since first boot
// Go from standby to power safe mode
// Go from initialization to power safe
// Start the power safe task
// If battery level less than vSafe, go into power safe (but only if in detumble, science or attitude alignment mode)
// When battery above vSafe in power safe mode, go into Detumble mode
// Start the detumble task
// When done detumbling, go into attitude realignment mode
// Start realignment task
// When aligned, go into science mode
// Start the science task
// Periodically check alignment status, and go back into realignment mode
