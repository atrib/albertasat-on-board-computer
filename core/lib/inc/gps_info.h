/*
 * gps_info.h
 *
 *  Created on: Nov 22, 2014
 *      Author: albertasat
 */

#ifndef GPS_INFO_H_
#define GPS_INFO_H_

//! Max char[] sizes for the different fields (includes trailing 0)
enum {
	GPS_INFO_UTC_SIZE = 11, // hhmmss.sss
	GPS_INFO_LAT_SIZE = 10, // ddmm.mmmm
	GPS_INFO_NS_SIZE  = 2,  // N or S
	GPS_INFO_LON_SIZE = 11, // dddmm.mmmm
	GPS_INFO_EW_SIZE  = 2,   // E or W
	GPS_INFO_POS_FIX_IND_SIZE = 2, // 0, 1, 2, or 3
	GPS_INFO_SATS_USED_SIZE = 3, // 00 - 12
	GPS_INFO_HDOP_SIZE = 8, // up to 99999.9 (guess)
	GPS_INFO_ALT_SIZE = 8, // up to 99999.9 (guess)
	GPS_INFO_ALT_UNITS_SIZE = 2, // M
	GPS_INFO_FIX_MODE_SIZE = 2, //M or A
	GPS_INFO_FIX_TYPE_SIZE = 2, // 1 2 or 3
	GPS_INFO_PDOP_SIZE = 8, // up to 99999.9 (guess)
	GPS_INFO_VDOP_SIZE = 8, // up to 99999.9 (guess)
	GPS_INFO_STATUS_SIZE = 2, // A or V
	GPS_INFO_SPEED_SIZE = 7, // up to 999.99 (guess)
	GPS_INFO_COURSE_SIZE = 7, // 999.99
	GPS_INFO_DATE_SIZE = 7, // ddmmyy
};


//! A convenient way to move gps information around.
typedef struct {
	char utc[GPS_INFO_UTC_SIZE]; //<! hhmmss.ddd coordinated universal time
	char lat[GPS_INFO_LAT_SIZE]; //<! latitude ddmm.mmmm
	char ns [GPS_INFO_NS_SIZE]; //<! N or S; north or south
	char lon[GPS_INFO_LON_SIZE]; //<! longitude dddmm.mmmm
	char ew [GPS_INFO_EW_SIZE]; //<! E or W; east or west
	/*! position fix indicator
	 0 Fix not available or invalid
	 1 GPS SPS Mode, fix valid
	 2 Differential GPS, SPS Mode , fix valid
	 3 GPS PPS Mode, fix valid
    */
	char posFixInd[GPS_INFO_POS_FIX_IND_SIZE];
	char satsUsed[GPS_INFO_SATS_USED_SIZE]; // satellites used for fix
	char hdop[GPS_INFO_HDOP_SIZE]; //<! horizontal degree of precision (metres)
	char alt[GPS_INFO_ALT_SIZE]; //<! altitude over sea level(metres)
	char fixMode[GPS_INFO_FIX_MODE_SIZE]; //<! M: manually forced to 2D/3D; A: automatic switch between 2D/3D
	char fixType[GPS_INFO_FIX_TYPE_SIZE]; //<! 1: no fix, 2: 2D fix; 3: 3D fix
	char pdop[GPS_INFO_PDOP_SIZE]; //!< Position degree of precision (metres)
	char vdop[GPS_INFO_VDOP_SIZE]; //!< Vertical degree of precisions (metres)
	char status[GPS_INFO_STATUS_SIZE]; //!< Status A (data valid) or V (not valid)
	char speed[GPS_INFO_SPEED_SIZE]; //!< speed over ground
	char course[GPS_INFO_COURSE_SIZE];//!< course over ground in degrees
	char date[GPS_INFO_DATE_SIZE]; //!< ddmmyy

} GpsInfo;

void GpsInfo_init(GpsInfo* info);

#endif /* GPS_INFO_H_ */
