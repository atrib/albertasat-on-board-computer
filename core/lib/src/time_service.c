#include "time_service.h"

/*! Keeps track of the time since launch.  */
/*!
 * Intended to be used by a time task that updates this for access
 * by all other onboard software.
 */
static struct {
	uint32_t timestamp; /*! Time since launch */
	uint32_t firstBootTime; /* Time of first boot after launch */
} data;

//! Set the timestamp
void TimeService_setTimestamp(uint32_t time){
    data.timestamp = time;
}
//! Seconds since first boot or last reset
uint32_t TimeService_timestamp(){
    return data.timestamp;
}

//! Resets time back to 0; only used for testing
void TimeService_reset(){
    data.timestamp = 0;
    data.firstBootTime = 0;
}

//! record the first boot time when this is called
/*!
 * \return 0 if already recorded (without a reset inbetween).  Otherwise returns the timestamp.
 */
uint32_t TimeService_recordFirstBootTime(){
	if (data.firstBootTime == 0){
		data.firstBootTime = TimeService_timestamp();
		return data.firstBootTime;
	}
	return 0; //error
}
//! Get the time of first boot after launch
uint32_t TimeService_firstBootTime(){
	return data.firstBootTime;
}
//! Get the number of seconds since first boot
/*!
 *\return 0 if first boot time not set, or if after TimeService_timestamp()
 */
uint32_t TimeService_secondsAfterFirstBoot(){
	uint32_t firstBootTime = TimeService_firstBootTime();
	if (firstBootTime == 0){ // not set!
	    return 0;
	}
	uint32_t timestamp = TimeService_timestamp();
	if (timestamp < firstBootTime){ //error!
		return 0;
	}
	return timestamp - firstBootTime;
}


