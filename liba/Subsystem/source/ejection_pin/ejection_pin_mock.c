/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file ejection_pin
 * @author Brendan Bruner
 * @date Feb 12, 2015
 */
#include "ejection_pin.h"
#include "portable_types.h"

/************************************************************************/
/* GPIO spoof of ejection pin												*/
/************************************************************************/
#define GPIO_SPOOF_MAX_NOT_EJECTED_RETURNS ((void *) 1)
eject_status_t is_ejected_gpio_spoof( ejection_pin_t *driver )
{
	DEV_ASSERT( driver );

	if( driver->data > GPIO_SPOOF_MAX_NOT_EJECTED_RETURNS )
	{
		return EJECTED;
	}

	driver->data = (void *)( (uint8_t *) driver->data + 1 );
	return NOT_EJECTED;
}

void initialize_ejection_pin_mock_up( ejection_pin_t *driver )
{
	DEV_ASSERT( driver );

	driver->data = 0;
	driver->is_ejected = &is_ejected_gpio_spoof;
}
