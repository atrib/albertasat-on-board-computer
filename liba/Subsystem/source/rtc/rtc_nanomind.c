/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file rtc_lpc.c
 * @author Brendan Bruner
 * @date Jun 24, 2015
 */

#include "rtc.h"
#include "portable_types.h"
#include <dev/arm/ds1302.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define _NOT_INIT 0
#define _IS_INIT 1


/********************************************************************************/
/* Private Variable Defines														*/
/********************************************************************************/
static uint8_t _is_init = _NOT_INIT;
static mutex_t _mutex;


/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static 	void enable( rtc_t *rtc )
{
	DEV_ASSERT( rtc );
	ds1302_clock_resume();
}

static void disable( rtc_t *rtc )
{
	DEV_ASSERT( rtc );
	ds1302_clock_halt();
}

static uint8_t set_time( rtc_t *rtc, rtc_time_t *time )
{
	DEV_ASSERT( rtc );
	DEV_ASSERT( time );

	if( time->seconds > 60 ||
		time->minutes > 60 ||
		time->hours > 23 ||
		time->day_of_month > 31 ||
		time->month > 12 ||
		time->year >= 2100 ||
		time->year < 2000
		)
	{
		return RTC_TIME_NOT_SET;
	}

	struct ds1302_clock date;
	date.seconds 	= time->seconds;
	date.minutes	= time->minutes;
	date.hour 		= time->hours;
	date.date 		= time->day_of_month;
	date.month 		= time->month;
	date.day 		= 0;
	date.year 		= time->year - 2000;
	date.wp			= 0;

	lock_mutex( _mutex, BLOCK_FOREVER );
	ds1302_clock_write_burst( &date );
	unlock_mutex( _mutex );

	return RTC_TIME_SET;
}

static void get_time( rtc_t *rtc, rtc_time_t *time )
{
	DEV_ASSERT( rtc );
	DEV_ASSERT( time );

	struct ds1302_clock date;

	lock_mutex( _mutex, BLOCK_FOREVER );
	ds1302_clock_read_burst( &date );
	unlock_mutex( _mutex );

	time->seconds = date.seconds;
	time->minutes = date.minutes;
	time->hours = date.hour;
	time->day_of_month = date.date;
	time->month = date.month;
	time->year = date.year + 2000;
}

static time_t rtc_mktime( struct tm* date )
{
	DEV_ASSERT( date );
	return mktime( date );
}


/********************************************************************************/
/* Constructor / Destructor														*/
/********************************************************************************/
uint8_t initialize_rtc_nanomind( rtc_t *rtc )
{
	DEV_ASSERT( rtc );

	extern void initialize_rtc_base( rtc_t* );
	initialize_rtc_base( rtc );

	rtc->enable = enable;
	rtc->disable = disable;
	rtc->set_time = set_time;
	rtc->get_time = get_time;
	rtc->_.mktime = rtc_mktime;

	if( _is_init == _IS_INIT )
	{
		return RTC_INIT;
	}

	new_mutex( _mutex );
	if( _mutex == NULL )
	{
		return RTC_INIT_FAIL;
	}

	ds1302_init();
	ds1302_set_24hr();
	ds1302_clock_resume();

	/* default time is 8:00:00PM, 2010-05-26 */
	struct ds1302_clock date;
	date.seconds 	= 0;
	date.minutes	= 0;
	date.hour 		= 8;
	date.date 		= 26;
	date.month 		= 5;
	date.day 		= 0;
	date.year 		= 10;
	date.wp			= 0;
	ds1302_clock_write_burst( &date );

	return RTC_INIT;
}


