/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file rtc_base.c
 * @author Brendan Bruner
 * @date Jun 24, 2015
 */

#include "rtc.h"
#include "portable_types.h"


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
static uint32_t seconds_since_epoch( rtc_t *rtc, rtc_time_t* epoch )
{
	DEV_ASSERT( rtc );

	rtc_time_t cur_date;
	struct tm tm_requested_epoch_date;
	struct tm tm_cur_date;
	time_t unix_epoch_to_cur_date;
	time_t unix_epoch_to_requested_epoch;

	rtc->get_time( rtc, &cur_date );

	/* Our current date converted to struct tm date. */
	tm_cur_date.tm_sec = cur_date.seconds;
	tm_cur_date.tm_min = cur_date.minutes;
	tm_cur_date.tm_hour = cur_date.hours;
	tm_cur_date.tm_mday = cur_date.day_of_month;
	tm_cur_date.tm_mon = cur_date.month;
	tm_cur_date.tm_year = cur_date.year - 1900;
	tm_cur_date.tm_isdst = -1;

	/* Our epoch as a date converted to a struct tm as a date. */
	tm_requested_epoch_date.tm_sec = epoch->seconds;
	tm_requested_epoch_date.tm_min = epoch->minutes;
	tm_requested_epoch_date.tm_hour = epoch->hours;
	tm_requested_epoch_date.tm_mday = epoch->day_of_month;
	tm_requested_epoch_date.tm_mon = epoch->month;
	tm_requested_epoch_date.tm_year = epoch->year - 1900;
	tm_requested_epoch_date.tm_isdst = -1;

	extern time_t gnu_mktime (struct tm *);
	/* The time since unix epoch to our current date. */
	unix_epoch_to_cur_date = rtc->_.mktime( &tm_cur_date );//seconds_since_unix_epoch( &tm_cur_date );
	/* The time since unix epoch to our requested epoch. */
	unix_epoch_to_requested_epoch = rtc->_.mktime( &tm_requested_epoch_date );//seconds_since_unix_epoch( &tm_requested_epoch_date );

	/* We assume 2 things with this return. */
	/*		1. our cur_date happened after our _RTC_EPOCH. */
	/*		2. our _RTC_EPOCH happend after the unix epoch. */
	/* 1. requires enforcement to hold true. */
	/* 2. will always be true so long as the the qb50 epoch is used as our epoch. */
	return (uint32_t) (unix_epoch_to_cur_date - unix_epoch_to_requested_epoch );
}

void destroy_rtc( rtc_t *rtc )
{
	DEV_ASSERT( rtc );
}


void initialize_rtc_base( rtc_t* rtc )
{
	DEV_ASSERT( rtc );
	rtc->seconds_since_epoch = seconds_since_epoch;
}
