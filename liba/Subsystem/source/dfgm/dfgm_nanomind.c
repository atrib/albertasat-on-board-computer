/*
 * Copyright (C) 2015 Collin Cupido, Brendan Bruner, Stefan Damkjar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file dfgm.c
 * @author Alex Hamilton
 * @date 2015-08-20
 */


#include "dfgm.h"
#include <stdint.h>
#include <portable_types.h>
#include <printing.h>

#if defined( NANOMIND )
#include <csp_internal.h>
#include <dev/usart.h>
#endif

#include <FFT/kiss_fft.h>
#include <FFT/kiss_fftr.h>

int8_t dfgm_get8(void){
#if defined( NANOMIND )
	return (int8_t) usart_getc(DFGM_USART_HANDLE);
#else
	return 0;
#endif
}
int16_t dfgm_get16(void){
	return (int16_t)((uint16_t) dfgm_get8()<<8)+((uint16_t) dfgm_get8());
}
int32_t dfgm_get24(void){
	return ((int8_t)dfgm_get8())*0x10000+dfgm_get8()*0x100+dfgm_get8();
}
int32_t dfgm_get32(void){
	return (int32_t) (((uint32_t) dfgm_get8()<<24)+((uint32_t) dfgm_get8()<<16)+((uint32_t) dfgm_get8()<<8)+((uint32_t) dfgm_get8()));
}

uint8_t last_pid=0;

xQueueHandle xadc_queue;
xQueueHandle yadc_queue;
xQueueHandle zadc_queue;
xSemaphoreHandle dfgm_smphr;
xSemaphoreHandle dfgm_fft_smphr;

uint8_t fmode=1;
uint8_t dmode=1;
const int32_t c0=1;
const int32_t c1=1;
const int32_t c2=0;


const uint32_t decimation[4]={10000, 1000, 100, 10};
int32_t spectral[2049];


int32_t buffera[4096];
int32_t bufferb[4096];
int32_t * currbuff=buffera;

char dfgm_filename[15]="sd/D0000000.bin";




void DFGM_init(void){
	xadc_queue=xQueueCreate(DFGM_QUEUE_SIZE,sizeof(uint32_t));
	yadc_queue=xQueueCreate(DFGM_QUEUE_SIZE,sizeof(uint32_t));
	zadc_queue=xQueueCreate(DFGM_QUEUE_SIZE,sizeof(uint32_t));
	vSemaphoreCreateBinary(dfgm_smphr);
	vSemaphoreCreateBinary(dfgm_fft_smphr);
	take_semaphore( dfgm_smphr, BLOCK_FOREVER );
	take_semaphore( dfgm_fft_smphr, BLOCK_FOREVER );
	xTaskCreate(vTaskDFGM, (const signed char*) "DFGM", 1024*4,NULL,0,NULL);
	xTaskCreate(vTaskDFGMWrite, (const signed char*) "DFGMW", 1024*4,NULL,0,NULL);
	xTaskCreate(vTaskDFGMfft, (const signed char*) "DFGMF", 1024*8,NULL,0,NULL);
}

void DFGM_start(void){
#if defined( NANOMIND )
	while(usart_messages_waiting(DFGM_USART_HANDLE)){
		dfgm_get8();
	}
#endif
	give_semaphore(dfgm_smphr);
}

void DFGM_stop(void){
	take_semaphore(dfgm_smphr,BLOCK_FOREVER);
}

#ifndef DFGM_TESTING
void vTaskDFGM(void * pvParameters){
	csp_printf("DFGM");
	struct dfgm_packet_t pack;
	FILE * fid;

	uint32_t i;

	while(1){
		if(usart_messages_waiting(DFGM_USART_HANDLE)>=(11*(DFGM_FS+4))){
			take_semaphore(dfgm_smphr,BLOCK_FOREVER);
			if(dfgm_get8()==DFGM_DLE){
				if(dfgm_get8()==DFGM_STX){
					pack.pid=dfgm_get8();
					pack.packet_type=dfgm_get8();
					pack.packet_length=dfgm_get16();
					pack.fs=dfgm_get16();
					pack.pps_offset=dfgm_get32();
					for(i=0; i<12; i++){
						pack.hk[i]=dfgm_get16();
					}
					for(i=0; i<DFGM_FS; i++){
						pack.x[i]=(c0*dfgm_get8())+(c1*dfgm_get24())+c2;
						pack.y[i]=(c0*dfgm_get8())+(c1*dfgm_get24())+c2;
						pack.z[i]=(c0*dfgm_get8())+(c1*dfgm_get24())+c2;
						if(fmode==1||fmode==2){
							xQueueSend(xadc_queue,&pack.x[i],0);
							xQueueSend(yadc_queue,&pack.y[i],0);
							xQueueSend(zadc_queue,&pack.z[i],0);
						}
					}
					pack.board_id=dfgm_get16();
					pack.sensor_id=dfgm_get16();
					dfgm_get8(); //reserved
					dfgm_get8(); //reserved
					dfgm_get8(); //reserved
					dfgm_get8(); //reserved
					dfgm_get8(); //reserved
					dfgm_get8(); //ETX
					pack.crc=dfgm_get16();
					//csp_printf("DFGM packet received!\n pid: %d\n packet type: %d\n packet length: %d\n FS: %d\n pps offset: %d",pack.pid, pack.packet_type,pack.packet_length,pack.fs,pack.pps_offset);

					if(fmode==0){
						fid=fopen("sd/dfgmpack.bin","w");
						fwrite(&pack,sizeof(struct dfgm_packet_t),1,fid);
						fclose(fid);
						break;
					}
				}
			}
			give_semaphore(dfgm_smphr);
		}
	}
}
#endif

void vTaskDFGMWrite(void * pvParameters){

	int32_t xbuffer[DFGM_FILTER_ORDER];
	int32_t ybuffer[DFGM_FILTER_ORDER];
	int32_t zbuffer[DFGM_FILTER_ORDER];
	int32_t xdat,ydat,zdat;
	uint32_t j=0;
	uint32_t i=0;
	int32_t * currbuff=buffera;

	while(1){
		if(uxQueueMessagesWaiting(xadc_queue)>0){
			i++;
			xQueueReceive(xadc_queue,&xdat,0);
			xQueueReceive(yadc_queue,&ydat,0);
			xQueueReceive(zadc_queue,&zdat,0);
			//csp_printf("xdat: %d, ydat: %d, zdat: %d",xdat,ydat,zdat);

			if(i==4096){
				if(currbuff==buffera){
					currbuff=bufferb;
					give_semaphore(dfgm_fft_smphr);
				}
				else{
					currbuff=buffera;
					give_semaphore(dfgm_fft_smphr);
				}
				i=0;
				currbuff[i]=(int32_t)sqrt(((long long)xdat *(long long)xdat)+((long long)ydat*(long long)ydat)+((long long)zdat*(long long)zdat));
			}
			else{
				currbuff[i]=(int32_t)sqrt(((long long)xdat *(long long)xdat)+((long long)ydat*(long long)ydat)+((long long)zdat*(long long)zdat));
			}

			if(decimation[dmode]>DFGM_FILTER_ORDER){
				if(j<DFGM_FILTER_ORDER){
					xbuffer[j]=xdat;
					ybuffer[j]=ydat;
					zbuffer[j]=zdat;
				}
				j++;
				if(j>=decimation[dmode]){
					j=0;
					xdat=DFGM_filter(xbuffer);
					ydat=DFGM_filter(ybuffer);
					zdat=DFGM_filter(zbuffer);
					DEMO_PRINTF("filter: x: %d, y: %d, z: %d",xdat,ydat,zdat);
				}
			}
		}
	}

}


int32_t DFGM_filter(int32_t * buffer){
	int i;
	long long b[4][101]={	{12706566,12850760,13282730,14000775,15002061,16282639,17837455,19660374,21744204,24080721,26660705,29473975,32509429,35755088,39198144,42825009,46621369,50572244,54662039,58874617,63193350,67601197,72080760,76614361,81184107,85771965,90359828,94929589,99463213,103942808,108350695,112669478,116882111,120971970,124922912,128719346,132346288,135789423,139035164,142070699,144884049,147464111,149800702,151884600,153707581,155262449,156543069,157544387,158262450,158694425,158838606,158694425,158262450,157544387,156543069,155262449,153707581,151884600,149800702,147464111,144884049,142070699,139035164,135789423,132346288,128719346,124922912,120971970,116882111,112669478,108350695,103942808,99463213,94929589,90359828,85771965,81184107,76614361,72080760,67601197,63193350,58874617,54662039,50572244,46621369,42825009,39198144,35755088,32509429,29473975,26660705,24080721,21744204,19660374,17837455,16282639,15002061,14000775,13282730,12850760,12706566},
				{12663243,12809013,13241676,13959664,14960279,16239699,17792997,19614155,21696092,24030691,26608829,29420413,32454425,35698956,39141264,42767814,46564341,50515898,54606921,58821289,63142387,67553172,72036243,76573905,81148246,85741201,90334629,94910382,99450379,103936677,108351540,112677514,116897493,120994787,124953191,128757046,132391303,135841583,139094233,142136382,144955991,147541899,149883872,151972639,153799932,155358518,156642227,157645976,158365791,158798824,158943359,158798824,158365791,157645976,156642227,155358518,153799932,151972639,149883872,147541899,144955991,142136382,139094233,135841583,132391303,128757046,124953191,120994787,116897493,112677514,108351540,103936677,99450379,94910382,90334629,85741201,81148246,76573905,72036243,67553172,63142387,58821289,54606921,50515898,46564341,42767814,39141264,35698956,32454425,29420413,26608829,24030691,21696092,19614155,17792997,16239699,14960279,13959664,13241676,12809013,12663243},
				{8636796,8908655,9386018,10078942,10996304,12145721,13533470,15164425,17041992,19168068,21542992,24165523,27032814,30140408,33482237,37050637,40836369,44828654,49015219,53382353,57914969,62596683,67409896,72335890,77354925,82446353,87588731,92759946,97937338,103097836,108218093,113274624,118243946,123102718,127827887,132396824,136787462,140978436,144949208,148680202,152152917,155350049,158255595,160854954,163135021,165084264,166692800,167952457,168856826,169401299,169583099,169401299,168856826,167952457,166692800,165084264,163135021,160854954,158255595,155350049,152152917,148680202,144949208,140978436,136787462,132396824,127827887,123102718,118243946,113274624,108218093,103097836,97937338,92759946,87588731,82446353,77354925,72335890,67409896,62596683,57914969,53382353,49015219,44828654,40836369,37050637,33482237,30140408,27032814,24165523,21542992,19168068,17041992,15164425,13533470,12145721,10996304,10078942,9386018,8908655,8636796},
				{0,1391726,2793204,4138558,5326427,6213670,6620972,6352075,5225729,3116746,0,-4010410,-8635275,-13425330,-17782794,-21011622,-22394538,-21290464,-17241826,-10078062,0,12369899,25984061,39421276,50995310,58915725,61488798,57339856,45633456,26265283,0,-31467268,-65541738,-98859168,-127512184,-147358633,-154387760,-145111842,-116945774,-68535640,0,86945715,189001136,301435992,418381074,533234228,639148398,729559640,798707963,842103460,856894491,842103460,798707963,729559640,639148398,533234228,418381074,301435992,189001136,86945715,0,-68535640,-116945774,-145111842,-154387760,-147358633,-127512184,-98859168,-65541738,-31467268,0,26265283,45633456,57339856,61488798,58915725,50995310,39421276,25984061,12369899,0,-10078062,-17241826,-21290464,-22394538,-21011622,-17782794,-13425330,-8635275,-4010410,0,3116746,5225729,6352075,6620972,6213670,5326427,4138558,2793204,1391726,0}
				};

	long long int value=0;
	int32_t buf;
	for(i=0;i<DFGM_FILTER_ORDER;i++){
		buf=buffer[i];
		value+=b[dmode][i]*((long long)buf)*0x100000000;
	}
	return (int32_t) (value/0x100000000);
}

void vTaskDFGMfft(void * pvParameters){

	kiss_fftr_cfg fftr_cfg;
	fftr_cfg = kiss_fftr_alloc(4096,0,NULL,NULL);
	kiss_fft_cpx currfft[2049];
	uint32_t i;
	uint32_t j=0;

	while(1){
		take_semaphore(dfgm_fft_smphr,BLOCK_FOREVER);
		DEMO_PRINTF("FFT!");
		if(currbuff==buffera){
			kiss_fftr(fftr_cfg,bufferb,currfft);
		}
		else{
			kiss_fftr(fftr_cfg,buffera,currfft);
		}

		int32_t squif=0;
		for(i=0;i<2049;i++){
			spectral[i]+=(int32_t) sqrt(((long long) currfft[i].r* (long long)currfft[i].r)+((long long)currfft[i].i*(long long)currfft[i].i));
			squif+=spectral[i];
			if((i%128)==0){
				DEMO_PRINTF("spectral: %d",squif/128);
				squif=0;
			}
		}
		j++;
		if(j==10){
			//fid=fopen("sd/DS000000.bin",'w');
			//fwrite(spectral,sizeof(uint32_t),2049,fid);
			//fclose(fid);
			memset(spectral,0,2049*sizeof(uint32_t));
			j=0;
		}
	}
}

