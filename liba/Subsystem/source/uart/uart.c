/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file uart.c
 * @author Brendan Bruner
 * @date Sep 25, 2015
 */

#include <uart/uart.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static uint32_t send( uart_t* self, uint8_t const* data, uint32_t length )
{
	DEV_ASSERT( self );
	DEV_ASSERT( data );

	return 0;
}

static uint32_t read( uart_t* self, uint8_t* data, uint32_t length )
{
	DEV_ASSERT( self );
	DEV_ASSERT( data );

	return 0;
}

static uint32_t available( uart_t* self )
{
	DEV_ASSERT( self );

	return 0;
}

static void reset( uart_t* self )
{
	DEV_ASSERT( self );
}

static void reset_rx( uart_t* self )
{
	DEV_ASSERT( self );
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/
static void destroy( uart_t* self )
{
	DEV_ASSERT( self );
}


/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_uart_( uart_t* self )
{
	DEV_ASSERT( self );

	self->send = send;
	self->read = read;
	self->available = available;
	self->reset = reset;
	self->reset_rx = reset_rx;
	self->destroy = destroy;

	return true;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


