/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 *
 * Code here is heavily based on the demo code provided by nxp.
 * http://developer.mbed.org/users/frank26080115/code/LPC1700CMSIS_Examples/file/bf7b9fba3924/I2C/Master_Slave_Interrupt/i2c_master_slave_int_test.c  [March 2015]
 * The only original code is the wrappers and task safety.
 */
/**
 * @file lpc_uart
 * @author Brendan Bruner
 * @date Mar 28, 2015
 */


#include <chip.h>
#include <stdlib.h>
#include <portable_types.h>
#include <subsystem_defines.h>
#include <uart/uart_lpc.h>

/********************************************************************/
/* PRIVATE DEFINES													*/
/********************************************************************/
#define UART_NOT_INIT	0
#define UART_IS_INIT 	1

#define UART_RING_BUFSIZE 256

#define UART_PIN_MUX_LENGTH 2

/********************************************************************/
/* PRIVATE GLOBAL VARIABLES											*/
/********************************************************************/
#ifdef __LPC17XX__
char uart_print_buffer[UART_PRINT_BUFFER_SIZE];
#endif

#ifdef USE_UART0
STATIC const PINMUX_GRP_T uart0_mux[] = {
	{0,  2,   IOCON_MODE_INACT | IOCON_FUNC1},	/* TXD0 */
	{0,  3,   IOCON_MODE_INACT | IOCON_FUNC1},	/* RXD0 */
};
static RINGBUFF_T uart0_rx_ring_buffer, uart0_tx_ring_buffer;
static uint8_t uart0_rx_buffer[UART_RING_BUFSIZE], uart0_tx_buffer[UART_RING_BUFSIZE];
static uart_lpc_t uart0 = (uart_lpc_t)	{
											._uart_port_type_ = LPC_UART0,
											._irq_ = UART0_IRQn,
											._is_init_ = UART_NOT_INIT,
											._tx_ring_ = &uart0_tx_ring_buffer,
											._rx_ring_ = &uart0_rx_ring_buffer,
											._tx_buff_ = uart0_tx_buffer,
											._rx_buff_ = uart0_rx_buffer,
											._pin_mux_ = uart0_mux
	  									};
#endif

#ifdef USE_UART1
STATIC const PINMUX_GRP_T uart1_mux[] = {
	{0,  15,   IOCON_MODE_INACT | IOCON_FUNC1},	/* TXD1 */
	{0,  16,   IOCON_MODE_INACT | IOCON_FUNC1},	/* RXD1 */
};
static RINGBUFF_T uart1_rx_ring_buffer, uart1_tx_ring_buffer;
static uint8_t uart1_rx_buffer[UART_RING_BUFSIZE], uart1_tx_buffer[UART_RING_BUFSIZE];
static uart_lpc_t uart1 = (uart_lpc_t)	{
											._uart_port_type_ = LPC_UART1,
											._irq_ = UART1_IRQn,
											._is_init_ = UART_NOT_INIT,
											._tx_ring_ = &uart1_tx_ring_buffer,
											._rx_ring_ = &uart1_rx_ring_buffer,
											._tx_buff_ = uart1_tx_buffer,
											._rx_buff_ = uart1_rx_buffer,
											._pin_mux_ = uart1_mux
										};
#endif

#ifdef USE_UART2
STATIC const PINMUX_GRP_T uart2_mux[] = {
	{0,  10,   IOCON_MODE_INACT | IOCON_FUNC1},	/* TXD2 */
	{0,  11,   IOCON_MODE_INACT | IOCON_FUNC1},	/* RXD2 */
};
static RINGBUFF_T uart2_rx_ring_buffer, uart2_tx_ring_buffer;
static uint8_t uart2_rx_buffer[UART_RING_BUFSIZE], uart2_tx_buffer[UART_RING_BUFSIZE];
static uart_lpc_t uart2 = (uart_lpc_t)	{
											._uart_port_type_ = LPC_UART2,
											._irq_ = UART2_IRQn,
											._is_init_ = UART_NOT_INIT,
											._tx_ring_ = &uart2_tx_ring_buffer,
											._rx_ring_ = &uart2_rx_ring_buffer,
											._tx_buff_ = uart2_tx_buffer,
											._rx_buff_ = uart2_rx_buffer,
											._pin_mux_ = uart2_mux
						  	  	  	  	};
#endif

#ifdef USE_UART3
STATIC const PINMUX_GRP_T uart3_mux[] = {
	{0,  0,   IOCON_MODE_INACT | IOCON_FUNC2},	/* TXD3 */
	{0,  1,   IOCON_MODE_INACT | IOCON_FUNC2},	/* RXD3 */
};
static RINGBUFF_T uart3_rx_ring_buffer, uart3_tx_ring_buffer;
static uint8_t uart3_rx_buffer[UART_RING_BUFSIZE], uart3_tx_buffer[UART_RING_BUFSIZE];
static uart_lpc_t uart3 = (uart_lpc_t)	{
											._uart_port_type_ = LPC_UART3,
											._irq_ = UART3_IRQn,
											._is_init_ = UART_NOT_INIT,
											._tx_ring_ = &uart3_tx_ring_buffer,
											._rx_ring_ = &uart3_rx_ring_buffer,
											._tx_buff_ = uart3_tx_buffer,
											._rx_buff_ = uart3_rx_buffer,
											._pin_mux_ = uart3_mux
	  									};
#endif


/********************************************************************/
/* PRIVATE METHOD DELCARATIONS										*/
/********************************************************************/


/********************************************************************/
/* PRIVATE METHOD DEFINES											*/
/********************************************************************/
#ifdef USE_UART0
void UART0_IRQHandler(void)
{
	Chip_UART_IRQRBHandler( uart0._uart_port_type_, uart0._rx_ring_, uart0._tx_ring_ );
	post_semaphore( uart0._send_mutex_ );
}
#endif

#ifdef USE_UART1
void UART1_IRQHandler(void)
{
	Chip_UART_IRQRBHandler( uart1._uart_port_type_, uart1._rx_ring_, uart1._tx_ring_ );
	post_semaphore( uart1._send_mutex_ );
}
#endif

#ifdef USE_UART2
void UART2_IRQHandler(void)
{
	Chip_UART_IRQRBHandler( uart2._uart_port_type_, uart2._rx_ring_, uart2._tx_ring_ );
	post_semaphore( uart2._send_mutex_ );
}
#endif

#ifdef USE_UART3
void UART3_IRQHandler(void)
{
	Chip_UART_IRQRBHandler( uart3._uart_port_type_, uart3._rx_ring_, uart3._tx_ring_ );
	post_semaphore( uart3._send_mutex_ );
}
#endif


/********************************************************************/
/* PUBLIC VIRTUAL METHOD DEFINES									*/
/********************************************************************/
static uint32_t send( uart_t* self_, uint8_t const *data, uint32_t buflen )
{
	DEV_ASSERT( self_ );
	uart_lpc_t* self = (uart_lpc_t* ) self_;

	uint32_t bytes = 0;

	/* loop until everything is transmitted. */
	while( buflen > 0 )
	{
		take_semaphore( self->_send_mutex_, BLOCK_FOREVER );
		bytes += Chip_UART_SendRB( self->_uart_port_type_, self->_tx_ring_, (void*) data + bytes, buflen );
		buflen = buflen - bytes;
	}
	return bytes;
}

static uint32_t read( uart_t* self_, uint8_t *data, uint32_t buflen )
{
	DEV_ASSERT( self_ );
	uart_lpc_t* self = (uart_lpc_t* ) self_;

	return Chip_UART_ReadRB( self->_uart_port_type_, self->_rx_ring_, (void*) data, buflen );
}

static uint32_t available( uart_t* self_ )
{
	DEV_ASSERT( self_ );
	uart_lpc_t* self = (uart_lpc_t* ) self_;

	return RingBuffer_GetFree( self->_rx_ring_ );
}

static void reset( uart_t* self_ )
{
	DEV_ASSERT( self_ );
	uart_lpc_t* self = (uart_lpc_t* ) self_;

	RingBuffer_Flush( self->_rx_ring_ );
	RingBuffer_Flush( self->_tx_ring_ );
}

static void reset_rx( uart_t* self_ )
{
	DEV_ASSERT( self_ );
	uart_lpc_t* self = (uart_lpc_t* ) self_;

	RingBuffer_Flush( self->_rx_ring_ );
}

static void destroy( uart_t* self_ )
{
	DEV_ASSERT( self_ );
}

/********************************************************************************/
/* Constructor																	*/
/********************************************************************************/
uart_lpc_t* initialize_uart_lpc
(
		uint8_t uart_port,
		uint32_t baud,
		uint32_t uart_config, /* See Chip_UART_ConfigData( ). */
		uint32_t fifo_config /* See Chip_UART_SetupFIFOS( ). */
)
{
	uart_lpc_t* self;

	/* Find out which uart port is requested. */
	switch( uart_port )
	{
		#ifdef USE_UART0
		case UART_PORT0:
			self = &uart0;
			break;
		#endif

		#ifdef USE_UART1
		case UART_PORT1:
			self = &uart1;
			break;
		#endif

		#ifdef USE_UART2
		case UART_PORT2:
			self = &uart2;
			break;
		#endif

		#ifdef USE_UART3
		case UART_PORT3:
			self = &uart3;
			break;
		#endif

		default:
			return NULL;
	}

	/* Check if the uart port is already initialized. */
	if( self->_is_init_ == UART_IS_INIT )
	{
		return self;
	}

	self->_uart_port_num_ = uart_port;
	((uart_t *) self)->available = available;
	((uart_t *) self)->destroy = destroy;
	((uart_t *) self)->read = read;
	((uart_t *) self)->reset = reset;
	((uart_t *) self)->reset_rx = reset_rx;
	((uart_t *) self)->send = send;

	/* Uart port hasn't been initialized yet. */
	/* Create its mutexs. */
	new_mutex( self->_read_mutex_ );
	if( self->_read_mutex_ == NULL )
	{
		/* Error creating mutex. */
		return NULL;
	}

	new_semaphore( self->_send_mutex_, 1, 1 );
	if( self->_send_mutex_ == NULL )
	{
		/* Error creating mutex. */
		delete_mutex( self->_read_mutex_ );
		return NULL;
	}

	Board_UART_Init( self->_uart_port_type_ );

	/* Mux the pins. */
	Chip_IOCON_SetPinMuxing( LPC_IOCON, self->_pin_mux_, UART_PIN_MUX_LENGTH );

	/* Initialize uart hardware. */
	Chip_UART_Init( self->_uart_port_type_ );
	Chip_UART_SetBaud( self->_uart_port_type_, baud );
	Chip_UART_ConfigData( self->_uart_port_type_, uart_config );
	Chip_UART_SetupFIFOS( self->_uart_port_type_, fifo_config );
	Chip_UART_TXEnable( self->_uart_port_type_ );

	/* Initialize ring buffers. */
	RingBuffer_Init( self->_rx_ring_, self->_rx_buff_, 1, UART_RING_BUFSIZE );
	RingBuffer_Init( self->_tx_ring_, self->_tx_buff_, 1, UART_RING_BUFSIZE );

	/* Reset and enable FIFOs */
	Chip_UART_SetupFIFOS( self->_uart_port_type_, fifo_config );

	/* Enable receive data and line status interrupt */
	Chip_UART_IntEnable( self->_uart_port_type_, (UART_IER_RBRINT | UART_IER_RLSINT));

	/* Enable interrupts in the NVIC */
	NVIC_SetPriority( self->_irq_, 1 );
	NVIC_EnableIRQ( self->_irq_ );

	self->_is_init_ = UART_IS_INIT;

	return self;
}

uart_lpc_t* initialize_uart_lpc_simple( uint8_t uart_port )
{
	return initialize_uart_lpc
			(
				uart_port,
				115200,
				(UART_LCR_WLEN8 | UART_LCR_SBS_1BIT | UART_LCR_PARITY_DIS ),
				(UART_FCR_FIFO_EN | UART_FCR_RX_RS | UART_FCR_TX_RS | UART_FCR_TRG_LEV0)
			);
}
