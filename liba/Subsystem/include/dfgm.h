/*
 * Copyright (C) 2015 Collin Cupido, Brendan Bruner, Stefan Damkjar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file dfgm.h
 * @author Alexander Hamilton
 * @date 2015-08-20
 */

#ifndef DFGM_H_
#define DFGM_H_

#include <stdint.h>

#define ENABLE_DFGM 1
#define DFGM_TESTING

#define DFGM_USART_HANDLE 2
#define DFGM_USART_BAUD 115200

#define DFGM_DLE 0x10
#define DFGM_STX 0x02
#define DFGM_ETX 0x03

#define DFGM_FS 100

#define DFGM_QUEUE_SIZE 2*4096

#define DFGM_FILTER_ORDER 101


struct __attribute__((packed)) dfgm_packet_t{
	uint8_t pid;
	uint8_t packet_type;
	uint8_t packet_length;
	uint16_t fs;
	uint32_t pps_offset;
	int16_t hk[12];
	int32_t x[DFGM_FS];
	int32_t y[DFGM_FS];
	int32_t z[DFGM_FS];
	uint16_t board_id;
	uint16_t sensor_id;
	uint16_t crc;

};


void vTaskDFGM(void * pvParameters);
void vTaskDFGMWrite(void * pvParameters);
void vTaskDFGMfft(void * pvParameters);

int32_t DFGM_filter(int32_t *buffer);
void DFGM_init(void);
void DFGM_start(void);
void DFGM_stop(void);

#endif /*DFGM_H_*/
