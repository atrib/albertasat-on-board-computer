/*
 * spi_sd_lpc17xx.h
 *
 *  Created on: 09.07.2010
 *      Author: mthomas
 */

#ifndef SPI_SD_LPC17XX_H_
#define SPI_SD_LPC17XX_H_

#include "diskio.h"
#include "lpc_types.h"
#include "FreeRTOS.h"
#include "timers.h"

xTimerHandle sdtimer1;
xTimerHandle sdtimer2;

DSTATUS MMC_disk_initialize(void);
DSTATUS MMC_disk_status(void);
DSTATUS MMC_disk_ioctl(BYTE ctrl, void *buff);
DRESULT MMC_disk_read(BYTE* buff, DWORD sector, BYTE count);
DRESULT MMC_disk_write(const BYTE* buff, DWORD sector, BYTE count);
void spi_init(void);
void spi_close(void);
BYTE wait_ready(void);
void release_spi(void);
Bool rcvr_datablock(BYTE *buff, UINT btr);
Bool xmit_datablock(const BYTE *buff, BYTE token);
BYTE send_cmd(BYTE cmd, DWORD arg);

#endif /* SPI_SD_LPC17XX_H_ */
