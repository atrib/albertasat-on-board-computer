/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file printing.h
 * @author Brendan Bruner
 * @date Oct 1, 2015
 */
#ifndef INCLUDE_PRINTING_H_
#define INCLUDE_PRINTING_H_


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#include <uart/uart_lpc.h>
#define DEBUG_PRINT( )	\
	do { serial_printf( "debug: file %s on line %d\n", (uint8_t *)__FILE__, __LINE__); } while( 0 )

#ifdef DEBUG
#define DEBUG_PRINTF( ... ) do { serial_printf( __VA_ARGS__ ); } while( 0 )
#else
#define DEBUG_PRINTF( ... )
#endif

#ifdef DEMO_MODE
#define DEMO_PRINTF( ... )  //do { serial_printf( __VA_ARGS__ ); } while( 0 )
#else
#define DEMO_PRINTF( ... )
#endif

/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/



/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/



/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_PRINTING_H_ */
