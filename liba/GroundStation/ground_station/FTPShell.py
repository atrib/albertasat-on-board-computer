 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #
import UpLinker, DownLinker, Ports, binascii, os, math, struct, time

# Error Messages
FTP_FATAL = "ftp: fatal error: "
FTP_NO_FILE = "File does not exist"
FTP_TERMINATION = "\nftp terminated"
FTP_BAD_OPTIONS = "bad options, use ftp --help"
FTP_NO_OPTIONS = "no options"
FTP_UNKOWN_COMMAND = "unkown command"

# Help Messages
FTP_USAGE = "Usage: ftp [option]\n" \
               "Options:\n" \
               "  help\t\tDisplay this information\n" \
               "  version\tDisplay version\n" \
               "  shell\t\tOpen a shell to issue ftp commands\n"
FTP_ENTER_MSG = "ftp v2.0\n" \
                "copyright (C) 2015 Brendan Bruner\n" \
                "Usage:\n" \
                "  get [file]\t\tDownlink argument file from the satellite\n" \
                "  put [file]\t\tUplink argument file to the satellite\n" \
                "  ls\t\t\tList all files in the satellites memory\n" \
                "  rm [file]\t\tDelete argument file from satellite's memory\n" \
                "  mv [file] [file]\tMove the argument file\n"
FTP_VERSION_MSG = "ftp v2.0\n" \
                  "copyright (C) 2015 Brendan Bruner\n" \
                  "This is free software; see the source for copying conditions.  There is NO\n" \
                  "warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"

# Options
FTP_HELP = "help"
FTP_VERSION = "version"
FTP_SHELL = "shell"
FTP_LS = "ls"
FTP_GET = "get"
FTP_PUT = "put"
FTP_RM = "rm"
FTP_MV = "mv"
FTP_EXIT = "QUIT"

FTP_PORT = Ports.FTP_PORT
NAME = "ftp"
FTP_PROMPT = "ftp shell$ "

FTP_PUT_CHUNK_SIZE = 50

class FTPShell:

    def __init__( self, upLinker, downLinker ):
        self._period = 0
        self._upLinker = upLinker
        self._downLinker = downLinker

    def shell( self, command ):

        # Check what the user wants to do
        if len( command ) == 2:
            if command[1] == FTP_HELP:
                print FTP_USAGE
            elif command[1] == FTP_VERSION:
                print FTP_VERSION_MSG
            elif command[1] == FTP_SHELL:
                self.interactiveMode( )
            else:
                print FTP_FATAL + FTP_UNKOWN_COMMAND + " \"" + command[1] + "\""
                print FTP_USAGE
        else:
            print FTP_FATAL + FTP_UNKOWN_COMMAND
            print FTP_USAGE

    def interactiveMode( self ):

        print FTP_ENTER_MSG
        while 1:

            # Prompt user for input
            action = raw_input( FTP_PROMPT )

            # Check if user wants to exit or hit enter by accident
            if action == FTP_EXIT:
                return
            if action.isspace( ) or action == '':
                continue
                
            # Check what the action is
            action = action.split( )
            if action[0] == FTP_LS and len( action ) == 1:
                self._ls( )
            elif action[0] == FTP_PUT and len( action ) == 2:
                self._put( action[1] )
            elif action[0] == FTP_GET and len( action ) == 2:
                self._get( action[1] )
            elif action[0] == FTP_RM and len( action ) == 2:
                self._rm( action[1] )
            elif action[0] == FTP_MV and len( action ) == 3:
                print "mv"
            else:
                print FTP_FATAL + FTP_UNKOWN_COMMAND
                print FTP_USAGE

    def _ls( self ):

        self._upLinker.write( '\x14', FTP_PORT )

        packet = self._downLinker.read( FTP_PORT, 15 ) # 15 second time out
        if packet == '':
            print "Failed to list"
            return
        if len( packet ) == 1:
            print "No files to list"
            return

        i = 0
        while True:
            fileName = ""

            # Get the size of the file to list
            fileSize = int( binascii.hexlify(packet[i:i+2]), 16 )
            
            # Get the name of the file to list
            i += 2
            while True:
                char = packet[i]
                i = i + 1
                if char == '\0':
                    break
                fileName += char
            
            # Print file name and size to console.
            print( str( fileSize ) + ".0\t" + binascii.unhexlify( binascii.hexlify( fileName ) ) )

            # Check if at end of packet. If there is greater than one more byte in the packet
            # then it contains more files to list
            if len(packet) == i+1:
                # At the end of the packet, check if there are more packets coming or if this was the last one.
                if packet[i] == '\x01':
                    # More packets comming.
                    packet = self._downLinker.read( FTP_PORT, 15 ) # 15 second time out
                    if len( packet ) == 1:
                        return
                    i = 0
                    continue
                else:
                    # This was the last packet.
                    return

    def _put( self, fileName ):

        # Make sure the file path is null terminated
        if str(fileName)[len(str(fileName))-1] != '\0':
            filePath = str(fileName) + '\0'
        else:
            filePath = str(fileName)

        # Extract file name from the full path entered
        startOfName = len( filePath )
        for char in filePath[::-1]:
            if char == '/':
                break
            startOfName = startOfName - 1
        fileName = filePath[startOfName:]

        # Determine how many packets to break the upload into.
        fileSize = 0
        try:
            fileSize = os.path.getsize( filePath[:len(filePath)-1] )
        except os.error:
            print FTP_FATAL + FTP_NO_FILE
            return 
        packetCount = [0]
        packetCount[0] = int( math.ceil(fileSize / float(FTP_PUT_CHUNK_SIZE - 3)) )
        intFormat = struct.Struct('! H')
        packedCount = intFormat.pack(*packetCount)

        # Uplink the map packet.
        mapPacket = '\x28' + fileName + packedCount
        self._upLinker.write( mapPacket, FTP_PORT )

        # Packetize file data and uplink it
        fileObj = open( filePath[:len(filePath)-1], 'r' )
        dataSize = [0]
        for i in range(packetCount[0]):
            time.sleep( 0.05 ) # Give satellite time to proccess incoming data, it is slower than the desktop PC
            fileData = fileObj.read( FTP_PUT_CHUNK_SIZE - 3 )
            dataSize[0] = len(fileData)

            dataPacket = '\x13' + intFormat.pack(*dataSize)  + fileData
            self._upLinker.write( dataPacket, FTP_PORT )
            
        return


    def _get( self, fileName ):

        # Add null terminated character if there is not one.
        if str(fileName)[len(str(fileName))-1] != '\0':
            command  = '\x15' + str(fileName) + '\0'
        else:
            command = '\x15' + str(fileName)

        # Up link the command to get a file
        self._upLinker.write( command, FTP_PORT )

        # Satellite will send us a map packet.
        packet = self._downLinker.read( FTP_PORT, 15 ) # 15 second time out

        # Get name of file being downlinked
        logFile = ""
        i = 0
        while True:
            char = packet[i]
            i = i + 1
            if char == '\0':
                break
            logFile = logFile + char

        # Convert file name to a string
        logFile = binascii.unhexlify( binascii.hexlify( logFile ) )

        # Get number of following files
        followingCount = int( binascii.hexlify(packet[i:i+2]), 16 )

        # File to downlink does not exist or is empty
        if followingCount == 0:
            print "0.0\t" + logFile
            return

        # Open a file on this PC's file system by the name of the file being downlinked
        fileObj = open( "downlinked_telemetry/" + logFile, "wb" )

        # Begin writing the data into memory
        for seqID in range( followingCount ):
            
            # Get a chunk of the data to write
            packet = self._downLinker.read( FTP_PORT, 15 ) # 15 second time out
            if packet == "":
                print( "Error: Timed out" )
                fileObj.close( )
                return

            # Decode data header
            err = int( binascii.hexlify(packet[0:1]), 16 )
            dataSize = int( binascii.hexlify(packet[1:3]), 16 )
            sequence = int( binascii.hexlify(packet[3:5]), 16 )

            # Error check 
            if err == 1:
                # Error occured
                fileObj.close( )
                print( "Error: Remote server failed to downlink data" )
                return

            # write data into file
            fileObj.write( packet[5:] )

        # Done, close file and return
        fileObj.close( )
        return


    def _rm( self, fileName ):

        # Add null terminated character and uplink command
        if str(fileName)[len(str(fileName))-1] != '\0':
            command = '\x12' + str(fileName) + '\0'
        else:
            command = '\x12' + str(fileName)
        self._upLinker.write( command, FTP_PORT )
