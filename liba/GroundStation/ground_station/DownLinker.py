 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #
import Queue, threading, time, binascii, Ports

IS_RUN = False
PORT_QUEUES = dict( )

class DownLinker:

    def __init__( self, serialInterface ):
        self._serial = serialInterface

        # Set up a queue for each port
        self._portQueues = dict( )
        self._isRun = False



    def run( self ):
        # Try to start the downlinker thread
        if self._isRun == False:
            for i in Ports.PORT_NUMBERS:
                self._portQueues[i] = Queue.Queue( 0 )
            threading.Thread( target=self._downlinkThread ).start( )
            self._isRun = True


    def _getPacket( self, rawRead ):
        packet = ""
        port = Ports.BROADCAST_PORT

        if len(rawRead) != 0:
            # second or greater pass through loop, rawRead may contain information, pop until stop condition is met
            for index in range(len(rawRead)):
                if rawRead[index] == '\x2F':
                    # escape character, check next character
                    if rawRead[index+1] == '\x03':
                        # end of raw data
                        rawRead = rawRead[index+2:]
                        break

        # read data out
        while True:
            if self._serial.inWaiting( ) > 0:
                #print "reading data"
                rawRead += self._serial.read( self._serial.inWaiting( ) )
            # Check for end of packet
            if len(rawRead) >= 2 and rawRead[len(rawRead)-2] == '\x2F' and rawRead[len(rawRead)-1] == '\x03':
                break
           # time.sleep( 0.05 )

        # Check header is not corrupt
        if rawRead[0] != '\x2F' or rawRead[1] != '\x02':
            # corrupt packet
            return (port, packet)

        # extract raw data from packet
        index = 0
        while True:
            if rawRead[index] == '\x2F':
                # escape character, check next character
                if rawRead[index+1] == '\x02':
                    #start of packet
                    index += 1
                    port = rawRead[index+1]
                elif rawRead[index+1] == '\x03':
                    # end of raw data
                    break
                elif rawRead[index+1] == '\x2F':
                    # escape character as data
                    packet += rawRead[index+1]
                # pass over escape character
                index += 1
            else:
                packet += rawRead[index]
            index += 1

        return (int( binascii.hexlify(port), 16 ), packet)


    def _downlinkThread( self ):

        rawRead = ""
        packet = ""
        port = Ports.BROADCAST_PORT

        while True:
            port = Ports.BROADCAST_PORT
            rawRead = ""
            (port, packet) = self._getPacket( rawRead )
            try:
                self._portQueues[port].put( packet )
            except Queue.Full or KeyError:
                continue


    def read( self, port, timeout = 0 ):
        try:
            return self._portQueues[port].get(True, timeout)
        except Queue.Empty or KeyError:
            return ""

