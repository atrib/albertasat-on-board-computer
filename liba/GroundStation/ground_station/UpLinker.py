 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #

import struct

FTP_PORT = 0
SCRIPT_PORT = 1

class UpLinker:

    def __init__( self, serialI ):
        self._serialI = serialI

        escapeFormat = struct.Struct('! B')
        self._escapeChar = escapeFormat.pack(*[0x2f])

        packetHeaderList = [0x2F, 0x02]
        packetHeaderFormat = struct.Struct('! 2B')
        self._packetHeader = packetHeaderFormat.pack( *packetHeaderList )

        packetTailList = [0x2F, 0x03]
        packetTailFormat = struct.Struct('! 2B')
        self._packetTail = packetTailFormat.pack( *packetTailList )

    def write( self, packet, port ):
        wholePacket = ''
        portFormat = struct.Struct('! B')

        wholePacket += self._packetHeader
        wholePacket += portFormat.pack(port)
        for i in range( len(packet) ):
            wholePacket += packet[i]
            if packet[i] == self._escapeChar:
                wholePacket += self._escapeChar
        wholePacket += self._packetTail

        self._serialI.write( wholePacket )
        #print( repr(wholePacket) )#binascii.hexlify(wholePacket) )

        
