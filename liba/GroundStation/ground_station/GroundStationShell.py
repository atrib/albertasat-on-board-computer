 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #

import sys
sys.path.insert( 0, 'Interfaces/' )
sys.path.insert( 0, 'Parsers/' )

from SerialInterface import SerialInterface
import ScriptShell
import FTPShell
import UpLinker
import DownLinker

# Start up message
GROUND_STATION_START_MESSAGE = "\nAlbertaSat ground station. ( version 2.0 )\n" \
                               "copyright (C) 2015 Brendan Bruner. GNU GPL V2\n"\
                               "type 'help' to recieve help\n"
GROUND_STATION_PROMPT = "ground station$ "

# Commands that can be run
GROUND_STATION_VERSION = "version"
GROUND_STATION_HELP = "help"

# Messages that accompany the commands above
GROUND_STATION_FATAL = "gs: fatal error: " 
GROUND_STATION_HINT_MSG = "type 'help' to receive help\n"
GROUND_STATION_USAGE = "Usage: program [options]\n" \
                       "Programs:\n" \
                       "\thelp\t\tDisplay this help information\n" \
                       "\tversion\t\tDisplay version information\n" \
                       "\tftp\t\tFile transfer protocol. Type ftp help for help\n" \
                       "\tscript\t\tScript shell. Type script help for help\n" 
GROUND_STATION_VERSION_MSG = "ground_station v3.0\n" \
                             "copyright (C) 2015 Brendan Bruner\n" \
                             "This is free software; see the source for copying conditions.  There is NO\n" \
                             "warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"
GROUND_STATION_UNKOWN_COMMAND = "unkown command"
GROUND_STATION_NO_SERIAL_PORT = "No serial port found. Try again.\n"

# Constants used when initializing serial interface
GROUND_STATION_BAUD = 115200
GROUND_STATION_DEFAULT_PORT = "/dev/ttyUSB1"

class GroundStationShell:

    def __init__( self ):
        self._serial = SerialInterface( )
        self._upLinker = UpLinker.UpLinker( self._serial )
        self._downLinker = DownLinker.DownLinker( self._serial )
        self._scriptShell = ScriptShell.ScriptShell( self._upLinker )
        self._ftpShell = FTPShell.FTPShell( self._upLinker, self._downLinker )
        # Run the set
        self._setUp( )

    def console_run( self ):
        while True:
            if self._usbInterface.inWaiting( ) > 0:
                rawRead = self._usbInterface.read( self._usbInterface.inWaiting( ) )
                sys.stdout.write( binascii.unhexlify( binascii.hexlify( rawRead ) ) )

    def _setUp( self ):        
        print GROUND_STATION_START_MESSAGE
        
        # Set port and baud of serial interface
        port = raw_input( "Please enter serial port. Default is " + GROUND_STATION_DEFAULT_PORT + "\n" \
                          "if nothing is entered.\n" + GROUND_STATION_PROMPT )

        # Loop until user enters a valid serial port
        while 1:
            if( port == '' ):
                self._serial.initSerial( GROUND_STATION_DEFAULT_PORT, GROUND_STATION_BAUD )
            else:
                self._serial.initSerial( port, GROUND_STATION_BAUD )

            if self._serial.portOpen( ):
                break
            print GROUND_STATION_FATAL + GROUND_STATION_NO_SERIAL_PORT
            port = raw_input( GROUND_STATION_PROMPT )
        print "port connected"
        self._downLinker.run( )

    def run( self ):

        # This is the main task. Run for ever.
        while 1:

            # Prompt user for input
            command = raw_input( GROUND_STATION_PROMPT )

            # Check if valid input was entered
            if command != '':
                command = command.split( )

                if command[0] == FTPShell.NAME:
                    self._ftpShell.shell( command )

                elif command[0] == ScriptShell.NAME:
                    self._scriptShell.shell( command )

                elif command[0] == GROUND_STATION_HELP:
                    print GROUND_STATION_USAGE

                elif command[0] == GROUND_STATION_VERSION:
                    print GROUND_STATION_VERSION_MSG

                else:
                    print GROUND_STATION_FATAL + GROUND_STATION_UNKOWN_COMMAND + " \"" + command[0] + "\""
                    print GROUND_STATION_HINT_MSG


