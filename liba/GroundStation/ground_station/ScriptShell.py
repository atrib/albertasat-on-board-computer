 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #

import UpLinker

# Possible shell commands
SCRIPT_SHELL_HELP = "help"
SCRIPT_SHELL_VERSION = "version"
SCRIPT_SHELL_SHELL = "shell"
SCRIPT_SHELL_EXIT = "QUIT"

# Possible messages
SCRIPT_SHELL_USAGE = "Usage: script [options]\n" \
                     "options:\n" \
                     "\thelp\t\tDisplay this help information\n" \
                     "\tversion\t\tDisplay version information\n" \
                     "\tshell\t\tStart interactive shell.\n" \
                     "\tQUIT\t\tQuit interactive shell\n"
SCRIPT_SHELL_ENTER_MSG = "Welcome to the interactive script shell\n" \
                         "Upon hitting the return key, your script will\n" \
                         "be sent to the satellite's on board computer.\n" \
                         "\tTo quit, type QUIT followed by the return.\n"
SCRIPT_SHELL_VERSION_MSG = "Script Shell v1.0\n" \
                           "copyright (C) 2015 Brendan Bruner\n" \
                           "This is free software; see the source for copying conditions.  There is NO\n" \
                           "warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"
SCRIPT_SHELL_FATAL = "script: fatal error: " 
SCRIPT_SHELL_UNKOWN_COMMAND = "unkown command"

# Prompt in interactive shell
SCRIPT_SHELL_PROMPT = "script shell$ "

SCRIPT_PORT = UpLinker.SCRIPT_PORT
NAME = "script"

class ScriptShell:

    def __init__( self, upLinker ):
        self._upLinker = upLinker

    def shell( self, command ):

        # Check what the user wants to do
        if len( command ) == 2:
            if command[1] == SCRIPT_SHELL_HELP:
                print SCRIPT_SHELL_USAGE
            elif command[1] == SCRIPT_SHELL_VERSION:
                print SCRIPT_SHELL_VERSION_MSG
            elif command[1] == SCRIPT_SHELL_SHELL:
                self.interactiveMode( )
            else:
                print SCRIPT_SHELL_FATAL + SCRIPT_SHELL_UNKOWN_COMMAND + " \"" + command[1] + "\""
                print SCRIPT_SHELL_USAGE
        else:
            print SCRIPT_SHELL_FATAL + SCRIPT_SHELL_UNKOWN_COMMAND
            print SCRIPT_SHELL_USAGE
                    
            
    def interactiveMode( self ):
        print SCRIPT_SHELL_ENTER_MSG
        while 1:

            # Prompt user for input
            script = raw_input( SCRIPT_SHELL_PROMPT )
            if script == SCRIPT_SHELL_EXIT:
                return
            if script.isspace( ) or script == '':
                continue
            self._upLinker.write( script, SCRIPT_PORT )

