 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #

# This code was guided by the code examples at:
# http://stackoverflow.com/questions/7821661/how-to-code-autocompletion-in-python

import readline
from Telecommand import Telecommand
from Beacon import Beacon
from FTP import FTP

class GSCompleter:
    _isInit = False
    _instance = None

    def __init__( self, gsCompletes, tcCompletes, bcCompletes, ftpCompletes ):
        self._gsCompletes = sorted( gsCompletes )
        self._tcCompletes = sorted( tcCompletes )
        self._bcCompletes = sorted( bcCompletes )
        self._ftpCompletes = sorted( ftpCompletes )
        
    
    def complete( self, partialMsg, state  ):
        # Check which program is being auto completed
        if readline.get_line_buffer( ).startswith( Telecommand.name ):
            self._autoCompletes = self._tcCompletes
        elif readline.get_line_buffer( ).startswith( Beacon.name ):
            self._autoCompletes = self._bcCompletes
        elif readline.get_line_buffer( ).startswith( FTP.name ):
            self._autoCompletes = self._ftpCompletes
        else:
            self._autoCompletes = self._gsCompletes

        # If there is anything to match, build the matches
        if partialMsg:
            self._matches = [text for text in self._autoCompletes
                             if text and text.startswith( partialMsg )]
        # Else, fill matches with everything
        else:
            self._matches = self._autoCompletes[:]

        # Return the matches
        try:
            return self._matches[state]
        except IndexError:
            return None
