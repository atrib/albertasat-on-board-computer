 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #

import time, binascii
from serial import Serial, SerialException

class SerialInterface:
    _ports = {}

    def __init__( self, port, baud ):
        self._serial = None
        self._myPort = None
        self.initSerial( port, baud )

    def __init__( self ):
        self._serial = None
        self._myPort = None

    def initSerial( self, port, baud ):
        # If the port isn't open in a seperate SerialInterface instance, open it here.
        if port not in SerialInterface._ports:
            try:
                self._serial = Serial( port, baud )
                self._myPort = port
                SerialInterface._ports[ port ] = port
                time.sleep( 2 )
            except SerialException:
                self._serial = None
                self._myPort = None

    def deInit( self ):
        if self._myPort in SerialInterface._ports:
            del SerialInterface._ports[ self._myPort ]

    def portOpen( self ):
        if self._serial == None:
            return False
        else:
            return True

    def write( self, data ):
        if self.portOpen:
            written = self._serial.write( data )
            time.sleep( 1.5 )
            return written
        else:
            return 0

    def read( self, size=1 ):
        if self.portOpen:
            read = ''
            read += self._serial.read( size )
            return read
        else:
            return 0

    def inWaiting( self ):
        if self.portOpen:
            return self._serial.inWaiting( )
        else:
            return 0
