/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_blink.h
 * @author Brendan Bruner
 * @date Aug 12, 2015
 */
#ifndef INCLUDE_TELECOMMANDS_SELF_ALTERATION_TELECOMMAND_BLINK_H_
#define INCLUDE_TELECOMMANDS_SELF_ALTERATION_TELECOMMAND_BLINK_H_

#include <telecommands/telecommand.h>
#include <gpio/gpio_lpc.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct telecommand_blink_t
 * @extends telecommand_t
 * @brief
 * 		A telecommand which pings the ground stations.
 * @details
 * 		A telecommand which pings the ground stations.
 * @var telecommand_blink_t::_super_
 * 		<b>Private</b>
 * 		Super struct data.
 */
typedef struct telecommand_blink_t telecommand_blink_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct telecommand_blink_t
{
	telecommand_t _super_;
	struct
	{
		gpio_lpc_t gpio;
	}_; /* Private. */
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof telecommand_blink_t
 * @brief
 * 		A command which pings the ground station.
 * @details
 * 		A command which pings the ground station.
 * @param command
 * 		A pointer to the structure to initialize.
 * @param kit[in]
 * 		This structure will be used, in general, for its rtc and cmnd status logger.
 * 		However, additional drivers may be utilized.
 */
uint8_t initialize_telecommand_blink( telecommand_blink_t *command, driver_toolkit_t *kit );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/


#endif /* INCLUDE_TELECOMMANDS_SELF_ALTERATION_TELECOMMAND_BLINK_H_ */
