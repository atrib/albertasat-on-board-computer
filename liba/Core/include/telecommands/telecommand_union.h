/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_union.h
 * @author Brendan Bruner
 * @date Aug 12, 2015
 */
#ifndef INCLUDE_TELECOMMANDS_TELECOMMAND_UNION_H_
#define INCLUDE_TELECOMMANDS_TELECOMMAND_UNION_H_

#include <stdint.h>

#include <telecommands/telecommand.h>
#include <telecommands/self_alteration/telecommand_blink.h>
#include <telecommands/self_alteration/telecommand_reboot.h>
#include <telecommands/self_alteration/telecommand_sync_rtc.h>
#include <telecommands/subsystem_alteration/telecommand_configure_adcs.h>
#include <telecommands/subsystem_alteration/telecommand_configure_comm.h>
#include <telecommands/subsystem_alteration/telecommand_configure_eps.h>
#include <telecommands/subsystem_alteration/telecommand_operate_adcs.h>
#include <telecommands/telemetry_alteration/telecommand_automated_downlink.h>
#include <telecommands/telemetry_alteration/telecommand_lock_config.h>
#include <telecommands/telemetry_alteration/telecommand_log_wod.h>
#include <telecommands/telemetry_alteration/telecommand_prioritize_downlink.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct telecommand_union_t
 * @brief
 * 		Contains the union of all telecommand types.
 * @details
 * 		Contains the union of all telecommand types.
 */
typedef union telecommand_union_t telecommand_union_t;


/********************************************************************************/
/* Structure Declares															*/
/********************************************************************************/
union telecommand_union_t
{
	/* TODO: Continue filling this with telecommands as they are defined. */
	telecommand_t 						telecommand;
	telecommand_blink_t					blink;
	telecommand_reboot_t				reboot;
	telecommand_sync_rtc_t				sync_rtc;
	telecommand_configure_adcs_t		configure_adcs;
	telecommand_configure_comm_t		configure_comm;
	telecommand_configure_eps_t			configure_eps;
	telecommand_operate_adcs_t			operate_adcs;
	telecommand_automated_downlink_t	automated_downlink;
	telecommand_log_wod_t				log_wod;
	telecommand_prioritize_downlink_t	prioritize_downlink;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/



/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_TELECOMMANDS_TELECOMMAND_UNION_H_ */
