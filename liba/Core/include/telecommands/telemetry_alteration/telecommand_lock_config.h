/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_lock_state_config.h
 * @author Brendan Bruner
 * @date Aug 24, 2015
 */
#ifndef INCLUDE_TELECOMMANDS_TELEMETRY_ALTERATION_TELECOMMAND_LOCK_CONFIG_H_
#define INCLUDE_TELECOMMANDS_TELEMETRY_ALTERATION_TELECOMMAND_LOCK_CONFIG_H_

#include <telecommands/telecommand.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @extends telecommand_t
 * @struct telecommand_lock_config_t
 * @brief
 * 		Locks all configuration files.
 * @details
 * 		Locks all configuration files from being accessed in all instances of structures
 * 		which use the configuration file. This command
 * 		is used so that the ftp service can safetly change the configuration file
 * 		in memory without corrupting it.
 * 		<br>There is a five minute time out on all locks. The lock will be removed
 * 		automatically after five minutes and it will no longer be safe for the ftp
 * 		service to change the configuration file.
 * 		<br>telecommand_unlock_config_t can be used to unlock this before the five
 * 		minutes time out expires. In addition, an explicit unlock will force every a
 * 		refresh of all cached configurations with the version in non volatile
 * 		memory.
 * @attention
 * 		Issuing this command multiple times will result in the telecommanding system
 * 		of the obc locking. If <b>n</b> of these command are issued in succession, then
 * 		the telecommanding system will become locked for <b>5*(n-1) minutes</b>.
 * @var telecommand_lock_config_t::_super_
 * 		<b>Private</b> Super struct data.
 */
typedef struct telecommand_lock_config_t telecommand_lock_config_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct telecommand_lock_config_t
{
	telecommand_t _super_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof telecommand_lock_config_t
 * @brief
 * 		Initializer for telecommand_lock_config_t
 * @details
 * 		Initializer for telecommand_lock_config_t
 * @param kit[in]
 * 		Used by the command to access the rtc API and logger API.
 * @returns
 * 		<b>TELECOMMAND_SUCCESS</b>: Successful initialization.
 * 		<br><b>TELECOMMAND_FAILURE</b>: Fatal error initializing. The structure is not safe to use.
 */
uint8_t initialize_telecommand_lock_config( telecommand_lock_config_t *self, driver_toolkit_t *kit );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/


#endif /* INCLUDE_TELECOMMANDS_TELEMETRY_ALTERATION_TELECOMMAND_LOCK_CONFIG_H_ */
