/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file core_defines
 * @author Brendan Bruner
 * @date Mar 12, 2015
 */
#ifndef INCLUDE_CORE_DEFINES_H_
#define INCLUDE_CORE_DEFINES_H_

#include <portable_types.h>

#define ONE_BYTE 1

/********************************************************************************/
/* Configuration Files															*/
/********************************************************************************/
/* Delay of the global lock on configuration files. For release, this should be 5 minutes. */
#ifdef DEBUG
#define CONFIG_LOCK_DELAY ( (1000 * 10) / portTICK_RATE_MS ) /* 10 seconds. */
#else
#define CONFIG_LOCK_DELAY ( (1000 * 60 * 5) / portTICK_RATE_MS ) /* 5 minutes. */
#endif

/********************************************************************************/
/* Black Out State																*/
/********************************************************************************/
#define BLACK_OUT_FILE "lsbof.txt"
#ifdef DEBUG
#define BLACK_OUT_DELAY ( (1000 * 10) / portTICK_RATE_MS ) /* 10 seconds. */
#define BLACK_OUT_RESOLUTION 5 /* 5 seconds */
#else
#define BLACK_OUT_DELAY ( (1000 * 60 * 30) / portTICK_RATE_MS ) /* 30 minutes. */
#define BLACK_OUT_RESOLUTION 60 /* 60 seconds */
#endif

/********************************************************************************/
/* LEOP Detumble																*/
/********************************************************************************/
#ifdef DEBUG
#define LEOP_DETUMBLE_RESOLUTION 5 /* 5 seconds. */
#else
#define LEOP_DETUMBLE_RESOLUTION 60 /* 60 seconds. */
#endif

/********************************************************************************/
/* State Relay																	*/
/********************************************************************************/
#define LEOP_FLAG_FILE "lsff.txt"
#define BLACK_OUT_TIME_OUT_LOG "lsbo_tl.txt"
#define DETUMBLE_TIME_OUT_LOG "lsd_tl.txt"
#ifdef DEBUG_MODE
#define BLACK_OUT_TIME_OUT (30) /* 30 seconds. */
#define DETUMBLE_TIME_OUT (60) /* 1 minute. */
#else
#define BLACK_OUT_TIME_OUT (30 * 60) /* 30 minutes. */
#define DETUMBLE_TIME_OUT (3 * 24 * 60 * 60) /* 3 days. */
#endif

/********************************************************************************/
/* States																		*/
/********************************************************************************/
#define STATE_LEOP_CHECK_LOG			"slc_l.txt"
#define STATE_BLACK_OUT_LOG				"lsbo_l.txt"
#define STATE_DETUMBLE_LOG				"lsd_l.txt"
#define STATE_LEOP_POWER_CHARGE_LOG 	"lslpc_l.txt"
#define STATE_BOOM_DEPLOYMENT_LOG 		"lsbd_l.txt"
#define STATE_ANTENNA_DEPLOYMENT_LOG 	"lsad_l.txt"
#define STATE_ALIGNMENT_LOG 			"osa_l.txt"
#define STATE_BRING_UP_LOG				"osbu_l.txt"
#define STATE_LOW_POWER_LOG				"oslp_l.txt"
#define STATE_SCIENCE_LOG				"oss_l.txt"

/********************************************************************************/
/* Command Logger																*/
/********************************************************************************/
#define CMND_STATUS_LOGGER_PACKET_SIZE 		PACKET_TYPE_CMND_STATUS_SIZE
#define CMND_STATUS_LOGGER_DELETE_BUFFER	"CLog-DB.txt"
#define CMND_STATUS_LOGGER_PACKET_NAMES		"C0000000.txt"
#define CMND_STATUS_LOGGER_NAME_LENGTH		12
#define CMND_STATUS_LOGGER_INDEX_END		7
#define CMND_STATUS_LOGGER_INDEX_BEGIN 		1
#define CMND_STATUS_LOGGER_MASTER_TABLE		"CLog-MT.txt"

/********************************************************************************/
/* DFGM Logger																	*/
/********************************************************************************/
#define DFGM_LOGGER_PACKET_SIZE 		PACKET_TYPE_DFGM_SIZE
#define DFGM_LOGGER_DELETE_BUFFER		"DLog_DB.txt"
#define DFGM_LOGGER_PACKET_NAMES		"D0000000.txt"
#define DFGM_LOGGER_NAME_LENGTH			12
#define DFGM_LOGGER_INDEX_END			7
#define DFGM_LOGGER_INDEX_BEGIN 		1
#define DFGM_LOGGER_MASTER_TABLE		"DLog-MT.txt"

/********************************************************************************/
/* MNLP Logger																	*/
/********************************************************************************/
#define MNLP_LOGGER_PACKET_SIZE 		PACKET_TYPE_MNLP_SIZE
#define MNLP_LOGGER_DELETE_BUFFER		"MLog-DB.txt"
#define MNLP_LOGGER_PACKET_NAMES		"M0000000.txt"
#define MNLP_LOGGER_NAME_LENGTH			12
#define MNLP_LOGGER_INDEX_END			7
#define MNLP_LOGGER_INDEX_BEGIN 		1
#define MNLP_LOGGER_MASTER_TABLE		"MLog-MT.txt"

/********************************************************************************/
/* State Logger																	*/
/********************************************************************************/
#define STATE_LOGGER_PACKET_SIZE 	PACKET_TYPE_STATE_SIZE
#define STATE_LOGGER_DELETE_BUFFER	"SLog-DB.txt"
#define STATE_LOGGER_PACKET_NAMES	"S0000000.txt"
#define STATE_LOGGER_NAME_LENGTH	12
#define STATE_LOGGER_INDEX_END		7
#define STATE_LOGGER_INDEX_BEGIN 	1
#define STATE_LOGGER_MASTER_TABLE	"SLog-MT.txt"

/********************************************************************************/
/* WOD Logger																	*/
/********************************************************************************/
#define WOD_LOGGER_PACKET_SIZE 		PACKET_TYPE_WOD_SIZE
#define WOD_LOGGER_DELETE_BUFFER	"WLog-DB.txt"
#define WOD_LOGGER_PACKET_NAMES		"W0000_00.txt"
#define WOD_LOGGER_NAME_LENGTH		12
#define WOD_LOGGER_DAY_INDEX_0 		1
#define WOD_LOGGER_DAY_INDEX_1 		2
#define WOD_LOGGER_DAY_INDEX_2 		3
#define WOD_LOGGER_DAY_INDEX_3 		4
#define WOD_LOGGER_SEQ_INDEX_0 		6
#define WOD_LOGGER_SEQ_INDEX_1 		7
#define WOD_LOGGER_MASTER_TABLE		"WLog-MT.txt"

/********************************************************************************/
/* Persistent Timer																*/
/********************************************************************************/
#define PTIMER_DEFAULT_RESOLUTION (60) /* 1 minute */
#ifdef DEBUG
#define PTIMER_CONTROLLER_UPDATE_CADENCE_MS (1000) /* 1 seconds */
#else
#define PTIMER_CONTROLLER_UPDATE_CADENCE_MS (5 * 1000) /* 5 seconds */
#endif
#define PTIMER_MANAGER_STACK_DEPTH 700
#define PTIMER_MANAGER_PRIORITY (BASE_PRIORITY+1)
#define PTIMER_MANAGER_NAME "ptimer mnger"

/********************************************************************************/
/* FTP																			*/
/********************************************************************************/
#define FTP_SLEEP_PERIOD 1000 /* 1 second */
#define FTP_NAME "ftp task"
#define FTP_STACK 700
#define FTP_PRIO (BASE_PRIORITY+1)

/********************************************************************************/
/* LEOP flag values																*/
/********************************************************************************/
#define LEOP_FLAG_UNFINISHED 0
#define LEOP_FLAG_FINISHED 2


/********************************************************************************/
/* File to store telemetry priorities											*/
/********************************************************************************/
#define PRIORITY_LOG_FILE "odp.txt"


/********************************************************************************/
/* Script Daemon																*/
/********************************************************************************/
#define SCRIPT_DAEMON_SLEEP_PERIOD	100 /* 100 ms */
#define SCRIPT_DAEMON_NAME			"script daemon"
#define SCRIPT_DAEMON_STACK			256
#define SCRIPT_DAEMON_PRIO			(BASE_PRIORITY+1)
/* Max length of script sent directly from ground station. */
/* Does not include length of scripts in put in file system via ftp. */
#define SCRIPT_MAX_LENGTH			100

#endif /* INCLUDE_CORE_DEFINES_H_ */
