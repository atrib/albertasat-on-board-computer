/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file mnlp_logger.h
 * @author Brendan Bruner
 * @date Aug 26, 2015
 */
#ifndef INCLUDE_LOGGER_MNLP_LOGGER_H_
#define INCLUDE_LOGGER_MNLP_LOGGER_H_

#include "logger.h"


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct mnlp_logger_t
 * @extends logger_t
 * @brief
 * 		Used to log mnlp payload data.
 * @details
 * 		Used to log mnlp payload data. No packet definition is documented in house yet.
 */
typedef struct mnlp_logger_t mnlp_logger_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct mnlp_logger_t
{
	logger_t _super_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof mnlp_logger_t
 * @brief
 * 		Initialize to log mnlp payload data.
 * @details
 * 		Initialize to log mnlp payload data.
 * @param fs[in]
 * 		The filesystem for the logger to use.
 * @returns
 * 		Same return as _initialize_logger.
 */
logger_error_t initialize_mnlp_logger( mnlp_logger_t *logger, filesystem_t *fs );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_LOGGER_MNLP_LOGGER_H_ */
