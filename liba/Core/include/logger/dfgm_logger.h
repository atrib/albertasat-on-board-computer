/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file dfgm_logger.h
 * @author Brendan Bruner
 * @date Aug 26, 2015
 */
#ifndef INCLUDE_LOGGER_DFGM_LOGGER_H_
#define INCLUDE_LOGGER_DFGM_LOGGER_H_

#include "logger.h"


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct dfgm_logger_t
 * @extends logger_t
 * @brief
 * 		Used to log dfgm payload data.
 * @details
 * 		Used to log dfgm payload data. A packet is defined here:
 * 		@htmlonly
 * 		<a href="https://bytebucket.org/bbruner0/albertasat-on-board-computer/wiki/2.%20Architecture/Diagrams/packet_structure/DFGM_packet_definition_V0.0.1.pdf?rev=7ddfde4f5ac03d32cefd749dd415a5f4ac1393b8</a>
 * 		@endhtmlonly
 */
typedef struct dfgm_logger_t dfgm_logger_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct dfgm_logger_t
{
	logger_t _super_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof dfgm_logger_t
 * @brief
 * 		Initialize to log dfgm payload data.
 *
 * @details
 * 		Initialize to log dfgm payload data.
 * @param fs[in]
 * 		The filesystem for the logger to use.
 * @returns
 * 		Same return as _initialize_logger.
 */
logger_error_t initialize_dfgm_logger( dfgm_logger_t *logger, filesystem_t *fs );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_LOGGER_DFGM_LOGGER_H_ */
