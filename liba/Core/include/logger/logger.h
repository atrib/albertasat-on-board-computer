/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file logger.h
 * @author Brendan Bruner
 * @date May 14, 2015
 */
#ifndef INCLUDE_TELEMETRY_LOGGER_H_
#define INCLUDE_TELEMETRY_LOGGER_H_

#include <filesystems/filesystem.h>
#include <portable_types.h>

#include <stdint.h>

/* TODO: when master table is erased and only writes are done it keeps on chugging. */
/* Error checks need to be put in place EVERY time master table is opened and a */
/* recover_master_table( ) function needs to be made to help recover. */
/* For example, us the OPEN_EXISTING flag when opening master table. */

/********************************************************************************/
/* Defines																		*/
/********************************************************************************/
#define LOGGER_MAX_FILE_NAME_LENGTH FILESYSTEM_MAX_NAME_LENGTH


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct logger_t
 * @brief
 * 		Structure which abstracts telemetry filesystem requirements.
 * @details
 * 		This structure abstracts away the requirements for telemetry (section 2.
 * 		of the bitbucket wiki). It handles the creation and retrieval of files,
 * 		but never removes files from memory. The logger acts as a queue, it
 * 		creates files and pushes them to the top of the queue. When a request
 * 		to retrieve files is made, the files are removed from the bottom of the
 * 		queue (but are never deleted from the filesystem). The loggers queue is
 * 		non volatile and therefore persists through power cycles.
 * @var logger_t::queue_file_name;
 * 		<b>Private</b>
 * 		The name of a file which contains meta data about packet files. The file is structured
 * 		like this:
 * 		| Checksum (1 byte) | read index (4 bytes ) | oldest name (LOGGER_MAX_FILE_NAME_LENGTH+1 bytes) | newest name (LOGGER_MAX_FILE_NAME_LENGTH+1 bytes) |
 * @var logger_t::delete_buffer_name;
 * 		<b>Private</b>
 * 		Name of file to use as a buffer
 * @var logger_t::packet_name
 * 		<b>Private</b>
 * 		Allocated space in memory to cache the name of the currently used packet file. This name
 * 		is null terminated.
 * @var logger_t::packet_name_length;
 * 		<b>Private</b>
 * 		Length of logger_t::_packet_name_, does not include the null character.
 * @var logger_t::filesystem;
 * 		<b>Private</b>
 * 		The filesystem used by the logger.
 * @var logger_t::sync_mutex;
 * 		<b>Private</b>
 * 		Mutex used for mutual exclusion. This is a singleton shared by all logger instances.
 */
typedef struct logger_t logger_t;

/** Error codes returned by logger_t methods. */
typedef enum
{
	LOGGER_OK = 0,		/*!< (0) Succeeded. */
	LOGGER_NVMEM_ERR,		/*!< (1) An error occurred interacting with non volatile memory.
	 	 	 	 	 	 	 	 operation aborted, no changes made. */
	LOGGER_HOOK_ERR,	/*!< (2) Hook method reported failure. This would be a failure
	 	 	 	 	 	 	 	 in new_packet_hook( ) or new_entry_hook( ) */
	LOGGER_MUTEX_ERR,	/*!< (3) Failed to create synchronization objects. */
	LOGGER_RECOVERED,	/*!< (4) Unrecoverable corruption in logger's queue occurred.
								 Queue restored to initial, empty, set up. */
	LOGGER_EMPTY,		/*!< (5) No files in the loggers queue to peek / pop. */
} logger_error_t;


/********************************************************************************/
/* Structure Definition															*/
/********************************************************************************/
struct logger_t
{
	void (*destroy)( logger_t * );

	struct
	{
		size_t (*next_name)( logger_t *, char * );
		fs_error_t (*new_packet_hook)( logger_t *, file_t * );
		fs_error_t (*new_entry_hook)( logger_t *, file_t * );
	}__; /* Protected. */

	struct
	{
		char	 			queue_file_name[LOGGER_MAX_FILE_NAME_LENGTH+1];
		char	 			delete_buffer_name[LOGGER_MAX_FILE_NAME_LENGTH+1];
		char 				packet_name[LOGGER_MAX_FILE_NAME_LENGTH+1];
		char 				first_packet_name[LOGGER_MAX_FILE_NAME_LENGTH+1];
		size_t				packet_name_length;
		filesystem_t		*fs;
		mutex_t				*sync_mutex;
		bool_t				is_queue_init;
	}_; /* Private. */
};


/********************************************************************************/
/* Non Virtual Method Declares													*/
/********************************************************************************/
/**
 * @memberof logger_t
 * @brief
 * 		Get the current packet file - to edit.
 * @details
 * 		Get the current packet file - to edit. Successive calls to this
 * 		method return an opened handle to the same file. When the packet
 * 		file has been filled (according the applications requirements) it can
 * 		be pushed into the loggers queue with logger_push_packet( ). The next
 * 		time this function is called, it will return a handle to a new packet file.
 * 		Finally, note that the returned handle must be closed when the application is
 * 		finished with it, file_t::close( ).
 *
 * 		Everytime this method gets called, logger_t::new_entry_hook( ) is called
 * 		on the file before it gets returned.
 *
 * 		The location of the write cursor is at the end of the file.
 * @attention
 * 		The handle returned by successive calls is not the same (but the file is the same).
 * 		If your file system does not support opening the same
 * 		file with multiple handles, do not successively call this method.
 * @attention
 * 		If an unexpected power cycle occurs before logger_push_packet( ) is
 *		called, data is not lost. A reference to the file returned by this method
 *		is kept in non volatile memory.
 * @attention
 * 		file_t::close( ) must always be called on the returned file, regardless of the
 * 		returned error code.
 * @param err[out]
 * 		An error code. This argument must point to valid memory.
 * @returns
 * 		An opened handle for the current packet file.
 */
file_t* logger_get_packet( logger_t*, logger_error_t* err );

/**
 * @memberof logger_t
 * @brief
 * 		Same as logger_get_packet( ) but logger_t::new_entry_hook( ) is not called.
 * @details
 * 		Same as logger_get_packet( ) but logger_t::new_entry_hook( ) is not called.
 * 		Allows access to the top of the logger queue without calling the new entry
 * 		hook on the returned file handle.
 * @param err[out]
 * 		An error code. This argument must point to valid memory.
 * @returns
 * 		An opened file handle.
 */
file_t* logger_peek_top_packet( logger_t*, logger_error_t* err );

/**
 * @memberof logger_t
 * @brief
 * 		The file which has been returned by logger_get_packet( ) is pushed onto
 * 		the logger's queue.
 * @details
 * 		The file which has been returned by logger_get_packet( ) is pushed onto
 * 		the top of the logger's queue. After pushing a packet onto the queue,
 * 		logger_get_packet( ) will start returning a handle to a different
 * 		packet file. Note, it is possible to successively call this function
 * 		without calling logger_get_packet( ) in between. Doing this will result in
 * 		empty files being pushed onto the loggers queue.
 * 		Every time this method gets called, the new_packet_hook( ) method is called
 * 		on the next packet that will be returned by logger_get_packet( ).
 * @returns
 * 		An error code. If LOGGER_NVMEM_ERR is returned, there was no effect from calling
 * 		this method, ie, the state of the logger is unchanged and continues to function as if this
 * 		method was never called.
 */
logger_error_t logger_push_packet( logger_t* );

/**
 * @memberof logger_t
 * @brief
 * 		Peek at the packet file in the end of the queue.
 * @details
 * 		Peek at the packet file in the end of the queue. The file is
 * 		not removed from the queue and successive calls to logger_peek_packet( )
 * 		will return a handle to the same file.
 * @attention
 * 		If your filesystem does not support multiple open handles of the same file,
 * 		do not call this function a successive time until the returned handle is closed,
 * 		file_t::close( ).
 * @param err[out]
 * 		Error code. This argument must point to valid memory.
 * @returns
 * 		An opened handle for the packet file at the end of the queue. This file
 *		must be closed when finished with, file_t::close( ).
 */
file_t* logger_peek_packet( logger_t*, logger_error_t* err );

/**
 * @memberof logger_t
 * @brief
 * 		Pop the packet file in the end of the queue out.
 * @details
 * 		Pop the packet file in the end of the queue out. If this packet file
 * 		is needed for reading then logger_peek_packet( ) should be called first.
 * 		For example:
 * 		@code
 * 			extern logger_t*	logger;
 * 			logger_error_t 		err;
 * 			file_t* oldest_packet = logger_peek_packet( logger, &err );
 * 			if( err == LOGGER_OK )
 * 			{
 * 				// Got the packet file.
 * 				err = logger_pop_packet( logger );
 * 				if( err == LOGGER_OK )
 * 				{
 * 					// Removed it from the logger queue.
 * 					// Code doing stuff with the oldest packet
 * 				}
 * 			}
 * 		@endcode
 * @returns
 * 		An error code.
 */
logger_error_t logger_pop_packet( logger_t* );

/**
 * @memberof logger_t
 * @brief
 * 		Rewind the loggers queue to a previous state.
 * @details
 * 		Rewind the loggers queue to a previous state.
 * 		For example, take the following packets to be in memory, and the loggers queue:
 * 		<br>W0000_00.txt
 * 		<br>W0000_01.txt
 * 		<br>W0000_02.txt
 * 		<br>W0000_03.txt
 * 		<br>then, the logger_pop_packet( ) function is called twice. The loggers queue will contain:
 * 		<br>W0000_02.txt
 * 		<br>W0000_03.txt
 * 		<br>Note, none of the files have been removed from the file system.
 * 		A successive call to logger_pop_packet( ) would remove W0000_02.txt from the queue.
 * 		Finally, logger_reset( ) is called. The contents of the loggers queue would reset
 * 		to whats in memory:
 * 		<br>W0000_00.txt
 * 		<br>W0000_01.txt
 * 		<br>W0000_02.txt
 * 		<br>W0000_03.txt
 * 		<br>What if W0000_00.txt was deleted from memory (by an external application) before
 * 		logger_reset( ) and after it was popped out of the loggers queue? The loggers queue
 * 		would default to whats in memory:
 * 		<br>W0000_01.txt
 * 		<br>W0000_02.txt
 * 		<br>W0000_03.txt
 * @returns
 * 		An error code.
 */
logger_error_t logger_reset( logger_t * );


/********************************************************************************/
/* Initialization Method Declares												*/
/********************************************************************************/
/**
 * @memberof logger_t
 * @brief
 * 		Initialize an abstract logger_t structure
 * @details
 * 		Initialize an abstract logger_t structure.
 * @attention
 * 		The constructor uses the method logger_t::next_name( ). This method
 * 		must be bound to an implementation before calling the constructor.
 * 		@code
 * 			extern logger_t* self;
 * 			self->__.next_name = next_name_function;
 * 			_initialize_logger( self, ... );
 * 		@endcode
 * @param logger
 * 		A pointer to the logger_t structure to initialize.
 * @param filesystem
 * 		A pointer to the filesystem_t structure the logger will use to interact with
 * 		the file system. Must remain valid in memory for the duration the <b>logger</b>
 * 		instance is used.
 * @param delete_buffer
 * 		Copied. The name to use for a delete buffer (null terminated). This name must be unique amongst
 * 		every file which will every be open with the <b>filesystem</b> argument.
 * 		If this name is not unique (ie, one of a kind), the original file with this
 * 		name will become corrupt.
 * @param queue_file
 * 		Copied. The name to use for a queue file - the file acts as the logger's non volatile queue.
 * 		(null terminated). Like the <b>delete_buffer</b> this name must be unique for
 * 		the same reasons.
 * @param packet_name
 * 		Copied. The name which the very fist packet file should have. Null terminated.
 * @returns
 * 		An error code. Note, if this is the first time initializing a logger and no master
 * 		table exists, it will return <b>LOGGER_RECOVERED</b> on success.
 */
logger_error_t _initialize_logger( logger_t *logger, filesystem_t *filesystem,
								  char const *delete_buffer, char const *queue_file,
								  char const *packet_name );


#endif /* INCLUDE_TELEMETRY_LOGGER_H_ */
