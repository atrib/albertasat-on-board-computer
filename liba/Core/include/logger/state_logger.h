/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file state_logger.h
 * @author Brendan Bruner
 * @date Aug 26, 2015
 */
#ifndef INCLUDE_LOGGER_STATE_LOGGER_H_
#define INCLUDE_LOGGER_STATE_LOGGER_H_

#include "logger.h"

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct state_logger_t
 * @extends logger_t
 * @brief
 * 		Used to log state changes.
 * @details
 * 		Used to log state changes. The logger_t::_new_entry hook is
 * 		overrode to insert the current time. This means that any logging done is
 * 		always appended to a time stamp of the current time.
 */
typedef struct state_logger_t state_logger_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct state_logger_t
{
	logger_t _super_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof state_logger_t
 * @brief
 * 		Initialize to log state transistions.
 * @details
 * 		Initialize to log state transistions.
 * @param fs[in]
 * 		The filesystem for the logger to use.
 * @returns
 * 		Same return as _initialize_logger.
 */
logger_error_t initialize_state_logger( state_logger_t *logger, filesystem_t *fs );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_LOGGER_STATE_LOGGER_H_ */
