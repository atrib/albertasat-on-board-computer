/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file wod_logger.h
 * @author Brendan Bruner
 * @date Aug 26, 2015
 */
#ifndef INCLUDE_LOGGER_WOD_LOGGER_H_
#define INCLUDE_LOGGER_WOD_LOGGER_H_

#include "logger.h"
#include <rtc.h>


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct wod_logger_t
 * @extends logger_t
 * @brief
 * 		Used to log wod.
 * @details
 * 		Used to log wod. The logger_t::_new_packet_hook is
 * 		overrode to insert the current time. One packet file
 * 		is 232 bytes, this is equivalent to one whole orbit data packet as
 * 		defined by qb50. See
 * 		@htmlonly
 * 		<a href="https://bytebucket.org/bbruner0/albertasat-on-board-computer/wiki/1.%20Resources/1.1.%20DataSheets/WOD/QB50%20Whole%20Orbit%20Data%20-%20Iss4.pdf?rev=64d382b29fcad617c6b5bf14202d5433d5c61d84">WOD Format.</a>
 * 		@endhtmlonly
 */
typedef struct wod_logger_t wod_logger_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct wod_logger_t
{
	logger_t _super_;
	struct
	{
		rtc_t* rtc;
	}_; /* Private. */
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof wod_logger_t
 * @brief
 * 		Initialize to log whole orbit data (wod)
 * @details
 * 		Initialize to log whole orbit data (wod).
 * @param fs[in]
 * 		The filesystem for the logger to use.
 * @param rtc[in]
 * 		A pointer to an rtc_t object to use. This is used to time stamp wod.
 * @returns
 * 		Same return as _initialize_logger.
 */
logger_error_t initialize_wod_logger( wod_logger_t *logger, filesystem_t *fs, rtc_t* rtc );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_LOGGER_WOD_LOGGER_H_ */
