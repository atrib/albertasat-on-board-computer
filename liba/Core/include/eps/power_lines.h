/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file power_lines.h
 * @author Brendan Bruner
 * @date Oct 7, 2015
 */
#ifndef INCLUDE_POWER_LINES_H_
#define INCLUDE_POWER_LINES_H_

#include <eps/eps.h>
#include <hub/hub.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
/* Defines the power line each system is on. */

/* Systems on the EPS' power line. */
#if defined( DEMO )

#define ADCS_POWER_LINE				EPS_POWER_LINE0
#define COMM_POWER_LINE 			EPS_POWER_LINE1
#define DFGM_POWER_LINE 			EPS_POWER_LINE2
#define TELEDYNE_POWER_LINE 		EPS_POWER_LINE4
#define NANOHUB_POWER_LINE 			EPS_POWER_LINE5
#define STEFANS_BOARDS_POWER_LINE 	EPS_POWER_LINE0

/* Systems on the hub's power line. */
#define MNLP_3V3_POWER_LINE 		HUB_POWER_LINE0
#define MNLP_5V0_POWER_LINE			HUB_POWER_LINE1

#elif defined( NANOMIND )

#define ADCS_POWER_LINE				EPS_POWER_LINE0
#define COMM_POWER_LINE 			EPS_POWER_LINE1
#define DFGM_POWER_LINE 			EPS_POWER_LINE2
#define TELEDYNE_POWER_LINE 		EPS_POWER_LINE4
#define NANOHUB_POWER_LINE 			EPS_POWER_LINE5
#define STEFANS_BOARDS_POWER_LINE 	EPS_POWER_LINE0

/* Systems on the hub's power line. */
#define MNLP_3V3_POWER_LINE 		HUB_POWER_LINE0
#define MNLP_5V0_POWER_LINE			HUB_POWER_LINE1

#else

#define ADCS_POWER_LINE				EPS_POWER_LINE0
#define COMM_POWER_LINE 			EPS_POWER_LINE1
#define DFGM_POWER_LINE 			EPS_POWER_LINE2
#define TELEDYNE_POWER_LINE 		EPS_POWER_LINE3
#define NANOHUB_POWER_LINE 			EPS_POWER_LINE4
#define STEFANS_BOARDS_POWER_LINE 	EPS_POWER_LINE5

/* Systems on the hub's power line. */
#define MNLP_3V3_POWER_LINE 		HUB_POWER_LINE0
#define MNLP_5V0_POWER_LINE			HUB_POWER_LINE1

#endif



#endif /* INCLUDE_POWER_LINES_H_ */
