/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file ftp.h
 * @author Brendan Bruner
 * @date Jul 20, 2015
 */
#ifndef INCLUDE_FTP_H_
#define INCLUDE_FTP_H_

#include <ground_station.h>
#include <driver_toolkit/driver_toolkit.h>
#include <filesystems/filesystem.h>
#include <portable_types.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define FTP_SUCCESS 1
#define FTP_FAILURE 0

#define FTP_SERVICE_RM		18
#define FTP_SERVICE_LIST	20
#define FTP_SERVICE_PUT		19
#define FTP_SERVICE_GET		21
#define FTP_SERVICE_PUT_MAP	40


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct ftp_t
 * @brief
 * 		This structure is used to maintain state data while running an ftp service.
 * @details
 * 		This structure is used to maintain state data while running an ftp service.
 * 		The memory resource that the ftp service will enable access to is defined
 * 		at initialization. See initialize_ftp_service( ). Multiple instances of
 * 		this structure should not be used to give access to the same memory
 * 		resource.
 */
typedef struct ftp_t ftp_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct ftp_t
{
	filesystem_t		*_fs;
	ground_station_t	*_gs;
	task_t				_service_task;
};


/********************************************************************************/
/* Constructor / Destructor	Declare												*/
/********************************************************************************/
/**
 * @memberof ftp_t
 * @brief
 * 		Initialize an ftp_t structure to provide an ftp service.
 * @details
 * 		Initialize an ftp_t structure to provide an ftp service. The service
 * 		does not start until a call to start_ftp_service( ) is made.
 * @param ftp[in]
 * 		The structure to initialize.
 * @param kit[in]
 * 		Contains two drivers which will be used. The first is
 * 		driver_toolkit_t::fs, which is used to access the memory resource
 * 		being service. The second is driver_toolkit_t::gs which is used
 * 		for IO to service requests.
 * @returns
 * 		An error code.
 * 		<br><b>FTP_SUCCESS</b>: If initialized correctly.
 * 		<br><b>FTP_FAILURE</b>: If failed to initialize. The structure is not
 * 		safe to use.
 */
uint8_t initialize_ftp_service( ftp_t *ftp, driver_toolkit_t *kit );

/**
 * @memberof ftp_t
 * @brief
 * 		Destroy an ftp_t structure when it is no longer needed.
 * @details
 * 		Destroy an ftp_t structure when it is no longer needed.
 * @param ftp[in]
 * 		The ftp structure to destroy.
 */
void destroy_ftp_service( ftp_t *ftp );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/
/**
 * @memberof ftp_t
 * @brief
 * 		Starts the service of an ftp_t structure which has been initialized.
 * @details
 * 		Starts the service of an ftp_t structure which has been initialized.
 * @param ftp[in]
 * 		The ftp structure to start the service of.
 */
void start_ftp_service( ftp_t *ftp );


#endif /* INCLUDE_FTP_H_ */
