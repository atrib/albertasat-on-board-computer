/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file teledyne.h
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */
#ifndef INCLUDE_teledyne_teledyne_H_
#define INCLUDE_teledyne_teledyne_H_

#include <eps/eps.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct teledyne_t
 * @brief
 * 		Abstract class defining interface to the teledyne.
 * @details
 * 		Abstract class defining interface to the teledyne.
 */
typedef struct teledyne_t teledyne_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct teledyne_t
{
	bool_t (*power)( teledyne_t*, eps_t*, bool_t );
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/



/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_teledyne_teledyne_H_ */
