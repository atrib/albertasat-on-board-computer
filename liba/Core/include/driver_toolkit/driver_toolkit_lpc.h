/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file driver_toolkit_lpc.h
 * @author Brendan Bruner
 * @date Oct 23, 2015
 */
#ifndef INCLUDE_DRIVER_TOOLKIT_DRIVER_TOOLKIT_LPC_H_
#define INCLUDE_DRIVER_TOOLKIT_DRIVER_TOOLKIT_LPC_H_

#include "driver_toolkit.h"
#include <filesystems/fatfs/filesystem_fatfs_lpc.h>
#include <eps/eps_local.h>
#include <gpio/gpio_lpc.h>
#include <comm/comm_local.h>
#include <hub/hub_local.h>
#include <dfgm/dfgm_local.h>
#include <mnlp/mnlp_local.h>
#include <teledyne/teledyne_local.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct driver_toolkit_lpc_t
 * @extends driver_toolkit_t
 * @brief
 * 		A toolkit which works on an LPC board.
 * @details
 * 		A toolkit which works on an LPC board.
 */
typedef struct driver_toolkit_lpc_t driver_toolkit_lpc_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct driver_toolkit_lpc_t
{
	driver_toolkit_t s_; /* MUST be first. */
	struct
	{
		eps_local_t 			eps;
		comm_local_t			comm;
		ground_station_t		gs;
		hub_local_t				hub;

		gpio_lpc_t eps_state_gpios_mem[EPS_LOCAL_STATE_GPIO_COUNT];
		gpio_lpc_t eps_power_line_gpios_mem[EPS_TOTAL_POWER_LINES];
		gpio_lpc_t hub_deployment_lines_mem[HUB_TOTAL_DEPLOYMENT_LINES];
		gpio_lpc_t hub_power_line_gpios_mem[HUB_TOTAL_POWER_LINES];

		filesystem_fatfs_lpc_t	fs;

		dfgm_local_t 			dfgm;
		mnlp_local_t			mnlp;
		teledyne_local_t		teledyne;
	}_; /* Private. */
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof driver_toolkit_lpc_t
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 */
bool_t initialize_driver_toolkit_lpc( driver_toolkit_lpc_t *toolkit );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_DRIVER_TOOLKIT_DRIVER_TOOLKIT_LPC_H_ */
