/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file hub_local.h
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */
#ifndef INCLUDE_HUB_HUB_LOCAL_H_
#define INCLUDE_HUB_HUB_LOCAL_H_

#include "hub.h"
#include <gpio/gpio.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct hub_local_t
 * @extends hub_t
 * @brief
 * 		Provides an API mock up for the nanohub.
 * @details
 * 		Provides an API mock up for the nanohub. Deployment and power control
 * 		is done via toggling GPIOs.
 */
typedef struct hub_local_t hub_local_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct hub_local_t
{
	hub_t s_; /* MUST be first. */
	struct
	{
		gpio_t* deployment_gpios[HUB_TOTAL_DEPLOYMENT_LINES];
		bool_t using_deployment_gpios;
		gpio_t* power_line_gpios[HUB_TOTAL_POWER_LINES];
		bool_t using_power_line_gpios;
		bool_t hub_powered_on;
	}_; /* Private. */
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof hub_local_t
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 * 		<br>Uses GPIO pins to simulate deployment of artifacts and toggling of power lines.
 * 		The GPIO pins are set low when a power line is off, or a deployment line is <b>not</b>
 * 		deployed. The GPIO pins are then set high when a power line is on, or a deployment
 * 		line is deployed. Note, all GPIOs are set to at initialization. This means all power
 * 		lines are set inactive and deployment is not done yet.
 * @param power_line_gpios[in]
 * 		An array of pointers to GPIOs. The memory pointed to must remain valid in memory for
 * 		the life of the hub_local_t instance. The array must be at least
 * 		<b>HUB_TOTAL_POWER_LINES</b> long.
 * 		<br>Element zero of the array is power line 0.
 * 		<br>Element one of the array is power line 1.
 * 		<br>Pass in <b>NULL</b> if this functionality is not wanted.
 * @param deployment_gpios[in]
 * 		An array of pointers to GPIOs. The memory pointed to must remain valid in memory for
 * 		the life of the hub_local_t instance. The array must be at least
 * 		<b>HUB_TOTAL_DEPLOYMENT_LINES</b> lone.
 * 		<br>Element zero of the array is deployment line 0.
 * 		<br>Element one of the array is deployment line 1.
 * 		<br>Pass in <b>NULL</b> if this functionality is not wanted.
 */
void initialize_hub_local( hub_local_t*, gpio_t** power_line_gpios, gpio_t** deployment_gpios );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_HUB_HUB_LOCAL_H_ */
