/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file startup.h
 * @author Alexander Hamilton
 * @date Aug 10, 2015
 */


#ifndef STARTUP_H_
#define STARTUP_H_
#include <stdint.h>
#include "driver_toolkit.h"
#ifdef LPC1763

#else /*NANOMIND*/

void StartupTest(int * test_status,driver_toolkit_t * driver);
void StartupLog(uint8_t status);
void StartupNewLog(void);

int FSTest(void);

#define STARTLOG_DRIVER_INIT 	1
#define STARTLOG_RELAY_INIT		2
#define STARTLOG_EPS_PING		3
#define STARTLOG_EPS_GET		4
#define STARTLOG_EPS_SET		5
#define STARTLOG_COMM_PING		6
#define STARTLOG_COMM_GET0		7
#define STARTLOG_COMM_GET4		8
#define STARTLOG_COMM_SET		9
#define STARTLOG_ADCS_TEST		10




#endif /*LPC1763*/
#endif /*STARTUP_H_*/
