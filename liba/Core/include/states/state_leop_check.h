/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file state_leop_check.h
 * @author Brendan Bruner
 * @date Oct 21, 2015
 */
#ifndef INCLUDE_STATES_STATE_LEOP_CHECK_H_
#define INCLUDE_STATES_STATE_LEOP_CHECK_H_

#include <states/state.h>
#include <non_volatile/non_volatile_variable.h>
#include <core_defines.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @extends state_t
 * @struct state_leop_check_t
 * @brief
 * 		State determines if leop has been done or not.
 * @details
 * 		State determines if leop has been done or not. Next state is chosen based on
 * 		that decision.
 * @var state_leop_check_t::_super_
 * 		<b>Private</b> Super struct data.
 */
typedef struct state_leop_check_t state_leop_check_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct state_leop_check_t
{
	state_t _super_;
	struct
	{
		non_volatile_variable_t* leop_flag;
	} _; /* Private. */
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof state_leop_check_t
 * @brief
 * 		Constructor
 * @details
 * 		Constructor
 * @param fs[in]
 * 		A pointer to the filesystem_t structure which will be used when reading / writing
 * 		to the state's configuration file.
 * @param leop_flag
 * 		Used to check if leop has finished or not.
 * @returns
 * 		<b>STATE_SUCCESS</b>: Successful initialization.
 * 		<br><b>STATE_FAILURE</b>: Fatal error initializing. The structure is not safe to use.
 */
uint8_t initialize_state_leop_check( state_leop_check_t *state, filesystem_t *fs, non_volatile_variable_t* leop_flag );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/


#endif /* INCLUDE_STATES_STATE_LEOP_CHECK_H_ */
