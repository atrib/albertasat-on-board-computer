/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file state_leop_power_charge.h
 * @author Brendan Bruner
 * @date Sep 14, 2015
 */
#ifndef INCLUDE_STATES_LEOP_STATE_LEOP_POWER_CHARGE_H_
#define INCLUDE_STATES_LEOP_STATE_LEOP_POWER_CHARGE_H_

#include <states/state.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @extends state_t
 * @struct state_leop_power_charge_t
 * @brief
 * 		An idle state for replenishing power reserves.
 * @details
 * 		An idle state for replenishing power reserves.
 * @var state_leop_power_charge_t::_super_
 * 		<b>Private</b> Super struct data.
 */
typedef struct state_leop_power_charge_t state_leop_power_charge_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct state_leop_power_charge_t
{
	state_t _super_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof state_leop_power_charge_t
 * @brief
 * 		Constructor
 * @details
 * 		Constructor
 * @param fs[in]
 * 		A pointer to the filesystem_t structure which will be used when reading / writing
 * 		to the state's configuration file.
 * @returns
 * 		<b>STATE_SUCCESS</b>: Successful initialization.
 * 		<br><b>STATE_FAILURE</b>: Fatal error initializing. The structure is not safe to use.
 */
uint8_t initialize_state_leop_power_charge( state_leop_power_charge_t *state, filesystem_t *fs );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/


#endif /* INCLUDE_STATES_LEOP_STATE_LEOP_POWER_CHARGE_H_ */
