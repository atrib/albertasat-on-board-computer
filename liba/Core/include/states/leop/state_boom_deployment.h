/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file state_boom_deployment.h
 * @author Brendan Bruner
 * @date Sep 14, 2015
 */
#ifndef INCLUDE_STATES_LEOP_STATE_BOOM_DEPLOYMENT_H_
#define INCLUDE_STATES_LEOP_STATE_BOOM_DEPLOYMENT_H_

#include <states/state.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @extends state_t
 * @struct state_boom_deployment_t
 * @brief
 * 		State where mag boom is deployed.
 * @details
 * 		State where mag boom is deployed. Mag data collection is done before, during, and
 * 		after boom deployment.
 * @var state_boom_deployment_t::_super_
 * 		<b>Private</b> Super struct data.
 */
typedef struct state_boom_deployment_t state_boom_deployment_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct state_boom_deployment_t
{
	state_t _super_;
	struct
	{
		bool_t is_deployment_done;
	} _; /* Private */
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof state_boom_deployment_t
 * @brief
 * 		Constructor
 * @details
 * 		Constructor
 * @param fs[in]
 * 		A pointer to the filesystem_t structure which will be used when reading / writing
 * 		to the state's configuration file.
 * @returns
 * 		<b>STATE_SUCCESS</b>: Successful initialization.
 * 		<br><b>STATE_FAILURE</b>: Fatal error initializing. The structure is not safe to use.
 */
uint8_t initialize_state_boom_deployment( state_boom_deployment_t *state, filesystem_t *fs );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/


#endif /* INCLUDE_STATES_LEOP_STATE_BOOM_DEPLOYMENT_H_ */
