/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file command_allocator
 * @author Brendan Bruner
 * @date 2015-01-28
 */
#ifndef INCLUDE_allocator_H_
#define INCLUDE_allocator_H_

#include <stdlib.h>
#include <stdint.h>


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct allocator_t
 * @brief
 * 		Structure used when dynamically allocating data.
 * @details
 * 		Structure used when dynamically allocating data.
 */
typedef struct allocator_t allocator_t;

/**
 * @struct allocator_node_t
 * @brief
 * 		Used by allocator_t structures to manage memory.
 * @details
 * 		Used by allocator_t structures to manage memory.
 */
typedef struct allocator_node_t allocator_node_t;


/********************************************************************************/
/* Structure Define																*/
/********************************************************************************/
struct allocator_t
{
	allocator_node_t *_head;
	allocator_node_t *_tail;
	uint32_t _remaining;
};

struct allocator_node_t
{
	void 				*_memory;
	allocator_node_t 	*_next;
};

/********************************************************************************/
/* Constructor / Destructor														*/
/********************************************************************************/
/**
 * @memberof allocator_t
 * @brief
 * 		Initialize a allocator_t structure.
 *
 * @details
 * 		Initialize an allocator_t structure. This structure
 * 		is then used to dynamically allocate memory without using dynamic memory.
 * 		The allocator can be used to allocate at most <b>size_of_memory</b> elements.
 *
 * 		An example call is:
 * 		@code
 * 			uint32_t 		  max_memory[20];
 * 			allocator_nodes_t memory_nodes[20];
 * 			allocator_t		  allocator;
 *
 * 			initialize_allocator( &allocator, (void *) max_memory, memory_nodes,
 * 								  sizeof( uint32_t ), 20 );
 * 		@endcode
 * 		This allocator can now be used to allocate up to 20 uint32_t. They are allocated
 * 		with alloc_element( ) and freed with free_element( ).
 *
 * @param alloc
 * 		A pointer to the allocator_t structure to initialize.
 *
 * @param memory
 * 		A pointer to an array of the data type which will be allocated. This is used
 * 		as the memory resource when allocating / freeing. For example, if you want
 * 		to use the allocator to allocate uints this would be an array of uints.
 *
 * @param nodes
 * 		A pointer to an array of allocator_node_t structures. These are required
 * 		by the allocator's algorithms. The array must be the length of
 * 		<b>size_of_memory</b>
 *
 * @param size_of_element
 * 		The size in bytes of the elements in the <b>memory</b> array. For example, if
 * 		<b>memory</b> points to an array of uints, this would be sizeof( uint ). A
 * 		char would be one.
 *
 * @param size_of_memory
 * 		The size of the array <b>memory</b> and <b>nodes</b>.
 */
void initialize_allocator(  allocator_t *alloc,
							void *memory,
							allocator_node_t *nodes,
							uint32_t size_of_element,
							uint32_t size_of_memory );

/**
 * @memberof allocator_t
 * @brief
 * 		Destroy a allocator_t structure.
 *
 * @details
 * 		Destroy a allocator_t structure. Always call this when
 * 		finished with a allocator_t structure.
 *
 * @param alloc
 * 		A pointer to the allocator_t structure to destroy.
 */
void destroy_allocator( allocator_t *alloc );


/********************************************************************************/
/* Method Declares																*/
/********************************************************************************/
/**
 * @memberof allocator_t
 * @brief
 * 		Allocate memory
 *
 * @details
 * 		Allocate memory. It will return a pointer to one element. For example,
 * 		if the initialize_allocator( ) method was called with an array of uints
 * 		then this method will return a pointer to one uint.
 *
 * @param alloc
 * 		A pointer to the allocator_t structure being used as the
 * 		memory source.
 *
 * @returns
 * 		A pointer to allocated element.
 *
 * @memberof allocator_t
 */
void *alloc_element( allocator_t *alloc );

/**
 * @memberof allocator_t
 * @brief
 * 		Frees the space allocated by alloc_element( ).
 *
 * @details
 * 		Frees the space allocated by alloc_element( ). This method must
 * 		never be called twice on the same pointer otherwise the allocator will
 * 		become corrupt.
 *
 * @param command
 * 		A pointer to the element to free. This is the same
 * 		pointer returned by alloc_element( ).
 *
 * @param alloc
 * 		A pointer to the allocator_t structure used when the
 * 		element was allocated.
 *
 * @memberof allocator_t
 */
void free_element( allocator_t *alloc, void *element );

/**
 * @memberof allocator_t
 * @brief
 * 		Returns the remaining number of elements which can be allocated.
 * @details
 * 		Returns the remaining number of elements which can be allocated.
 * @param alloc[in]
 * 		The allocator to query the remaining elements of.
 * @returns
 * 		The remaining number of elements which can be allocated.
 */
uint32_t remaining_elements( allocator_t *alloc );

#endif /* INCLUDE_allocator_H_ */
