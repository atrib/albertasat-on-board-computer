/*
 * mnlp_common.c
 *
 *  Created on: 15-Jul-2015
 *      Author: Atri Bhattacharyya
 */

#include "mnlp.h"
#include <string.h>
#include "rtc.h"
#include "driver_toolkit.h"

#ifdef LPC1769
#include "mnlp_lpc.h"
#else
#include "mnlp_nanomind.h"
#include "dev/usart.h"
#endif

/****************************************************************/
/*  Private Function prototypes									*/
/****************************************************************/

void mnlp_rx_data();
void mnlp_start_rx_processing();
void mnlp_exec_cmd(mnlp_seq_cmd_t cmd, uint8_t *param);
void mnlp_task_wait(uint8_t min, uint8_t sec);
void mnlpTask( void *pvParameters);
uint32_t mnlp_gettime();
int mnlp_is_eot(mnlp_times_table_t entry);
void fletcher_checksum_update(uint8_t *buf, uint32_t len);
int fletcher_checksum_correct();
void create_output_filename(char *name, uint8_t rsp_id, uint32_t time);
void create_script_filename(char *name, int script_num);
void create_seq_filename(char *name, int script_num, int seq_num);

/****************************************************************/
/*  mNLP globals												*/
/****************************************************************/

mnlp_script_t script_slot[7];				//Sevel slots for scripts - QB50 specs
filesystem_t filesys;						//File system var.
file_t *data_file;						//File for output of mNLP data
semaphore_t rx_task_control;				//Semaphore used to signal rx task to process new packet from mNLP
task_t cmd_tx_task, pkt_rx_task;			//Task handles for mnlpTask and mnlp_rx_data tasks
volatile uint8_t mnlp_error_detected = 0;	//When mNLP responds with SU_ERR packet, this variable is set
volatile uint8_t mnlp_is_on = 0;			//mNLP status - knows when mNLP is on or off based on mNLP power commands sent
uint8_t run_script = 0;						//Identified currently running script

uint16_t c0_int;							//Variable used to checksum script
uint16_t c1_int;							//Variable used to checksum script
uint16_t xsum_w;							//Variable used to checksum script

//ping-pong buffer for received data
uint8_t *current_buffer;					//current buffer is for incoming data
uint8_t *process_buffer;					//process buffer is for data while its being processed
uint8_t current_buffer_index;				//Keeps track of position in current buffer

char daily_mnlp_data_filename[13] = "Mdaily.bin";


/****************************************************************/
/*  Local defines												*/
/****************************************************************/
#define QB50_EPOCH_OFFSET_FROM_UNIX_EPOCH	946684800			//Offset from unix epoch(adcs) to QB50 epoch(mNLP)

//Units for angular values for ADCS and MNLP data
#define ADCS_ANGLE_UNIT		0.01	//degrees
#define MNLP_ANGLE_UNIT 	2		//degrees
//Units for position values for ADCS and MNLP data
#define ADCS_POSITION_UNIT	0.25	//kilometers
#define MNLP_POSITION_UNIT	5		//kilometers

#define SCRIPT_ENTRY_IS_VALID(x) 	((x.script_index == SCPT_SEQ_FIVE || x.script_index == SCPT_SEQ_FOUR || x.script_index == SCPT_SEQ_THREE || x.script_index == SCPT_SEQ_TWO || x.script_index == SCPT_SEQ_ONE || x.script_index == END_OF_TABLE) && x.time_seconds < 60 && x.time_minutes < 60 && x.time_hours < 60)
#define CMD_HEADER_IS_VALID(x)		(x.delSec < 60 && x.delMin < 60 && 						\
(	x.CMD_ID == OBC_SU_ON 	|| x.CMD_ID == OBC_SU_OFF 	|| x.CMD_ID == SU_RESET 	|| x.CMD_ID == SU_LDP || x.CMD_ID == SU_HC		\
|| 	x.CMD_ID == SU_CAL 		|| x.CMD_ID == SU_SCI		|| x.CMD_ID == SU_HK 		|| x.CMD_ID == SU_STM							\
|| 	x.CMD_ID == SU_DUMP 	|| x.CMD_ID == SU_BIAS_ON 	|| x.CMD_ID == SU_BIAS_OFF 	|| x.CMD_ID == SU_MTEE_ON						\
|| 	x.CMD_ID == SU_MTEE_OFF	|| x.CMD_ID == SU_ERR 		|| x.CMD_ID == OBC_SU_ERR 	|| x.CMD_ID == OBC_EOT))

/****************************************************************/
/*  External variables											*/
/****************************************************************/
extern driver_toolkit_t drivers;


/****************************************************************/
/*  mNLP Public functions										*/
/****************************************************************/

/**
 * @brief
 * 	Initialize and setup function for mNLP
 *
 * @details
 * 	Reads the scripts, stores headers.
 * 	It stores the first times-table entry and points the file pointer to the next entry.
 * 	Stores the script sequences for all scripts in unique files
 * 	Creates the script running task and data storing task before deleting itself
 */
void mnlp_init(void *pvParameters) {

	int i;						//Loop variable
	uint32_t br;				//Bytes read - used multiple times in different reads
	uint32_t bw;				//Bytes written - used multiple times in different writes
	uint8_t num_seq;			//Number of different script sequences
	fs_error_t error;
	file_t *seq_file;		//File handle used to store sequences to different files

	//Initialize rx buffers
	current_buffer = (uint8_t *)pvPortMalloc(DATA_PACKET_SIZE * sizeof(uint8_t));
	process_buffer = NULL;
	current_buffer_index = 0;

	//TODO: Check for filesystem init errors, and uncomment following code if error
	//INITIALIZE_FILESYSTEM(&filesys);
//**************************FILESYSTEM INIT ERROR CODE*************************
//	{
//		//All scripts are erroneous
//		for(i=0;i<7;i++)
//			script_slot[i].status.script_error = 1;
//		INIT_ERROR(FILESYSTEM_NOT_OPENED, "Error mounting file system\n");
//	}
//******************************************************************************

	//Initialize semaphore to control rx packet processing
	new_semaphore(rx_task_control, 1, 0);


	for(i=0;i<7;i++)
	{
		//Fletcher-16 var inits
		c0_int = 0x0000;
		c1_int = 0x0000;
		xsum_w = 0xFFFF;

		//Create filename
		create_script_filename(script_slot[i].script_file_name, i);

		//File opening
		script_slot[i].fp = filesys.open(&filesys, &error, script_slot[i].script_file_name, FS_OPEN_EXISTING, BLOCK_FOREVER);
		if(error != FS_OK)
			INIT_WARNING(i, "Opening file for script %d failed\n", i)
		else
		{
			//If file opened successfully, read the header
			if(script_slot[i].fp->read(script_slot[i].fp, (void *)&script_slot[i].script_hdr, sizeof(mnlp_script_hdr_t), &br) != FS_OK || br != sizeof(mnlp_script_hdr_t))
				INIT_WARNING(i, "Unable to read header for script %d\n", i);
			fletcher_checksum_update((void *)(&script_slot[i].script_hdr), br);
		}
		if(script_slot[i].script_hdr.script_length != script_slot[i].fp->size(script_slot[i].fp, &error))
			INIT_WARNING(i, "Size error for script %d",i)

		/*The following seems to be an error in the mNLP documentation */
		//Discard one byte which is not used
		//f_lseek(&script_slot[i].fp, f_tell(&script_slot[i].fp) + 1);

		//Load first times table entry into script struct
		mnlp_times_table_t tt_entry;
		uint8_t num_tt_entries = 1;
		if(script_slot[i].fp->read(script_slot[i].fp, (void *)&tt_entry, sizeof(mnlp_times_table_t), &br) != FS_OK || br != sizeof(mnlp_times_table_t))
			INIT_WARNING(i, "Error reading first times table entry for script %d\n", i);
		fletcher_checksum_update((uint8_t *)&tt_entry, 1);
		if(mnlp_is_eot(tt_entry))
			INIT_WARNING(i, "Error: first time table entry in script %d is EOT\n", i)
		//Check and parse through remaining times table entries
		else
		{
			script_slot[i].next_seq = tt_entry; 							//First(next) script sequence goes into script struct
			script_slot[i].info.daily_start_time = 3600 * tt_entry.time_hours + 60 * tt_entry.time_minutes + tt_entry.time_seconds;
			do
			{
				fletcher_checksum_update((uint8_t *)&tt_entry + 1, br - 1);
				if(!SCRIPT_ENTRY_IS_VALID(tt_entry))
					INIT_WARNING(i, "Invalid script index in script %d", i);
				if(script_slot[i].fp->read(script_slot[i].fp, (void *)&tt_entry, sizeof(mnlp_times_table_t), &br) != FS_OK || br != sizeof(mnlp_times_table_t))	//Parse over the remaining table
					INIT_WARNING(i, "Error in reading times table for script %d\n", i)
				fletcher_checksum_update((uint8_t *)&tt_entry, 1);
				num_tt_entries++;
			}while(!mnlp_is_eot(tt_entry) && !script_slot[i].status.script_error);
			if(script_slot[i].status.script_error)
				continue;

			script_slot[i].info.num_tt_entries = num_tt_entries;

		    // Rewind 3 bytes - after reading 4 bytes at eot, I will have read 3 extra bytes, hence rewind
			uint32_t position = script_slot[i].fp->position(script_slot[i].fp, &error);
			if(error != FS_OK)
				INIT_WARNING(i, "Error in finding position in script %d\n", i)
			if(script_slot[i].fp->seek(script_slot[i].fp, position - 3) != FS_OK)
				INIT_WARNING(i, "Error in rewinding in script %d\n", i)
		}

		//Store script sequences in different file - s1seq1.bin, s1seq2.bin, s2seq1.bin, etc.
		num_seq = 0;
		mnlp_seq_cmd_t ss_cmd;
		uint8_t *buf;
		uint8_t seq_cnt = 0;	// Each command is associated with a count within the sequence. Can be used for error detection
		char script_seq_filename[13];

		do
		{
			//Read each script sequence and keep storing it in appropriate files
			create_seq_filename(script_seq_filename, i, num_seq);
			seq_file = filesys.open(&filesys, &error, script_seq_filename , FS_CREATE_ALWAYS, BLOCK_FOREVER);
			if(error != FS_OK)
				INIT_WARNING(i, "Unable to open sequence file for script %d sequence %d\n", i, num_seq)
			else
			{
				do
				{
					//Read command header
					if(script_slot[i].fp->read(script_slot[i].fp, (void *)&ss_cmd, sizeof(mnlp_seq_cmd_t), &br) != FS_OK || br != sizeof(mnlp_seq_cmd_t))
						INIT_WARNING(i, "Size error in command sequence for script %d\n", i)
					fletcher_checksum_update((uint8_t *)&ss_cmd, br);
					if(ss_cmd.SEQ_CNT != seq_cnt)
						INIT_WARNING(i, "Error in sequence command count at count %d in sequence %d in script %d\n", seq_cnt, num_seq, i)
					if(!CMD_HEADER_IS_VALID(ss_cmd))
						INIT_WARNING(i, "Error in header parameters in script %d sequence %d", i, num_seq);


					if(seq_file->write(seq_file, (void *)&ss_cmd, sizeof(mnlp_seq_cmd_t), &bw) != FS_OK || bw != sizeof(mnlp_seq_cmd_t))
						INIT_WARNING(i, "File write failed or volume full for script %d sequence %d\n", i, num_seq)
					//If parameters exist
					if(ss_cmd.LEN > 1)
					{
						buf = pvPortMalloc(ss_cmd.LEN - 1);
						//Read parameters
						if(script_slot[i].fp->read(script_slot[i].fp, buf, ss_cmd.LEN - 1, &br) != FS_OK || br != (ss_cmd.LEN - 1))
							INIT_WARNING(i, "File ends before parameters end for script %d\n", i)
						fletcher_checksum_update(buf, br);

						if(seq_file->write(seq_file, buf, ss_cmd.LEN - 1, &bw) != FS_OK || bw != (ss_cmd.LEN - 1))
							INIT_ERROR(VOLUME_FULL, "Parameter write failed or volume full\n")
						vPortFree(buf);
					}
					seq_cnt++;
				}while(ss_cmd.CMD_ID != OBC_EOT && !script_slot[i].status.script_error);
				if(script_slot[i].status.script_error)
					continue;
				filesys.close(&filesys, seq_file);
			}
			num_seq++;

		}while( script_slot[i].fp->position(script_slot[i].fp, &error) != (script_slot[i].script_hdr.script_length - sizeof(script_slot[i].xsum) ) && !script_slot[i].status.script_error); //Continue until end of file minus size of xsum
		if(script_slot[i].status.script_error)
			continue;

		script_slot[i].info.num_seq = num_seq;
		if(script_slot[i].fp->read(script_slot[i].fp,  (void *)&(script_slot[i].xsum), sizeof(script_slot[i].xsum), &br) != FS_OK || br != sizeof(script_slot[i].xsum))
			INIT_WARNING(i, "Unable to read XSUM for script %d\n", i)
		fletcher_checksum_update((uint8_t *)(&script_slot[i].xsum), br);

		if(!fletcher_checksum_correct())
			INIT_WARNING(i, "Checksum error for script %d\n", i);

		//Mark as done for the day
		script_slot[i].status.done_today = 1;

		//Set up file read/write pointer to point to second times table entry
		if(script_slot[i].fp->seek(script_slot[i].fp, sizeof(mnlp_script_hdr_t) + sizeof(mnlp_times_table_t) ) != FS_OK)
			INIT_WARNING(i, "Unable to seek to proper times table position for script %d\n", i)
	}

init_end:
PRINT_DEBUG_STATEMENT("Init finished - generating tasks");
	//Create the task that checks and runs scripts at appropriate times
	create_task(mnlpTask, "mNLP task", 1024, NULL, BASE_PRIORITY + 1, &cmd_tx_task );
	create_task(mnlp_rx_data, "mnlp Rx task", 1024, NULL, BASE_PRIORITY + 2, &pkt_rx_task);	//Processing task should have high priority
	delete_task(NO_HANDLE);

}

/**
 * @brief
 * 	De-initializes the mNLP and all associated tasks
 *
 * @details
 * 	Kills all running mNLP tasks and switches off the mNLP.
 * 	Also clears all allocated memory.
 */
void mnlp_deinit( void *pvParameters)
{
	int i;

	//Delete running mNLP tasks
	delete_task(cmd_tx_task);
	delete_task(pkt_rx_task);

	//Close all open files
	if(filesys.close_all(&filesys) != FS_OK)
		PRINT_DEBUG_STATEMENT("Closing files failed in de-init");

	for(i = 0; i < 7; i++)
	{
		//Set status bits appropriately
		script_slot[i].status.done_today = 0;
		script_slot[i].status.script_error = 0;
	}

	//Switch off mNLP
	mnlp_seq_cmd_t shut_down_cmd;
	shut_down_cmd.CMD_ID = OBC_SU_OFF;
	shut_down_cmd.LEN = 1;
	shut_down_cmd.SEQ_CNT = 0;
	shut_down_cmd.delMin = 0;
	shut_down_cmd.delSec = 0;
	mnlp_exec_cmd(shut_down_cmd, NULL);

	if(process_buffer != NULL)
		vPortFree(process_buffer);
	if(current_buffer != NULL)
		vPortFree(current_buffer);
	process_buffer = NULL;
	current_buffer = NULL;
	current_buffer_index = 0;

	//Delete other resources
	delete_semaphore(rx_task_control);

	PRINT_DEBUG_STATEMENT("mNLP Deinitialized\n");
	//De init done. Now task deletes itself
	delete_task( NO_HANDLE );
}





/****************************************************************/
/*  mNLP Private functions										*/
/****************************************************************/
/**
* @brief
*	Initiates task which processes mNLP response packet
*/
void mnlp_start_rx_processing()
{
	base_t pxHigherPriorityTaskWoken;
	give_semaphore_from_isr(rx_task_control, &pxHigherPriorityTaskWoken);
	yield_task(pxHigherPriorityTaskWoken);
}

/**
* @brief
*	Used to handle mNLP errors
*	Basically switch mNLP off and wait 60 seconds
*/
void handle_mnlp_error()
{
	//Switch off mNLP
	mnlp_seq_cmd_t shut_down_cmd;
	shut_down_cmd.CMD_ID = OBC_SU_OFF;
	shut_down_cmd.LEN = 1;
	shut_down_cmd.SEQ_CNT = 0;
	shut_down_cmd.delMin = 0;
	shut_down_cmd.delSec = 0;
	mnlp_exec_cmd(shut_down_cmd, NULL);

	//Wait 60 seconds
	mnlp_task_wait(1, 0);
}


/**
* @brief
*	Generates OBC error packet according to QB50 specs
*
* @param response_pkt
* 	Response packet to be filled with data
*/
void generate_obc_su_err_pkt(mnlp_response_pkt_t *response_pkt)
{
	int i;
	mnlp_obc_err_pkt_data *err_pkt;

	memset(response_pkt, 0, DATA_PACKET_SIZE);

	response_pkt->rsp_id = OBC_SU_ERR;
	err_pkt = (mnlp_obc_err_pkt_data *) response_pkt->data;
	err_pkt->rsp_err_code = RX_TIMEOUT_ERR;

	//Running script details
	err_pkt->XSUM_R = script_slot[run_script].xsum;
	memcpy(&(err_pkt->hdr_R), &(script_slot[run_script].script_hdr.start_time), sizeof(err_pkt->hdr_R));

	//All script details
	for(i=0;i<7;i++)
	{
		err_pkt->script_info[i].XSUM_n = script_slot[i].xsum;
		memcpy(&(err_pkt->script_info[i].hdr_n), &(script_slot[i].script_hdr.start_time), sizeof(err_pkt->script_info[i].hdr_n));
	}
}


/**
* @brief
*	Computes incremental Fletcher checksum for the next few bytes of the script
*
* @param buf
* 	Pointer to latest bytes read from script file
*
* @param len
* 	Number of bytes read from script file
*/
void fletcher_checksum_update(uint8_t *buf, uint32_t len)
{
	unsigned int i;
	for(i = 0; i < len; i++ )
	{
		c0_int += buf[i];
		c1_int += c0_int;
		c0_int %= 0xFF;
		c1_int %= 0xFF;
	}
}

/**
* @brief
*	Finally checks for checksum correctness for the script
*
* @return
* 	If script has correct checksum returns 1, else 0
*/
int fletcher_checksum_correct()
{
	xsum_w = c0_int + (c1_int << 8);
	return (xsum_w == 0);
}

/**
* @brief
*	Checks whether the last read times-table entry was the end-of-table
*
* @param entry
* 	Times table entry from the mNLP script
*
* @return
* 	If entry is EOT entry, returns 1, else 0
*/
int mnlp_is_eot(mnlp_times_table_t entry)
{
	//EOT entry does not have seconds, minutes, hours fields
	//Therefore, when a times-table entry sized struct is read, the EOT entry goes into the first byte which is the seconds field
	return (entry.time_seconds == END_OF_TABLE);
}


/**
* @brief
* 	Decides if a script is ready to run
*
* @details
* 	Checks current time with start time of script.
* 	Also, if a new day has started, the done-for-today bit is reset
* 	for scripts that have finished running
*
* @return
* 	Returns 1 if script is ready to run today, else 0
*/
int mnlp_script_is_ready()
{
	PRINT_DEBUG_STATEMENT("Checking time\n");
	uint32_t script_time = script_slot[run_script].script_hdr.start_time;
	uint32_t current_time = mnlp_gettime();
	csp_printf("Current: %#x\n", current_time);
	csp_printf("Script: %#x\n", script_time);
	if(current_time > script_time)
	{
		if((current_time % 86400) >= script_slot[run_script].info.daily_start_time)
		{
			if(!script_slot[run_script].status.done_today)
				return 1;
			else
				return 0;
		}
		else if(script_slot[run_script].status.done_today)
			script_slot[run_script].status.done_today = 0;
	}
	return 0;
}

/**
* @brief
* 	Decides if the next script-sequence is ready to run
*
* @details
* 	Checks next script sequence's start time. If that time has passed, the sequence is ready to run
*
* @return
* 	Returns 1 if script sequence is ready to run, else 0
*/
int mnlp_seq_is_ready()
{
	mnlp_times_table_t next_sequence = script_slot[run_script].next_seq;
	uint32_t start_time = 3600 * next_sequence.time_hours + 60 * next_sequence.time_minutes + next_sequence.time_seconds;
	if((mnlp_gettime() % 86400) >= start_time)
		return 1;
	else
		return 0;
}

/**
* @brief
* 	Task delay for the mNLP
*
* @param min
* 	Minutes to wait
*
* @param sec
* 	Seconds to wait
*/
void mnlp_task_wait(uint8_t min, uint8_t sec)
{
	task_delay(min*ONE_MINUTE + sec*ONE_SECOND);
}



/**
* @brief
* 	Process and store received data packets from mNLP
*
* @details
* 	This task processes data packets received from the mNLP
* 	It must add the relevant headers from ADCS and time data
* 	and save packets to date-tagged files on mass memory.
*/
void mnlp_rx_data()
{
	uint32_t bw;									//Bytes written
	mnlp_science_hdr_t data_hdr;					//Data packet header - to be attached before all packets
	uint8_t file_error = 0;							//File handling error flag
	fs_error_t status;								//Return status for file handling operations
	mnlp_response_pkt_t *response_pkt;				//Packet containing received data
	uint8_t res_seq_cnt[9] = {0,0,0,0,0,0,0,0,0};	//Sequence count for each type of packet
	char data_file_name[13];						//File name for data write to file
	uint32_t current_date = 0;

	while(1)
	{
		//Check if mNLP is on
		while(mnlp_is_on)
		{
			//Wait on semaphore
			if(take_semaphore(rx_task_control, 400*ONE_SECOND) == pdFALSE)
			{
				//Generate OBC_error packet
				response_pkt = (mnlp_response_pkt_t *)pvPortMalloc(sizeof(mnlp_response_pkt_t));

				//Put error data
				generate_obc_su_err_pkt(response_pkt);
			}
			else
			{
				if(process_buffer == NULL)		//Basic error check
				{
					PRINT_DEBUG_STATEMENT("mNLP processing task wrongly created\n");
					continue;
				}
				response_pkt = (mnlp_response_pkt_t *)process_buffer;
				process_buffer = NULL;
			}

			switch(response_pkt->rsp_id)
			{
				case SU_LDP_PKT	:	response_pkt->seq_cnt = res_seq_cnt[0]++;	break;
				case SU_HC_PKT	:	response_pkt->seq_cnt = res_seq_cnt[1]++;	break;
				case SU_CAL_PKT	:	response_pkt->seq_cnt = res_seq_cnt[2]++;	break;
				case SU_SCI_PKT	:	response_pkt->seq_cnt = res_seq_cnt[3]++;	break;
				case SU_HK_PKT	:	response_pkt->seq_cnt = res_seq_cnt[4]++;	break;
				case SU_STM_PKT	:	response_pkt->seq_cnt = res_seq_cnt[5]++;	break;
				case SU_DUMP_PKT:	response_pkt->seq_cnt = res_seq_cnt[6]++;	break;
				case SU_ERR_PKT	:	response_pkt->seq_cnt = res_seq_cnt[7]++;	break;
				case OBC_SU_ERR	:	response_pkt->seq_cnt = res_seq_cnt[8]++;	break;
				default			:	PRINT_DEBUG_STATEMENT("Illegal response ID\n");
			}

			PRINT_DEBUG_STATEMENT("Data packet logged with RSP_ID %d and sequence no  %d\n", response_pkt->rsp_id, response_pkt->seq_cnt);


			drivers.adcs.getframe_adcs_state(&drivers.adcs);
			data_hdr.utc = drivers.adcs.adcs_state.current_unix_time - QB50_EPOCH_OFFSET_FROM_UNIX_EPOCH;
			data_hdr.roll = drivers.adcs.adcs_state.estimated_roll_angle * (ADCS_ANGLE_UNIT / MNLP_ANGLE_UNIT);
			data_hdr.pitch = drivers.adcs.adcs_state.estimated_pitch_angle * (ADCS_ANGLE_UNIT / MNLP_ANGLE_UNIT);
			data_hdr.yaw = drivers.adcs.adcs_state.estimated_yaw_angle * (ADCS_ANGLE_UNIT / MNLP_ANGLE_UNIT);
			data_hdr.rolldot = drivers.adcs.adcs_state.estimated_X_angular_rate;
			data_hdr.pitchdot = drivers.adcs.adcs_state.estimated_Y_angular_rate;
			data_hdr.yawdot = drivers.adcs.adcs_state.estimated_Z_angular_rate;
			data_hdr.x_eci = drivers.adcs.adcs_state.X_position * (ADCS_POSITION_UNIT / MNLP_POSITION_UNIT);
			data_hdr.y_eci = drivers.adcs.adcs_state.Y_position * (ADCS_POSITION_UNIT / MNLP_POSITION_UNIT);
			data_hdr.z_eci = drivers.adcs.adcs_state.Z_position * (ADCS_POSITION_UNIT / MNLP_POSITION_UNIT);

			//Write to daily data file
			if(current_date == data_hdr.utc / 86400)
			{
				//Same day, append file
				data_file = filesys.open(&filesys, &status, daily_mnlp_data_filename, FS_OPEN_ALWAYS, ONE_SECOND);
			}
			else
			{
				//New day, overwrite file
				current_date = data_hdr.utc / 86400;
				data_file = filesys.open(&filesys, &status, daily_mnlp_data_filename, FS_CREATE_ALWAYS, ONE_SECOND);
			}
			if(status != FS_OK)
			{
				PRINT_DEBUG_STATEMENT("Opening daily data output file failed\n");
			}
			else
			{
				//Move to end of file to append, doesn't matter if file is new
				uint32_t size = data_file->size(data_file, &status);
				if(data_file->seek(data_file, size) != FS_OK)
				{	PRINT_DEBUG_STATEMENT("Output file seek read failed\n"); file_error = 1;}
				else
				{
					//Create packet and store to SD card
					if(data_file->write(data_file, (void *)&data_hdr, sizeof(mnlp_science_hdr_t), &bw) != FS_OK || bw != sizeof(mnlp_science_hdr_t))
					{	PRINT_DEBUG_STATEMENT("Data header write failed\n"); 	file_error = 1;	}
					if(data_file->write(data_file, (void *)response_pkt, DATA_PACKET_SIZE, &bw) != FS_OK || bw != DATA_PACKET_SIZE)
					{	PRINT_DEBUG_STATEMENT("Packet write failed\n"); 	file_error = 1;	}
					if(data_file->write(data_file, (void *)"\n", 1, &bw) != FS_OK || bw != 1)
					{	PRINT_DEBUG_STATEMENT("Newline write failed\n"); 	file_error = 1;	}
					if(!file_error)
						PRINT_DEBUG_STATEMENT("Daily data write successful\n");
				}
				filesys.close(&filesys, data_file);
			}

			//Write to date-stamped data file
			create_output_filename(data_file_name, response_pkt->rsp_id, data_hdr.utc);
			data_file = filesys.open(&filesys, &status, data_file_name, FS_OPEN_ALWAYS, ONE_SECOND);
			if(status != FS_OK)
			{
				PRINT_DEBUG_STATEMENT("Opening data output file failed\n");
			}
			else
			{
				//Move to end of file to append
				uint32_t size = data_file->size(data_file, &status);
				if(data_file->seek(data_file, size) != FS_OK)
				{	PRINT_DEBUG_STATEMENT("Output file seek read failed\n"); file_error = 1;}
				else
				{
					//Create packet and store to SD card
					if(data_file->write(data_file, (void *)&data_hdr, sizeof(mnlp_science_hdr_t), &bw) != FS_OK || bw != sizeof(mnlp_science_hdr_t))
					{	PRINT_DEBUG_STATEMENT("Data header write failed\n"); 	file_error = 1;	}
					if(data_file->write(data_file, (void *)response_pkt, DATA_PACKET_SIZE, &bw) != FS_OK || bw != DATA_PACKET_SIZE)
					{	PRINT_DEBUG_STATEMENT("Packet write failed\n"); 	file_error = 1;	}
					if(data_file->write(data_file, (void *)"\n", 1, &bw) != FS_OK || bw != 1)
					{	PRINT_DEBUG_STATEMENT("Newline write failed\n"); 	file_error = 1;	}
					if(!file_error)
						PRINT_DEBUG_STATEMENT("Data write successful\n");
				}
				filesys.close(&filesys, data_file);
			}

			if(response_pkt->rsp_id == SU_ERR_PKT || response_pkt->rsp_id == OBC_SU_ERR)
			{
				mnlp_error_detected = 1;
				PRINT_DEBUG_STATEMENT("ERROR PACKET LOGGED\n");
				//Error handling
				handle_mnlp_error();
			}

			//free allocated memory
			vPortFree(response_pkt);
			response_pkt = NULL;
		}
		//Wait a sec
		mnlp_task_wait(0,1);
	}

}

/**
* @brief
*	Executes a command
*
* @details
* 	Sends SU commands to the mNLP via UART and
* 	processes OBC commands
*
* @param cmd
* 	Command to be executed
*
* @param param
* 	Parameters for the command
*/
void mnlp_exec_cmd(mnlp_seq_cmd_t cmd, uint8_t *param)
{
	PRINT_DEBUG_STATEMENT("Execute command %d\n", cmd.CMD_ID);

	//Update mNLP power status if command is an mNLP power command
	if(cmd.CMD_ID == OBC_SU_ON)
		mnlp_is_on = 1;
	else if(cmd.CMD_ID == OBC_SU_OFF)
		mnlp_is_on = 0;

	//If appropriate, send command to mNLP
#ifdef DEBUG
	if(cmd.CMD_ID == OBC_SU_ON || cmd.CMD_ID == OBC_SU_OFF || ((cmd.CMD_ID & 0xF0) != 0xF0))
	{
		//Send command to mNLP simulator
		USART_PUTSTR(&(cmd.CMD_ID), 3);		//Write 3 bytes - CMD_ID, LEN, SEQ_CNT
		USART_PUTSTR(param, cmd.LEN - 1);	//Write remaining parameters
	}
	else
	{
		//TODO: Process command on OBC - NanoPower switching on or off. Should not send this over USART.
	}
#else
	if((cmd.CMD_ID & 0xF0) != 0xF0)
	{
		//Send command to mNLP
		USART_PUTSTR(&(cmd.CMD_ID), 3);	//Write 3 bytes - CMD_ID, LEN, SEQ_CNT
		USART_PUTSTR(param, cmd.LEN - 1);	//Write remaining parameters
	}
	else
	{
		//TODO: Process command on OBC - NanoPower switching on or off. Should not send this over USART.
	}
#endif
}


/**
 * @brief
 *	This task executes the scripts
 *
 * @details
 * 	This task iterates through scripts and script sequences
 * 	When ready, it starts script sequences, and sends the commands
 * 	to be appropriately executed.
 */
void mnlpTask( void *pvParameters)
{
	PRINT_DEBUG_STATEMENT("MNLP task start\n");

	uint32_t br;					//Used to count bytes read
	uint8_t seq_num;				//Identifies sequence running
	mnlp_seq_cmd_t cmd;				//Current command to be run
	fs_error_t status;				//Common var to store error returns
	file_t *run_seq_file;		//File handle for sequence currently running
	char script_seq_filename[13];
	do
	{
		if(run_script >= 7)
			run_script = 0;

		//1. Check next script UTC time for start
		if(!script_slot[run_script].status.script_error && mnlp_script_is_ready(run_script))
		{
			PRINT_DEBUG_STATEMENT("Stript start\n");
			//2. Check current times table for readiness
			if(mnlp_seq_is_ready(run_script))
			{
				switch(script_slot[run_script].next_seq.script_index)
				{
					PRINT_DEBUG_STATEMENT("Sequence select\n");
					case SCPT_SEQ_ONE 	:	seq_num = 0 ; break;
					case SCPT_SEQ_TWO 	:	seq_num = 1 ; break;
					case SCPT_SEQ_THREE :	seq_num = 2 ; break;
					case SCPT_SEQ_FOUR 	:	seq_num = 3 ; break;
					case SCPT_SEQ_FIVE 	:	seq_num = 4 ; break;
					default: PRINT_DEBUG_STATEMENT("Invalid script index detected at runtime\n"); continue;
				}

				create_seq_filename(script_seq_filename, run_script, seq_num);
				run_seq_file = filesys.open(&filesys, &status, script_seq_filename, FS_OPEN_EXISTING, BLOCK_FOREVER);
				if(status != FS_OK)
				{	PRINT_DEBUG_STATEMENT("Unable to open commands file for script %d sequence %d\n", run_script, seq_num);	continue;}
				PRINT_DEBUG_STATEMENT("Running script %d sequence S%d\n", run_script, seq_num + 1);
				do
				{
					uint8_t *buf = NULL;

					//3. Load next command header
					if(run_seq_file->read(run_seq_file, (void *)&cmd, sizeof(mnlp_seq_cmd_t), &br) != FS_OK || br != sizeof(mnlp_seq_cmd_t))
						PRINT_DEBUG_STATEMENT("Error reading command header from script %d sequence %d at runtime\n", run_script, seq_num);

					//4. Load next command parameters
					if(cmd.LEN > 1)
					{
						buf = pvPortMalloc(cmd.LEN - 1);
						//Read parameters
						if(run_seq_file->read(run_seq_file, (void *)buf, cmd.LEN - 1, &br) != FS_OK || br != (cmd.LEN - 1))
							PRINT_DEBUG_STATEMENT("Error reading parameters from script %d sequence %d at runtime\n", run_script, seq_num);
					}

					//5. Wait and execute command
					mnlp_task_wait(cmd.delMin, cmd.delSec);
					//If mNLP returns error packet, then break out of current script sequence execution
					if(mnlp_error_detected)
					{
						//Wait a minute, to allow mnlp reset
						mnlp_task_wait(1,0);
						mnlp_error_detected = 0;
						break;
					}
					mnlp_exec_cmd(cmd, buf);

					//6. Deallocate command memory
					if(cmd.LEN > 1)
						vPortFree(buf);

				}while(cmd.CMD_ID !=  OBC_EOT); 	//7. while command is not OBC_EOT */

				filesys.close(&filesys, run_seq_file);

				//8. Load next times table sequence entry
				//Assumption is that the file read/write pointer points to
				//the beginning of the next entry already. I shall try to maintain this
				if(script_slot[run_script].fp->read(script_slot[run_script].fp, (void *)&(script_slot[run_script].next_seq), sizeof(mnlp_times_table_t), &br) != FS_OK || br != sizeof(mnlp_times_table_t))
					PRINT_DEBUG_STATEMENT("Error reading first times table sequence - file too small\n");

				if(mnlp_is_eot(script_slot[run_script].next_seq))	//9. If next sequence is EOT
				{
					//10. Mark script as done for today
					script_slot[run_script].status.done_today = 1;
					//11. Load first times table sequence
					if(script_slot[run_script].fp->seek(script_slot[run_script].fp, sizeof(mnlp_script_hdr_t)))
						PRINT_DEBUG_STATEMENT("Error seeking to first times table entry for script %d", run_script);
					if(script_slot[run_script].fp->read(script_slot[run_script].fp, (void *)&(script_slot[run_script].next_seq), sizeof(mnlp_times_table_t), &br) != FS_OK || br != sizeof(mnlp_times_table_t))
						PRINT_DEBUG_STATEMENT("Error reading first times table sequence for script %d - file too small\n", run_script);
				}

			}

		}
		//PRINT_DEBUG_STATEMENT("MNLP task finish\n");
		task_delay(500 * DELAY_ONE_MS);
	}while(++run_script);
}

/**
 * @brief
 * 	Get current time
 *
 * @return
 * 	Seconds since QB50 epoch
 */
uint32_t mnlp_gettime()
{
	drivers.adcs.getframe_current_time(&drivers.adcs);
	return (drivers.adcs.current_time.unix_time - QB50_EPOCH_OFFSET_FROM_UNIX_EPOCH);
}

/**
 * @brief
 * 	USART callback when data has been received from mNLP
 *
 * @details
 * 	Puts characters into appropriate buffers of correct length and sends them to process task to be processed.
 */
void mnlp_usart_callback(uint8_t * buf, int len, void * pxTaskWoken)
{
	int i;
	for(i=0;i<len;i++)
	{
		if(current_buffer_index != DATA_PACKET_SIZE)
		{
			//Read into current buffer
			current_buffer[current_buffer_index++] = buf[i];
		}
		//If buffer is full
		if(current_buffer_index == DATA_PACKET_SIZE)
		{
			if(process_buffer != NULL)		//If processing buffer is not null, then wait. This case is pretty unlikely
			{
				/*	We shall delay reading the next character
				 *  This is in the hope that until the next IRQ, the processing task is able to complete
				 *  The current IRQ exits immediately after this function returns
				 *  If there were characters left to be read in the FIFO, we will get another CTI interrupt or RDA interrupt soon
				 *  If processing task takes a long time, we may have data loss.
				 */
				return;
			}
			else							//If no buffer is currently being processed
			{
				//switch buffers and send current buffer for processing
				process_buffer = current_buffer;
				current_buffer = (uint8_t *)pvPortMalloc(DATA_PACKET_SIZE * sizeof(uint8_t));
				current_buffer_index = 0;
				mnlp_start_rx_processing();
			}

		}
	}
}

/**
 * @brief
 * 	Checks if a year is a leap year
 *
 * @param year
 * 	Year to be checked
 */
inline int isleap(int year)
{
	return ((year % 4 == 0) && (year % 100 != 0 || year % 400 == 0));
}

/**
 * @brief
 * 	Create the filename string for output file
 *
 * @param name
 * 	Pointer to char array to write filename to.
 *
 * @param rsp_id
 * 	Response ID of packet to be written
 *
 * @param time
 * 	Current QB50 epoch time
 */
void create_output_filename(char *name, uint8_t rsp_id, uint32_t time)
{
	int year, month, days;
	int days_in_month[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	int days_in_month_leap[12] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	days = time / 86400;		//Remove seconds

	//Extract year, month, date from time
	year = 2000;
	while(( days -= (isleap(year)?366:365)) > 0)
		year++;
	days += (isleap(year))?366:365;
	month = 1;
	if(isleap(year))
	{
		while((days -= days_in_month_leap[month - 1]) > 0)
			month++;
		days += days_in_month_leap[month - 1];
	}
	else
	{
		while((days -= days_in_month[month - 1]) > 0)
			month++;
		days += days_in_month[month - 1];
	}

	//Create file name
	switch(rsp_id)
	{
		case SU_LDP_PKT	:
		case SU_HC_PKT	:
		case SU_CAL_PKT	:
		case SU_HK_PKT	:
		case SU_STM_PKT	:	sprintf(name, "M%02d%02d%02dH.bin", year % 100, month, days);	break;
		case SU_SCI_PKT	:
		case SU_DUMP_PKT:	sprintf(name, "M%02d%02d%02dS.bin", year % 100, month, days);	break;
		case SU_ERR_PKT	:
		case OBC_SU_ERR	:	sprintf(name, "M%02d%02d%02dE.bin", year % 100, month, days);	break;
		default			:	sprintf(name, "M%02d%02d%02dD.bin", year % 100, month, days);	PRINT_DEBUG_STATEMENT("Illegal response ID\n");
	}
}

/**
 * @brief
 * 	Create the filename string for script file
 *
 * @param name
 * 	Pointer to char array to write filename to.
 *
 * @param script_num
 * 	Script index
 */
void create_script_filename(char *name, int script_num)
{
	if(script_num < 0 || script_num > 6)
	{
		PRINT_DEBUG_STATEMENT("Illegal script number in create filename function");
		return;
	}
	sprintf(name, "MNLP_%d.bin", script_num);
}

/**
 * @brief
 * 	Create the filename string for script sequence file
 *
 * @param name
 * 	Pointer to char array to write filename to.
 *
 * @param script_num
 * 	Script index
 *
 * @param seq_num
 * 	Sequence index
 */
void create_seq_filename(char *name, int script_num, int seq_num)
{
	if(script_num < 0 || script_num > 6)
	{
		PRINT_DEBUG_STATEMENT("Illegal script number in create filename function");
		return;
	}
	if(seq_num < 0 || seq_num > 4)
	{
		PRINT_DEBUG_STATEMENT("Illegal sequence number in create filename function");
		return;
	}
	sprintf(name, "s%dseq%d.bin", script_num, seq_num);
}

/****************************************************************/
/*  End of mNLP driver											*/
/****************************************************************/

