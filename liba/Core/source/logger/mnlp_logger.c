/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file wod_logger.c
 * @author Brendan Bruner
 * @date May 25, 2015
 */
#include <packets/telemetry_packet.h>
#include <logger/mnlp_logger.h>
#include <core_defines.h>


/********************************************************************************/
/* Defines																		*/
/********************************************************************************/


/********************************************************************************/
/* Private Singleton Variables													*/
/********************************************************************************/
static char	const	*delete_buffer = MNLP_LOGGER_DELETE_BUFFER;
static char			*name_space = MNLP_LOGGER_PACKET_NAMES;
static char	const	*master_tbl = MNLP_LOGGER_MASTER_TABLE;


/********************************************************************************/
/* Virtual Methods																*/
/********************************************************************************/
/*
 * Increment name like so:
 * C0000000.txt
 * C0000001.txt
 * C0000002.txt
 * ...
 * C0000041.txt
 * ...
 * C9999999.txt
 */
static size_t _next_name( logger_t *logger, char *name )
{
	DEV_ASSERT( logger );
	DEV_ASSERT( name );

	int32_t iter1, iter2;
	for( iter1 = MNLP_LOGGER_INDEX_END; iter1 >= MNLP_LOGGER_INDEX_BEGIN; --iter1 )
	{
		if( name[iter1] != '9' )
		{
			++name[iter1];
			for( iter2 = iter1+1; iter2 <= MNLP_LOGGER_INDEX_END; ++iter2 )
			{
				name[iter2] = '0';
			}
			break;
		}
	}

	return MNLP_LOGGER_NAME_LENGTH;
}


/********************************************************************************/
/* Initialization Method														*/
/********************************************************************************/
logger_error_t initialize_mnlp_logger( mnlp_logger_t *logger, filesystem_t *fs )
{
	DEV_ASSERT( logger );
	DEV_ASSERT( fs );

	logger_error_t init_err;

	init_err = _initialize_logger( (logger_t *) logger, fs, delete_buffer,
											master_tbl, name_space );
	((logger_t *) logger)->__.next_name = _next_name;

	return init_err;
}

