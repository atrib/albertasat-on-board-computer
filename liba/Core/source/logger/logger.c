/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file logger_base.c
 * @author Brendan Bruner
 * @date May 15, 2015
 */

#include <filesystems/fs_handle.h>
#include <logger/logger.h>
#include <string.h>


/********************************************************************************/
/* Defines																		*/
/********************************************************************************/
#define CHECKSUM_OK			0
#define SEARCH_RETRY_LIMIT	150
#define QUEUE_START			1
#define CHECKSUM_SIZE		1
#define CHECKSUM_OFFSET		0
#define QUEUE_BOTTOM_OFFSET	CHECKSUM_SIZE
#define QUEUE_TOP_OFFSET	QUEUE_BOTTOM_OFFSET + LOGGER_MAX_FILE_NAME_LENGTH + 1
#define QUEUE_SIZE			CHECKSUM_SIZE + (LOGGER_MAX_FILE_NAME_LENGTH + 1)*2


/* Asserts any file operation and closes all logger files upon failure. */
#define ASSERT_FS_OP( logger, err, file ) \
										do {																				\
											if( err != FS_OK ) {															\
												logger->_fs_->close( logger->_fs_, file );									\
												unlock_mutex( logger->_sync_mutex_ );										\
												return LOGGER_FS_ERR;														\
											} 																				\
										} while ( 0 )

/* Return error code 'err'. */
#define EXIT_WITH_ERR( logger, err )	do {																			\
											unlock_mutex( logger->_sync_mutex_ );										\
											return err;																	\
										} while ( 0 )


/* Asserts the filesystem did not run out of memory. */
/* 'should' is how should have been written to file. */
/* 'did' is how much was actually written to file. */
#define ASSERT_NV_MEMORY( logger, should, did, file ) \
													do {																\
														if( should != did )												\
														{																\
															logger->_fs_->close( logger->_fs_, file );					\
															unlock_mutex( logger->_sync_mutex_ );						\
															return LOGGER_FULL;											\
														}																\
													} while( 0 )


/********************************************************************************/
/* Singletons																	*/
/********************************************************************************/
static mutex_t logger_sync_mutex;
static bool_t logger_is_init = false;

/********************************************************************************/
/* Private Method Definitions													*/
/********************************************************************************/
static uint32_t logger_queue_get_checksum( logger_t *self, uint8_t* data, uint32_t length )
{
	DEV_ASSERT( self );
	DEV_ASSERT( data );

	uint32_t iter;
	uint32_t checksum;

	checksum = 0;
	for( iter = 0; iter < length; ++iter )
	{
		checksum ^= data[iter];
	}

	return checksum;
}
/**
 * @memberof logger_t
 * @private
 * @brief
 * 		Compute parity byte checksum of input file.
 * @details
 * 		Compute parity byte checksum of input file. Be aware, a seek operation
 * 		is performed on the input file.
 */
static fs_error_t logger_queue_evaluate_checksum( logger_t* self, file_t* queue_handle )
{
	DEV_ASSERT( self );
	DEV_ASSERT( queue_handle );

	uint32_t	bytes_used;
	uint8_t		queue_data[QUEUE_SIZE];
	uint8_t 	checksum;
	fs_error_t	fs_err;

	/* Seek to the beginning of the queue. */
	fs_err = queue_handle->seek( queue_handle, QUEUE_START );
	if( fs_err != FS_OK )
	{
		return fs_err;
	}

	/* Get the contents of the queue to perform the checksum. */
	fs_err = queue_handle->read( queue_handle, queue_data, QUEUE_SIZE, &bytes_used );
	if( fs_err != FS_OK )
	{
		return fs_err;
	}

	/* Perform the checksum. */
	checksum = logger_queue_get_checksum( self, queue_data, bytes_used );

	/* Write the checksum back. */
	fs_err = queue_handle->seek( queue_handle, CHECKSUM_OFFSET );
	if( fs_err != FS_OK )
	{
		return fs_err;
	}
	fs_err = queue_handle->write( queue_handle, (uint8_t*) &checksum, sizeof(checksum), &bytes_used );
	return fs_err;
}

/**
 * @memberof logger_t
 * @private
 * @brief
 * 		Creates a new queue.
 * @details
 * 		Creates a new queue.
 * @returns
 * 		Error code.
 */
static logger_error_t logger_create_new_queue( logger_t *self )
{
	DEV_ASSERT( self );

	file_t 		*queue_handle;
	file_t*		packet_handle;
	fs_error_t	fs_err;
	uint8_t		queue_contents[QUEUE_SIZE];
	uint32_t	written;

	/* Open queue. */
	queue_handle = self->_.fs->open( self->_.fs, &fs_err, self->_.queue_file_name, FS_CREATE_ALWAYS, BLOCK_FOREVER );
	if( fs_err != FS_OK )
	{
		return LOGGER_NVMEM_ERR;
	}

	/* Construct contents of table. */
	strncpy( (char *) (queue_contents + QUEUE_BOTTOM_OFFSET), self->_.first_packet_name, LOGGER_MAX_FILE_NAME_LENGTH );
	queue_contents[QUEUE_BOTTOM_OFFSET + LOGGER_MAX_FILE_NAME_LENGTH] = '\0'; /* Fail safe. */

	strncpy( (char *) (queue_contents + QUEUE_TOP_OFFSET), self->_.first_packet_name, LOGGER_MAX_FILE_NAME_LENGTH );
	queue_contents[QUEUE_TOP_OFFSET + LOGGER_MAX_FILE_NAME_LENGTH] = '\0'; /* Fail safe. */

	/* Write table into memory. */
	fs_err = queue_handle->write( queue_handle, queue_contents, QUEUE_SIZE, &written );
	if( fs_err != FS_OK )
	{
		return LOGGER_NVMEM_ERR;
	}
	logger_queue_evaluate_checksum( self, queue_handle );
	queue_handle->close( queue_handle );

	/* Call the first packet hook on this newly created first packet. */
	packet_handle = self->_.fs->open( self->_.fs, &fs_err, self->_.first_packet_name, FS_CREATE_ALWAYS, BLOCK_FOREVER );
	if( fs_err != FS_OK )
	{
		return LOGGER_NVMEM_ERR;
	}
	fs_err = self->__.new_packet_hook( self, packet_handle );
	packet_handle->close( packet_handle );
	if( fs_err != FS_OK )
	{
		return LOGGER_HOOK_ERR;
	}

	return LOGGER_RECOVERED;
}

/**
 * @memberof logger_t
 * @private
 * @brief
 * 		Attempts to restore a corrupt queue.
 * @details
 * 		Attempts to restore a corrupt queue. If it cannot be
 * 		restored, then a new queue is created.
 * @returns
 * 		Error code.
 */
static logger_error_t logger_restore_queue( logger_t *self )
{
	DEV_ASSERT( self );

	filesystem_t 	*fs;
	fs_error_t		fs_err;
	file_t			*queue_handle;
	char			oldest_packet_file_name[LOGGER_MAX_FILE_NAME_LENGTH+1];
	char			newest_packet_file_name[LOGGER_MAX_FILE_NAME_LENGTH+1];
	char			previous_packet_file_name[LOGGER_MAX_FILE_NAME_LENGTH+1];
	uint8_t			queue_contents[QUEUE_SIZE];
	uint32_t		time_out;
	uint32_t		written;

	fs = self->_.fs;
	time_out = 0;

	strncpy( oldest_packet_file_name, self->_.first_packet_name, LOGGER_MAX_FILE_NAME_LENGTH );
	oldest_packet_file_name[LOGGER_MAX_FILE_NAME_LENGTH] = '\0';

	/* Start by looking for the oldest name. */
	for( time_out = 0; time_out < SEARCH_RETRY_LIMIT; ++time_out )
	{
		/* Check for existance of the file name. */
		fs_err = fs->file_exists( fs, oldest_packet_file_name );
		if( fs_err == FS_OK || fs_err != FS_NO_FILE ){ break; } /* Found oldest file or file system error. */

		/* Premptively retrieve the next file name. */
		self->__.next_name( self, oldest_packet_file_name );
		oldest_packet_file_name[LOGGER_MAX_FILE_NAME_LENGTH] = '\0';
	}
	if( time_out >= SEARCH_RETRY_LIMIT )
	{
		/* Timed out. */
		return logger_create_new_queue( self );
	}
	if( fs_err != FS_OK )
	{
		/* Encountered a filesystem error. */
		return LOGGER_NVMEM_ERR;
	}

	/* Got the name of the oldest packet file in existence. */
	/* Start looking for the newest packet file in existence. */
	time_out = 0;
	strncpy( newest_packet_file_name, oldest_packet_file_name, LOGGER_MAX_FILE_NAME_LENGTH );
	newest_packet_file_name[LOGGER_MAX_FILE_NAME_LENGTH] = '\0';
	strncpy( previous_packet_file_name, oldest_packet_file_name, LOGGER_MAX_FILE_NAME_LENGTH );
	previous_packet_file_name[LOGGER_MAX_FILE_NAME_LENGTH] = '\0';

	/* Look for the newest packet file name. */
	/* FIXME: This algorithm does not account for gaps in names. */
	do
	{
		/* Increment time out. */
		++time_out;

		/* Check for existance of the file name. */
		fs_err = fs->file_exists( fs, newest_packet_file_name );

		/* Premptively retrieve the next file name. */
		strncpy( previous_packet_file_name, newest_packet_file_name, LOGGER_MAX_FILE_NAME_LENGTH );
		previous_packet_file_name[LOGGER_MAX_FILE_NAME_LENGTH] = '\0';
		self->__.next_name( self, newest_packet_file_name );
		newest_packet_file_name[LOGGER_MAX_FILE_NAME_LENGTH] = '\0';
	}
	while( fs_err == FS_OK && time_out < SEARCH_RETRY_LIMIT );
	if( time_out >= SEARCH_RETRY_LIMIT )
	{
		/* Timed out. */
		return logger_create_new_queue( self );
	}
	if( fs_err != FS_NO_FILE )
	{
		/* File system error. */
		return LOGGER_NVMEM_ERR;
	}

	/* Got the oldest and newest packet file names now. */
	/* Begin restoring the queue. */
	queue_handle = fs->open( fs, &fs_err, self->_.queue_file_name, FS_CREATE_ALWAYS, BLOCK_FOREVER );
	if( fs_err != FS_OK )
	{
		return LOGGER_NVMEM_ERR;
	}

	/* Construct queue. */
	strncpy( (char *) (queue_contents + QUEUE_BOTTOM_OFFSET), oldest_packet_file_name, LOGGER_MAX_FILE_NAME_LENGTH );
	queue_contents[QUEUE_BOTTOM_OFFSET + LOGGER_MAX_FILE_NAME_LENGTH] = '\0'; /* Fail safe. */
	strncpy( (char *) (queue_contents + QUEUE_TOP_OFFSET), previous_packet_file_name, LOGGER_MAX_FILE_NAME_LENGTH );
	queue_contents[QUEUE_TOP_OFFSET + LOGGER_MAX_FILE_NAME_LENGTH] = '\0'; /* Fail safe. */
	strncpy( self->_.packet_name, previous_packet_file_name, LOGGER_MAX_FILE_NAME_LENGTH );
	self->_.packet_name[LOGGER_MAX_FILE_NAME_LENGTH] = '\0'; /* Fail safe. */

	/* Write table into memory. */
	fs_err = queue_handle->write( queue_handle, queue_contents, QUEUE_SIZE, &written );
	if( fs_err != FS_OK )
	{
		queue_handle->close( queue_handle );
		return LOGGER_NVMEM_ERR;
	}
	logger_queue_evaluate_checksum( self, queue_handle );
	queue_handle->close( queue_handle );
	return LOGGER_OK;
}

/**
 * @memberof logger_t
 * @private
 * @brief
 * 		Checks for and fixes corruption in the queue.
 * @details
 * 		Checks for and fixes corruption in the queue. If it fails
 * 		to fix corruption then it creates a new queue.
 * @returns
 * 		Error code.
 */
static logger_error_t logger_assert_queue( logger_t *self )
{
	DEV_ASSERT( self );

	file_t*			queue_handle;
	filesystem_t*	fs;
	fs_error_t		fs_err;
	uint8_t			queue_contents[QUEUE_SIZE];
	uint8_t			checksum;
	uint32_t		read;

	fs = self->_.fs;

	/* Open master table. */
	queue_handle = fs->open( fs, &fs_err, self->_.queue_file_name, FS_OPEN_EXISTING, BLOCK_FOREVER );
	if( fs_err == FS_NO_FILE )
	{
		/* Table does't exist, attempt to restore it. */
		return logger_restore_queue( self );
	}
	else if( fs_err != FS_OK )
	{
		return LOGGER_NVMEM_ERR;
	}

	/* Read out contents and assert the checksum. */
	fs_err = queue_handle->read( queue_handle, queue_contents, QUEUE_SIZE, &read );
	queue_handle->close( queue_handle );
	if( fs_err != FS_OK )
	{
		return LOGGER_NVMEM_ERR;
	}
	if( read < QUEUE_SIZE )
	{
		/* The queue is smaller than it should be, it is corrupt. */
		return logger_restore_queue( self );
	}

	/* Assert checksum. */
	checksum = logger_queue_get_checksum( self, queue_contents, QUEUE_SIZE );
	if( checksum != CHECKSUM_OK )
	{
		/* queue is corrupt, attempt to restore. */
		return logger_restore_queue( self );
	}
	else
	{
		/* No corruption, assert passed. */
		return LOGGER_OK;
	}
}

/**
 * @memberof logger_t
 * @private
 * @brief
 * 		Push a new file name onto the top of the loggers queue.
 * @details
 * 		Push a new file name onto the top of the loggers queue.
 * @param packet_file[in]
 * 		Name of the new file to push onto the queue.
 * @returns
 * 		Error code.
 */
static logger_error_t logger_queue_push( logger_t *self, char* packet_file )
{
	DEV_ASSERT( self );
	DEV_ASSERT( packet_file );

	file_t			*queue_handle;
	filesystem_t*	fs;
	fs_error_t		fs_err;
	uint32_t		bytes_used;

	fs = self->_.fs;

	/* Open master table. */
	queue_handle = fs->open( fs, &fs_err, self->_.queue_file_name, FS_OPEN_EXISTING, BLOCK_FOREVER );
	if( fs_err != FS_OK )
	{
		return LOGGER_NVMEM_ERR;
	}

	/* Seek to the top of the queue. */
	fs_err = queue_handle->seek( queue_handle, QUEUE_TOP_OFFSET );
	if( fs_err != FS_OK )
	{
		queue_handle->close( queue_handle );
		return LOGGER_NVMEM_ERR;
	}

	/* Insert the new name. */
	fs_err = queue_handle->write( queue_handle, (uint8_t*) packet_file, LOGGER_MAX_FILE_NAME_LENGTH+1, &bytes_used );
	if( fs_err != FS_OK )
	{
		queue_handle->close( queue_handle );
		return LOGGER_NVMEM_ERR;
	}

	/* Update the queues check sum. */
	logger_queue_evaluate_checksum( self, queue_handle );
	queue_handle->close( queue_handle );

	return LOGGER_OK;
}

/**
 * @memberof logger_t
 * @private
 * @brief
 * 		Get a handle for the oldest packet file.
 * @details
 * 		Peeks at the queue to get the oldest packet file. It then
 * 		returns an opened handle for that packet file.
 * @param fs_err[in]
 * 		Error using filesystem.
 * @returns
 * 		The handle of the opened packet file.
 */
static file_t *logger_queue_peek( logger_t *self, logger_error_t *l_err )
{
	DEV_ASSERT( self );
	DEV_ASSERT( l_err );

	file_t*			queue_handle;
	file_t*			packet_handle;
	fs_error_t		fs_err;
	char			packet_file_name[LOGGER_MAX_FILE_NAME_LENGTH+1];
	uint32_t 		bytes_used;
	filesystem_t*	fs;

	fs = self->_.fs;

	/* Open master table. */
	queue_handle = fs->open( fs, &fs_err, self->_.queue_file_name, FS_OPEN_EXISTING, BLOCK_FOREVER );
	if( fs_err != FS_OK )
	{
		*l_err = LOGGER_NVMEM_ERR;
		return _filesystem_get_null_file( fs );
	}

	/* Seek the the bottom of the queue. */
	fs_err = queue_handle->seek( queue_handle, QUEUE_BOTTOM_OFFSET );
	if( fs_err != FS_OK )
	{
		*l_err = LOGGER_NVMEM_ERR;
		queue_handle->close( queue_handle );
		return _filesystem_get_null_file( fs );
	}

	/* Read file name at bottom of queue. */
	fs_err = queue_handle->read( queue_handle, (uint8_t*) packet_file_name, LOGGER_MAX_FILE_NAME_LENGTH+1, &bytes_used );
	queue_handle->close( queue_handle );
	if( fs_err != FS_OK )
	{
		*l_err = LOGGER_NVMEM_ERR;
		return _filesystem_get_null_file( fs );
	}

	/* Fail safe. */
	packet_file_name[LOGGER_MAX_FILE_NAME_LENGTH] = '\0';

	/* Return handle to packet file. */
	packet_handle = fs->open( fs, &fs_err, packet_file_name, FS_OPEN_EXISTING, BLOCK_FOREVER );
	if( fs_err == FS_NO_FILE )
	{
		/* Files have been deleted from memory, the queue needs to be updated. */
		*l_err = logger_restore_queue( self );
		if( *l_err != LOGGER_OK )
		{
			return _filesystem_get_null_file( self->_.fs );
		}
		/* Note, recursion is limited to one level on average. */
		/* If logger_restore_queue( ) returne LOGGER_OK, this means the bottom of the queue */
		/* contains a file that exists in the file system. There will not be another   		*/
		/* FS_NO_FILE error. The only exception is if the file at the bottom of the queue	*/
		/* is deleted from memory just after logger_restore_queue( ) finishes, but before 	*/
		/* the next recursive level of logger_queue_peek( ) finishes. As you can see,		*/
		/* recursing deeper than one level is very unlikely.								*/
		return logger_queue_peek( self, l_err );
	}
	else if( fs_err != FS_OK )
	{
		*l_err = LOGGER_NVMEM_ERR;
		return _filesystem_get_null_file( fs );
	}
	*l_err = LOGGER_OK;
	return packet_handle;
}

/**
 * @memberof logger_t
 * @private
 * @brief
 * 		Pop the oldest packet file out of the queue.
 * @details
 * 		Pop the oldest packet file out of the queue.
 * @returns
 * 		Filesystem error.
 */
static fs_error_t logger_queue_pop( logger_t *self )
{
	DEV_ASSERT( self );

	filesystem_t*	fs;
	file_t*			queue_handle;
	fs_error_t		fs_err;
	char			packet_file_name[LOGGER_MAX_FILE_NAME_LENGTH+1];
	uint32_t		bytes_used;
	uint32_t		time_out;

	fs = self->_.fs;
	time_out = 0;

	/* Open master table. */
	queue_handle = fs->open( fs, &fs_err, self->_.queue_file_name, FS_OPEN_EXISTING, BLOCK_FOREVER );
	if( fs_err != FS_OK )
	{
		return LOGGER_NVMEM_ERR;
	}

	/* Seek to bottom of queue. */
	fs_err = queue_handle->seek( queue_handle, QUEUE_BOTTOM_OFFSET );
	if( fs_err != FS_OK )
	{
		queue_handle->close( queue_handle );
		return LOGGER_NVMEM_ERR;
	}

	/* The queue only holds the top and bottom elements. The elements in between */
	/* are discovered algorithmically. This makes a trade off of memory for computation. */
	/* Therefore, we need to discover what the next oldest packet file name is. */
	/* First, we need the name of the oldest file. */
	fs_err = queue_handle->read( queue_handle, (uint8_t*) packet_file_name, LOGGER_MAX_FILE_NAME_LENGTH+1, &bytes_used );
	if( fs_err != FS_OK )
	{
		queue_handle->close( queue_handle );
		return LOGGER_NVMEM_ERR;
	}

	/* Next, search until we find the next oldest file name, or until a file system */
	/* error occurs or timeout. */
	do
	{
		++time_out;
		self->__.next_name( self, packet_file_name );
		fs_err = fs->file_exists( fs, packet_file_name );
	} while( fs_err == FS_NO_FILE && time_out < SEARCH_RETRY_LIMIT );
	if( fs_err != FS_OK )
	{
		/* Filesystem error occured. */
		queue_handle->close( queue_handle );
		return LOGGER_NVMEM_ERR;
	}

	/* Write next oldest packet file name into bottom of queue - replacing the oldest. */
	fs_err = queue_handle->seek( queue_handle, QUEUE_BOTTOM_OFFSET );
	if( fs_err != FS_OK )
	{
		queue_handle->close( queue_handle );
		return LOGGER_NVMEM_ERR;
	}
	fs_err = queue_handle->write( queue_handle, (uint8_t*) packet_file_name, LOGGER_MAX_FILE_NAME_LENGTH+1, &bytes_used );
	if( fs_err != FS_OK )
	{
		queue_handle->close( queue_handle );
		return LOGGER_NVMEM_ERR;
	}

	/* Recalculate checksum. */
	logger_queue_evaluate_checksum( self, queue_handle );
	queue_handle->close( queue_handle );

	return LOGGER_OK;
}

/**
 * @memberof logger_t
 * @private
 * @brief
 * 		Initializes the logger's queue.
 * @details
 * 		Initializes the logger's queue. This method can take a long time
 * 		to complete on very first power up. A proxy is used so that this
 * 		method does not get called until the logger is actually needed.
 * 		IE, the queue is not initialized at construction time.
 */
logger_error_t logger_initialize_queue( logger_t* self )
{
	DEV_ASSERT( self );

	logger_error_t lerr;
	file_t*			queue_handle;
	fs_error_t		fs_err;
	char			newest_packet_file_name[LOGGER_MAX_FILE_NAME_LENGTH+1];
	uint32_t 		bytes_used;


	/* Assert the contents of the queue. */
	lerr = logger_assert_queue( self );
	if( lerr != LOGGER_OK && lerr != LOGGER_RECOVERED )
	{
		return lerr;
	}

	/* Get a local copy of the most recent packet file name. */
	queue_handle = self->_.fs->open( self->_.fs, &fs_err, self->_.queue_file_name, FS_OPEN_EXISTING, BLOCK_FOREVER );
	if( fs_err != FS_OK )
	{
		return LOGGER_NVMEM_ERR;
	}

	/* Seek the the top of the queue. */
	fs_err = queue_handle->seek( queue_handle, QUEUE_TOP_OFFSET );
	if( fs_err != FS_OK )
	{
		queue_handle->close( queue_handle );
		return LOGGER_NVMEM_ERR;
	}

	/* Read file name at bottom of queue. */
	fs_err = queue_handle->read( queue_handle, (uint8_t*) newest_packet_file_name, LOGGER_MAX_FILE_NAME_LENGTH+1, &bytes_used );
	queue_handle->close( queue_handle );
	if( fs_err != FS_OK )
	{
		return LOGGER_NVMEM_ERR;
	}

	strncpy( self->_.packet_name, newest_packet_file_name, LOGGER_MAX_FILE_NAME_LENGTH );
	/* Fail safe. */
	self->_.packet_name[LOGGER_MAX_FILE_NAME_LENGTH] = '\0';

	return LOGGER_OK;
}


/********************************************************************************/
/* Virtual Method Definitions													*/
/********************************************************************************/
/**
 * @memberof logger_t
 * @protected
 * @brief
 * 		Abstract definition of logger_t::next_name.
 * @details
 * 		Abstract definition of logger_t::next_name. It makes the next packet file
 * 		name nothing, '\0'. The input string has room for LOGGER_MAX_FILE_NAME_LENGTH+1
 * 		bytes, including null character.
 * @param name[in][out]
 * 		The current packet file name - when the function completes, it will be
 * 		the next packet file name.
 * @returns
 * 		The length of new packet file name, in this case, 0.
 */
static size_t _next_name( logger_t *self, char *name )
{
	DEV_ASSERT( self );
	DEV_ASSERT( name );

	*name = '\0';
	return 0; /* New name has zero length. */
}

/**
 * @memberof logger_t
 * @protected
 * @brief
 * 		Abstract implementation. Called when a new packet file is created.
 * @details
 * 		Abstract implementation. Called when a new packet file is created. Does
 * 		nothing.
 * @param packet_file[in]
 * 		The handle of the newly opened packet file. The cursor is at the beginning
 * 		of the file.
 * @returns
 * 		Should return any filesystem error which occurs.
 */
static fs_error_t _new_packet_hook( logger_t *self, file_t * packet_file )
{
	DEV_ASSERT( self );
	DEV_ASSERT( packet_file );

	return FS_OK;
}

/**
 * @memberof logger_t
 * @protected
 * @brief
 * 		Abstract Implementation. Called when application calls logger_write( ).
 * @details
 * 		Abstract Implementation. Called when application calls logger_write( ). This method
 * 		is called before any data is written to the packet file within the logger_write( )
 * 		function. This gives the ability to write data to the packet file when ever an
 * 		application calls logger_write( ).
 * 		<br>This implementation does nothing.
 * @param packet_file[in]
 * 		The handle for the opened packet file.
 * @returns
 * 		Should return any filesystem error which occurs.
 */
static fs_error_t _new_entry_hook( logger_t *self, file_t * packet_file )
{
	DEV_ASSERT( self );
	DEV_ASSERT( packet_file );

	return FS_OK;
}


/********************************************************************************/
/* Public Method Definitions													*/
/********************************************************************************/
file_t* logger_get_packet( logger_t* self, logger_error_t* err )
{
	DEV_ASSERT( self );
	DEV_ASSERT( err );

	filesystem_t* 	fs;
	file_t*			packet;
	fs_error_t		file_err;
	char*			file_name;
	uint32_t		eof;

	lock_mutex( *self->_.sync_mutex, BLOCK_FOREVER );

	if( self->_.is_queue_init == false )
	{
		logger_error_t lerr;
		lerr = logger_initialize_queue( self );
		if( lerr != LOGGER_OK && lerr != LOGGER_RECOVERED )
		{
			*err = lerr;
			unlock_mutex( *self->_.sync_mutex );
			return _filesystem_get_null_file( self->_.fs );
		}
		self->_.is_queue_init = true;
	}

	/* Assert master table is not corrupt before doing anything. */
	logger_assert_queue( self );

	fs = self->_.fs;
	file_name = self->_.packet_name;

	unlock_mutex( *self->_.sync_mutex );

	/* Open the top most packet file. */
	/* This method assumes the contents of self->_.packet_name has been set to */
	/* be the name of the most recent packet file. */
	packet = fs->open( fs, &file_err, file_name, FS_OPEN_ALWAYS, BLOCK_FOREVER );
	if( file_err != FS_OK )
	{
		*err = LOGGER_NVMEM_ERR;
		return _filesystem_get_null_file( self->_.fs );
	}

	/* Call the new entry hook on the file. */
	eof = packet->size( packet, &file_err );
	if( file_err != FS_OK )
	{
		*err = LOGGER_HOOK_ERR;
		return packet;
	}
	file_err = packet->seek( packet, eof );
	if( file_err != FS_OK )
	{
		*err = LOGGER_HOOK_ERR;
		return packet;
	}
	file_err = self->__.new_entry_hook( self, packet );
	if( file_err != FS_OK )
	{
		*err = LOGGER_HOOK_ERR;
		return packet;
	}
	eof = packet->size( packet, &file_err );
	if( file_err != FS_OK )
	{
		*err = LOGGER_HOOK_ERR;
		return packet;
	}
	file_err = packet->seek( packet, eof );
	if( file_err != FS_OK )
	{
		*err = LOGGER_HOOK_ERR;
		return packet;
	}

	*err = LOGGER_OK;
	return packet;
}

file_t* logger_peek_top_packet( logger_t* self, logger_error_t* err )
{
	DEV_ASSERT( self );
	DEV_ASSERT( err );

	filesystem_t* 	fs;
	file_t*			packet;
	fs_error_t		file_err;
	char*			file_name;

	lock_mutex( *self->_.sync_mutex, BLOCK_FOREVER );

	if( self->_.is_queue_init == false )
	{
		logger_error_t lerr;
		lerr = logger_initialize_queue( self );
		if( lerr != LOGGER_OK && lerr != LOGGER_RECOVERED )
		{
			*err = lerr;
			unlock_mutex( *self->_.sync_mutex );
			return _filesystem_get_null_file( self->_.fs );
		}
		self->_.is_queue_init = true;
	}

	/* Assert master table is not corrupt before doing anything. */
	logger_assert_queue( self );

	fs = self->_.fs;
	file_name = self->_.packet_name;

	unlock_mutex( *self->_.sync_mutex );

	/* Open the top most packet file. */
	/* This method assumes the contents of self->_.packet_name has been set to */
	/* be the name of the most recent packet file. */
	packet = fs->open( fs, &file_err, file_name, FS_OPEN_ALWAYS, BLOCK_FOREVER );
	if( file_err != FS_OK )
	{
		*err = LOGGER_NVMEM_ERR;
		return _filesystem_get_null_file( self->_.fs );
	}
	*err = LOGGER_OK;
	return packet;
}

logger_error_t logger_push_packet( logger_t* self )
{
	DEV_ASSERT( self );

	logger_error_t 	lerr;
	file_t*			packet;
	fs_error_t		fs_err;

	lock_mutex( *self->_.sync_mutex, BLOCK_FOREVER );

	if( self->_.is_queue_init == false )
	{
		logger_error_t lerr;
		lerr = logger_initialize_queue( self );
		if( lerr != LOGGER_OK && lerr != LOGGER_RECOVERED )
		{
			unlock_mutex( *self->_.sync_mutex );
			return lerr;
		}
		self->_.is_queue_init = true;
	}

	/* Assert master table is not corrupt before doing anything. */
	logger_assert_queue( self );

	/* Get the next packet name and push it into the queue. */
	self->__.next_name( self, self->_.packet_name );
	lerr = logger_queue_push( self, self->_.packet_name );
	if( lerr != LOGGER_OK )
	{
		unlock_mutex( *self->_.sync_mutex );
		return lerr;
	}

	/* Call the new packet hook. */
	packet = self->_.fs->open( self->_.fs, &fs_err, self->_.packet_name, FS_OPEN_ALWAYS, BLOCK_FOREVER );
	if( fs_err != FS_OK )
	{
		unlock_mutex( *self->_.sync_mutex );
		return LOGGER_HOOK_ERR;
	}
	fs_err = self->__.new_packet_hook( self, packet );
	packet->close( packet );
	if( fs_err != FS_OK )
	{
		unlock_mutex( *self->_.sync_mutex );
		return LOGGER_HOOK_ERR;
	}

	unlock_mutex( *self->_.sync_mutex );
	return LOGGER_OK;
}

file_t* logger_peek_packet( logger_t* self, logger_error_t* err )
{
	DEV_ASSERT( self );
	DEV_ASSERT( err );

	file_t* packet;

	/* Assert queue then peek at the queue. */
	lock_mutex( *self->_.sync_mutex, BLOCK_FOREVER );

	if( self->_.is_queue_init == false )
	{
		logger_error_t lerr;
		lerr = logger_initialize_queue( self );
		if( lerr != LOGGER_OK && lerr != LOGGER_RECOVERED )
		{
			*err = lerr;
			unlock_mutex( *self->_.sync_mutex );
			return _filesystem_get_null_file( self->_.fs );
		}
		self->_.is_queue_init = true;
	}

	logger_assert_queue( self );
	packet = logger_queue_peek( self, err );
	unlock_mutex( *self->_.sync_mutex );

	return packet;
}

logger_error_t logger_pop_packet( logger_t* self )
{
	DEV_ASSERT( self );

	logger_error_t lerr;

	/* Assert queue then pop from the queue. */
	lock_mutex( *self->_.sync_mutex, BLOCK_FOREVER );

	if( self->_.is_queue_init == false )
	{
		logger_error_t lerr;
		lerr = logger_initialize_queue( self );
		if( lerr != LOGGER_OK && lerr != LOGGER_RECOVERED )
		{
			unlock_mutex( *self->_.sync_mutex );
			return lerr;
		}
		self->_.is_queue_init = true;
	}

	logger_assert_queue( self );
	lerr = logger_queue_pop( self );
	unlock_mutex( *self->_.sync_mutex );

	return lerr;
}

logger_error_t logger_reset( logger_t *self )
{
	DEV_ASSERT( self );

	logger_error_t lerr;

	lock_mutex( *self->_.sync_mutex, BLOCK_FOREVER );
	lerr = logger_restore_queue( self );
	unlock_mutex( *self->_.sync_mutex );

	return lerr;
}


/********************************************************************************/
/* Constructor / Destructor Define												*/
/********************************************************************************/
static void destroy( logger_t *self )
{
	DEV_ASSERT( self );
}

logger_error_t _initialize_logger( logger_t *self, filesystem_t *filesystem,
								  char const *delete_buffer_name, char const *master_table_name,
								  char const *packet_file_name )
{
	DEV_ASSERT( self );
	DEV_ASSERT( filesystem );
	DEV_ASSERT( delete_buffer_name );
	DEV_ASSERT( master_table_name );
	DEV_ASSERT( packet_file_name );

	/* Link virtual methods. */
	self->destroy = destroy;
	self->__.new_entry_hook = _new_entry_hook;
	self->__.new_packet_hook = _new_packet_hook;
	self->__.next_name = _next_name;

	/* Initialize singleton mutex. */
	if( logger_is_init == false )
	{
		new_mutex( logger_sync_mutex );
		if( logger_sync_mutex == NULL )
		{
			return LOGGER_MUTEX_ERR;
		}
		logger_is_init = true;
		unlock_mutex( logger_sync_mutex );
	}

	/* Setup Member data. */
	self->_.fs = filesystem;
	self->_.sync_mutex = &logger_sync_mutex;
	self->_.is_queue_init = false;

	/* Copy packet file name, queue_file_name, and delete buffer name into self. */
	strncpy( self->_.packet_name, packet_file_name, LOGGER_MAX_FILE_NAME_LENGTH );
	self->_.packet_name[LOGGER_MAX_FILE_NAME_LENGTH] = '\0'; /* Fail safe. */
	strncpy( self->_.first_packet_name, packet_file_name, LOGGER_MAX_FILE_NAME_LENGTH );
	self->_.first_packet_name[LOGGER_MAX_FILE_NAME_LENGTH] = '\0'; /* Fail safe. */
	strncpy( self->_.queue_file_name, master_table_name, LOGGER_MAX_FILE_NAME_LENGTH );
	self->_.queue_file_name[LOGGER_MAX_FILE_NAME_LENGTH] = '\0'; /* Fail safe. */
	strncpy( self->_.delete_buffer_name, delete_buffer_name, LOGGER_MAX_FILE_NAME_LENGTH );
	self->_.delete_buffer_name[LOGGER_MAX_FILE_NAME_LENGTH] = '\0'; /* Fail safe. */

	/* Get length of packet name. */
	/* Note, strlen is safe to use because, a few lines above, a null terminated character was */
	/* added to packet_name. */
	self->_.packet_name_length = strlen( self->_.packet_name );

	return LOGGER_OK;
}
