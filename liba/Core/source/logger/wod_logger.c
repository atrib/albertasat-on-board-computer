/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file wod_logger.c
 * @author Brendan Bruner
 * @date May 25, 2015
 */
#include <packets/telemetry_packet.h>
#include <logger/wod_logger.h>
#include <core_defines.h>
#include <packets/packet_base.h>


/********************************************************************************/
/* Defines																		*/
/********************************************************************************/


/********************************************************************************/
/* Private Singleton Variables													*/
/********************************************************************************/
static char	const	*delete_buffer = WOD_LOGGER_DELETE_BUFFER;
static char			*name_space = WOD_LOGGER_PACKET_NAMES;
static char	const	*master_tbl = WOD_LOGGER_MASTER_TABLE;


/********************************************************************************/
/* Virtual Methods																*/
/********************************************************************************/
static fs_error_t _new_packet_hook( logger_t* self_, file_t* packet )
{
	wod_logger_t* self = (wod_logger_t*) self_;
	DEV_ASSERT( self );

	/* time since qb50 epoch converted to little endian, 4 bytes. */
	uint32_t	pre_processed_entry_time;
	uint8_t		entry_time[sizeof(pre_processed_entry_time)];
	rtc_t* 		rtc;
	uint32_t	written;

	rtc = self->_.rtc;
	pre_processed_entry_time = rtc->seconds_since_epoch( rtc, QB50_EPOCH );
	to_little_endian_from32( &pre_processed_entry_time, entry_time );

	/* Write entry_time to the file. */
	return packet->write( packet, entry_time, sizeof(pre_processed_entry_time), &written );
}

static size_t _next_name( logger_t *logger, char *name )
{
	DEV_ASSERT( logger );
	DEV_ASSERT( name );

	/* TODO: use RTC to get current day of mission. */
	if( name[WOD_LOGGER_SEQ_INDEX_0] == '4' && name[WOD_LOGGER_SEQ_INDEX_1] == '5' )
	{
		/* At end of a sequence, increment day counter. */

		name[WOD_LOGGER_SEQ_INDEX_0] = '0';
		name[WOD_LOGGER_SEQ_INDEX_1] = '0';

		if( name[WOD_LOGGER_DAY_INDEX_3] != '9' )
		{
			++name[WOD_LOGGER_DAY_INDEX_3];
		}
		else if( name[WOD_LOGGER_DAY_INDEX_2] != '9' )
		{
			++name[WOD_LOGGER_DAY_INDEX_2];
			name[WOD_LOGGER_DAY_INDEX_3] = '0';
		}
		else if( name[WOD_LOGGER_DAY_INDEX_1] != '9' )
		{
			++name[WOD_LOGGER_DAY_INDEX_1];
			name[WOD_LOGGER_DAY_INDEX_2] = '0';
			name[WOD_LOGGER_DAY_INDEX_3] = '0';
		}
		else if( name[WOD_LOGGER_DAY_INDEX_0] != '9' )
		{
			++name[WOD_LOGGER_DAY_INDEX_0];
			name[WOD_LOGGER_DAY_INDEX_1] = '0';
			name[WOD_LOGGER_DAY_INDEX_2] = '0';
			name[WOD_LOGGER_DAY_INDEX_3] = '0';
		}
		else
		{
			/* All hope is lost. */
			name[WOD_LOGGER_DAY_INDEX_0] = '0';
			name[WOD_LOGGER_DAY_INDEX_1] = '0';
			name[WOD_LOGGER_DAY_INDEX_2] = '0';
			name[WOD_LOGGER_DAY_INDEX_3] = '0';
		}
	}
	else
	{
		/* Increment sequence counter. */
		if( name[WOD_LOGGER_SEQ_INDEX_1] == '9' )
		{
			/* Increment most sig number. Set least sig to zero. */
			++name[WOD_LOGGER_SEQ_INDEX_0];
			name[WOD_LOGGER_SEQ_INDEX_1] = '0';
		}
		else
		{
			/* Increment least sig number. */
			++name[WOD_LOGGER_SEQ_INDEX_1];
		}
	}

	return WOD_LOGGER_NAME_LENGTH;
}


/********************************************************************************/
/* Initialization Method														*/
/********************************************************************************/
logger_error_t initialize_wod_logger( wod_logger_t *logger, filesystem_t *fs, rtc_t* rtc )
{
	DEV_ASSERT( logger );
	DEV_ASSERT( fs );

	logger_error_t init_err;

	init_err = _initialize_logger( (logger_t *) logger, fs, delete_buffer,
											master_tbl, name_space );
	((logger_t *) logger)->__.next_name = _next_name;
	((logger_t*) logger)->__.new_packet_hook = _new_packet_hook;
	logger->_.rtc = rtc;

	return init_err;
}
