/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_blink.c
 * @author Brendan Bruner
 * @date Aug 12, 2015
 */

#include <telecommands/self_alteration/telecommand_blink.h>
#include <portable_types.h>
#include <printing.h>
#include <gpio/gpio_lpc.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute_blink( telecommand_t *command )
{
	telecommand_blink_t* self = (telecommand_blink_t*) command;
	DEV_ASSERT( command );

	gpio_t*				gpio;

	gpio = (gpio_t*) &self->_.gpio;
	gpio->set_state( gpio, GPIO_HIGH );
	task_delay( 1000 );
	gpio->set_state( gpio, GPIO_LOW );
}

static telecommand_t* clone( telecommand_t* self_ )
{
	telecommand_blink_t* self = (telecommand_blink_t*) self_;
	DEV_ASSERT( self );

	telecommand_blink_t* clone;
	uint8_t err;

	clone = (telecommand_blink_t*) OBCMalloc( sizeof(telecommand_blink_t) );
	if( clone == NULL )
	{
		return NULL;
	}

	err = initialize_telecommand_blink( clone, ((telecommand_t*) self)->_kit );
	if( err != TELECOMMAND_SUCCESS )
	{
		return NULL;
	}

	return (telecommand_t*) clone;
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_telecommand_blink( telecommand_blink_t *command, driver_toolkit_t *kit )
{
	DEV_ASSERT( command );
	DEV_ASSERT( kit );

	uint8_t err;

	err = initialize_telecommand( (telecommand_t *) command, kit );
	((telecommand_t *) command)->_execute = execute_blink;
	((telecommand_t*) command)->clone = clone;
	_telecommand_set_type( (telecommand_t *) command, TELECOMMAND_TYPE_BLINK );

	initialize_gpio_lpc( &command->_.gpio, 0, 3, GPIO_BARE );
	((gpio_t*) &command->_.gpio)->set_direction( (gpio_t*) &command->_.gpio, GPIO_OUTPUT );
	((gpio_t*) &command->_.gpio)->set_state( (gpio_t*) &command->_.gpio, GPIO_LOW );

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


