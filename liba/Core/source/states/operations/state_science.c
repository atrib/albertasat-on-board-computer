/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file state_science.c
 * @author Brendan Bruner
 * @date Aug 11, 2015
 */
#include "states/operations/state_science.h"
#include <states/state_relay.h>
#include <core_defines.h>
#include <dfgm/dfgm.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define DEFAULT_HK			STATE_ENABLES_EXECUTION
#define DEFAULT_TRANSMIT	STATE_ENABLES_EXECUTION
#define DEFAULT_RESPONSE	STATE_ENABLES_EXECUTION
#define DEFAULT_DFGM		STATE_ENABLES_EXECUTION
#define DEFAULT_MNLP		STATE_ENABLES_EXECUTION


/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/
static char const * const config_log = STATE_SCIENCE_LOG;


/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void state_entry( state_t *state, driver_toolkit_t *kit )
{
	DEV_ASSERT( state );
	DEV_ASSERT( kit );

	/* FIXME: Cannot simply turn on/off. Must work with configuration. */
	kit->dfgm->power( kit->dfgm, kit->eps, true );
	kit->hub->power( kit->hub, kit->eps, true );
	kit->mnlp->power( kit->mnlp, kit->hub, true );
	kit->teledyne->power( kit->teledyne, kit->eps, true );

//	DFGM_start();
	/* TODO: turn on payloads if state enables execution of payload commands. */
	/* For example, if state configuration enables execution of dfgm commands */
	/* then turn on dfgm. */
}

static void state_exit_science( state_t *state, driver_toolkit_t *kit )
{
	DEV_ASSERT( state );
	DEV_ASSERT( kit );

	/* FIXME: Cannot simply turn on/off. Must work with configuration. */
	kit->dfgm->power( kit->dfgm, kit->eps, false );
	kit->mnlp->power( kit->mnlp, kit->hub, false );
	kit->hub->power( kit->hub, kit->eps, false );
	kit->teledyne->power( kit->teledyne, kit->eps, false );
//	DFGM_stop();

	/* TODO: Turn off all payloads. Some power downs may be redundant, but are */
	/* needed. For example, in entry method, dfgm is turned on. Then, a telecommand */
	/* is received which disables dfgm commands in science state. Checking the states */
	/* configuration would mislead a person into thinking the dfgm was never turned on */
	/* (since those commands are disabled), however, this is not true. */
}

static state_t *next_state(	state_t *state,	state_relay_t *relay )
{
	DEV_ASSERT( state );
	DEV_ASSERT( relay );

	eps_t *eps;
	adcs_t *adcs;

	eps = relay->drivers->eps;
	adcs = &relay->drivers->adcs;

	/* if battery is in power safe or critical, go to low power. */
	if( eps->mode( eps ) == EPS_MODE_POWER_SAFE || eps->mode( eps ) == EPS_MODE_CRITICAL )
	{
		return (state_t *) &relay->low_power;
	}

	/* Else, if the adcs is not stabilized, to go alignment state. */
	/* TODO: change so the get frame isn't done with every call - too much io. do it */
	/* at most once / 500ms */
	if( adcs_get_detumbling_state( adcs ) != ADCS_Y_STEADY )
	{
		return (state_t *) &relay->alignment;
	}

	/* Otherwise, stay in this state. */
	return (state_t *) NULL;
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_state_science( state_science_t *state, filesystem_t *fs )
{
	DEV_ASSERT( state );
	DEV_ASSERT( fs );

	uint8_t 		err;
	state_config_t	config;

	state_config_set_hk( &config, DEFAULT_HK );
	state_config_set_transmit( &config, DEFAULT_TRANSMIT );
	state_config_set_response( &config, DEFAULT_RESPONSE );
	state_config_set_mnlp( &config, DEFAULT_MNLP );
	state_config_set_dfgm( &config, DEFAULT_DFGM );

	err = initialize_state( (state_t *) state, config_log, fs, &config );

	((state_t *) state)->_id_ = 		STATE_SCIENCE_ID;
	((state_t *) state)->enter_state = 	state_entry;
	((state_t *) state)->exit_state = 	state_exit_science;
	((state_t *) state)->next_state = 	next_state;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
