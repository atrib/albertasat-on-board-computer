/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file state_config.c
 * @author Brendan Bruner
 * @date Aug 10, 2015
 */
#include <states/state_config.h>
#include <portable_types.h>


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
void state_config_set_hk( state_config_t *config, uint8_t option )
{
	DEV_ASSERT( config );

	config->_uses_hk = option;
}

void state_config_set_transmit( state_config_t *config, uint8_t option )
{
	DEV_ASSERT( config );

	config->_uses_transmit = option;
}

void state_config_set_response( state_config_t *config, uint8_t option )
{
	DEV_ASSERT( config );

	config->_uses_response = option;
}

void state_config_set_dfgm( state_config_t *config, uint8_t option )
{
	DEV_ASSERT( config );

	config->_uses_dfgm = option;
}

void state_config_set_mnlp( state_config_t *config, uint8_t option )
{
	DEV_ASSERT( config );

	config->_uses_mnlp = option;
}

uint8_t state_config_detect_corruption( state_config_t *config )
{
	DEV_ASSERT( config );

	uint8_t result;

	/* All variables are either a 1 or 0. Therefore, the logical OR of */
	/* all of them must never be greater than 1. */
	result = (config->_uses_dfgm | config->_uses_hk | config->_uses_mnlp |
				config->_uses_response | config->_uses_transmit );

	return result > 1 ? STATE_CONFIG_CORRUPT : STATE_CONFIG_OK;
}

