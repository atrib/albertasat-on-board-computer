/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file teledyne.c
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */

#include <teledyne/teledyne.h>
#include <eps/power_lines.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/
/**
 * @memberof teledyne_t
 * @brief
 * 		Turn the teledyne's power line on/off
 * @details
 * 		Turn the teledyne's power line on/off. This will invoke a hard power down / power on.
 * @param eps[in]
 * 		The eps which controls the teledyne's power line.
 * @param state
 * 		<b>true</b> to power on, <b>false</b> to power off.
 * @returns
 * 		<b>true</b> if successful, <b>false</b> otherwise.
 */
static bool_t power( teledyne_t* self, eps_t* eps, bool_t state )
{
	DEV_ASSERT( self );
	DEV_ASSERT( eps );

	bool_t status;

	if( state == true )
	{
		status = eps->power_line( eps, TELEDYNE_POWER_LINE, EPS_POWER_LINE_ACTIVE );
	}
	else
	{
		status = eps->power_line( eps, TELEDYNE_POWER_LINE, EPS_POWER_LINE_INACTIVE );
	}

	return status;
}


/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
/**
 * @memberof teledyne_t
 * @protected
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 */
void initialize_teledyne_( teledyne_t* self )
{
	DEV_ASSERT( self );

	self->power = power;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


