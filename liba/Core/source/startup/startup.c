/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file startup.c
 * @author Alex Hamilton
 * @date Aug 10, 2015
 */

#include "startup.h"

#include "csp/csp.h"
#include "comm.h"
#include "adcs.h"
#include "cmd_rparam.h"
#include "eps.h"
#include <rparam_client.h>
#include "param/param.h"
#include "freertos/task.h"
#include <dev/arm/ds1302.h>
#include <dev/cpu.h>
#include "csp_internal.h"
#include <unistd.h>
#include "driver_toolkit.h"
#include "io/nanohub.h"


uint8_t buff[600]={0};


void StartupTest(int * test_status,driver_toolkit_t * drivers){


	int CommTest(comm_t * comm);
	int PowerTest(eps_t * eps);
	int ADCSTest(adcs_t * adcs);
	int HubTest(void);

	*test_status|=PowerTest(&drivers->eps);
	*test_status|=CommTest(&drivers->comm);
	//*test_status|=ADCSTest(&drivers->adcs);
	*test_status|=HubTest();
}


int CommTest(comm_t * comm){

	int tests=0;
	int CommPingTest(void);
	int CommConf0Test(comm_t * comm);
	int CommConf4Test(comm_t * comm);

	tests|=CommPingTest();
	if(tests>=0){
		StartupLog(STARTLOG_COMM_PING);
	}
	tests|=CommConf0Test(comm);
	if(tests>=0){
		StartupLog(STARTLOG_COMM_GET0);
	}
	tests|=CommConf4Test(comm);
	if(tests>=0){
		StartupLog(STARTLOG_COMM_GET4);
	}
	return tests;
}


int PowerTest(eps_t * eps){ /*Test the power board*/

	int tests=0;
	int PowerPingTest(void);
	int PowerConfTest(eps_t * eps_hk_test);

	tests|=PowerPingTest();
	if(tests>=0){
		StartupLog(STARTLOG_EPS_PING);
	}
	tests|=PowerConfTest(eps);
	if(tests>=0){
		StartupLog(STARTLOG_EPS_GET);
	}
	return tests;
}


int PowerPingTest(void){
	int tests;
	tests=csp_ping(2,1000,100,CSP_O_NONE);
	if(tests==-1){
		vTaskDelay(10/portTICK_RATE_MS);
		tests=csp_ping(2,1000,100,CSP_O_NONE);
		if(tests==-1){
			vTaskDelay(100/portTICK_RATE_MS);
			tests=csp_ping(2,1000,100,CSP_O_NONE);
			if(tests==-1){
				//TODO: handle no power board?
				return -1;
			}
		}
	}
	return 0;
}

int PowerConfTest(eps_t * eps){
	int tests;
	tests=eps->hk_refresh(eps);
	if(tests!=sizeof(eps_hk_t)){
		vTaskDelay(10/portTICK_RATE_MS);
		tests=eps->hk_refresh(eps);

		if(tests!=sizeof(eps_hk_t)){
			vTaskDelay(100/portTICK_RATE_MS);
			tests=eps->hk_refresh(eps);

			if(tests!=sizeof(eps_hk_t)){
				return -1;

			}
		}
	}
	return 0;
}


int CommPingTest(void){
	int tests;
	/*Test the comm board*/
	tests=csp_ping(5,1000,100,CSP_O_NONE);
	if(tests==-1){ //ping failed
		vTaskDelay(10/portTICK_RATE_MS);
		tests=csp_ping(5,1000,100,CSP_O_NONE);
		if(tests==-1){ //ping failed
			vTaskDelay(100/portTICK_RATE_MS);
			tests=csp_ping(5,1000,100,CSP_O_NONE);
			if(tests==-1){ //ping failed
				return -1;
			}
		}
	}
	else{
		return 1;
	}
}

int CommConf0Test(comm_t * comm_test){
	int test,tests;
	test=access("/sd/par-5-0.bin",R_OK);
	if(test!=-1){
		tests=com_init("0");
		tests|=com_getall(comm_test);
	}
	else{
		tests=com_download("0");
		tests|=com_getall(comm_test);
	}

	if((comm_test->preambflags!=56)||(comm_test->intfrmflags!=56)) return-1;
	else return tests;

}

int CommConf4Test(comm_t * comm_test){
	int test,tests;
	test=access("/sd/par-5-4.bin",R_OK);
	if(test!=-1){
		tests=com_init("4");
		tests|=com_getall(comm_test);
	}
	else{
		tests=com_download("4");
		tests|=com_getall(comm_test);
	}
	return tests;
}


void StartupLog(uint8_t status){
	FILE * fid;
	buff[5]=status;
	fid=fopen("/boot/bc.bin","r");
	if(fid!=NULL){
		fwrite(buff,6,100,fid);
	}
	fclose(fid);
}

void StartupNewLog(void){

	cpu_reset_cause_t cause = cpu_read_reset_cause();
	FILE * fid;

	struct ds1302_clock clock;
	int32_t utime;
	ds1302_clock_read_burst(&clock);
	ds1302_clock_to_time(&utime,&clock);

	memcpy(buff,&utime,4);
	buff[4]=cause;
	buff[5]=0;

	fid=fopen("/boot/bc.bin","r");
	if(fid!=NULL){
		fread(&buff[6],6,99,fid);
	}
	else{
		csp_printf("null read pointer!");
	}
	fclose(fid);

	fid=fopen("/boot/bc.bin","w");
	if(fid!=NULL){
		fwrite(buff,6,100,fid);
	}
	else{
		csp_printf("null write pointer!");
	}
		fclose(fid);
}

int FSTest(void){

	//TODO: Write Filesystem Test
	return 0;
}



int ADCSTest(adcs_t * adcs){
	adcs->getframe_identification(adcs);
	if(adcs->identification.node_type!=11){

		adcs->getframe_identification(adcs);
		if(adcs->identification.node_type!=11){

			adcs->getframe_identification(adcs);
			if(adcs->identification.node_type!=11){
				return -1;
			}
		}
	}
	//TODO: ADCS set test
	StartupLog(STARTLOG_ADCS_TEST);
	return 0;
}


int HubTest(void){
	int tests=0;
	int HubPingTest(void);
	int HubGetTest(void);

	tests|=HubPingTest();
	tests|=HubGetTest();
	return tests;
}

int HubPingTest(void){
	int tests;
	tests=csp_ping(3,1000,100,CSP_O_NONE);
	if(tests==-1){
		vTaskDelay(10/portTICK_RATE_MS);
		tests=csp_ping(3,1000,100,CSP_O_NONE);
		if(tests==-1){
			vTaskDelay(100/portTICK_RATE_MS);
			tests=csp_ping(3,1000,100,CSP_O_NONE);
			if(tests==-1){
				//TODO: handle no nanohub
				return -1;
			}
		}
	}
	return 0;
}

int HubGetTest(){
	nanohub_hk_t nanohub_hk;
	if(hub_get_hk(&nanohub_hk)<0){
		if(hub_get_hk(&nanohub_hk)<0){
			if(hub_get_hk(&nanohub_hk)<0){
				return -1;
			}
		}
	}
	return 0;
}
