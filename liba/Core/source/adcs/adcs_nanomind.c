/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file adcs_lpc_local
 * @author Alex Hamilton
 * @author Brendan Bruner
 * @date Feb 11, 2015
 */

#include "adcs.h"
#ifndef LPC1769
//#include "dev/i2c.h"
#include "dev/usart.h"
#include <stdio.h>
#include <string.h>
#include "csp_internal.h"
#endif

/* Do not warn for strict alaising */
#if (__GNUC__ == 4 && 3 <= __GNUC_MINOR__) || 4 < __GNUC__
# pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif


void adcs_get(uint8_t cmd, uint8_t *buff, uint32_t size){
	uint32_t i;
	uint8_t temp;

	usart_putc(ADCS_USART_HANDLE,ADCS_ESC);
	if(ADCS_DEBUG) csp_printf("send USART0: %x",ADCS_ESC);
	usart_putc(ADCS_USART_HANDLE,0x7F);
	if(ADCS_DEBUG) csp_printf("send USART0: %x",0x7F);
	if(cmd==0x1F){
		usart_putc(ADCS_USART_HANDLE,cmd);
		if(ADCS_DEBUG) csp_printf("send USART0: %x",cmd);
		usart_putc(ADCS_USART_HANDLE,ADCS_ESC);
		if(ADCS_DEBUG) csp_printf("send USART0: %x",ADCS_ESC);
	}
	else{
		usart_putc(ADCS_USART_HANDLE,cmd);
		if(ADCS_DEBUG) csp_printf("send USART0: %x",cmd);
	}
	usart_putc(ADCS_USART_HANDLE,ADCS_ESC);
	if(ADCS_DEBUG) csp_printf("send USART0: %x",ADCS_ESC);
	usart_putc(ADCS_USART_HANDLE,0xFF);
	if(ADCS_DEBUG) csp_printf("send USART0: %x",0xFF);



	temp=usart_getc(ADCS_USART_HANDLE);
	if(ADCS_DEBUG) csp_printf("USART0: %x",temp);
	temp=usart_getc(ADCS_USART_HANDLE);
	if(ADCS_DEBUG) csp_printf("USART0: %x",temp);
	for(i=0;i<ADCS_IDENTIFICATION_SIZE;i++){
			temp=usart_getc(ADCS_USART_HANDLE);
			if(ADCS_DEBUG) csp_printf("USART0: %x",temp);
			if(temp==0x1f){
				temp=usart_getc(ADCS_USART_HANDLE);
				if(ADCS_DEBUG) csp_printf("USART0: %x",temp);
				if(temp==0xff) break;
			}
			buff[i]=temp;
		}
		while(usart_messages_waiting(ADCS_USART_HANDLE)) usart_getc(ADCS_USART_HANDLE);
}

void adcs_set(uint8_t *buff, uint32_t size){
	uint32_t i;
	uint8_t error;
	usart_putc(ADCS_USART_HANDLE,ADCS_ESC);
	usart_putc(ADCS_USART_HANDLE,0x7F);

	for(i=0; i<size; i++){
		if(buff[i]==ADCS_ESC){
			usart_putc(ADCS_USART_HANDLE,ADCS_ESC);
		}
		usart_putc(ADCS_USART_HANDLE,buff[i]);
	}
	usart_putc(ADCS_USART_HANDLE,ADCS_ESC);
	usart_putc(ADCS_USART_HANDLE,0xFF);

	usart_getc(ADCS_USART_HANDLE);
	usart_getc(ADCS_USART_HANDLE);
	error=usart_getc(ADCS_USART_HANDLE);
	while(usart_messages_waiting(ADCS_USART_HANDLE)) usart_getc(ADCS_USART_HANDLE);

	if(error!=0) csp_printf("error: %d",error);
}



void adcs_getframe_identification (adcs_t *adcs)
{
	uint8_t buff[ADCS_IDENTIFICATION_SIZE];
	memset(buff,0,ADCS_IDENTIFICATION_SIZE);
	uint8_t cmd = ADCS_IDENTIFICATION_ID;
	adcs_get(cmd,buff,ADCS_IDENTIFICATION_SIZE);
	memcpy(&adcs->identification,buff,ADCS_IDENTIFICATION_SIZE);
}

void adcs_getframe_communication_status (adcs_t *adcs)
{
	uint8_t buff[ADCS_COMMUNICATION_STATUS_SIZE];
	memset(buff,0,ADCS_COMMUNICATION_STATUS_SIZE);
	uint8_t cmd = ADCS_COMMUNICATION_STATUS_ID;
	adcs_get(cmd,buff,ADCS_COMMUNICATION_STATUS_SIZE);
	memcpy(&adcs->communication_status,buff,ADCS_COMMUNICATION_STATUS_SIZE);
}

void adcs_getframe_telecommand_acknowledge (adcs_t *adcs)
{
	uint8_t buff[ADCS_TELECOMMAND_ACKNOWLEDGE_SIZE];
	memset(buff,0,ADCS_TELECOMMAND_ACKNOWLEDGE_SIZE);
	uint8_t cmd = ADCS_TELECOMMAND_ACKNOWLEDGE_ID;
	adcs_get(cmd,buff,ADCS_TELECOMMAND_ACKNOWLEDGE_SIZE);
	memcpy(&adcs->telecommand_acknowledge,buff,ADCS_TELECOMMAND_ACKNOWLEDGE_SIZE);
}

void adcs_getframe_reset_cause (adcs_t *adcs)
{
	uint8_t buff[ADCS_RESET_CAUSE_SIZE];
	memset(buff,0,ADCS_RESET_CAUSE_SIZE);
	uint8_t cmd = ADCS_RESET_CAUSE_ID;
	adcs_get(cmd,buff,ADCS_RESET_CAUSE_SIZE);
	memcpy(&adcs->reset_cause,buff,ADCS_RESET_CAUSE_SIZE);
}

void adcs_getframe_actuator_commands (adcs_t *adcs)
{
	uint8_t buff[ADCS_ACTUATOR_COMMANDS_SIZE];
	memset(buff,0,ADCS_ACTUATOR_COMMANDS_SIZE);
	uint8_t cmd = ADCS_ACTUATOR_COMMANDS_ID;
	adcs_get(cmd,buff,ADCS_ACTUATOR_COMMANDS_SIZE);
	memcpy(&adcs->actuator_commands,buff,ADCS_ACTUATOR_COMMANDS_SIZE);
}

void adcs_getframe_acp_execution_state (adcs_t *adcs)
{
	uint8_t buff[ADCS_ACP_EXECUTION_STATE_SIZE];
	memset(buff,0,ADCS_ACP_EXECUTION_STATE_SIZE);
	uint8_t cmd = ADCS_ACP_EXECUTION_STATE_ID;
	adcs_get(cmd,buff,ADCS_ACP_EXECUTION_STATE_SIZE);
	memcpy(&adcs->acp_execution_state,buff,ADCS_ACP_EXECUTION_STATE_SIZE);
}

void adcs_getframe_acp_execution_times (adcs_t *adcs)
{
	uint8_t buff[ADCS_ACP_EXECUTION_TIMES_SIZE];
	memset(buff,0,ADCS_ACP_EXECUTION_TIMES_SIZE);
	uint8_t cmd = ADCS_ACP_EXECUTION_TIMES_ID;
	adcs_get(cmd,buff,ADCS_ACP_EXECUTION_TIMES_SIZE);
	memcpy(&adcs->acp_execution_times,buff,ADCS_ACP_EXECUTION_TIMES_SIZE);
}

void adcs_getframe_edac_and_latchup_counters (adcs_t *adcs)
{
	uint8_t buff[ADCS_EDAC_AND_LATCHUP_COUNTERS_SIZE];
	memset(buff,0,ADCS_EDAC_AND_LATCHUP_COUNTERS_SIZE);
	uint8_t cmd = ADCS_EDAC_AND_LATCHUP_COUNTERS_ID;
	adcs_get(cmd,buff,ADCS_EDAC_AND_LATCHUP_COUNTERS_SIZE);
	memcpy(&adcs->edac_and_latchup_counters,buff,ADCS_EDAC_AND_LATCHUP_COUNTERS_SIZE);
}

void adcs_getframe_start_up_mode (adcs_t *adcs)
{
	uint8_t buff[ADCS_START_UP_MODE_SIZE];
	memset(buff,0,ADCS_START_UP_MODE_SIZE);
	uint8_t cmd = ADCS_START_UP_MODE_ID;
	adcs_get(cmd,buff,ADCS_START_UP_MODE_SIZE);
	memcpy(&adcs->start_up_mode,buff,ADCS_START_UP_MODE_SIZE);
}

void adcs_getframe_file_information (adcs_t *adcs)
{
	uint8_t buff[ADCS_FILE_INFORMATION_SIZE];
	memset(buff,0,ADCS_FILE_INFORMATION_SIZE);
	uint8_t cmd = ADCS_FILE_INFORMATION_ID;
	adcs_get(cmd,buff,ADCS_FILE_INFORMATION_SIZE);
	memcpy(&adcs->file_information,buff,ADCS_FILE_INFORMATION_SIZE);
}

void adcs_getframe_file_block_crc (adcs_t *adcs)
{
	uint8_t buff[ADCS_FILE_BLOCK_CRC_SIZE];
	memset(buff,0,ADCS_FILE_BLOCK_CRC_SIZE);
	uint8_t cmd = ADCS_FILE_BLOCK_CRC_ID;
	adcs_get(cmd,buff,ADCS_FILE_BLOCK_CRC_SIZE);
	memcpy(&adcs->file_block_crc,buff,ADCS_FILE_BLOCK_CRC_SIZE);
}

void adcs_getframe_file_data_block (adcs_t *adcs)
{
	uint8_t buff[ADCS_FILE_DATA_BLOCK_SIZE];
	memset(buff,0,ADCS_FILE_DATA_BLOCK_SIZE);
	uint8_t cmd = ADCS_FILE_DATA_BLOCK_ID;
	adcs_get(cmd,buff,ADCS_FILE_DATA_BLOCK_SIZE);
	memcpy(&adcs->file_data_block,buff,ADCS_FILE_DATA_BLOCK_SIZE);
}

void adcs_getframe_power_control_selection (adcs_t *adcs)
{
	uint8_t buff[ADCS_POWER_CONTROL_SELECTION_SIZE];
	memset(buff,0,ADCS_POWER_CONTROL_SELECTION_SIZE);
	uint8_t cmd = ADCS_POWER_CONTROL_SELECTION_ID;
	adcs_get(cmd,buff,ADCS_POWER_CONTROL_SELECTION_SIZE);
	memcpy(&adcs->power_control_selection,buff,ADCS_POWER_CONTROL_SELECTION_SIZE);
}

void adcs_getframe_power_and_temperature_measurements (adcs_t *adcs)
{
	uint8_t buff[ADCS_POWER_AND_TEMPERATURE_MEASUREMENTS_SIZE];
	memset(buff,0,ADCS_POWER_AND_TEMPERATURE_MEASUREMENTS_SIZE);
	uint8_t cmd = ADCS_POWER_AND_TEMPERATURE_MEASUREMENTS_ID;
	adcs_get(cmd,buff,ADCS_POWER_AND_TEMPERATURE_MEASUREMENTS_SIZE);
	memcpy(&adcs->power_and_temperature_measurements,buff,ADCS_POWER_AND_TEMPERATURE_MEASUREMENTS_SIZE);
}

void adcs_getframe_adcs_state (adcs_t *adcs)
{
	uint8_t buff[ADCS_ADCS_STATE_SIZE];
	memset(buff,0,ADCS_ADCS_STATE_SIZE);
	uint8_t cmd = ADCS_ADCS_STATE_ID;
	adcs_get(cmd,buff,ADCS_ADCS_STATE_SIZE);
	memcpy(&adcs->adcs_state,buff,ADCS_ADCS_STATE_SIZE);
}

/**
 * @memberof adcs_t
 * @brief
 * 		Command over adcs' detumbling unit.
 * @details
 * 		Command over adcs' detumbling unit.
 * @attention
 * 		No other setup has to be done. and adcs struct can be used at any point
 * 		to command the adcs' detumbling unit. Regardless of what the adcs is/was
 * 		doing and what initialization has been done to it thus far.
 * @param command
 * 		The command to perform.
 * @returns
 * 		<b>true</b> on successful delivery of command to adcs.
 * 		<br><b>false</b> on failure to send command to adcs.
 */
static bool_t detumble_command( adcs_t* self, adcs_detumbe_command_enum_t command )
{
	DEV_ASSERT( self );

#error "No implementation exists for adcs_t::detumble_command"
	/* TODO: Implement this method. */
	return true;
}

void adcs_getframe_adcs_measurements (adcs_t *adcs)
{
	uint8_t buff[ADCS_ADCS_MEASUREMENTS_SIZE];
	memset(buff,0,ADCS_ADCS_MEASUREMENTS_SIZE);
	uint8_t cmd = ADCS_ADCS_MEASUREMENTS_ID;
	adcs_get(cmd,buff,ADCS_ADCS_MEASUREMENTS_SIZE);
	memcpy(&adcs->adcs_measurements,buff,ADCS_ADCS_MEASUREMENTS_SIZE);
}

void adcs_getframe_current_time (adcs_t *adcs)
{
	uint8_t buff[ADCS_CURRENT_TIME_SIZE];
	memset(buff,0,ADCS_CURRENT_TIME_SIZE);
	uint8_t cmd = ADCS_CURRENT_TIME_ID;
	adcs_get(cmd,buff,ADCS_CURRENT_TIME_SIZE);
	memcpy(&adcs->current_time,buff,ADCS_CURRENT_TIME_SIZE);
}

void adcs_getframe_current_state (adcs_t *adcs)
{
	uint8_t buff[ADCS_CURRENT_STATE_SIZE];
	memset(buff,0,ADCS_CURRENT_STATE_SIZE);
	uint8_t cmd = ADCS_CURRENT_STATE_ID;
	adcs_get(cmd,buff,ADCS_CURRENT_STATE_SIZE);
	memcpy(&adcs->current_state,buff,ADCS_CURRENT_STATE_SIZE);
}

void adcs_getframe_estimated_attitude_angles (adcs_t *adcs)
{
	uint8_t buff[ADCS_ESTIMATED_ATTITUDE_ANGLES_SIZE];
	memset(buff,0,ADCS_ESTIMATED_ATTITUDE_ANGLES_SIZE);
	uint8_t cmd = ADCS_ESTIMATED_ATTITUDE_ANGLES_ID;
	adcs_get(cmd,buff,ADCS_ESTIMATED_ATTITUDE_ANGLES_SIZE);
	memcpy(&adcs->estimated_attitude_angles,buff,ADCS_ESTIMATED_ATTITUDE_ANGLES_SIZE);
}

void adcs_getframe_estimated_angular_rates (adcs_t *adcs)
{
	uint8_t buff[ADCS_ESTIMATED_ANGULAR_RATES_SIZE];
	memset(buff,0,ADCS_ESTIMATED_ANGULAR_RATES_SIZE);
	uint8_t cmd = ADCS_ESTIMATED_ANGULAR_RATES_ID;
	adcs_get(cmd,buff,ADCS_ESTIMATED_ANGULAR_RATES_SIZE);
	memcpy(&adcs->estimated_angular_rates,buff,ADCS_ESTIMATED_ANGULAR_RATES_SIZE);
}

void adcs_getframe_satellite_position_llh (adcs_t *adcs)
{
	uint8_t buff[ADCS_SATELLITE_POSITION_LLH_SIZE];
	memset(buff,0,ADCS_SATELLITE_POSITION_LLH_SIZE);
	uint8_t cmd = ADCS_SATELLITE_POSITION_LLH_ID;
	adcs_get(cmd,buff,ADCS_SATELLITE_POSITION_LLH_SIZE);
	memcpy(&adcs->satellite_position_llh,buff,ADCS_SATELLITE_POSITION_LLH_SIZE);
}

void adcs_getframe_satellite_velocity_eci (adcs_t *adcs)
{
	uint8_t buff[ADCS_SATELLITE_VELOCITY_ECI_SIZE];
	memset(buff,0,ADCS_SATELLITE_VELOCITY_ECI_SIZE);
	uint8_t cmd = ADCS_SATELLITE_VELOCITY_ECI_ID;
	adcs_get(cmd,buff,ADCS_SATELLITE_VELOCITY_ECI_SIZE);
	memcpy(&adcs->satellite_velocity_eci,buff,ADCS_SATELLITE_VELOCITY_ECI_SIZE);
}

void adcs_getframe_magnetic_field_vector (adcs_t *adcs)
{
	uint8_t buff[ADCS_MAGNETIC_FIELD_VECTOR_SIZE];
	memset(buff,0,ADCS_MAGNETIC_FIELD_VECTOR_SIZE);
	uint8_t cmd = ADCS_MAGNETIC_FIELD_VECTOR_ID;
	adcs_get(cmd,buff,ADCS_MAGNETIC_FIELD_VECTOR_SIZE);
	memcpy(&adcs->magnetic_field_vector,buff,ADCS_MAGNETIC_FIELD_VECTOR_SIZE);
}

void adcs_getframe_coarse_sun_vector (adcs_t *adcs)
{
	uint8_t buff[ADCS_COARSE_SUN_VECTOR_SIZE];
	memset(buff,0,ADCS_COARSE_SUN_VECTOR_SIZE);
	uint8_t cmd = ADCS_COARSE_SUN_VECTOR_ID;
	adcs_get(cmd,buff,ADCS_COARSE_SUN_VECTOR_SIZE);
	memcpy(&adcs->coarse_sun_vector,buff,ADCS_COARSE_SUN_VECTOR_SIZE);
}

void adcs_getframe_fine_sun_vector (adcs_t *adcs)
{
	uint8_t buff[ADCS_FINE_SUN_VECTOR_SIZE];
	memset(buff,0,ADCS_FINE_SUN_VECTOR_SIZE);
	uint8_t cmd = ADCS_FINE_SUN_VECTOR_ID;
	adcs_get(cmd,buff,ADCS_FINE_SUN_VECTOR_SIZE);
	memcpy(&adcs->fine_sun_vector,buff,ADCS_FINE_SUN_VECTOR_SIZE);
}

void adcs_getframe_nadir_vector (adcs_t *adcs)
{
	uint8_t buff[ADCS_NADIR_VECTOR_SIZE];
	memset(buff,0,ADCS_NADIR_VECTOR_SIZE);
	uint8_t cmd = ADCS_NADIR_VECTOR_ID;
	adcs_get(cmd,buff,ADCS_NADIR_VECTOR_SIZE);
	memcpy(&adcs->nadir_vector,buff,ADCS_NADIR_VECTOR_SIZE);
}

void adcs_getframe_rate_sensor_rates (adcs_t *adcs)
{
	uint8_t buff[ADCS_RATE_SENSOR_RATES_SIZE];
	memset(buff,0,ADCS_RATE_SENSOR_RATES_SIZE);
	uint8_t cmd = ADCS_RATE_SENSOR_RATES_ID;
	adcs_get(cmd,buff,ADCS_RATE_SENSOR_RATES_SIZE);
	memcpy(&adcs->rate_sensor_rates,buff,ADCS_RATE_SENSOR_RATES_SIZE);
}

void adcs_getframe_wheel_speed (adcs_t *adcs)
{
	uint8_t buff[ADCS_WHEEL_SPEED_SIZE];
	memset(buff,0,ADCS_WHEEL_SPEED_SIZE);
	uint8_t cmd = ADCS_WHEEL_SPEED_ID;
	adcs_get(cmd,buff,ADCS_WHEEL_SPEED_SIZE);
	memcpy(&adcs->wheel_speed,buff,ADCS_WHEEL_SPEED_SIZE);
}

void adcs_getframe_cubesense_current (adcs_t *adcs)
{
	uint8_t buff[ADCS_CUBESENSE_CURRENT_SIZE];
	memset(buff,0,ADCS_CUBESENSE_CURRENT_SIZE);
	uint8_t cmd = ADCS_CUBESENSE_CURRENT_ID;
	adcs_get(cmd,buff,ADCS_CUBESENSE_CURRENT_SIZE);
	memcpy(&adcs->cubesense_current,buff,ADCS_CUBESENSE_CURRENT_SIZE);
}

void adcs_getframe_cubecontrol_current (adcs_t *adcs)
{
	uint8_t buff[ADCS_CUBECONTROL_CURRENT_SIZE];
	memset(buff,0,ADCS_CUBECONTROL_CURRENT_SIZE);
	uint8_t cmd = ADCS_CUBECONTROL_CURRENT_ID;
	adcs_get(cmd,buff,ADCS_CUBECONTROL_CURRENT_SIZE);
	memcpy(&adcs->cubecontrol_current,buff,ADCS_CUBECONTROL_CURRENT_SIZE);
}

void adcs_getframe_peripheral_current_and_temperature_measurements (adcs_t *adcs)
{
	uint8_t buff[ADCS_PERIPHERAL_CURRENT_AND_TEMPERATURE_MEASUREMENTS_SIZE];
	memset(buff,0,ADCS_PERIPHERAL_CURRENT_AND_TEMPERATURE_MEASUREMENTS_SIZE);
	uint8_t cmd = ADCS_PERIPHERAL_CURRENT_AND_TEMPERATURE_MEASUREMENTS_ID;
	adcs_get(cmd,buff,ADCS_PERIPHERAL_CURRENT_AND_TEMPERATURE_MEASUREMENTS_SIZE);
	memcpy(&adcs->peripheral_current_and_temperature_measurements,buff,ADCS_PERIPHERAL_CURRENT_AND_TEMPERATURE_MEASUREMENTS_SIZE);
}

void adcs_getframe_raw_sensor_measurements (adcs_t *adcs)
{
	uint8_t buff[ADCS_RAW_SENSOR_MEASUREMENTS_SIZE];
	memset(buff,0,ADCS_RAW_SENSOR_MEASUREMENTS_SIZE);
	uint8_t cmd = ADCS_RAW_SENSOR_MEASUREMENTS_ID;
	adcs_get(cmd,buff,ADCS_RAW_SENSOR_MEASUREMENTS_SIZE);
	memcpy(&adcs->raw_sensor_measurements,buff,ADCS_RAW_SENSOR_MEASUREMENTS_SIZE);
}

void adcs_getframe_angular_rate_covariance (adcs_t *adcs)
{
	uint8_t buff[ADCS_ANGULAR_RATE_COVARIANCE_SIZE];
	memset(buff,0,ADCS_ANGULAR_RATE_COVARIANCE_SIZE);
	uint8_t cmd = ADCS_ANGULAR_RATE_COVARIANCE_ID;
	adcs_get(cmd,buff,ADCS_ANGULAR_RATE_COVARIANCE_SIZE);
	memcpy(&adcs->angular_rate_covariance,buff,ADCS_ANGULAR_RATE_COVARIANCE_SIZE);
}

void adcs_getframe_raw_nadir_sensor (adcs_t *adcs)
{
	uint8_t buff[ADCS_RAW_NADIR_SENSOR_SIZE];
	memset(buff,0,ADCS_RAW_NADIR_SENSOR_SIZE);
	uint8_t cmd = ADCS_RAW_NADIR_SENSOR_ID;
	adcs_get(cmd,buff,ADCS_RAW_NADIR_SENSOR_SIZE);
	memcpy(&adcs->raw_nadir_sensor,buff,ADCS_RAW_NADIR_SENSOR_SIZE);
}

void adcs_getframe_raw_sun_sensor (adcs_t *adcs)
{
	uint8_t buff[ADCS_RAW_SUN_SENSOR_SIZE];
	memset(buff,0,ADCS_RAW_SUN_SENSOR_SIZE);
	uint8_t cmd = ADCS_RAW_SUN_SENSOR_ID;
	adcs_get(cmd,buff,ADCS_RAW_SUN_SENSOR_SIZE);
	memcpy(&adcs->raw_sun_sensor,buff,ADCS_RAW_SUN_SENSOR_SIZE);
}

void adcs_getframe_raw_css (adcs_t *adcs)
{
	uint8_t buff[ADCS_RAW_CSS_SIZE];
	memset(buff,0,ADCS_RAW_CSS_SIZE);
	uint8_t cmd = ADCS_RAW_CSS_ID;
	adcs_get(cmd,buff,ADCS_RAW_CSS_SIZE);
	memcpy(&adcs->raw_css,buff,ADCS_RAW_CSS_SIZE);
}

void adcs_getframe_raw_magnetometer (adcs_t *adcs)
{
	uint8_t buff[ADCS_RAW_MAGNETOMETER_SIZE];
	memset(buff,0,ADCS_RAW_MAGNETOMETER_SIZE);
	uint8_t cmd = ADCS_RAW_MAGNETOMETER_ID;
	adcs_get(cmd,buff,ADCS_RAW_MAGNETOMETER_SIZE);
	memcpy(&adcs->raw_magnetometer,buff,ADCS_RAW_MAGNETOMETER_SIZE);
}

void adcs_getframe_raw_gps_status (adcs_t *adcs)
{
	uint8_t buff[ADCS_RAW_GPS_STATUS_SIZE];
	memset(buff,0,ADCS_RAW_GPS_STATUS_SIZE);
	uint8_t cmd = ADCS_RAW_GPS_STATUS_ID;
	adcs_get(cmd,buff,ADCS_RAW_GPS_STATUS_SIZE);
	memcpy(&adcs->raw_gps_status,buff,ADCS_RAW_GPS_STATUS_SIZE);
}

void adcs_getframe_raw_gps_time (adcs_t *adcs)
{
	uint8_t buff[ADCS_RAW_GPS_TIME_SIZE];
	memset(buff,0,ADCS_RAW_GPS_TIME_SIZE);
	uint8_t cmd = ADCS_RAW_GPS_TIME_ID;
	adcs_get(cmd,buff,ADCS_RAW_GPS_TIME_SIZE);
	memcpy(&adcs->raw_gps_time,buff,ADCS_RAW_GPS_TIME_SIZE);
}

void adcs_getframe_raw_gps_x (adcs_t *adcs)
{
	uint8_t buff[ADCS_RAW_GPS_X_SIZE];
	memset(buff,0,ADCS_RAW_GPS_X_SIZE);
	uint8_t cmd = ADCS_RAW_GPS_X_ID;
	adcs_get(cmd,buff,ADCS_RAW_GPS_X_SIZE);
	memcpy(&adcs->raw_gps_x,buff,ADCS_RAW_GPS_X_SIZE);
}

void adcs_getframe_raw_gps_y (adcs_t *adcs)
{
	uint8_t buff[ADCS_RAW_GPS_Y_SIZE];
	memset(buff,0,ADCS_RAW_GPS_Y_SIZE);
	uint8_t cmd = ADCS_RAW_GPS_Y_ID;
	adcs_get(cmd,buff,ADCS_RAW_GPS_Y_SIZE);
	memcpy(&adcs->raw_gps_y,buff,ADCS_RAW_GPS_Y_SIZE);
}

void adcs_getframe_raw_gps_z (adcs_t *adcs)
{
	uint8_t buff[ADCS_RAW_GPS_Z_SIZE];
	memset(buff,0,ADCS_RAW_GPS_Z_SIZE);
	uint8_t cmd = ADCS_RAW_GPS_Z_ID;
	adcs_get(cmd,buff,ADCS_RAW_GPS_Z_SIZE);
	memcpy(&adcs->raw_gps_z,buff,ADCS_RAW_GPS_Z_SIZE);
}

void adcs_getframe_estimation_data (adcs_t *adcs)
{
	uint8_t buff[ADCS_ESTIMATION_DATA_SIZE];
	memset(buff,0,ADCS_ESTIMATION_DATA_SIZE);
	uint8_t cmd = ADCS_ESTIMATION_DATA_ID;
	adcs_get(cmd,buff,ADCS_ESTIMATION_DATA_SIZE);
	memcpy(&adcs->estimation_data,buff,ADCS_ESTIMATION_DATA_SIZE);
}

void adcs_getframe_igrf_modelled_magnetic_field_vector (adcs_t *adcs)
{
	uint8_t buff[ADCS_IGRF_MODELLED_MAGNETIC_FIELD_VECTOR_SIZE];
	memset(buff,0,ADCS_IGRF_MODELLED_MAGNETIC_FIELD_VECTOR_SIZE);
	uint8_t cmd = ADCS_IGRF_MODELLED_MAGNETIC_FIELD_VECTOR_ID;
	adcs_get(cmd,buff,ADCS_IGRF_MODELLED_MAGNETIC_FIELD_VECTOR_SIZE);
	memcpy(&adcs->igrf_modelled_magnetic_field_vector,buff,ADCS_IGRF_MODELLED_MAGNETIC_FIELD_VECTOR_SIZE);
}

void adcs_getframe_modelled_sun_vector (adcs_t *adcs)
{
	uint8_t buff[ADCS_MODELLED_SUN_VECTOR_SIZE];
	memset(buff,0,ADCS_MODELLED_SUN_VECTOR_SIZE);
	uint8_t cmd = ADCS_MODELLED_SUN_VECTOR_ID;
	adcs_get(cmd,buff,ADCS_MODELLED_SUN_VECTOR_SIZE);
	memcpy(&adcs->modelled_sun_vector,buff,ADCS_MODELLED_SUN_VECTOR_SIZE);
}

void adcs_getframe_estimated_gyro_bias (adcs_t *adcs)
{
	uint8_t buff[ADCS_ESTIMATED_GYRO_BIAS_SIZE];
	memset(buff,0,ADCS_ESTIMATED_GYRO_BIAS_SIZE);
	uint8_t cmd = ADCS_ESTIMATED_GYRO_BIAS_ID;
	adcs_get(cmd,buff,ADCS_ESTIMATED_GYRO_BIAS_SIZE);
	memcpy(&adcs->estimated_gyro_bias,buff,ADCS_ESTIMATED_GYRO_BIAS_SIZE);
}

void adcs_getframe_estimation_innovation_vector (adcs_t *adcs)
{
	uint8_t buff[ADCS_ESTIMATION_INNOVATION_VECTOR_SIZE];
	memset(buff,0,ADCS_ESTIMATION_INNOVATION_VECTOR_SIZE);
	uint8_t cmd = ADCS_ESTIMATION_INNOVATION_VECTOR_ID;
	adcs_get(cmd,buff,ADCS_ESTIMATION_INNOVATION_VECTOR_SIZE);
	memcpy(&adcs->estimation_innovation_vector,buff,ADCS_ESTIMATION_INNOVATION_VECTOR_SIZE);
}

void adcs_getframe_quaternion_error_vector (adcs_t *adcs)
{
	uint8_t buff[ADCS_QUATERNION_ERROR_VECTOR_SIZE];
	memset(buff,0,ADCS_QUATERNION_ERROR_VECTOR_SIZE);
	uint8_t cmd = ADCS_QUATERNION_ERROR_VECTOR_ID;
	adcs_get(cmd,buff,ADCS_QUATERNION_ERROR_VECTOR_SIZE);
	memcpy(&adcs->quaternion_error_vector,buff,ADCS_QUATERNION_ERROR_VECTOR_SIZE);
}

void adcs_getframe_quaternion_covariance (adcs_t *adcs)
{
	uint8_t buff[ADCS_QUATERNION_COVARIANCE_SIZE];
	memset(buff,0,ADCS_QUATERNION_COVARIANCE_SIZE);
	uint8_t cmd = ADCS_QUATERNION_COVARIANCE_ID;
	adcs_get(cmd,buff,ADCS_QUATERNION_COVARIANCE_SIZE);
	memcpy(&adcs->quaternion_covariance,buff,ADCS_QUATERNION_COVARIANCE_SIZE);
}

void adcs_getframe_magnetorquer_command (adcs_t *adcs)
{
	uint8_t buff[ADCS_MAGNETORQUER_COMMAND_SIZE];
	memset(buff,0,ADCS_MAGNETORQUER_COMMAND_SIZE);
	uint8_t cmd = ADCS_MAGNETORQUER_COMMAND_ID;
	adcs_get(cmd,buff,ADCS_MAGNETORQUER_COMMAND_SIZE);
	memcpy(&adcs->magnetorquer_command,buff,ADCS_MAGNETORQUER_COMMAND_SIZE);
}

void adcs_getframe_wheel_speed_commands (adcs_t *adcs)
{
	uint8_t buff[ADCS_WHEEL_SPEED_COMMANDS_SIZE];
	memset(buff,0,ADCS_WHEEL_SPEED_COMMANDS_SIZE);
	uint8_t cmd = ADCS_WHEEL_SPEED_COMMANDS_ID;
	adcs_get(cmd,buff,ADCS_WHEEL_SPEED_COMMANDS_SIZE);
	memcpy(&adcs->wheel_speed_commands,buff,ADCS_WHEEL_SPEED_COMMANDS_SIZE);
}

void adcs_getframe_sgp4_orbit_parameters (adcs_t *adcs)
{
	uint8_t buff[ADCS_SGP4_ORBIT_PARAMETERS_SIZE];
	memset(buff,0,ADCS_SGP4_ORBIT_PARAMETERS_SIZE);
	uint8_t cmd = ADCS_SGP4_ORBIT_PARAMETERS_ID;
	adcs_get(cmd,buff,ADCS_SGP4_ORBIT_PARAMETERS_SIZE);
	memcpy(&adcs->sgp4_orbit_parameters,buff,ADCS_SGP4_ORBIT_PARAMETERS_SIZE);
}

void adcs_getframe_configuration (adcs_t *adcs)
{
	int i;
	uint8_t buff[ADCS_CONFIGURATION_SIZE];
	memset(buff,0,ADCS_CONFIGURATION_SIZE);
	uint8_t cmd = ADCS_CONFIGURATION_ID;
	adcs_get(cmd,buff,ADCS_CONFIGURATION_SIZE);
	memcpy(&adcs->configuration,buff,ADCS_CONFIGURATION_SIZE);
}

void adcs_getframe_status_of_image_capture_and_save_operation (adcs_t *adcs)
{
	uint8_t buff[ADCS_STATUS_OF_IMAGE_CAPTURE_AND_SAVE_OPERATION_SIZE];
	memset(buff,0,ADCS_STATUS_OF_IMAGE_CAPTURE_AND_SAVE_OPERATION_SIZE);
	uint8_t cmd = ADCS_STATUS_OF_IMAGE_CAPTURE_AND_SAVE_OPERATION_ID;
	adcs_get(cmd,buff,ADCS_STATUS_OF_IMAGE_CAPTURE_AND_SAVE_OPERATION_SIZE);
	memcpy(&adcs->status_of_image_capture_and_save_operation,buff,ADCS_STATUS_OF_IMAGE_CAPTURE_AND_SAVE_OPERATION_SIZE);
}

void adcs_getframe_uploaded_program_status (adcs_t *adcs)
{
	uint8_t buff[ADCS_UPLOADED_PROGRAM_STATUS_SIZE];
	memset(buff,0,ADCS_UPLOADED_PROGRAM_STATUS_SIZE);
	uint8_t cmd = ADCS_UPLOADED_PROGRAM_STATUS_ID;
	adcs_get(cmd,buff,ADCS_UPLOADED_PROGRAM_STATUS_SIZE);
	memcpy(&adcs->uploaded_program_status,buff,ADCS_UPLOADED_PROGRAM_STATUS_SIZE);
}

void adcs_getframe_get_flash_program_list (adcs_t *adcs)
{
	uint8_t buff[ADCS_GET_FLASH_PROGRAM_LIST_SIZE];
	memset(buff,0,ADCS_GET_FLASH_PROGRAM_LIST_SIZE);
	uint8_t cmd = ADCS_GET_FLASH_PROGRAM_LIST_ID;
	adcs_get(cmd,buff,ADCS_GET_FLASH_PROGRAM_LIST_SIZE);
	memcpy(&adcs->get_flash_program_list,buff,ADCS_GET_FLASH_PROGRAM_LIST_SIZE);
}

void adcs_getframe_reset (adcs_t *adcs)
{
	uint8_t buff[ADCS_RESET_SIZE];
	memset(buff,0,ADCS_RESET_SIZE);
	uint8_t cmd = ADCS_RESET_ID;
	adcs_get(cmd,buff,ADCS_RESET_SIZE);
	memcpy(&adcs->reset,buff,ADCS_RESET_SIZE);
}

void adcs_getframe_set_unix_time (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_UNIX_TIME_SIZE];
	memset(buff,0,ADCS_SET_UNIX_TIME_SIZE);
	uint8_t cmd = ADCS_SET_UNIX_TIME_ID;
	adcs_get(cmd,buff,ADCS_SET_UNIX_TIME_SIZE);
	memcpy(&adcs->set_unix_time,buff,ADCS_SET_UNIX_TIME_SIZE);
}

void adcs_getframe_adcs_run_mode (adcs_t *adcs)
{
	uint8_t buff[ADCS_ADCS_RUN_MODE_SIZE];
	memset(buff,0,ADCS_ADCS_RUN_MODE_SIZE);
	uint8_t cmd = ADCS_ADCS_RUN_MODE_ID;
	adcs_get(cmd,buff,ADCS_ADCS_RUN_MODE_SIZE);
	memcpy(&adcs->adcs_run_mode,buff,ADCS_ADCS_RUN_MODE_SIZE);
}

void adcs_getframe_selected_logged_data (adcs_t *adcs)
{
	uint8_t buff[ADCS_SELECTED_LOGGED_DATA_SIZE];
	memset(buff,0,ADCS_SELECTED_LOGGED_DATA_SIZE);
	uint8_t cmd = ADCS_SELECTED_LOGGED_DATA_ID;
	adcs_get(cmd,buff,ADCS_SELECTED_LOGGED_DATA_SIZE);
	memcpy(&adcs->selected_logged_data,buff,ADCS_SELECTED_LOGGED_DATA_SIZE);
}

void adcs_getframe_power_control (adcs_t *adcs)
{
	uint8_t buff[ADCS_POWER_CONTROL_SIZE];
	memset(buff,0,ADCS_POWER_CONTROL_SIZE);
	uint8_t cmd = ADCS_POWER_CONTROL_ID;
	adcs_get(cmd,buff,ADCS_POWER_CONTROL_SIZE);
	memcpy(&adcs->power_control,buff,ADCS_POWER_CONTROL_SIZE);
}

void adcs_getframe_deploy_magnetometer_boom (adcs_t *adcs)
{
	uint8_t buff[ADCS_DEPLOY_MAGNETOMETER_BOOM_SIZE];
	memset(buff,0,ADCS_DEPLOY_MAGNETOMETER_BOOM_SIZE);
	uint8_t cmd = ADCS_DEPLOY_MAGNETOMETER_BOOM_ID;
	adcs_get(cmd,buff,ADCS_DEPLOY_MAGNETOMETER_BOOM_SIZE);
	memcpy(&adcs->deploy_magnetometer_boom,buff,ADCS_DEPLOY_MAGNETOMETER_BOOM_SIZE);
}

void adcs_getframe_trigger_adcs_loop (adcs_t *adcs)
{
	uint8_t buff[ADCS_TRIGGER_ADCS_LOOP_SIZE];
	memset(buff,0,ADCS_TRIGGER_ADCS_LOOP_SIZE);
	uint8_t cmd = ADCS_TRIGGER_ADCS_LOOP_ID;
	adcs_get(cmd,buff,ADCS_TRIGGER_ADCS_LOOP_SIZE);
	memcpy(&adcs->trigger_adcs_loop,buff,ADCS_TRIGGER_ADCS_LOOP_SIZE);
}

void adcs_getframe_set_attitude_estimation_mode (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_ATTITUDE_ESTIMATION_MODE_SIZE];
	memset(buff,0,ADCS_SET_ATTITUDE_ESTIMATION_MODE_SIZE);
	uint8_t cmd = ADCS_SET_ATTITUDE_ESTIMATION_MODE_ID;
	adcs_get(cmd,buff,ADCS_SET_ATTITUDE_ESTIMATION_MODE_SIZE);
	memcpy(&adcs->set_attitude_estimation_mode,buff,ADCS_SET_ATTITUDE_ESTIMATION_MODE_SIZE);
}

void adcs_getframe_set_attitude_control_mode (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_ATTITUDE_CONTROL_MODE_SIZE];
	memset(buff,0,ADCS_SET_ATTITUDE_CONTROL_MODE_SIZE);
	uint8_t cmd = ADCS_SET_ATTITUDE_CONTROL_MODE_ID;
	adcs_get(cmd,buff,ADCS_SET_ATTITUDE_CONTROL_MODE_SIZE);
	memcpy(&adcs->set_attitude_control_mode,buff,ADCS_SET_ATTITUDE_CONTROL_MODE_SIZE);
}

void adcs_getframe_set_commanded_attitude_angles (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_COMMANDED_ATTITUDE_ANGLES_SIZE];
	memset(buff,0,ADCS_SET_COMMANDED_ATTITUDE_ANGLES_SIZE);
	uint8_t cmd = ADCS_SET_COMMANDED_ATTITUDE_ANGLES_ID;
	adcs_get(cmd,buff,ADCS_SET_COMMANDED_ATTITUDE_ANGLES_SIZE);
	memcpy(&adcs->set_commanded_attitude_angles,buff,ADCS_SET_COMMANDED_ATTITUDE_ANGLES_SIZE);
}

void adcs_getframe_set_wheel (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_WHEEL_SIZE];
	memset(buff,0,ADCS_SET_WHEEL_SIZE);
	uint8_t cmd = ADCS_SET_WHEEL_ID;
	adcs_get(cmd,buff,ADCS_SET_WHEEL_SIZE);
	memcpy(&adcs->set_wheel,buff,ADCS_SET_WHEEL_SIZE);
}

void adcs_getframe_set_magnetorquer_output (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_MAGNETORQUER_OUTPUT_SIZE];
	memset(buff,0,ADCS_SET_MAGNETORQUER_OUTPUT_SIZE);
	uint8_t cmd = ADCS_SET_MAGNETORQUER_OUTPUT_ID;
	adcs_get(cmd,buff,ADCS_SET_MAGNETORQUER_OUTPUT_SIZE);
	memcpy(&adcs->set_magnetorquer_output,buff,ADCS_SET_MAGNETORQUER_OUTPUT_SIZE);
}

void adcs_getframe_set_start_up_mode (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_START_UP_MODE_SIZE];
	memset(buff,0,ADCS_SET_START_UP_MODE_SIZE);
	uint8_t cmd = ADCS_SET_START_UP_MODE_ID;
	adcs_get(cmd,buff,ADCS_SET_START_UP_MODE_SIZE);
	memcpy(&adcs->set_start_up_mode,buff,ADCS_SET_START_UP_MODE_SIZE);
}

void adcs_getframe_set_sgp4_orbit_parameters (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_SGP4_ORBIT_PARAMETERS_SIZE];
	memset(buff,0,ADCS_SET_SGP4_ORBIT_PARAMETERS_SIZE);
	uint8_t cmd = ADCS_SET_SGP4_ORBIT_PARAMETERS_ID;
	adcs_get(cmd,buff,ADCS_SET_SGP4_ORBIT_PARAMETERS_SIZE);
	memcpy(&adcs->set_sgp4_orbit_parameters,buff,ADCS_SET_SGP4_ORBIT_PARAMETERS_SIZE);
}

void adcs_getframe_set_configuration (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_CONFIGURATION_SIZE];
	memset(buff,0,ADCS_SET_CONFIGURATION_SIZE);
	uint8_t cmd = ADCS_SET_CONFIGURATION_ID;
	adcs_get(cmd,buff,ADCS_SET_CONFIGURATION_SIZE);
	memcpy(&adcs->set_configuration,buff,ADCS_SET_CONFIGURATION_SIZE);
}

void adcs_getframe_set_magnetorquer (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_MAGNETORQUER_SIZE];
	memset(buff,0,ADCS_SET_MAGNETORQUER_SIZE);
	uint8_t cmd = ADCS_SET_MAGNETORQUER_ID;
	adcs_get(cmd,buff,ADCS_SET_MAGNETORQUER_SIZE);
	memcpy(&adcs->set_magnetorquer,buff,ADCS_SET_MAGNETORQUER_SIZE);
}

void adcs_getframe_set_wheel_configuration (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_WHEEL_CONFIGURATION_SIZE];
	memset(buff,0,ADCS_SET_WHEEL_CONFIGURATION_SIZE);
	uint8_t cmd = ADCS_SET_WHEEL_CONFIGURATION_ID;
	adcs_get(cmd,buff,ADCS_SET_WHEEL_CONFIGURATION_SIZE);
	memcpy(&adcs->set_wheel_configuration,buff,ADCS_SET_WHEEL_CONFIGURATION_SIZE);
}

void adcs_getframe_set_css_configuration (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_CSS_CONFIGURATION_SIZE];
	memset(buff,0,ADCS_SET_CSS_CONFIGURATION_SIZE);
	uint8_t cmd = ADCS_SET_CSS_CONFIGURATION_ID;
	adcs_get(cmd,buff,ADCS_SET_CSS_CONFIGURATION_SIZE);
	memcpy(&adcs->set_css_configuration,buff,ADCS_SET_CSS_CONFIGURATION_SIZE);
}

void adcs_getframe_set_sun_sensor_configuration (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_SUN_SENSOR_CONFIGURATION_SIZE];
	memset(buff,0,ADCS_SET_SUN_SENSOR_CONFIGURATION_SIZE);
	uint8_t cmd = ADCS_SET_SUN_SENSOR_CONFIGURATION_ID;
	adcs_get(cmd,buff,ADCS_SET_SUN_SENSOR_CONFIGURATION_SIZE);
	memcpy(&adcs->set_sun_sensor_configuration,buff,ADCS_SET_SUN_SENSOR_CONFIGURATION_SIZE);
}

void adcs_getframe_set_nadir_sensor_configuration (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_NADIR_SENSOR_CONFIGURATION_SIZE];
	memset(buff,0,ADCS_SET_NADIR_SENSOR_CONFIGURATION_SIZE);
	uint8_t cmd = ADCS_SET_NADIR_SENSOR_CONFIGURATION_ID;
	adcs_get(cmd,buff,ADCS_SET_NADIR_SENSOR_CONFIGURATION_SIZE);
	memcpy(&adcs->set_nadir_sensor_configuration,buff,ADCS_SET_NADIR_SENSOR_CONFIGURATION_SIZE);
}

void adcs_getframe_set_magnetometer_configuration (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_MAGNETOMETER_CONFIGURATION_SIZE];
	memset(buff,0,ADCS_SET_MAGNETOMETER_CONFIGURATION_SIZE);
	uint8_t cmd = ADCS_SET_MAGNETOMETER_CONFIGURATION_ID;
	adcs_get(cmd,buff,ADCS_SET_MAGNETOMETER_CONFIGURATION_SIZE);
	memcpy(&adcs->set_magnetometer_configuration,buff,ADCS_SET_MAGNETOMETER_CONFIGURATION_SIZE);
}

void adcs_getframe_set_rate_sensor_configuration (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_RATE_SENSOR_CONFIGURATION_SIZE];
	memset(buff,0,ADCS_SET_RATE_SENSOR_CONFIGURATION_SIZE);
	uint8_t cmd = ADCS_SET_RATE_SENSOR_CONFIGURATION_ID;
	adcs_get(cmd,buff,ADCS_SET_RATE_SENSOR_CONFIGURATION_SIZE);
	memcpy(&adcs->set_rate_sensor_configuration,buff,ADCS_SET_RATE_SENSOR_CONFIGURATION_SIZE);
}

void adcs_getframe_set_detumbling_control_parameters (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_DETUMBLING_CONTROL_PARAMETERS_SIZE];
	memset(buff,0,ADCS_SET_DETUMBLING_CONTROL_PARAMETERS_SIZE);
	uint8_t cmd = ADCS_SET_DETUMBLING_CONTROL_PARAMETERS_ID;
	adcs_get(cmd,buff,ADCS_SET_DETUMBLING_CONTROL_PARAMETERS_SIZE);
	memcpy(&adcs->set_detumbling_control_parameters,buff,ADCS_SET_DETUMBLING_CONTROL_PARAMETERS_SIZE);
}

void adcs_getframe_set_y_momentum_control_parameters (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_Y_MOMENTUM_CONTROL_PARAMETERS_SIZE];
	memset(buff,0,ADCS_SET_Y_MOMENTUM_CONTROL_PARAMETERS_SIZE);
	uint8_t cmd = ADCS_SET_Y_MOMENTUM_CONTROL_PARAMETERS_ID;
	adcs_get(cmd,buff,ADCS_SET_Y_MOMENTUM_CONTROL_PARAMETERS_SIZE);
	memcpy(&adcs->set_y_momentum_control_parameters,buff,ADCS_SET_Y_MOMENTUM_CONTROL_PARAMETERS_SIZE);
}

void adcs_getframe_set_moment_of_inertia (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_MOMENT_OF_INERTIA_SIZE];
	memset(buff,0,ADCS_SET_MOMENT_OF_INERTIA_SIZE);
	uint8_t cmd = ADCS_SET_MOMENT_OF_INERTIA_ID;
	adcs_get(cmd,buff,ADCS_SET_MOMENT_OF_INERTIA_SIZE);
	memcpy(&adcs->set_moment_of_inertia,buff,ADCS_SET_MOMENT_OF_INERTIA_SIZE);
}

void adcs_getframe_set_estimation_parameters (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_ESTIMATION_PARAMETERS_SIZE];
	memset(buff,0,ADCS_SET_ESTIMATION_PARAMETERS_SIZE);
	uint8_t cmd = ADCS_SET_ESTIMATION_PARAMETERS_ID;
	adcs_get(cmd,buff,ADCS_SET_ESTIMATION_PARAMETERS_SIZE);
	memcpy(&adcs->set_estimation_parameters,buff,ADCS_SET_ESTIMATION_PARAMETERS_SIZE);
}

void adcs_getframe_save_configuration (adcs_t *adcs)
{
	uint8_t buff[ADCS_SAVE_CONFIGURATION_SIZE];
	memset(buff,0,ADCS_SAVE_CONFIGURATION_SIZE);
	uint8_t cmd = ADCS_SAVE_CONFIGURATION_ID;
	adcs_get(cmd,buff,ADCS_SAVE_CONFIGURATION_SIZE);
	memcpy(&adcs->save_configuration,buff,ADCS_SAVE_CONFIGURATION_SIZE);
}

void adcs_getframe_save_orbit_parameters (adcs_t *adcs)
{
	uint8_t buff[ADCS_SAVE_ORBIT_PARAMETERS_SIZE];
	memset(buff,0,ADCS_SAVE_ORBIT_PARAMETERS_SIZE);
	uint8_t cmd = ADCS_SAVE_ORBIT_PARAMETERS_ID;
	adcs_get(cmd,buff,ADCS_SAVE_ORBIT_PARAMETERS_SIZE);
	memcpy(&adcs->save_orbit_parameters,buff,ADCS_SAVE_ORBIT_PARAMETERS_SIZE);
}

void adcs_getframe_capture_and_save_image (adcs_t *adcs)
{
	uint8_t buff[ADCS_CAPTURE_AND_SAVE_IMAGE_SIZE];
	memset(buff,0,ADCS_CAPTURE_AND_SAVE_IMAGE_SIZE);
	uint8_t cmd = ADCS_CAPTURE_AND_SAVE_IMAGE_ID;
	adcs_get(cmd,buff,ADCS_CAPTURE_AND_SAVE_IMAGE_SIZE);
	memcpy(&adcs->capture_and_save_image,buff,ADCS_CAPTURE_AND_SAVE_IMAGE_SIZE);
}

void adcs_getframe_reset_file_list (adcs_t *adcs)
{
	uint8_t buff[ADCS_RESET_FILE_LIST_SIZE];
	memset(buff,0,ADCS_RESET_FILE_LIST_SIZE);
	uint8_t cmd = ADCS_RESET_FILE_LIST_ID;
	adcs_get(cmd,buff,ADCS_RESET_FILE_LIST_SIZE);
	memcpy(&adcs->reset_file_list,buff,ADCS_RESET_FILE_LIST_SIZE);
}

void adcs_getframe_advance_file_list_index (adcs_t *adcs)
{
	uint8_t buff[ADCS_ADVANCE_FILE_LIST_INDEX_SIZE];
	memset(buff,0,ADCS_ADVANCE_FILE_LIST_INDEX_SIZE);
	uint8_t cmd = ADCS_ADVANCE_FILE_LIST_INDEX_ID;
	adcs_get(cmd,buff,ADCS_ADVANCE_FILE_LIST_INDEX_SIZE);
	memcpy(&adcs->advance_file_list_index,buff,ADCS_ADVANCE_FILE_LIST_INDEX_SIZE);
}

void adcs_getframe_initialize_file_download (adcs_t *adcs)
{
	uint8_t buff[ADCS_INITIALIZE_FILE_DOWNLOAD_SIZE];
	memset(buff,0,ADCS_INITIALIZE_FILE_DOWNLOAD_SIZE);
	uint8_t cmd = ADCS_INITIALIZE_FILE_DOWNLOAD_ID;
	adcs_get(cmd,buff,ADCS_INITIALIZE_FILE_DOWNLOAD_SIZE);
	memcpy(&adcs->initialize_file_download,buff,ADCS_INITIALIZE_FILE_DOWNLOAD_SIZE);
}

void adcs_getframe_advance_file_read_pointer (adcs_t *adcs)
{
	uint8_t buff[ADCS_ADVANCE_FILE_READ_POINTER_SIZE];
	memset(buff,0,ADCS_ADVANCE_FILE_READ_POINTER_SIZE);
	uint8_t cmd = ADCS_ADVANCE_FILE_READ_POINTER_ID;
	adcs_get(cmd,buff,ADCS_ADVANCE_FILE_READ_POINTER_SIZE);
	memcpy(&adcs->advance_file_read_pointer,buff,ADCS_ADVANCE_FILE_READ_POINTER_SIZE);
}

void adcs_getframe_erase_file (adcs_t *adcs)
{
	uint8_t buff[ADCS_ERASE_FILE_SIZE];
	memset(buff,0,ADCS_ERASE_FILE_SIZE);
	uint8_t cmd = ADCS_ERASE_FILE_ID;
	adcs_get(cmd,buff,ADCS_ERASE_FILE_SIZE);
	memcpy(&adcs->erase_file,buff,ADCS_ERASE_FILE_SIZE);
}

void adcs_getframe_erase_all_files (adcs_t *adcs)
{
	uint8_t buff[ADCS_ERASE_ALL_FILES_SIZE];
	memset(buff,0,ADCS_ERASE_ALL_FILES_SIZE);
	uint8_t cmd = ADCS_ERASE_ALL_FILES_ID;
	adcs_get(cmd,buff,ADCS_ERASE_ALL_FILES_SIZE);
	memcpy(&adcs->erase_all_files,buff,ADCS_ERASE_ALL_FILES_SIZE);
}

void adcs_getframe_set_boot_index (adcs_t *adcs)
{
	uint8_t buff[ADCS_SET_BOOT_INDEX_SIZE];
	memset(buff,0,ADCS_SET_BOOT_INDEX_SIZE);
	uint8_t cmd = ADCS_SET_BOOT_INDEX_ID;
	adcs_get(cmd,buff,ADCS_SET_BOOT_INDEX_SIZE);
	memcpy(&adcs->set_boot_index,buff,ADCS_SET_BOOT_INDEX_SIZE);
}

void adcs_getframe_erase_program (adcs_t *adcs)
{
	uint8_t buff[ADCS_ERASE_PROGRAM_SIZE];
	memset(buff,0,ADCS_ERASE_PROGRAM_SIZE);
	uint8_t cmd = ADCS_ERASE_PROGRAM_ID;
	adcs_get(cmd,buff,ADCS_ERASE_PROGRAM_SIZE);
	memcpy(&adcs->erase_program,buff,ADCS_ERASE_PROGRAM_SIZE);
}

void adcs_getframe_upload_program_block (adcs_t *adcs)
{
	uint8_t buff[ADCS_UPLOAD_PROGRAM_BLOCK_SIZE];
	memset(buff,0,ADCS_UPLOAD_PROGRAM_BLOCK_SIZE);
	uint8_t cmd = ADCS_UPLOAD_PROGRAM_BLOCK_ID;
	adcs_get(cmd,buff,ADCS_UPLOAD_PROGRAM_BLOCK_SIZE);
	memcpy(&adcs->upload_program_block,buff,ADCS_UPLOAD_PROGRAM_BLOCK_SIZE);
}

void adcs_getframe_finalize_program_upload (adcs_t *adcs)
{
	uint8_t buff[ADCS_FINALIZE_PROGRAM_UPLOAD_SIZE];
	memset(buff,0,ADCS_FINALIZE_PROGRAM_UPLOAD_SIZE);
	uint8_t cmd = ADCS_FINALIZE_PROGRAM_UPLOAD_ID;
	adcs_get(cmd,buff,ADCS_FINALIZE_PROGRAM_UPLOAD_SIZE);
	memcpy(&adcs->finalize_program_upload,buff,ADCS_FINALIZE_PROGRAM_UPLOAD_SIZE);
}



void adcs_setframe_identification (adcs_t *adcs, adcs_identification_t *frame)
{
	uint8_t buff[ADCS_IDENTIFICATION_SIZE+1];
	buff[0]=ADCS_IDENTIFICATION_ID;
	memcpy(&buff[1],frame,ADCS_IDENTIFICATION_SIZE);
	adcs_set(buff, ADCS_IDENTIFICATION_SIZE+1);
}
void adcs_setframe_communication_status (adcs_t *adcs, adcs_communication_status_t *frame)
{
	uint8_t buff[ADCS_COMMUNICATION_STATUS_SIZE+1];
	buff[0]=ADCS_COMMUNICATION_STATUS_ID;
	memcpy(&buff[1],frame,ADCS_COMMUNICATION_STATUS_SIZE);
	adcs_set(buff, ADCS_COMMUNICATION_STATUS_SIZE+1);
}
void adcs_setframe_telecommand_acknowledge (adcs_t *adcs, adcs_telecommand_acknowledge_t *frame)
{
	uint8_t buff[ADCS_TELECOMMAND_ACKNOWLEDGE_SIZE+1];
	buff[0]=ADCS_TELECOMMAND_ACKNOWLEDGE_ID;
	memcpy(&buff[1],frame,ADCS_TELECOMMAND_ACKNOWLEDGE_SIZE);
	adcs_set(buff, ADCS_TELECOMMAND_ACKNOWLEDGE_SIZE+1);
}
void adcs_setframe_reset_cause (adcs_t *adcs, adcs_reset_cause_t *frame)
{
	uint8_t buff[ADCS_RESET_CAUSE_SIZE+1];
	buff[0]=ADCS_RESET_CAUSE_ID;
	memcpy(&buff[1],frame,ADCS_RESET_CAUSE_SIZE);
	adcs_set(buff, ADCS_RESET_CAUSE_SIZE+1);
}
void adcs_setframe_actuator_commands (adcs_t *adcs, adcs_actuator_commands_t *frame)
{
	uint8_t buff[ADCS_ACTUATOR_COMMANDS_SIZE+1];
	buff[0]=ADCS_ACTUATOR_COMMANDS_ID;
	memcpy(&buff[1],frame,ADCS_ACTUATOR_COMMANDS_SIZE);
	adcs_set(buff, ADCS_ACTUATOR_COMMANDS_SIZE+1);
}
void adcs_setframe_acp_execution_state (adcs_t *adcs, adcs_acp_execution_state_t *frame)
{
	uint8_t buff[ADCS_ACP_EXECUTION_STATE_SIZE+1];
	buff[0]=ADCS_ACP_EXECUTION_STATE_ID;
	memcpy(&buff[1],frame,ADCS_ACP_EXECUTION_STATE_SIZE);
	adcs_set(buff, ADCS_ACP_EXECUTION_STATE_SIZE+1);
}
void adcs_setframe_acp_execution_times (adcs_t *adcs, adcs_acp_execution_times_t *frame)
{
	uint8_t buff[ADCS_ACP_EXECUTION_TIMES_SIZE+1];
	buff[0]=ADCS_ACP_EXECUTION_TIMES_ID;
	memcpy(&buff[1],frame,ADCS_ACP_EXECUTION_TIMES_SIZE);
	adcs_set(buff, ADCS_ACP_EXECUTION_TIMES_SIZE+1);
}
void adcs_setframe_edac_and_latchup_counters (adcs_t *adcs, adcs_edac_and_latchup_counters_t *frame)
{
	uint8_t buff[ADCS_EDAC_AND_LATCHUP_COUNTERS_SIZE+1];
	buff[0]=ADCS_EDAC_AND_LATCHUP_COUNTERS_ID;
	memcpy(&buff[1],frame,ADCS_EDAC_AND_LATCHUP_COUNTERS_SIZE);
	adcs_set(buff, ADCS_EDAC_AND_LATCHUP_COUNTERS_SIZE+1);
}
void adcs_setframe_start_up_mode (adcs_t *adcs, adcs_start_up_mode_t *frame)
{
	uint8_t buff[ADCS_START_UP_MODE_SIZE+1];
	buff[0]=ADCS_START_UP_MODE_ID;
	memcpy(&buff[1],frame,ADCS_START_UP_MODE_SIZE);
	adcs_set(buff, ADCS_START_UP_MODE_SIZE+1);
}
void adcs_setframe_file_information (adcs_t *adcs, adcs_file_information_t *frame)
{
	uint8_t buff[ADCS_FILE_INFORMATION_SIZE+1];
	buff[0]=ADCS_FILE_INFORMATION_ID;
	memcpy(&buff[1],frame,ADCS_FILE_INFORMATION_SIZE);
	adcs_set(buff, ADCS_FILE_INFORMATION_SIZE+1);
}
void adcs_setframe_file_block_crc (adcs_t *adcs, adcs_file_block_crc_t *frame)
{
	uint8_t buff[ADCS_FILE_BLOCK_CRC_SIZE+1];
	buff[0]=ADCS_FILE_BLOCK_CRC_ID;
	memcpy(&buff[1],frame,ADCS_FILE_BLOCK_CRC_SIZE);
	adcs_set(buff, ADCS_FILE_BLOCK_CRC_SIZE+1);
}
void adcs_setframe_file_data_block (adcs_t *adcs, adcs_file_data_block_t *frame)
{
	uint8_t buff[ADCS_FILE_DATA_BLOCK_SIZE+1];
	buff[0]=ADCS_FILE_DATA_BLOCK_ID;
	memcpy(&buff[1],frame,ADCS_FILE_DATA_BLOCK_SIZE);
	adcs_set(buff, ADCS_FILE_DATA_BLOCK_SIZE+1);
}
void adcs_setframe_power_control_selection (adcs_t *adcs, adcs_power_control_selection_t *frame)
{
	uint8_t buff[ADCS_POWER_CONTROL_SELECTION_SIZE+1];
	buff[0]=ADCS_POWER_CONTROL_SELECTION_ID;
	memcpy(&buff[1],frame,ADCS_POWER_CONTROL_SELECTION_SIZE);
	adcs_set(buff, ADCS_POWER_CONTROL_SELECTION_SIZE+1);
}
void adcs_setframe_power_and_temperature_measurements (adcs_t *adcs, adcs_power_and_temperature_measurements_t *frame)
{
	uint8_t buff[ADCS_POWER_AND_TEMPERATURE_MEASUREMENTS_SIZE+1];
	buff[0]=ADCS_POWER_AND_TEMPERATURE_MEASUREMENTS_ID;
	memcpy(&buff[1],frame,ADCS_POWER_AND_TEMPERATURE_MEASUREMENTS_SIZE);
	adcs_set(buff, ADCS_POWER_AND_TEMPERATURE_MEASUREMENTS_SIZE+1);
}
void adcs_setframe_adcs_state (adcs_t *adcs, adcs_adcs_state_t *frame)
{
	uint8_t buff[ADCS_ADCS_STATE_SIZE+1];
	buff[0]=ADCS_ADCS_STATE_ID;
	memcpy(&buff[1],frame,ADCS_ADCS_STATE_SIZE);
	adcs_set(buff, ADCS_ADCS_STATE_SIZE+1);
}
void adcs_setframe_adcs_measurements (adcs_t *adcs, adcs_adcs_measurements_t *frame)
{
	uint8_t buff[ADCS_ADCS_MEASUREMENTS_SIZE+1];
	buff[0]=ADCS_ADCS_MEASUREMENTS_ID;
	memcpy(&buff[1],frame,ADCS_ADCS_MEASUREMENTS_SIZE);
	adcs_set(buff, ADCS_ADCS_MEASUREMENTS_SIZE+1);
}
void adcs_setframe_current_time (adcs_t *adcs, adcs_current_time_t *frame)
{
	uint8_t buff[ADCS_CURRENT_TIME_SIZE+1];
	buff[0]=ADCS_CURRENT_TIME_ID;
	memcpy(&buff[1],frame,ADCS_CURRENT_TIME_SIZE);
	adcs_set(buff, ADCS_CURRENT_TIME_SIZE+1);
}
void adcs_setframe_current_state (adcs_t *adcs, adcs_current_state_t *frame)
{
	uint8_t buff[ADCS_CURRENT_STATE_SIZE+1];
	buff[0]=ADCS_CURRENT_STATE_ID;
	memcpy(&buff[1],frame,ADCS_CURRENT_STATE_SIZE);
	adcs_set(buff, ADCS_CURRENT_STATE_SIZE+1);
}
void adcs_setframe_estimated_attitude_angles (adcs_t *adcs, adcs_estimated_attitude_angles_t *frame)
{
	uint8_t buff[ADCS_ESTIMATED_ATTITUDE_ANGLES_SIZE+1];
	buff[0]=ADCS_ESTIMATED_ATTITUDE_ANGLES_ID;
	memcpy(&buff[1],frame,ADCS_ESTIMATED_ATTITUDE_ANGLES_SIZE);
	adcs_set(buff, ADCS_ESTIMATED_ATTITUDE_ANGLES_SIZE+1);
}
void adcs_setframe_estimated_angular_rates (adcs_t *adcs, adcs_estimated_angular_rates_t *frame)
{
	uint8_t buff[ADCS_ESTIMATED_ANGULAR_RATES_SIZE+1];
	buff[0]=ADCS_ESTIMATED_ANGULAR_RATES_ID;
	memcpy(&buff[1],frame,ADCS_ESTIMATED_ANGULAR_RATES_SIZE);
	adcs_set(buff, ADCS_ESTIMATED_ANGULAR_RATES_SIZE+1);
}
void adcs_setframe_satellite_position_llh (adcs_t *adcs, adcs_satellite_position_llh_t *frame)
{
	uint8_t buff[ADCS_SATELLITE_POSITION_LLH_SIZE+1];
	buff[0]=ADCS_SATELLITE_POSITION_LLH_ID;
	memcpy(&buff[1],frame,ADCS_SATELLITE_POSITION_LLH_SIZE);
	adcs_set(buff, ADCS_SATELLITE_POSITION_LLH_SIZE+1);
}
void adcs_setframe_satellite_velocity_eci (adcs_t *adcs, adcs_satellite_velocity_eci_t *frame)
{
	uint8_t buff[ADCS_SATELLITE_VELOCITY_ECI_SIZE+1];
	buff[0]=ADCS_SATELLITE_VELOCITY_ECI_ID;
	memcpy(&buff[1],frame,ADCS_SATELLITE_VELOCITY_ECI_SIZE);
	adcs_set(buff, ADCS_SATELLITE_VELOCITY_ECI_SIZE+1);
}
void adcs_setframe_magnetic_field_vector (adcs_t *adcs, adcs_magnetic_field_vector_t *frame)
{
	uint8_t buff[ADCS_MAGNETIC_FIELD_VECTOR_SIZE+1];
	buff[0]=ADCS_MAGNETIC_FIELD_VECTOR_ID;
	memcpy(&buff[1],frame,ADCS_MAGNETIC_FIELD_VECTOR_SIZE);
	adcs_set(buff, ADCS_MAGNETIC_FIELD_VECTOR_SIZE+1);
}
void adcs_setframe_coarse_sun_vector (adcs_t *adcs, adcs_coarse_sun_vector_t *frame)
{
	uint8_t buff[ADCS_COARSE_SUN_VECTOR_SIZE+1];
	buff[0]=ADCS_COARSE_SUN_VECTOR_ID;
	memcpy(&buff[1],frame,ADCS_COARSE_SUN_VECTOR_SIZE);
	adcs_set(buff, ADCS_COARSE_SUN_VECTOR_SIZE+1);
}
void adcs_setframe_fine_sun_vector (adcs_t *adcs, adcs_fine_sun_vector_t *frame)
{
	uint8_t buff[ADCS_FINE_SUN_VECTOR_SIZE+1];
	buff[0]=ADCS_FINE_SUN_VECTOR_ID;
	memcpy(&buff[1],frame,ADCS_FINE_SUN_VECTOR_SIZE);
	adcs_set(buff, ADCS_FINE_SUN_VECTOR_SIZE+1);
}
void adcs_setframe_nadir_vector (adcs_t *adcs, adcs_nadir_vector_t *frame)
{
	uint8_t buff[ADCS_NADIR_VECTOR_SIZE+1];
	buff[0]=ADCS_NADIR_VECTOR_ID;
	memcpy(&buff[1],frame,ADCS_NADIR_VECTOR_SIZE);
	adcs_set(buff, ADCS_NADIR_VECTOR_SIZE+1);
}
void adcs_setframe_rate_sensor_rates (adcs_t *adcs, adcs_rate_sensor_rates_t *frame)
{
	uint8_t buff[ADCS_RATE_SENSOR_RATES_SIZE+1];
	buff[0]=ADCS_RATE_SENSOR_RATES_ID;
	memcpy(&buff[1],frame,ADCS_RATE_SENSOR_RATES_SIZE);
	adcs_set(buff, ADCS_RATE_SENSOR_RATES_SIZE+1);
}
void adcs_setframe_wheel_speed (adcs_t *adcs, adcs_wheel_speed_t *frame)
{
	uint8_t buff[ADCS_WHEEL_SPEED_SIZE+1];
	buff[0]=ADCS_WHEEL_SPEED_ID;
	memcpy(&buff[1],frame,ADCS_WHEEL_SPEED_SIZE);
	adcs_set(buff, ADCS_WHEEL_SPEED_SIZE+1);
}
void adcs_setframe_cubesense_current (adcs_t *adcs, adcs_cubesense_current_t *frame)
{
	uint8_t buff[ADCS_CUBESENSE_CURRENT_SIZE+1];
	buff[0]=ADCS_CUBESENSE_CURRENT_ID;
	memcpy(&buff[1],frame,ADCS_CUBESENSE_CURRENT_SIZE);
	adcs_set(buff, ADCS_CUBESENSE_CURRENT_SIZE+1);
}
void adcs_setframe_cubecontrol_current (adcs_t *adcs, adcs_cubecontrol_current_t *frame)
{
	uint8_t buff[ADCS_CUBECONTROL_CURRENT_SIZE+1];
	buff[0]=ADCS_CUBECONTROL_CURRENT_ID;
	memcpy(&buff[1],frame,ADCS_CUBECONTROL_CURRENT_SIZE);
	adcs_set(buff, ADCS_CUBECONTROL_CURRENT_SIZE+1);
}
void adcs_setframe_peripheral_current_and_temperature_measurements (adcs_t *adcs, adcs_peripheral_current_and_temperature_measurements_t *frame)
{
	uint8_t buff[ADCS_PERIPHERAL_CURRENT_AND_TEMPERATURE_MEASUREMENTS_SIZE+1];
	buff[0]=ADCS_PERIPHERAL_CURRENT_AND_TEMPERATURE_MEASUREMENTS_ID;
	memcpy(&buff[1],frame,ADCS_PERIPHERAL_CURRENT_AND_TEMPERATURE_MEASUREMENTS_SIZE);
	adcs_set(buff, ADCS_PERIPHERAL_CURRENT_AND_TEMPERATURE_MEASUREMENTS_SIZE+1);
}
void adcs_setframe_raw_sensor_measurements (adcs_t *adcs, adcs_raw_sensor_measurements_t *frame)
{
	uint8_t buff[ADCS_RAW_SENSOR_MEASUREMENTS_SIZE+1];
	buff[0]=ADCS_RAW_SENSOR_MEASUREMENTS_ID;
	memcpy(&buff[1],frame,ADCS_RAW_SENSOR_MEASUREMENTS_SIZE);
	adcs_set(buff, ADCS_RAW_SENSOR_MEASUREMENTS_SIZE+1);
}
void adcs_setframe_angular_rate_covariance (adcs_t *adcs, adcs_angular_rate_covariance_t *frame)
{
	uint8_t buff[ADCS_ANGULAR_RATE_COVARIANCE_SIZE+1];
	buff[0]=ADCS_ANGULAR_RATE_COVARIANCE_ID;
	memcpy(&buff[1],frame,ADCS_ANGULAR_RATE_COVARIANCE_SIZE);
	adcs_set(buff, ADCS_ANGULAR_RATE_COVARIANCE_SIZE+1);
}
void adcs_setframe_raw_nadir_sensor (adcs_t *adcs, adcs_raw_nadir_sensor_t *frame)
{
	uint8_t buff[ADCS_RAW_NADIR_SENSOR_SIZE+1];
	buff[0]=ADCS_RAW_NADIR_SENSOR_ID;
	memcpy(&buff[1],frame,ADCS_RAW_NADIR_SENSOR_SIZE);
	adcs_set(buff, ADCS_RAW_NADIR_SENSOR_SIZE+1);
}
void adcs_setframe_raw_sun_sensor (adcs_t *adcs, adcs_raw_sun_sensor_t *frame)
{
	uint8_t buff[ADCS_RAW_SUN_SENSOR_SIZE+1];
	buff[0]=ADCS_RAW_SUN_SENSOR_ID;
	memcpy(&buff[1],frame,ADCS_RAW_SUN_SENSOR_SIZE);
	adcs_set(buff, ADCS_RAW_SUN_SENSOR_SIZE+1);
}
void adcs_setframe_raw_css (adcs_t *adcs, adcs_raw_css_t *frame)
{
	uint8_t buff[ADCS_RAW_CSS_SIZE+1];
	buff[0]=ADCS_RAW_CSS_ID;
	memcpy(&buff[1],frame,ADCS_RAW_CSS_SIZE);
	adcs_set(buff, ADCS_RAW_CSS_SIZE+1);
}
void adcs_setframe_raw_magnetometer (adcs_t *adcs, adcs_raw_magnetometer_t *frame)
{
	uint8_t buff[ADCS_RAW_MAGNETOMETER_SIZE+1];
	buff[0]=ADCS_RAW_MAGNETOMETER_ID;
	memcpy(&buff[1],frame,ADCS_RAW_MAGNETOMETER_SIZE);
	adcs_set(buff, ADCS_RAW_MAGNETOMETER_SIZE+1);
}
void adcs_setframe_raw_gps_status (adcs_t *adcs, adcs_raw_gps_status_t *frame)
{
	uint8_t buff[ADCS_RAW_GPS_STATUS_SIZE+1];
	buff[0]=ADCS_RAW_GPS_STATUS_ID;
	memcpy(&buff[1],frame,ADCS_RAW_GPS_STATUS_SIZE);
	adcs_set(buff, ADCS_RAW_GPS_STATUS_SIZE+1);
}
void adcs_setframe_raw_gps_time (adcs_t *adcs, adcs_raw_gps_time_t *frame)
{
	uint8_t buff[ADCS_RAW_GPS_TIME_SIZE+1];
	buff[0]=ADCS_RAW_GPS_TIME_ID;
	memcpy(&buff[1],frame,ADCS_RAW_GPS_TIME_SIZE);
	adcs_set(buff, ADCS_RAW_GPS_TIME_SIZE+1);
}
void adcs_setframe_raw_gps_x (adcs_t *adcs, adcs_raw_gps_x_t *frame)
{
	uint8_t buff[ADCS_RAW_GPS_X_SIZE+1];
	buff[0]=ADCS_RAW_GPS_X_ID;
	memcpy(&buff[1],frame,ADCS_RAW_GPS_X_SIZE);
	adcs_set(buff, ADCS_RAW_GPS_X_SIZE+1);
}
void adcs_setframe_raw_gps_y (adcs_t *adcs, adcs_raw_gps_y_t *frame)
{
	uint8_t buff[ADCS_RAW_GPS_Y_SIZE+1];
	buff[0]=ADCS_RAW_GPS_Y_ID;
	memcpy(&buff[1],frame,ADCS_RAW_GPS_Y_SIZE);
	adcs_set(buff, ADCS_RAW_GPS_Y_SIZE+1);
}
void adcs_setframe_raw_gps_z (adcs_t *adcs, adcs_raw_gps_z_t *frame)
{
	uint8_t buff[ADCS_RAW_GPS_Z_SIZE+1];
	buff[0]=ADCS_RAW_GPS_Z_ID;
	memcpy(&buff[1],frame,ADCS_RAW_GPS_Z_SIZE);
	adcs_set(buff, ADCS_RAW_GPS_Z_SIZE+1);
}
void adcs_setframe_estimation_data (adcs_t *adcs, adcs_estimation_data_t *frame)
{
	uint8_t buff[ADCS_ESTIMATION_DATA_SIZE+1];
	buff[0]=ADCS_ESTIMATION_DATA_ID;
	memcpy(&buff[1],frame,ADCS_ESTIMATION_DATA_SIZE);
	adcs_set(buff, ADCS_ESTIMATION_DATA_SIZE+1);
}
void adcs_setframe_igrf_modelled_magnetic_field_vector (adcs_t *adcs, adcs_igrf_modelled_magnetic_field_vector_t *frame)
{
	uint8_t buff[ADCS_IGRF_MODELLED_MAGNETIC_FIELD_VECTOR_SIZE+1];
	buff[0]=ADCS_IGRF_MODELLED_MAGNETIC_FIELD_VECTOR_ID;
	memcpy(&buff[1],frame,ADCS_IGRF_MODELLED_MAGNETIC_FIELD_VECTOR_SIZE);
	adcs_set(buff, ADCS_IGRF_MODELLED_MAGNETIC_FIELD_VECTOR_SIZE+1);
}
void adcs_setframe_modelled_sun_vector (adcs_t *adcs, adcs_modelled_sun_vector_t *frame)
{
	uint8_t buff[ADCS_MODELLED_SUN_VECTOR_SIZE+1];
	buff[0]=ADCS_MODELLED_SUN_VECTOR_ID;
	memcpy(&buff[1],frame,ADCS_MODELLED_SUN_VECTOR_SIZE);
	adcs_set(buff, ADCS_MODELLED_SUN_VECTOR_SIZE+1);
}
void adcs_setframe_estimated_gyro_bias (adcs_t *adcs, adcs_estimated_gyro_bias_t *frame)
{
	uint8_t buff[ADCS_ESTIMATED_GYRO_BIAS_SIZE+1];
	buff[0]=ADCS_ESTIMATED_GYRO_BIAS_ID;
	memcpy(&buff[1],frame,ADCS_ESTIMATED_GYRO_BIAS_SIZE);
	adcs_set(buff, ADCS_ESTIMATED_GYRO_BIAS_SIZE+1);
}
void adcs_setframe_estimation_innovation_vector (adcs_t *adcs, adcs_estimation_innovation_vector_t *frame)
{
	uint8_t buff[ADCS_ESTIMATION_INNOVATION_VECTOR_SIZE+1];
	buff[0]=ADCS_ESTIMATION_INNOVATION_VECTOR_ID;
	memcpy(&buff[1],frame,ADCS_ESTIMATION_INNOVATION_VECTOR_SIZE);
	adcs_set(buff, ADCS_ESTIMATION_INNOVATION_VECTOR_SIZE+1);
}
void adcs_setframe_quaternion_error_vector (adcs_t *adcs, adcs_quaternion_error_vector_t *frame)
{
	uint8_t buff[ADCS_QUATERNION_ERROR_VECTOR_SIZE+1];
	buff[0]=ADCS_QUATERNION_ERROR_VECTOR_ID;
	memcpy(&buff[1],frame,ADCS_QUATERNION_ERROR_VECTOR_SIZE);
	adcs_set(buff, ADCS_QUATERNION_ERROR_VECTOR_SIZE+1);
}
void adcs_setframe_quaternion_covariance (adcs_t *adcs, adcs_quaternion_covariance_t *frame)
{
	uint8_t buff[ADCS_QUATERNION_COVARIANCE_SIZE+1];
	buff[0]=ADCS_QUATERNION_COVARIANCE_ID;
	memcpy(&buff[1],frame,ADCS_QUATERNION_COVARIANCE_SIZE);
	adcs_set(buff, ADCS_QUATERNION_COVARIANCE_SIZE+1);
}
void adcs_setframe_magnetorquer_command (adcs_t *adcs, adcs_magnetorquer_command_t *frame)
{
	uint8_t buff[ADCS_MAGNETORQUER_COMMAND_SIZE+1];
	buff[0]=ADCS_MAGNETORQUER_COMMAND_ID;
	memcpy(&buff[1],frame,ADCS_MAGNETORQUER_COMMAND_SIZE);
	adcs_set(buff, ADCS_MAGNETORQUER_COMMAND_SIZE+1);
}
void adcs_setframe_wheel_speed_commands (adcs_t *adcs, adcs_wheel_speed_commands_t *frame)
{
	uint8_t buff[ADCS_WHEEL_SPEED_COMMANDS_SIZE+1];
	buff[0]=ADCS_WHEEL_SPEED_COMMANDS_ID;
	memcpy(&buff[1],frame,ADCS_WHEEL_SPEED_COMMANDS_SIZE);
	adcs_set(buff, ADCS_WHEEL_SPEED_COMMANDS_SIZE+1);
}
void adcs_setframe_sgp4_orbit_parameters (adcs_t *adcs, adcs_sgp4_orbit_parameters_t *frame)
{
	uint8_t buff[ADCS_SGP4_ORBIT_PARAMETERS_SIZE+1];
	buff[0]=ADCS_SGP4_ORBIT_PARAMETERS_ID;
	memcpy(&buff[1],frame,ADCS_SGP4_ORBIT_PARAMETERS_SIZE);
	adcs_set(buff, ADCS_SGP4_ORBIT_PARAMETERS_SIZE+1);
}
void adcs_setframe_configuration (adcs_t *adcs, adcs_configuration_t *frame)
{
	uint8_t buff[ADCS_CONFIGURATION_SIZE+1];
	buff[0]=ADCS_CONFIGURATION_ID;
	memcpy(&buff[1],frame,ADCS_CONFIGURATION_SIZE);
	adcs_set(buff, ADCS_CONFIGURATION_SIZE+1);
}
void adcs_setframe_status_of_image_capture_and_save_operation (adcs_t *adcs, adcs_status_of_image_capture_and_save_operation_t *frame)
{
	uint8_t buff[ADCS_STATUS_OF_IMAGE_CAPTURE_AND_SAVE_OPERATION_SIZE+1];
	buff[0]=ADCS_STATUS_OF_IMAGE_CAPTURE_AND_SAVE_OPERATION_ID;
	memcpy(&buff[1],frame,ADCS_STATUS_OF_IMAGE_CAPTURE_AND_SAVE_OPERATION_SIZE);
	adcs_set(buff, ADCS_STATUS_OF_IMAGE_CAPTURE_AND_SAVE_OPERATION_SIZE+1);
}
void adcs_setframe_uploaded_program_status (adcs_t *adcs, adcs_uploaded_program_status_t *frame)
{
	uint8_t buff[ADCS_UPLOADED_PROGRAM_STATUS_SIZE+1];
	buff[0]=ADCS_UPLOADED_PROGRAM_STATUS_ID;
	memcpy(&buff[1],frame,ADCS_UPLOADED_PROGRAM_STATUS_SIZE);
	adcs_set(buff, ADCS_UPLOADED_PROGRAM_STATUS_SIZE+1);
}
void adcs_setframe_get_flash_program_list (adcs_t *adcs, adcs_get_flash_program_list_t *frame)
{
	uint8_t buff[ADCS_GET_FLASH_PROGRAM_LIST_SIZE+1];
	buff[0]=ADCS_GET_FLASH_PROGRAM_LIST_ID;
	memcpy(&buff[1],frame,ADCS_GET_FLASH_PROGRAM_LIST_SIZE);
	adcs_set(buff, ADCS_GET_FLASH_PROGRAM_LIST_SIZE+1);
}
void adcs_setframe_reset (adcs_t *adcs, adcs_reset_t *frame)
{
	uint8_t buff[ADCS_RESET_SIZE+1];
	buff[0]=ADCS_RESET_ID;
	memcpy(&buff[1],frame,ADCS_RESET_SIZE);
	adcs_set(buff, ADCS_RESET_SIZE+1);
}
void adcs_setframe_set_unix_time (adcs_t *adcs, adcs_set_unix_time_t *frame)
{
	uint8_t buff[ADCS_SET_UNIX_TIME_SIZE+1];
	buff[0]=ADCS_SET_UNIX_TIME_ID;
	memcpy(&buff[1],frame,ADCS_SET_UNIX_TIME_SIZE);
	adcs_set(buff, ADCS_SET_UNIX_TIME_SIZE+1);
}
void adcs_setframe_adcs_run_mode (adcs_t *adcs, adcs_adcs_run_mode_t *frame)
{
	uint8_t buff[ADCS_ADCS_RUN_MODE_SIZE+1];
	buff[0]=ADCS_ADCS_RUN_MODE_ID;
	memcpy(&buff[1],frame,ADCS_ADCS_RUN_MODE_SIZE);
	adcs_set(buff, ADCS_ADCS_RUN_MODE_SIZE+1);
}
void adcs_setframe_selected_logged_data (adcs_t *adcs, adcs_selected_logged_data_t *frame)
{
	uint8_t buff[ADCS_SELECTED_LOGGED_DATA_SIZE+1];
	buff[0]=ADCS_SELECTED_LOGGED_DATA_ID;
	memcpy(&buff[1],frame,ADCS_SELECTED_LOGGED_DATA_SIZE);
	adcs_set(buff, ADCS_SELECTED_LOGGED_DATA_SIZE+1);
}
void adcs_setframe_power_control (adcs_t *adcs, adcs_power_control_t *frame)
{
	uint8_t buff[ADCS_POWER_CONTROL_SIZE+1];
	buff[0]=ADCS_POWER_CONTROL_ID;
	memcpy(&buff[1],frame,ADCS_POWER_CONTROL_SIZE);
	adcs_set(buff, ADCS_POWER_CONTROL_SIZE+1);
}
void adcs_setframe_deploy_magnetometer_boom (adcs_t *adcs, adcs_deploy_magnetometer_boom_t *frame)
{
	uint8_t buff[ADCS_DEPLOY_MAGNETOMETER_BOOM_SIZE+1];
	buff[0]=ADCS_DEPLOY_MAGNETOMETER_BOOM_ID;
	memcpy(&buff[1],frame,ADCS_DEPLOY_MAGNETOMETER_BOOM_SIZE);
	adcs_set(buff, ADCS_DEPLOY_MAGNETOMETER_BOOM_SIZE+1);
}
void adcs_setframe_trigger_adcs_loop (adcs_t *adcs, adcs_trigger_adcs_loop_t *frame)
{
	uint8_t buff[ADCS_TRIGGER_ADCS_LOOP_SIZE+1];
	buff[0]=ADCS_TRIGGER_ADCS_LOOP_ID;
	memcpy(&buff[1],frame,ADCS_TRIGGER_ADCS_LOOP_SIZE);
	adcs_set(buff, ADCS_TRIGGER_ADCS_LOOP_SIZE+1);
}
void adcs_setframe_set_attitude_estimation_mode (adcs_t *adcs, adcs_set_attitude_estimation_mode_t *frame)
{
	uint8_t buff[ADCS_SET_ATTITUDE_ESTIMATION_MODE_SIZE+1];
	buff[0]=ADCS_SET_ATTITUDE_ESTIMATION_MODE_ID;
	memcpy(&buff[1],frame,ADCS_SET_ATTITUDE_ESTIMATION_MODE_SIZE);
	adcs_set(buff, ADCS_SET_ATTITUDE_ESTIMATION_MODE_SIZE+1);
}
void adcs_setframe_set_attitude_control_mode (adcs_t *adcs, adcs_set_attitude_control_mode_t *frame)
{
	uint8_t buff[ADCS_SET_ATTITUDE_CONTROL_MODE_SIZE+1];
	buff[0]=ADCS_SET_ATTITUDE_CONTROL_MODE_ID;
	memcpy(&buff[1],frame,ADCS_SET_ATTITUDE_CONTROL_MODE_SIZE);
	adcs_set(buff, ADCS_SET_ATTITUDE_CONTROL_MODE_SIZE+1);
}
void adcs_setframe_set_commanded_attitude_angles (adcs_t *adcs, adcs_set_commanded_attitude_angles_t *frame)
{
	uint8_t buff[ADCS_SET_COMMANDED_ATTITUDE_ANGLES_SIZE+1];
	buff[0]=ADCS_SET_COMMANDED_ATTITUDE_ANGLES_ID;
	memcpy(&buff[1],frame,ADCS_SET_COMMANDED_ATTITUDE_ANGLES_SIZE);
	adcs_set(buff, ADCS_SET_COMMANDED_ATTITUDE_ANGLES_SIZE+1);
}
void adcs_setframe_set_wheel (adcs_t *adcs, adcs_set_wheel_t *frame)
{
	uint8_t buff[ADCS_SET_WHEEL_SIZE+1];
	buff[0]=ADCS_SET_WHEEL_ID;
	memcpy(&buff[1],frame,ADCS_SET_WHEEL_SIZE);
	adcs_set(buff, ADCS_SET_WHEEL_SIZE+1);
}
void adcs_setframe_set_magnetorquer_output (adcs_t *adcs, adcs_set_magnetorquer_output_t *frame)
{
	uint8_t buff[ADCS_SET_MAGNETORQUER_OUTPUT_SIZE+1];
	buff[0]=ADCS_SET_MAGNETORQUER_OUTPUT_ID;
	memcpy(&buff[1],frame,ADCS_SET_MAGNETORQUER_OUTPUT_SIZE);
	adcs_set(buff, ADCS_SET_MAGNETORQUER_OUTPUT_SIZE+1);
}
void adcs_setframe_set_start_up_mode (adcs_t *adcs, adcs_set_start_up_mode_t *frame)
{
	uint8_t buff[ADCS_SET_START_UP_MODE_SIZE+1];
	buff[0]=ADCS_SET_START_UP_MODE_ID;
	memcpy(&buff[1],frame,ADCS_SET_START_UP_MODE_SIZE);
	adcs_set(buff, ADCS_SET_START_UP_MODE_SIZE+1);
}
void adcs_setframe_set_sgp4_orbit_parameters (adcs_t *adcs, adcs_set_sgp4_orbit_parameters_t *frame)
{
	uint8_t buff[ADCS_SET_SGP4_ORBIT_PARAMETERS_SIZE+1];
	buff[0]=ADCS_SET_SGP4_ORBIT_PARAMETERS_ID;
	memcpy(&buff[1],frame,ADCS_SET_SGP4_ORBIT_PARAMETERS_SIZE);
	adcs_set(buff, ADCS_SET_SGP4_ORBIT_PARAMETERS_SIZE+1);
}
void adcs_setframe_set_configuration (adcs_t *adcs, adcs_set_configuration_t *frame)
{
	uint8_t buff[ADCS_SET_CONFIGURATION_SIZE+1];
	buff[0]=ADCS_SET_CONFIGURATION_ID;
	memcpy(&buff[1],frame,ADCS_SET_CONFIGURATION_SIZE);
	adcs_set(buff, ADCS_SET_CONFIGURATION_SIZE+1);
}
void adcs_setframe_set_magnetorquer (adcs_t *adcs, adcs_set_magnetorquer_t *frame)
{
	uint8_t buff[ADCS_SET_MAGNETORQUER_SIZE+1];
	buff[0]=ADCS_SET_MAGNETORQUER_ID;
	memcpy(&buff[1],frame,ADCS_SET_MAGNETORQUER_SIZE);
	adcs_set(buff, ADCS_SET_MAGNETORQUER_SIZE+1);
}
void adcs_setframe_set_wheel_configuration (adcs_t *adcs, adcs_set_wheel_configuration_t *frame)
{
	uint8_t buff[ADCS_SET_WHEEL_CONFIGURATION_SIZE+1];
	buff[0]=ADCS_SET_WHEEL_CONFIGURATION_ID;
	memcpy(&buff[1],frame,ADCS_SET_WHEEL_CONFIGURATION_SIZE);
	adcs_set(buff, ADCS_SET_WHEEL_CONFIGURATION_SIZE+1);
}
void adcs_setframe_set_css_configuration (adcs_t *adcs, adcs_set_css_configuration_t *frame)
{
	uint8_t buff[ADCS_SET_CSS_CONFIGURATION_SIZE+1];
	buff[0]=ADCS_SET_CSS_CONFIGURATION_ID;
	memcpy(&buff[1],frame,ADCS_SET_CSS_CONFIGURATION_SIZE);
	adcs_set(buff, ADCS_SET_CSS_CONFIGURATION_SIZE+1);
}
void adcs_setframe_set_sun_sensor_configuration (adcs_t *adcs, adcs_set_sun_sensor_configuration_t *frame)
{
	uint8_t buff[ADCS_SET_SUN_SENSOR_CONFIGURATION_SIZE+1];
	buff[0]=ADCS_SET_SUN_SENSOR_CONFIGURATION_ID;
	memcpy(&buff[1],frame,ADCS_SET_SUN_SENSOR_CONFIGURATION_SIZE);
	adcs_set(buff, ADCS_SET_SUN_SENSOR_CONFIGURATION_SIZE+1);
}
void adcs_setframe_set_nadir_sensor_configuration (adcs_t *adcs, adcs_set_nadir_sensor_configuration_t *frame)
{
	uint8_t buff[ADCS_SET_NADIR_SENSOR_CONFIGURATION_SIZE+1];
	buff[0]=ADCS_SET_NADIR_SENSOR_CONFIGURATION_ID;
	memcpy(&buff[1],frame,ADCS_SET_NADIR_SENSOR_CONFIGURATION_SIZE);
	adcs_set(buff, ADCS_SET_NADIR_SENSOR_CONFIGURATION_SIZE+1);
}
void adcs_setframe_set_magnetometer_configuration (adcs_t *adcs, adcs_set_magnetometer_configuration_t *frame)
{
	uint8_t buff[ADCS_SET_MAGNETOMETER_CONFIGURATION_SIZE+1];
	buff[0]=ADCS_SET_MAGNETOMETER_CONFIGURATION_ID;
	memcpy(&buff[1],frame,ADCS_SET_MAGNETOMETER_CONFIGURATION_SIZE);
	adcs_set(buff, ADCS_SET_MAGNETOMETER_CONFIGURATION_SIZE+1);
}
void adcs_setframe_set_rate_sensor_configuration (adcs_t *adcs, adcs_set_rate_sensor_configuration_t *frame)
{
	uint8_t buff[ADCS_SET_RATE_SENSOR_CONFIGURATION_SIZE+1];
	buff[0]=ADCS_SET_RATE_SENSOR_CONFIGURATION_ID;
	memcpy(&buff[1],frame,ADCS_SET_RATE_SENSOR_CONFIGURATION_SIZE);
	adcs_set(buff, ADCS_SET_RATE_SENSOR_CONFIGURATION_SIZE+1);
}
void adcs_setframe_set_detumbling_control_parameters (adcs_t *adcs, adcs_set_detumbling_control_parameters_t *frame)
{
	uint8_t buff[ADCS_SET_DETUMBLING_CONTROL_PARAMETERS_SIZE+1];
	buff[0]=ADCS_SET_DETUMBLING_CONTROL_PARAMETERS_ID;
	memcpy(&buff[1],frame,ADCS_SET_DETUMBLING_CONTROL_PARAMETERS_SIZE);
	adcs_set(buff, ADCS_SET_DETUMBLING_CONTROL_PARAMETERS_SIZE+1);
}
void adcs_setframe_set_y_momentum_control_parameters (adcs_t *adcs, adcs_set_y_momentum_control_parameters_t *frame)
{
	uint8_t buff[ADCS_SET_Y_MOMENTUM_CONTROL_PARAMETERS_SIZE+1];
	buff[0]=ADCS_SET_Y_MOMENTUM_CONTROL_PARAMETERS_ID;
	memcpy(&buff[1],frame,ADCS_SET_Y_MOMENTUM_CONTROL_PARAMETERS_SIZE);
	adcs_set(buff, ADCS_SET_Y_MOMENTUM_CONTROL_PARAMETERS_SIZE+1);
}
void adcs_setframe_set_moment_of_inertia (adcs_t *adcs, adcs_set_moment_of_inertia_t *frame)
{
	uint8_t buff[ADCS_SET_MOMENT_OF_INERTIA_SIZE+1];
	buff[0]=ADCS_SET_MOMENT_OF_INERTIA_ID;
	memcpy(&buff[1],frame,ADCS_SET_MOMENT_OF_INERTIA_SIZE);
	adcs_set(buff, ADCS_SET_MOMENT_OF_INERTIA_SIZE+1);
}
void adcs_setframe_set_estimation_parameters (adcs_t *adcs, adcs_set_estimation_parameters_t *frame)
{
	uint8_t buff[ADCS_SET_ESTIMATION_PARAMETERS_SIZE+1];
	buff[0]=ADCS_SET_ESTIMATION_PARAMETERS_ID;
	memcpy(&buff[1],frame,ADCS_SET_ESTIMATION_PARAMETERS_SIZE);
	adcs_set(buff, ADCS_SET_ESTIMATION_PARAMETERS_SIZE+1);
}
void adcs_setframe_save_configuration (adcs_t *adcs, adcs_save_configuration_t *frame)
{
	uint8_t buff[ADCS_SAVE_CONFIGURATION_SIZE+1];
	buff[0]=ADCS_SAVE_CONFIGURATION_ID;
	memcpy(&buff[1],frame,ADCS_SAVE_CONFIGURATION_SIZE);
	adcs_set(buff, ADCS_SAVE_CONFIGURATION_SIZE+1);
}
void adcs_setframe_save_orbit_parameters (adcs_t *adcs, adcs_save_orbit_parameters_t *frame)
{
	uint8_t buff[ADCS_SAVE_ORBIT_PARAMETERS_SIZE+1];
	buff[0]=ADCS_SAVE_ORBIT_PARAMETERS_ID;
	memcpy(&buff[1],frame,ADCS_SAVE_ORBIT_PARAMETERS_SIZE);
	adcs_set(buff, ADCS_SAVE_ORBIT_PARAMETERS_SIZE+1);
}
void adcs_setframe_capture_and_save_image (adcs_t *adcs, adcs_capture_and_save_image_t *frame)
{
	uint8_t buff[ADCS_CAPTURE_AND_SAVE_IMAGE_SIZE+1];
	buff[0]=ADCS_CAPTURE_AND_SAVE_IMAGE_ID;
	memcpy(&buff[1],frame,ADCS_CAPTURE_AND_SAVE_IMAGE_SIZE);
	adcs_set(buff, ADCS_CAPTURE_AND_SAVE_IMAGE_SIZE+1);
}
void adcs_setframe_reset_file_list (adcs_t *adcs, adcs_reset_file_list_t *frame)
{
	uint8_t buff[ADCS_RESET_FILE_LIST_SIZE+1];
	buff[0]=ADCS_RESET_FILE_LIST_ID;
	memcpy(&buff[1],frame,ADCS_RESET_FILE_LIST_SIZE);
	adcs_set(buff, ADCS_RESET_FILE_LIST_SIZE+1);
}
void adcs_setframe_advance_file_list_index (adcs_t *adcs, adcs_advance_file_list_index_t *frame)
{
	uint8_t buff[ADCS_ADVANCE_FILE_LIST_INDEX_SIZE+1];
	buff[0]=ADCS_ADVANCE_FILE_LIST_INDEX_ID;
	memcpy(&buff[1],frame,ADCS_ADVANCE_FILE_LIST_INDEX_SIZE);
	adcs_set(buff, ADCS_ADVANCE_FILE_LIST_INDEX_SIZE+1);
}
void adcs_setframe_initialize_file_download (adcs_t *adcs, adcs_initialize_file_download_t *frame)
{
	uint8_t buff[ADCS_INITIALIZE_FILE_DOWNLOAD_SIZE+1];
	buff[0]=ADCS_INITIALIZE_FILE_DOWNLOAD_ID;
	memcpy(&buff[1],frame,ADCS_INITIALIZE_FILE_DOWNLOAD_SIZE);
	adcs_set(buff, ADCS_INITIALIZE_FILE_DOWNLOAD_SIZE+1);
}
void adcs_setframe_advance_file_read_pointer (adcs_t *adcs, adcs_advance_file_read_pointer_t *frame)
{
	uint8_t buff[ADCS_ADVANCE_FILE_READ_POINTER_SIZE+1];
	buff[0]=ADCS_ADVANCE_FILE_READ_POINTER_ID;
	memcpy(&buff[1],frame,ADCS_ADVANCE_FILE_READ_POINTER_SIZE);
	adcs_set(buff, ADCS_ADVANCE_FILE_READ_POINTER_SIZE+1);
}
void adcs_setframe_erase_file (adcs_t *adcs, adcs_erase_file_t *frame)
{
	uint8_t buff[ADCS_ERASE_FILE_SIZE+1];
	buff[0]=ADCS_ERASE_FILE_ID;
	memcpy(&buff[1],frame,ADCS_ERASE_FILE_SIZE);
	adcs_set(buff, ADCS_ERASE_FILE_SIZE+1);
}
void adcs_setframe_erase_all_files (adcs_t *adcs, adcs_erase_all_files_t *frame)
{
	uint8_t buff[ADCS_ERASE_ALL_FILES_SIZE+1];
	buff[0]=ADCS_ERASE_ALL_FILES_ID;
	memcpy(&buff[1],frame,ADCS_ERASE_ALL_FILES_SIZE);
	adcs_set(buff, ADCS_ERASE_ALL_FILES_SIZE+1);
}
void adcs_setframe_set_boot_index (adcs_t *adcs, adcs_set_boot_index_t *frame)
{
	uint8_t buff[ADCS_SET_BOOT_INDEX_SIZE+1];
	buff[0]=ADCS_SET_BOOT_INDEX_ID;
	memcpy(&buff[1],frame,ADCS_SET_BOOT_INDEX_SIZE);
	adcs_set(buff, ADCS_SET_BOOT_INDEX_SIZE+1);
}
void adcs_setframe_erase_program (adcs_t *adcs, adcs_erase_program_t *frame)
{
	uint8_t buff[ADCS_ERASE_PROGRAM_SIZE+1];
	buff[0]=ADCS_ERASE_PROGRAM_ID;
	memcpy(&buff[1],frame,ADCS_ERASE_PROGRAM_SIZE);
	adcs_set(buff, ADCS_ERASE_PROGRAM_SIZE+1);
}
void adcs_setframe_upload_program_block (adcs_t *adcs, adcs_upload_program_block_t *frame)
{
	uint8_t buff[ADCS_UPLOAD_PROGRAM_BLOCK_SIZE+1];
	buff[0]=ADCS_UPLOAD_PROGRAM_BLOCK_ID;
	memcpy(&buff[1],frame,ADCS_UPLOAD_PROGRAM_BLOCK_SIZE);
	adcs_set(buff, ADCS_UPLOAD_PROGRAM_BLOCK_SIZE+1);
}
void adcs_setframe_finalize_program_upload (adcs_t *adcs, adcs_finalize_program_upload_t *frame)
{
	uint8_t buff[ADCS_FINALIZE_PROGRAM_UPLOAD_SIZE+1];
	buff[0]=ADCS_FINALIZE_PROGRAM_UPLOAD_ID;
	memcpy(&buff[1],frame,ADCS_FINALIZE_PROGRAM_UPLOAD_SIZE);
	adcs_set(buff, ADCS_FINALIZE_PROGRAM_UPLOAD_SIZE+1);
}



void initialize_adcs_nanomind( adcs_t *adcs )
{
	DEV_ASSERT( adcs );

	extern void initialize_adcs_( adcs_t* );
	initialize_adcs_( adcs );

	adcs->getframe_identification = &adcs_getframe_identification;
	adcs->getframe_communication_status = &adcs_getframe_communication_status;
	adcs->getframe_telecommand_acknowledge = &adcs_getframe_telecommand_acknowledge;
	adcs->getframe_reset_cause = &adcs_getframe_reset_cause;
	adcs->getframe_actuator_commands = &adcs_getframe_actuator_commands;
	adcs->getframe_acp_execution_state = &adcs_getframe_acp_execution_state;
	adcs->getframe_acp_execution_times = &adcs_getframe_acp_execution_times;
	adcs->getframe_edac_and_latchup_counters = &adcs_getframe_edac_and_latchup_counters;
	adcs->getframe_start_up_mode = &adcs_getframe_start_up_mode;
	adcs->getframe_file_information = &adcs_getframe_file_information;
	adcs->getframe_file_block_crc = &adcs_getframe_file_block_crc;
	adcs->getframe_file_data_block = &adcs_getframe_file_data_block;
	adcs->getframe_power_control_selection = &adcs_getframe_power_control_selection;
	adcs->getframe_power_and_temperature_measurements = &adcs_getframe_power_and_temperature_measurements;
	adcs->getframe_adcs_state = &adcs_getframe_adcs_state;
	adcs->detumble_command = &detumble_command;
	adcs->power = &power;
	adcs->getframe_adcs_measurements = &adcs_getframe_adcs_measurements;
	adcs->getframe_current_time = &adcs_getframe_current_time;
	adcs->getframe_current_state = &adcs_getframe_current_state;
	adcs->getframe_estimated_attitude_angles = &adcs_getframe_estimated_attitude_angles;
	adcs->getframe_estimated_angular_rates = &adcs_getframe_estimated_angular_rates;
	adcs->getframe_satellite_position_llh = &adcs_getframe_satellite_position_llh;
	adcs->getframe_satellite_velocity_eci = &adcs_getframe_satellite_velocity_eci;
	adcs->getframe_magnetic_field_vector = &adcs_getframe_magnetic_field_vector;
	adcs->getframe_coarse_sun_vector = &adcs_getframe_coarse_sun_vector;
	adcs->getframe_fine_sun_vector = &adcs_getframe_fine_sun_vector;
	adcs->getframe_nadir_vector = &adcs_getframe_nadir_vector;
	adcs->getframe_rate_sensor_rates = &adcs_getframe_rate_sensor_rates;
	adcs->getframe_wheel_speed = &adcs_getframe_wheel_speed;
	adcs->getframe_cubesense_current = &adcs_getframe_cubesense_current;
	adcs->getframe_cubecontrol_current = &adcs_getframe_cubecontrol_current;
	adcs->getframe_peripheral_current_and_temperature_measurements = &adcs_getframe_peripheral_current_and_temperature_measurements;
	adcs->getframe_raw_sensor_measurements = &adcs_getframe_raw_sensor_measurements;
	adcs->getframe_angular_rate_covariance = &adcs_getframe_angular_rate_covariance;
	adcs->getframe_raw_nadir_sensor = &adcs_getframe_raw_nadir_sensor;
	adcs->getframe_raw_sun_sensor = &adcs_getframe_raw_sun_sensor;
	adcs->getframe_raw_css = &adcs_getframe_raw_css;
	adcs->getframe_raw_magnetometer = &adcs_getframe_raw_magnetometer;
	adcs->getframe_raw_gps_status = &adcs_getframe_raw_gps_status;
	adcs->getframe_raw_gps_time = &adcs_getframe_raw_gps_time;
	adcs->getframe_raw_gps_x = &adcs_getframe_raw_gps_x;
	adcs->getframe_raw_gps_y = &adcs_getframe_raw_gps_y;
	adcs->getframe_raw_gps_z = &adcs_getframe_raw_gps_z;
	adcs->getframe_estimation_data = &adcs_getframe_estimation_data;
	adcs->getframe_igrf_modelled_magnetic_field_vector = &adcs_getframe_igrf_modelled_magnetic_field_vector;
	adcs->getframe_modelled_sun_vector = &adcs_getframe_modelled_sun_vector;
	adcs->getframe_estimated_gyro_bias = &adcs_getframe_estimated_gyro_bias;
	adcs->getframe_estimation_innovation_vector = &adcs_getframe_estimation_innovation_vector;
	adcs->getframe_quaternion_error_vector = &adcs_getframe_quaternion_error_vector;
	adcs->getframe_quaternion_covariance = &adcs_getframe_quaternion_covariance;
	adcs->getframe_magnetorquer_command = &adcs_getframe_magnetorquer_command;
	adcs->getframe_wheel_speed_commands = &adcs_getframe_wheel_speed_commands;
	adcs->getframe_sgp4_orbit_parameters = &adcs_getframe_sgp4_orbit_parameters;
	adcs->getframe_configuration = &adcs_getframe_configuration;
	adcs->getframe_status_of_image_capture_and_save_operation = &adcs_getframe_status_of_image_capture_and_save_operation;
	adcs->getframe_uploaded_program_status = &adcs_getframe_uploaded_program_status;
	adcs->getframe_get_flash_program_list = &adcs_getframe_get_flash_program_list;
	adcs->getframe_reset = &adcs_getframe_reset;
	adcs->getframe_set_unix_time = &adcs_getframe_set_unix_time;
	adcs->getframe_adcs_run_mode = &adcs_getframe_adcs_run_mode;
	adcs->getframe_selected_logged_data = &adcs_getframe_selected_logged_data;
	adcs->getframe_power_control = &adcs_getframe_power_control;
	adcs->getframe_deploy_magnetometer_boom = &adcs_getframe_deploy_magnetometer_boom;
	adcs->getframe_trigger_adcs_loop = &adcs_getframe_trigger_adcs_loop;
	adcs->getframe_set_attitude_estimation_mode = &adcs_getframe_set_attitude_estimation_mode;
	adcs->getframe_set_attitude_control_mode = &adcs_getframe_set_attitude_control_mode;
	adcs->getframe_set_commanded_attitude_angles = &adcs_getframe_set_commanded_attitude_angles;
	adcs->getframe_set_wheel = &adcs_getframe_set_wheel;
	adcs->getframe_set_magnetorquer_output = &adcs_getframe_set_magnetorquer_output;
	adcs->getframe_set_start_up_mode = &adcs_getframe_set_start_up_mode;
	adcs->getframe_set_sgp4_orbit_parameters = &adcs_getframe_set_sgp4_orbit_parameters;
	adcs->getframe_set_configuration = &adcs_getframe_set_configuration;
	adcs->getframe_set_magnetorquer = &adcs_getframe_set_magnetorquer;
	adcs->getframe_set_wheel_configuration = &adcs_getframe_set_wheel_configuration;
	adcs->getframe_set_css_configuration = &adcs_getframe_set_css_configuration;
	adcs->getframe_set_sun_sensor_configuration = &adcs_getframe_set_sun_sensor_configuration;
	adcs->getframe_set_nadir_sensor_configuration = &adcs_getframe_set_nadir_sensor_configuration;
	adcs->getframe_set_magnetometer_configuration = &adcs_getframe_set_magnetometer_configuration;
	adcs->getframe_set_rate_sensor_configuration = &adcs_getframe_set_rate_sensor_configuration;
	adcs->getframe_set_detumbling_control_parameters = &adcs_getframe_set_detumbling_control_parameters;
	adcs->getframe_set_y_momentum_control_parameters = &adcs_getframe_set_y_momentum_control_parameters;
	adcs->getframe_set_moment_of_inertia = &adcs_getframe_set_moment_of_inertia;
	adcs->getframe_set_estimation_parameters = &adcs_getframe_set_estimation_parameters;
	adcs->getframe_save_configuration = &adcs_getframe_save_configuration;
	adcs->getframe_save_orbit_parameters = &adcs_getframe_save_orbit_parameters;
	adcs->getframe_capture_and_save_image = &adcs_getframe_capture_and_save_image;
	adcs->getframe_reset_file_list = &adcs_getframe_reset_file_list;
	adcs->getframe_advance_file_list_index = &adcs_getframe_advance_file_list_index;
	adcs->getframe_initialize_file_download = &adcs_getframe_initialize_file_download;
	adcs->getframe_advance_file_read_pointer = &adcs_getframe_advance_file_read_pointer;
	adcs->getframe_erase_file = &adcs_getframe_erase_file;
	adcs->getframe_erase_all_files = &adcs_getframe_erase_all_files;
	adcs->getframe_set_boot_index = &adcs_getframe_set_boot_index;
	adcs->getframe_erase_program = &adcs_getframe_erase_program;
	adcs->getframe_upload_program_block = &adcs_getframe_upload_program_block;
	adcs->getframe_finalize_program_upload = &adcs_getframe_finalize_program_upload;

	adcs->setframe_identification = &adcs_setframe_identification;
	adcs->setframe_communication_status = &adcs_setframe_communication_status;
	adcs->setframe_telecommand_acknowledge = &adcs_setframe_telecommand_acknowledge;
	adcs->setframe_reset_cause = &adcs_setframe_reset_cause;
	adcs->setframe_actuator_commands = &adcs_setframe_actuator_commands;
	adcs->setframe_acp_execution_state = &adcs_setframe_acp_execution_state;
	adcs->setframe_acp_execution_times = &adcs_setframe_acp_execution_times;
	adcs->setframe_edac_and_latchup_counters = &adcs_setframe_edac_and_latchup_counters;
	adcs->setframe_start_up_mode = &adcs_setframe_start_up_mode;
	adcs->setframe_file_information = &adcs_setframe_file_information;
	adcs->setframe_file_block_crc = &adcs_setframe_file_block_crc;
	adcs->setframe_file_data_block = &adcs_setframe_file_data_block;
	adcs->setframe_power_control_selection = &adcs_setframe_power_control_selection;
	adcs->setframe_power_and_temperature_measurements = &adcs_setframe_power_and_temperature_measurements;
	adcs->setframe_adcs_state = &adcs_setframe_adcs_state;
	adcs->setframe_adcs_measurements = &adcs_setframe_adcs_measurements;
	adcs->setframe_current_time = &adcs_setframe_current_time;
	adcs->setframe_current_state = &adcs_setframe_current_state;
	adcs->setframe_estimated_attitude_angles = &adcs_setframe_estimated_attitude_angles;
	adcs->setframe_estimated_angular_rates = &adcs_setframe_estimated_angular_rates;
	adcs->setframe_satellite_position_llh = &adcs_setframe_satellite_position_llh;
	adcs->setframe_satellite_velocity_eci = &adcs_setframe_satellite_velocity_eci;
	adcs->setframe_magnetic_field_vector = &adcs_setframe_magnetic_field_vector;
	adcs->setframe_coarse_sun_vector = &adcs_setframe_coarse_sun_vector;
	adcs->setframe_fine_sun_vector = &adcs_setframe_fine_sun_vector;
	adcs->setframe_nadir_vector = &adcs_setframe_nadir_vector;
	adcs->setframe_rate_sensor_rates = &adcs_setframe_rate_sensor_rates;
	adcs->setframe_wheel_speed = &adcs_setframe_wheel_speed;
	adcs->setframe_cubesense_current = &adcs_setframe_cubesense_current;
	adcs->setframe_cubecontrol_current = &adcs_setframe_cubecontrol_current;
	adcs->setframe_peripheral_current_and_temperature_measurements = &adcs_setframe_peripheral_current_and_temperature_measurements;
	adcs->setframe_raw_sensor_measurements = &adcs_setframe_raw_sensor_measurements;
	adcs->setframe_angular_rate_covariance = &adcs_setframe_angular_rate_covariance;
	adcs->setframe_raw_nadir_sensor = &adcs_setframe_raw_nadir_sensor;
	adcs->setframe_raw_sun_sensor = &adcs_setframe_raw_sun_sensor;
	adcs->setframe_raw_css = &adcs_setframe_raw_css;
	adcs->setframe_raw_magnetometer = &adcs_setframe_raw_magnetometer;
	adcs->setframe_raw_gps_status = &adcs_setframe_raw_gps_status;
	adcs->setframe_raw_gps_time = &adcs_setframe_raw_gps_time;
	adcs->setframe_raw_gps_x = &adcs_setframe_raw_gps_x;
	adcs->setframe_raw_gps_y = &adcs_setframe_raw_gps_y;
	adcs->setframe_raw_gps_z = &adcs_setframe_raw_gps_z;
	adcs->setframe_estimation_data = &adcs_setframe_estimation_data;
	adcs->setframe_igrf_modelled_magnetic_field_vector = &adcs_setframe_igrf_modelled_magnetic_field_vector;
	adcs->setframe_modelled_sun_vector = &adcs_setframe_modelled_sun_vector;
	adcs->setframe_estimated_gyro_bias = &adcs_setframe_estimated_gyro_bias;
	adcs->setframe_estimation_innovation_vector = &adcs_setframe_estimation_innovation_vector;
	adcs->setframe_quaternion_error_vector = &adcs_setframe_quaternion_error_vector;
	adcs->setframe_quaternion_covariance = &adcs_setframe_quaternion_covariance;
	adcs->setframe_magnetorquer_command = &adcs_setframe_magnetorquer_command;
	adcs->setframe_wheel_speed_commands = &adcs_setframe_wheel_speed_commands;
	adcs->setframe_sgp4_orbit_parameters = &adcs_setframe_sgp4_orbit_parameters;
	adcs->setframe_configuration = &adcs_setframe_configuration;
	adcs->setframe_status_of_image_capture_and_save_operation = &adcs_setframe_status_of_image_capture_and_save_operation;
	adcs->setframe_uploaded_program_status = &adcs_setframe_uploaded_program_status;
	adcs->setframe_get_flash_program_list = &adcs_setframe_get_flash_program_list;
	adcs->setframe_reset = &adcs_setframe_reset;
	adcs->setframe_set_unix_time = &adcs_setframe_set_unix_time;
	adcs->setframe_adcs_run_mode = &adcs_setframe_adcs_run_mode;
	adcs->setframe_selected_logged_data = &adcs_setframe_selected_logged_data;
	adcs->setframe_power_control = &adcs_setframe_power_control;
	adcs->setframe_deploy_magnetometer_boom = &adcs_setframe_deploy_magnetometer_boom;
	adcs->setframe_trigger_adcs_loop = &adcs_setframe_trigger_adcs_loop;

	adcs->setframe_set_attitude_estimation_mode = &adcs_setframe_set_attitude_estimation_mode;
	adcs->setframe_set_attitude_control_mode = &adcs_setframe_set_attitude_control_mode;
	adcs->setframe_set_commanded_attitude_angles = &adcs_setframe_set_commanded_attitude_angles;
	adcs->setframe_set_wheel = &adcs_setframe_set_wheel;
	adcs->setframe_set_magnetorquer_output = &adcs_setframe_set_magnetorquer_output;
	adcs->setframe_set_start_up_mode = &adcs_setframe_set_start_up_mode;
	adcs->setframe_set_sgp4_orbit_parameters = &adcs_setframe_set_sgp4_orbit_parameters;
	adcs->setframe_set_configuration = &adcs_setframe_set_configuration;
	adcs->setframe_set_magnetorquer = &adcs_setframe_set_magnetorquer;
	adcs->setframe_set_wheel_configuration = &adcs_setframe_set_wheel_configuration;
	adcs->setframe_set_css_configuration = &adcs_setframe_set_css_configuration;
	adcs->setframe_set_sun_sensor_configuration = &adcs_setframe_set_sun_sensor_configuration;
	adcs->setframe_set_nadir_sensor_configuration = &adcs_setframe_set_nadir_sensor_configuration;
	adcs->setframe_set_magnetometer_configuration = &adcs_setframe_set_magnetometer_configuration;
	adcs->setframe_set_rate_sensor_configuration = &adcs_setframe_set_rate_sensor_configuration;
	adcs->setframe_set_detumbling_control_parameters = &adcs_setframe_set_detumbling_control_parameters;
	adcs->setframe_set_y_momentum_control_parameters = &adcs_setframe_set_y_momentum_control_parameters;
	adcs->setframe_set_moment_of_inertia = &adcs_setframe_set_moment_of_inertia;
	adcs->setframe_set_estimation_parameters = &adcs_setframe_set_estimation_parameters;
	adcs->setframe_save_configuration = &adcs_setframe_save_configuration;
	adcs->setframe_save_orbit_parameters = &adcs_setframe_save_orbit_parameters;
	adcs->setframe_capture_and_save_image = &adcs_setframe_capture_and_save_image;
	adcs->setframe_reset_file_list = &adcs_setframe_reset_file_list;
	adcs->setframe_advance_file_list_index = &adcs_setframe_advance_file_list_index;
	adcs->setframe_initialize_file_download = &adcs_setframe_initialize_file_download;
	adcs->setframe_advance_file_read_pointer = &adcs_setframe_advance_file_read_pointer;
	adcs->setframe_erase_file = &adcs_setframe_erase_file;
	adcs->setframe_erase_all_files = &adcs_setframe_erase_all_files;
	adcs->setframe_set_boot_index = &adcs_setframe_set_boot_index;
	adcs->setframe_erase_program = &adcs_setframe_erase_program;
	adcs->setframe_upload_program_block = &adcs_setframe_upload_program_block;
	adcs->setframe_finalize_program_upload = &adcs_setframe_finalize_program_upload;

}
