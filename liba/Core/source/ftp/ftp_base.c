/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file ftp_base.c
 * @author Brendan Bruner
 * @date Jul 21, 2015
 */
#include <ftp.h>
#include <packets/packet_base.h>
#include <packets/telecommand_packet.h>
#include <packets/telemetry_packet.h>
#include <core_defines.h>


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define GROUND_STATION_MAX_RX_LENGTH 50
#define GROUND_STATION_MAX_TX_LENGTH 100

#define FTP_LIST_MIN_PACKET_LENGTH 		(sizeof( uint16_t) + FILESYSTEM_MAX_NAME_LENGTH + 1 + 1)
#define FTP_DOWNLINK_MIN_PACKET_LENGTH 	(FILESYSTEM_MAX_NAME_LENGTH + 1 + sizeof(uint8_t) + sizeof(uint8_t))
#define DOWNLINK_COMMAND_DATA_OFFSET 	5
#define DOWNLINK_COMMAND_SEQ_OFFSET 	3
#define DOWNLINK_COMMAND_SIZE_OFFSET 	1
#define DOWNLINK_COMMAND_FS_ERR_OFFSET 	0
#define UPLINK_COMMAND_DATA_OFFSET		3
#define UPLINK_COMMAND_SIZE_OFFSET		1

/********************************************************************************/
/* Private Variable Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/
static void ftp_delete_file( ftp_t *ftp, uint8_t *packet, uint32_t packet_length )
{
	DEV_ASSERT( ftp );
	DEV_ASSERT( packet );

	filesystem_t *fs;
	fs_error_t err;
	uint32_t iter;

	fs = ftp->_fs;

	/* Make sure packet contains a valid string. */
	for( iter = 0; iter < packet_length; ++iter )
	{
		if( packet[iter] == '\0' )
		{
			break;
		}
	}
	if( iter == packet_length )
	{
		/* Loop ended naturally, not from break. packet contains no null character. */
		return;
	}

	err = fs->delete( fs, (char *) packet );
	if( err != FS_OK )
	{
		/* Failed to remove file. */
	}
}

static void ftp_list_files( ftp_t *ftp )
{
	DEV_ASSERT( ftp );

	file_t 			*file;
	fs_error_t 				err;
	uint32_t				size, iter, max_size, running_packet_length;
	uint8_t					out_data[GROUND_STATION_MAX_TX_LENGTH], is_finished;
	filesystem_t 			*fs;
	ground_station_t		*gs;
	dir_t					*dir;
	char const				*name;

	/* Fatal error check. */
	/* This is checking that the max size of a packet is at least big enough to downlink */
	/* one file name. */
	if( GROUND_STATION_MAX_TX_LENGTH <= FTP_LIST_MIN_PACKET_LENGTH )
	{
		DEV_ASSERT( NULL );
	}

	/* Set up local variables. */
	fs = ftp->_fs;
	gs = ftp->_gs;
	running_packet_length = 0;
	#ifdef d__LPC17XX__ /* Poor timmy isn't very smart.. must decrease buffer for him. */
	{
		max_size = GROUND_STATION_MAX_TX_LENGTH > 64 ? 64 : GROUND_STATION_MAX_TX_LENGTH;
	}
	#else
	{
		max_size = GROUND_STATION_MAX_TX_LENGTH;
	}
	#endif

	/* One second timeout. */
	dir = fs->open_dir( fs, &err, "", ONE_SECOND );
	if( err != FS_OK )
	{
		out_data[running_packet_length] = 0; /* Tells ground station no more packets will follow. */
		++running_packet_length;
		if( gs->write( gs, out_data, running_packet_length, FTP_PORT, USE_POLLING ) == 0 )
		{
			/* No packets got sent, no ground station in range. */
		}
		return;
	}

	/* Loop which lists files and downlinks results. */
	for( ;; )
	{

	#ifdef DEBUG
	#ifdef __LPC17XX__
	{
		/* In debug mode on LPC1769 the fs->list function doesn't work correctly */
		/* without this delay. The reason for this is unknown. */
	//	task_delay( 10 );
	}
	#endif
	#endif

		/* List a name in the current directory and error check it. */
		name = dir->list( dir, &err );
		if( err == FS_OK && name[0] != '\0' )
		{
			/* No error getting name of directory / file and not at the end of the list. */
			/* Open file and get its size. */
			/* If there was an error opening / getting file size, it is set to zero. */
			file = fs->open( fs, &err, name, FS_OPEN_EXISTING, USE_POLLING );
			size = 0;
			if( err == FS_OK )
			{
				size = file->size( file, &err );
				if( err != FS_OK )
				{
					size = 0;
				}
			}
			fs->close( fs, file );

			/* Fill in out data for telemetry packet. */
			to_network_from16( (uint16_t *) &size, out_data + running_packet_length );
			running_packet_length += sizeof( uint16_t );
			for( iter = 0; iter < FILESYSTEM_MAX_NAME_LENGTH+1; ++iter )
			{
				out_data[ running_packet_length ] = name[iter];
				++running_packet_length;
				if( name[iter] == '\0' )
				{
					break;
				}
			}

			/* Since no error occured, lets keep going. */
			is_finished = 0;
		}
		else
		{
			/* No error getting name, but at the end of the natural list. */
			/* OR an error occured getting name. Ether way, same action is taken. */
			/* Finished listing. */
			is_finished = 1;
		}

		if( is_finished == 1 )
		{
			/* Done listing. */
			/* Put stop condition into packet and break. */
			out_data[running_packet_length] = 0; /* Tells ground station no more packets will follow. */
			++running_packet_length;
			if( gs->write( gs, out_data, running_packet_length, FTP_PORT, USE_POLLING ) == 0 )
			{
				/* No packets got sent, no ground station in range. */
			}
			break;
		}

		/* Check if we can add more data to the packet without overflow. */
		if( running_packet_length + FTP_LIST_MIN_PACKET_LENGTH <= max_size )
		{
			/* Can fit in another packet. */
			continue;
		}

		/* If we are at this point then there are still more files / directories to list, */
		/* but the packet is full. Transmit it so we can fill it up again. */
		out_data[running_packet_length] = 1; /* Indicates another packet will follow this one. */
		++running_packet_length;
		if( gs->write( gs, out_data, running_packet_length, FTP_PORT, USE_POLLING ) == 0 )
		{
			/* No packets got sent */
			/* Stop trying to list. */
			break;
		}

		/* Reset the out data. */
		running_packet_length = 0;
	}
	/* Reset the listing. */
	dir->reset_list( dir );
	fs->close_dir( fs, dir );
}

static void ftp_downlink_file( ftp_t *ftp, uint8_t *packet, uint32_t packet_length )
{
	DEV_ASSERT( ftp );
	DEV_ASSERT( packet );

	file_t 					*file;
	fs_error_t 				err;
	uint32_t				size, bytes_read, max_read_size, iter;
	uint16_t				packets_to_send, seq_id;
	uint8_t					name_len;
	uint8_t					out_data[ GROUND_STATION_MAX_TX_LENGTH ];
	filesystem_t 			*fs;
	ground_station_t		*gs;

	/* Make sure packet contains a valid string. */
	for( iter = 0; iter < packet_length; ++iter )
	{
		if( packet[iter] == '\0' )
		{
			break;
		}
	}
	if( iter == packet_length )
	{
		/* Loop ended naturally, not from break. packet contains no null character. */
		return;
	}

	fs = ftp->_fs;
	gs = ftp->_gs;

	/* Fatal error check. */
	if( GROUND_STATION_MAX_TX_LENGTH < FTP_DOWNLINK_MIN_PACKET_LENGTH )
	{
		DEV_ASSERT( NULL );
	}

	/* Fill out data with name of file being downlinked. */
	for( name_len = 0; name_len < FILESYSTEM_MAX_NAME_LENGTH+1; ++name_len )
	{
		out_data[name_len] = packet[name_len];
		if( out_data[name_len] == '\0' )
		{
			++name_len;
			break;
		}
	}

	/* Open the file for reading. If there are any filesystem errors */
	/* then no data is downlinked. */
	#ifdef DEBUG
	#ifdef __LPC17XX__
	{
		/* The lpc board in debug mode tends to fail here without this delay :/ */
		task_delay( 500 * ONE_MS );
	}
	#endif
	#endif

	/* Open the file being downlinked and get its size in bytes. */
	file = fs->open( fs, &err, (char *) packet, FS_OPEN_EXISTING, USE_POLLING );
	size = 0;
	if( err == FS_OK )
	{
		size = file->size( file, &err );
		if( err != FS_OK ){ size = 0; }
	}

	#ifdef __LPC17XX__ /* Poor timmy isn't very smart.. must decrease buffer for him. */
	{
		max_read_size = GROUND_STATION_MAX_TX_LENGTH > 64 ?
						(64 - name_len - 2*sizeof(uint8_t)) : GROUND_STATION_MAX_TX_LENGTH;
	}
	#else
	{
		max_read_size = GROUND_STATION_MAX_TX_LENGTH - name_len - 2*sizeof(uint8_t);
	}
	#endif

	/* Create map data. */
	packets_to_send = size / max_read_size;
	/* Result must be rounded up. */
	if( packets_to_send * max_read_size < size )
	{
		++packets_to_send;
	}
	to_network_from16( &packets_to_send, out_data + name_len );
	/* Downlink the map packet. */
	if( gs->write( gs, out_data, name_len + sizeof( uint16_t ), FTP_PORT, USE_POLLING ) == 0 )
	{
		/* No packets got sent, no ground station in range. */
		fs->close( fs, file );
		return;
	}

	/* Read out data and downlink it. */
	bytes_read = 0;
	seq_id = 0;
	while( size > 0 )
	{
		/* Read as much data out as possible. */
		err = file->read(	file,
							out_data + DOWNLINK_COMMAND_DATA_OFFSET,
							size > max_read_size ? max_read_size : size,
							&bytes_read
						);

		/* File out the telemetry packet with data. */
		out_data[DOWNLINK_COMMAND_FS_ERR_OFFSET] = err;
		to_network_from16( (uint16_t *) &bytes_read, out_data + DOWNLINK_COMMAND_SIZE_OFFSET );
		to_network_from16( (uint16_t *) &seq_id, out_data + DOWNLINK_COMMAND_SEQ_OFFSET );

		/* Downlink the packet. */
		if( gs->write( gs, out_data, bytes_read + sizeof( uint8_t ) + 2 * sizeof( uint16_t ), FTP_PORT, USE_POLLING ) == 0 )
		{
			/* No packets got sent, no ground station in range. */
			break;
		}

		/* If a filesystem error occurred, stop reading. */
		if( err != FS_OK )
		{
			break;
		}

		/* Update metadata. */
		size = size - bytes_read;
	}
	fs->close( fs, file );
}

static void ftp_uplink_file( ftp_t *ftp, uint8_t *packet, uint32_t packet_length )
{
	DEV_ASSERT( ftp );
	DEV_ASSERT( packet );

	filesystem_t 		*fs;
	fs_error_t			err;
	file_t 				*file;
	uint32_t 			written, bytes_read, iter;
	uint16_t			packets_following, size;
	uint8_t				ftp_packet[GROUND_STATION_MAX_RX_LENGTH];
	ground_station_t	*gs;

	gs = ftp->_gs;
	fs = ftp->_fs;

	/* Make sure packet contains a valid string. */
	for( iter = 0; iter < packet_length; ++iter )
	{
		if( packet[iter] == '\0' )
		{
			break;
		}
	}
	if( iter == packet_length )
	{
		/* Loop ended naturally, not from break. packet contains no null character. */
		return;
	}

	to16_from_network( &packets_following, packet + iter + 1 );

	/* Open the file to be written. */
	/* Use a one second timeout. */
	file = fs->open( fs, &err, (char *) packet, FS_CREATE_ALWAYS, ONE_SECOND );
	if( err == FS_OK )
	{
		/* File opened, start reading packets from ground station and writing them */
		/* into memory. */
		for( ; packets_following > 0; --packets_following )
		{

			/* Use a timeout of one second for incoming packets. */
			bytes_read = gs->read( gs, ftp_packet, GROUND_STATION_MAX_RX_LENGTH, FTP_PORT, 5 * ONE_SECOND );
			if( bytes_read == 0 )
			{
				/* Ground station failed to send data before timeout. */
				break;
			}

			/* Got a packet of data, error check it. */
			if( ftp_packet[0] != FTP_SERVICE_PUT )
			{
				/* Got a bad packet. Abort operation. */
				break;
			}

			to16_from_network( &size, ftp_packet + UPLINK_COMMAND_SIZE_OFFSET );

			/* Write the packet into memory. */
			err = file->write( file, ftp_packet + UPLINK_COMMAND_DATA_OFFSET, (uint32_t) size, &written );
			if( err != FS_OK )
			{
				/* Some type of error writing, abort the operation. */
				break;
			}
		}
	}

	fs->close( fs, file );
}

static void ftp_service_task( void *ftp_void)
{
	ftp_t 				*ftp;
	ground_station_t 	*gs;
	uint8_t 			ftp_packet[GROUND_STATION_MAX_RX_LENGTH];
	uint32_t			bytes_read;

	ftp = (ftp_t *) ftp_void;
	gs = ftp->_gs;

	for( ;; )
	{
		bytes_read = gs->read( gs, ftp_packet, GROUND_STATION_MAX_RX_LENGTH, FTP_PORT, BLOCK_FOREVER );
		if( bytes_read >= 1 )
		{
			/* got an ftp packet. */
			switch( ftp_packet[0] )
			{
				case FTP_SERVICE_RM:
					ftp_delete_file( ftp, ftp_packet+1, bytes_read-1 );
					break;

				case FTP_SERVICE_LIST:
					ftp_list_files( ftp );
					break;

				case FTP_SERVICE_GET:
					ftp_downlink_file( ftp, ftp_packet+1, bytes_read-1 );
					break;

				case FTP_SERVICE_PUT_MAP:
					ftp_uplink_file( ftp, ftp_packet+1, bytes_read-1 );
					break;

				default:
					break;
			}
		}
		task_delay( FTP_SLEEP_PERIOD );
	}
}


/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Constructor / Destructor														*/
/********************************************************************************/
uint8_t initialize_ftp_service( ftp_t *ftp, driver_toolkit_t *kit )
{
	DEV_ASSERT( ftp );
	DEV_ASSERT( kit );

	base_t err;

	ftp->_fs = kit->fs;
	ftp->_gs = kit->gs;

	/* Create ftp service task. */
	err = create_task( ftp_service_task, FTP_NAME, FTP_STACK, (void *) ftp, FTP_PRIO, &ftp->_service_task );
	if( err != TASK_CREATED || ftp->_service_task == NULL )
	{
		return FTP_FAILURE;
	}

	/* Suspend ftp service task so that start_ftp_service( ) method will resume it. */
	suspend_task( ftp->_service_task );

	return FTP_SUCCESS;
}


void destroy_ftp_service( ftp_t *ftp )
{
	DEV_ASSERT( ftp );

	delete_task( ftp->_service_task );
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
void start_ftp_service( ftp_t *ftp )
{
	DEV_ASSERT( ftp );

	resume_task( ftp->_service_task );
}

