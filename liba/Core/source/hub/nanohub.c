/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file nanohub.c
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */

#include <hub/nanohub.h>
#error "Unimplement Nanohub API"

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static bool_t power_line( hub_t* self, hub_power_lines_t line, hub_power_line_state_t state )
{
	DEV_ASSERT( self );
	return false;
}

static bool_t deploy( hub_t* self, hub_deployment_line_t line )
{
	DEV_ASSERT( self );
	return false;
}

static bool_t power( hub_t* self, eps_t* eps, bool_t state )
{
	DEV_ASSERT( self );
	DEV_ASSERT( eps );
	return false;
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
void initialize_nanohub( nanohub_t* self )
{
	DEV_ASSERT( self );

	/* Initialize super class. */
	extern void initialize_hub_( hub_t* );
	initialize_hub_( (hub_t*) self );

	/* Override inherited methods. */
	((hub_t*) self)->power = power;
	((hub_t*) self)->power_line = power_line;
	((hub_t*) self)->deploy = deploy;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


