/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file driver_toolkit_base.c
 * @author Brendan Bruner
 * @date Jun 3, 2015
 */

#include <driver_toolkit/driver_toolkit.h>
#include <portable_types.h>
#include <core_defines.h>

/**
 * @memberof driver_toolkit_t
 * @protected
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 */
bool_t initialize_driver_toolkit_( driver_toolkit_t* toolkit, filesystem_t* fs )
{
	DEV_ASSERT( toolkit );
	DEV_ASSERT( fs );

	logger_error_t tl_err;
	tp_error_t tp_err;

	if( initialize_ptimer_controller( ) == NULL )
	{
		return false;
	}

	toolkit->fs = fs;

	tp_err = initialize_telemetry_priority( &toolkit->priority, toolkit->fs, PRIORITY_LOG_FILE );
	if( tp_err != TP_OK )
	{
		return false;
	}

	tl_err = initialize_wod_logger( &toolkit->_.wod_logger_mem, toolkit->fs, toolkit->rtc );
	if( tl_err != LOGGER_OK && tl_err != LOGGER_RECOVERED )
	{
		return false;
	}
	tl_err = initialize_dfgm_logger( &toolkit->_.dfgm_logger_mem, toolkit->fs );
	if( tl_err != LOGGER_OK && tl_err != LOGGER_RECOVERED )
	{
		return false;
	}
	tl_err = initialize_mnlp_logger( &toolkit->_.mnlp_logger_mem, toolkit->fs );
	if( tl_err != LOGGER_OK && tl_err != LOGGER_RECOVERED )
	{
		return false;
	}
	tl_err = initialize_command_logger( &toolkit->_.cmnd_status_logger_mem, toolkit->fs );
	if( tl_err != LOGGER_OK && tl_err != LOGGER_RECOVERED )
	{
		return false;
	}
	tl_err = initialize_state_logger( &toolkit->_.state_logger_mem, toolkit->fs );
	if( tl_err != LOGGER_OK && tl_err != LOGGER_RECOVERED )
	{
		return false;
	}

	toolkit->wod_logger = (logger_t *) &toolkit->_.wod_logger_mem;
	toolkit->dfgm_logger = (logger_t *) &toolkit->_.dfgm_logger_mem;
	toolkit->mnlp_logger = (logger_t *) &toolkit->_.mnlp_logger_mem;
	toolkit->cmnd_status_logger = (logger_t *) &toolkit->_.cmnd_status_logger_mem;
	toolkit->state_logger = (logger_t *) &toolkit->_.state_logger_mem;

	return true;
}

void destroy_driver_toolkit( driver_toolkit_t *kit )
{
	DEV_ASSERT( kit );

	destroy_adcs( &kit->adcs );
	destroy_ground_station( kit->gs );

	destroy_rtc( kit->rtc );

	kit->fs->destroy( kit->fs );

	destroy_telemetry_priority( &kit->priority );
	((logger_t *) &kit->_.wod_logger_mem)->destroy( (logger_t *) &kit->_.wod_logger_mem );
	((logger_t *) &kit->_.dfgm_logger_mem)->destroy( (logger_t *) &kit->_.dfgm_logger_mem );
	((logger_t *) &kit->_.mnlp_logger_mem)->destroy( (logger_t *) &kit->_.mnlp_logger_mem );
	((logger_t *) &kit->_.cmnd_status_logger_mem)->destroy( (logger_t *) &kit->_.cmnd_status_logger_mem );
	((logger_t *) &kit->_.state_logger_mem)->destroy( (logger_t *) &kit->_.state_logger_mem );
}
