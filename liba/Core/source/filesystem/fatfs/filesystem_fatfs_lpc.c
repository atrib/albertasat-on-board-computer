/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file filesystem_fatfs_lpc.c
 * @author Brendan Bruner
 * @date Aug 14, 2015
 */

#include <filesystems/fatfs/filesystem_fatfs_lpc.h>
#include <core_cm3.h>
#include <portable_types.h>
#include <spi_sd_lpc17xx.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define FILESYSTEM_FATFS_LPC_IS_INIT	1
#define FILESYSTEM_FATFS_LPC_NOT_INIT	0

#define DEFAULT_DRIVE ""
#define MOUNT_LATER 0


/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/
/* Used by native fatfs filesystem for mounting. */
static FATFS _filesystem_fatfs_lpc_native_filesystem_;

/* Used to prevent re-initialization of singletons. */
static uint8_t _filesystem_fatfs_lpc_is_init_ = FILESYSTEM_FATFS_LPC_NOT_INIT;
/* Used to prevent duplicate spi initialization. */
static uint8_t _filesystem_fatfs_lpc_spi_is_init_ = FILESYSTEM_FATFS_LPC_NOT_INIT;


/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/
/**
 * @memberof filesystem_fatfs_lpc_t
 * @private
 * @brief
 * 		Defines a timer callback for a FreeRTOS software timer.
 * @details
 * 		Defines a timer callback for a FreeRTOS software timer.
 */
static void fatfs_sd_timer_callback(  software_timer_t pxTimer )
{
	UNUSED( pxTimer );
	/* Do nothing. */
}


/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_filesystem_fatfs_lpc( filesystem_fatfs_lpc_t *self )
{
	uint8_t init_err;

	/* Initialize super data. */
	init_err = _initialize_filesystem_fatfs( (filesystem_fatfs_t *) self );
	if( init_err != FILESYSTEM_SUCCESS )
	{
		/* Failed to initialize super data, do not continue. */
		return init_err;
	}

	/* Check if singletons have already been initialized. */
	if( _filesystem_fatfs_lpc_is_init_ == FILESYSTEM_FATFS_LPC_IS_INIT )
	{
		/* Singletons are initialized, don't duplicate initialize them. */
		return FILESYSTEM_SUCCESS;
	}

	/* Initialize software timer1. */
	sdtimer1 = xTimerCreate( (signed char*) "SDTimer1", 100/portTICK_RATE_MS, pdFALSE, (void*) 1, fatfs_sd_timer_callback);
	if( sdtimer1 == NULL )
	{
		/* Failed to initialize timer 1. */
		return FILESYSTEM_FAILURE;
	}

	/* Initialize software timer2. */
	sdtimer2 = xTimerCreate( (signed char*) "SDTimer2", 500/portTICK_RATE_MS, pdFALSE, (void*) 2, fatfs_sd_timer_callback);
	if( sdtimer2 == NULL )
	{
		/* Failed to initialize timer 2. */
		xTimerDelete( sdtimer1, USE_POLLING );
		return FILESYSTEM_FAILURE;
	}

	/* Initialize the spi. */
	if( _filesystem_fatfs_lpc_spi_is_init_ == FILESYSTEM_FATFS_LPC_NOT_INIT )
	{
		spi_init( );
		_filesystem_fatfs_lpc_spi_is_init_ = FILESYSTEM_FATFS_LPC_IS_INIT;
	}

	/* Mount the filesystem. */
	if( f_mount( &_filesystem_fatfs_lpc_native_filesystem_, DEFAULT_DRIVE, MOUNT_LATER ) != FR_OK )
	{
		/* Failed to mount. */
		xTimerDelete( sdtimer1, USE_POLLING );
		xTimerDelete( sdtimer2, USE_POLLING );
		return FILESYSTEM_FAILURE;
	}

	return FILESYSTEM_SUCCESS;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


