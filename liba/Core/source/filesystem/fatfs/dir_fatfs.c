/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file fs_dir_fatfs.c
 * @author Brendan Bruner
 * @date Jul 22, 2015
 */
#include <filesystems/fatfs/dir_fatfs.h>
#include <filesystems/fatfs/fatfs_map.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/


/********************************************************************************/
/* Private Variable Defines														*/
/********************************************************************************/


/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static char const *list( dir_t *derived, fs_error_t *err )
{
	dir_fatfs_t *self = (dir_fatfs_t *) derived;

	DEV_ASSERT( self );
	DEV_ASSERT( err );
	ASSERT_FATFS_HANDLE_RETURN( self, *err, _dir_get_file_name( (dir_t *) self ) );

	FRESULT 		native_err;
	fat_dir_t		*dir;
	char 			*file_name;

	/* Get the directory's native handle. */
	dir = (fat_dir_t *) fs_handle_get_native( (fs_handle_t *) self );

	/* Initially set the name of the listed file to NULL. */
	_dir_set_file_name( (dir_t *) self, "\0" );

	/* Set up fatfs variables. */
#if _USE_LFN
	self->_file_info_.lfname = (TCHAR *) _dir_get_file_name( (dir_t *) self );
	self->_file_info_.lfsize = (UINT) FILESYSTEM_MAX_NAME_LENGTH+1;
#endif

	/* Get the next file/directory name. */
	native_err = f_readdir( dir, &self->_file_info_ );
	if( native_err != FR_OK )
	{
		/* Error. return */
		*err = _fatfs_result_map[native_err];
		return _dir_get_file_name( (dir_t *) self );
	}

	/* Must use fatfs structures to get the file/directory name read. */
	/* Taken from http://elm-chan.org/fsw/ff/en/readdir.html */
#if _USE_LFN
	file_name = *self->_file_info_.lfname ? self->_file_info_.lfname : self->_file_info_.fname;
#else
	file_name = self->_file_info_.fname;
#endif

	/* Set the name in self. */
	_dir_set_file_name( (dir_t *) self, file_name );
	*err = FS_OK;
	return _dir_get_file_name( (dir_t *) self );
}

static fs_error_t reset_list( dir_t *self )
{
	DEV_ASSERT( self );
	ASSERT_FATFS_HANDLE_RETURN_ERR( self );

	fs_error_t	err;
	fat_dir_t	*dir;

	/* Get native fatfs directory structure. */
	dir = (fat_dir_t *) fs_handle_get_native( (fs_handle_t *) self );

	/* Restart where listing begins. */
	err = f_readdir( dir, NULL );
	return _fatfs_result_map[err];
}


/********************************************************************************/
/* Constructor / Destructor														*/
/********************************************************************************/
uint8_t initialize_dir_fatfs( dir_fatfs_t *self, filesystem_t *fs )
{
	DEV_ASSERT( self );

	uint8_t init_err;

	/* Initiailze super struct. */
	init_err = _initialize_dir( (dir_t *) self, fs, HANDLE_FATFS );
	if( init_err != FS_HANDLE_SUCCESS )
	{
		return init_err;
	}

	/* Assign virtual functoin pointers. */
	((dir_t *) self)->list = list;
	((dir_t *) self)->reset_list = reset_list;

	/* Initialize member variables. */
	((fs_handle_t *) self)->_native_handle_ = (void *) &self->_native_dir_;

	return FS_HANDLE_SUCCESS;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


