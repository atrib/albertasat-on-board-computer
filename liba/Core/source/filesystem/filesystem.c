/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file filesystem_base.c
 * @author Brendan Bruner
 * @date May 12, 2015
 */

#include <filesystems/filesystem.h>
#include <filesystems/file_null.h>
#include <filesystems/dir_null.h>


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define FILESYSTEM_IS_INIT	1
#define FILESYSTEM_NOT_INIT	0


/********************************************************************************/
/* Private Singleton Variables													*/
/********************************************************************************/
/* Pointed to by filesystem_t::_null_file_*/
static file_null_t 	_filesystem_null_file_;

/* Pointed to by filesystem_t::_null_dir_ */
static dir_null_t	_filesystem_null_dir_;

/* Used to determine if the null handle has been initialized */
static uint8_t		_filesystem_is_init_ = FILESYSTEM_NOT_INIT;


/********************************************************************************/
/* Protected Methods															*/
/********************************************************************************/
file_t *_filesystem_get_null_file( filesystem_t *self )
{
	DEV_ASSERT( self );
	return (file_t *) self->_null_file_;
}

dir_t *_filesystem_get_null_dir( filesystem_t *self )
{
	DEV_ASSERT( self );
	return (dir_t *) self->_null_dir_;
}


/********************************************************************************/
/* Public Methods																*/
/********************************************************************************/



/********************************************************************************/
/* Destructor																	*/
/********************************************************************************/
static void destroy( filesystem_t *self )
{
	DEV_ASSERT( self );
}


/********************************************************************************/
/* Virtual Methods																*/
/********************************************************************************/
static file_t *open( filesystem_t *self, fs_error_t *err, char const *name, fs_open_attribute_t attri, block_time_t block )
{
	DEV_ASSERT( self );
	DEV_ASSERT( err );
	DEV_ASSERT( name );

	*err = FS_NULL_HANDLE;
	return _filesystem_get_null_file( self );
}

static fs_error_t close(filesystem_t *self, file_t *handle )
{
	DEV_ASSERT( self );
	DEV_ASSERT( handle );
	return FS_NO_FILESYSTEM;
}

static fs_error_t close_all( filesystem_t *self )
{
	DEV_ASSERT( self );
	return FS_NO_FILESYSTEM;
}

static fs_error_t delete( filesystem_t *self, char const *file_name )
{
	DEV_ASSERT( self );
	DEV_ASSERT( file_name );
	return FS_NO_FILESYSTEM;
}

static fs_error_t delete_handle( filesystem_t *self, file_t *handle )
{
	DEV_ASSERT( self );
	DEV_ASSERT( handle );
	return FS_NO_FILESYSTEM;
}

static fs_error_t reformat( filesystem_t *self )
{
	DEV_ASSERT( self );
	return FS_NO_FILESYSTEM;
}

static fs_error_t rename_file( filesystem_t *self, char const *file_name, char const *new_name )
{
	DEV_ASSERT( self );
	DEV_ASSERT( file_name );
	DEV_ASSERT( new_name );
	return FS_NO_FILESYSTEM;
}

static fs_error_t rename_handle( filesystem_t *self, file_t *handle, char const *new_name )
{
	DEV_ASSERT( self );
	DEV_ASSERT( handle );
	DEV_ASSERT( new_name );
	return FS_NO_FILESYSTEM;
}

static fs_error_t remap( filesystem_t *self, file_t *handle, char const *file_name, fs_open_attribute_t attri )
{
	DEV_ASSERT( self );
	DEV_ASSERT( handle );
	DEV_ASSERT( file_name );
	return FS_NO_FILESYSTEM;
}

static fs_error_t close_dir( filesystem_t *self, dir_t *dir )
{
	DEV_ASSERT( self );
	DEV_ASSERT( dir );
	return FS_NO_FILESYSTEM;
}

static dir_t *open_dir( filesystem_t *self, fs_error_t *err, char const *dir_name, block_time_t block )
{
	DEV_ASSERT( self );
	DEV_ASSERT( err );
	DEV_ASSERT( dir_name );
	*err = FS_NULL_HANDLE;
	return _filesystem_get_null_dir( self );
}

static fs_error_t file_exists( filesystem_t *self, char const *name )
{
	DEV_ASSERT( self );
	DEV_ASSERT( name );
	return FS_NO_FILESYSTEM;
}

/********************************************************************************/
/* Constructor																	*/
/********************************************************************************/
uint8_t _initialize_filesystem( filesystem_t *self )
{
	DEV_ASSERT( self );

	self->_null_file_ = &_filesystem_null_file_;
	self->_null_dir_ = &_filesystem_null_dir_;

	/* Bind method pointers. */
	self->open = open;
	self->close = close;
	self->close_all = close_all;
	self->delete = delete;
	self->delete_handle = delete_handle;
	self->reformat = reformat;
	self->rename = rename_file;
	self->rename_handle = rename_handle;
	self->remap = remap;
	self->open_dir = open_dir;
	self->close_dir = close_dir;
	self->file_exists = file_exists;
	self->destroy = destroy;

	if( _filesystem_is_init_ == FILESYSTEM_NOT_INIT )
	{
		_filesystem_is_init_ = FILESYSTEM_IS_INIT;
		initialize_dir_null( self->_null_dir_, self );
		initialize_file_null( self->_null_file_, self );
	}

	return FILESYSTEM_SUCCESS;
}
