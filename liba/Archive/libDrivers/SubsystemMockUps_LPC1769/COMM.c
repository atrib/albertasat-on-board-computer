/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file COMM.c
 * @author Brendan Bruner
 * @date 2015-01-12
 */
#include <stdio.h>
#include <string.h>
#include "COMM.h"
#include "FreeRTOS.h"
#include "task.h"
#include "TelecommandPacket.h"
#include "TelemetryPacket.h"

#define COMM_GATEWAY_PERIOD_MS	100 / portTICK_RATE_MS
#define COMM_GATEWAY_PRIORITY	(tskIDLE_PRIORITY + 1)
#define COMM_GATEWAY_STACK		5

struct telecommand_packet_t fullcommand;
struct telecommand_packet_t emptycommand;

/*****************************************************************************/
/* Methods																	 */
/*****************************************************************************/
/**
 * @public @memberof COMM
 */
static struct telecommand_packet_t *blockOnTelecommand(COMM *obj)
{
	struct telecommand_packet_t *packet = &obj->data.telecommandPacket;

	return packet;
}

/**
 * @details
 * 		Gets teleCommand packet from CSP network.
 * @param obj
 * 		A pointer to the COMM object.
 * @remark
 * 		Thread safe.
 * 		Non reentrant.
 * @public @memberof COMM
 */
static struct telecommand_packet_t getCommand(COMM *obj)
{
		strcpy(emptycommand.telecommand, "none");
		return emptycommand;
}

/*****************************************************************************/
/* Constructor																 */
/*****************************************************************************/
Constructor(COMM)
{
	LinkMethod(blockOnTelecommand);
	LinkMethod(getCommand);
}
