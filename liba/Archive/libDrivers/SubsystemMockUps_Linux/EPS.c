/*
 * Copyright (C) 2015 Collin Cupido, Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file Detumble.c
 * @author Collin Cupido
 * @author Brendan Bruner
 * @date 2014-12-28
 */

#include "EPS.h"

/*
 * A sample of possible battery voltages measured at different time
 * on the EPS.
 */
#define SIZE_OF_VOLTAGES 19
static int voltages[SIZE_OF_VOLTAGES] = {-4, 1500, 16200, 16400, 16200, 16800,
										 16400, 16000, 13500, 12000, 12000,
										 14000, 16000, 16400, 16000, 16300,
										 13000, 890, -4};


/*****************************************************************************/
/* Methods																	 */
/*****************************************************************************/
/**
 * @brief
 * 		Retrieve housekeeping data from EPS subsystem.
 * @param obj
 * 		A pointer to the EPS object this method is being invoked from.
 * @remark
 * 		Thread safe.
 * 		Non reentrant.
 * @public @memberof EPS
 */
static int getHouseKeeping(EPS *obj)
{
	int volt = voltages[obj->data.counter % SIZE_OF_VOLTAGES];
	++obj->data.counter;
	return volt;
}

/*****************************************************************************/
/* Constructor																 */
/*****************************************************************************/
/**
 * @details
 * 		Constructor for EPS objects.
 * @remark
 * 		Thread safe.
 * 		Reentrant.
 * @public @memberof EPS
 */
Constructor(EPS)
{
	LinkMethod(getHouseKeeping);
	obj->data.counter = 0;
	obj->data.voltage = 0;
}
