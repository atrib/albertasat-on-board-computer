/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file PostEjection.h
 * @author Brendan Bruner
 * @date 2015-01-07
 */

#ifndef POSTEJECTION_H_
#define POSTEJECTION_H_

#include "Class.h"
#include "State.h"

/**
 * @struct PostEjection
 * @brief Behavior after power is turned on and satellite is ejected from pod.
 * @extends State
 */
Class(PostEjection) Extends(State)
	Data
	Methods
EndClass;

#endif /* POSTEJECTION_H_ */
