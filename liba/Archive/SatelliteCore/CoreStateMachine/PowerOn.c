/*
 * Copyright (C) 2015 Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file PowerOn.c
 * @author Brendan Bruner
 * @date 2014-12-22
 *
 * Implements methods which override methods inherited from State class.
 */

#include "PowerOn.h"
#include "Satellite.h"

Constructor(PowerOn)
{
	Super(State);
	((State *) obj)->data._testID = POWER_ON_TEST_ID;
}
