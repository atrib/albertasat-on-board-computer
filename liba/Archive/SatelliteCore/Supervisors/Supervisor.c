/*
 * Copyright (C) 2015 Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file Supervisor.c
 * @author Brendan Bruner
 * @date 2014-12-31
 *
 * Implements methods of Supervisor class.
 */

#include "Supervisor.h"
#include "Satellite.h"

#define DEFAULT_PERIOD (period_t) 1000

/*****************************************************************************/
/* Methods																	 */
/*****************************************************************************/
/**
 * @brief
 * 		Sets the period of the Supervisor task
 * @details
 * 		Sleep time of supervisor task in ms. This requires that the
 * 		supvisortTask() method calls vTaskDelay( period / portTICK_RATE_MS ).
 * 		This is required to ensure the period is in ms and not some other time
 * 		increment.
 * @param obj
 * 		A pointer to the object invoking this method.
 * @param period
 * 		The period in ms.
 * @remark
 * 		Thread safe.
 * 		Non-reentrant.
 * @public @memberof Supervisor
 */
static void setPeriod(Supervisor *obj, period_t period)
{
	xSemaphoreTake(obj->data._setMutex, portMAX_DELAY);
	obj->data._period = period;
	xSemaphoreGive(obj->data._setMutex);
}

/**
 * @brief
 * 		Sets the Satellite object the supervisorTask() will have access to.
 * @param obj
 * 		A pointer to the object invoking this method.
 * @param sat
 * 		A pointer to the Satellite object being set.
 * @remark
 *		Thread safe.
 * 		Non-reentrant.
 * @public @memberof Supervisor
 */
static void setSatellite(Supervisor *obj, Satellite *sat)
{
	xSemaphoreTake(obj->data._setMutex, portMAX_DELAY);
	obj->data._sat = sat;
	xSemaphoreGive(obj->data._setMutex);

}

/**
 * @brief
 * 		Returns a pointer to the Satellite object that has been set.
 * @param obj
 * 		A pointer to the object invoking this method.
 * @return
 * 		A pointer to the Satellite object set.
 * @remark
 * 		Thread safe.
 * 		Reentrant.
 * @public @memberof Supervisor
 */
static Satellite *getSatellite(Supervisor *obj)
{
	Satellite *sat;

	xSemaphoreTake(obj->data._setMutex, portMAX_DELAY);
	sat = obj->data._sat;
	xSemaphoreGive(obj->data._setMutex);

	return sat;
}

/**
 * @brief
 * 		Returns the task handle to the FreeRTOS task being run.
 * @param obj
 * 		A pointer to the object invoking this method.
 * @return
 * 		The xTaskHandle pointer of the FreeRTOS task. Returns NULL if the task
 * 		has not been created yet with Supervisor::initSupervisorTask
 * @remark
 * 		Thread safe.
 * 		reentrant.
 * @public @memberof Supervisor
 */
static xTaskHandle *getTaskHandle(Supervisor *obj)
{
	return &obj->data._taskHandle;
}

/**
 * @brief
 * 		Starts the supervisorTask() - ie, the task will begin executing after
 * 		this is called.
 * @param obj
 * 		A pointer to the object invoking this method.
 * @attention
 * 		A call to initSupervisorTask() must be made before the first call
 * 		to this method. This method does nothing if initSupervisorTask() is
 * 		not called first.
 * @remark
 * 		Thread safe.
 * 		Non-reentrant
 * @public @memberof Supervisor
 */
static void start(Supervisor *obj)
{
	if(obj->data._taskHandle != NULL)
	{
		vTaskResume(obj->data._taskHandle);
	}
}

/**
 * @brief
 * 		Stops the supervisorTask() - ie, the task will stop executing after
 * 		this is called.
 * @param obj
 * 		A pointer to the object invoking this method.
 * @attention
 * 		A call to initSupervisorTask() must be made before the first call
 * 		to this method. This method does nothing if initSupervisorTask() is
 * 		not called first.
 * @remark
 * 		Thread safe.
 * 		Non-reentrant
 * @public @memberof Supervisor
 */
static void stop(Supervisor *obj)
{
	if(obj->data._taskHandle != NULL)
	{
		vTaskSuspend(obj->data._taskHandle);
	}
}

/**
 * @brief
 * 		The FreeRTOS task that is executed.
 * @details
 * 		This method defines the code that is executed by a FreeRTOS task
 * 		when a call the start() method is made.
 *
 * 		Subclasses of Supervisor must override this method to define what
 * 		is done by the FreeRTOS task.
 *
 * 		This method must never be called by the application - it is a
 * 		protected method.
 *
 * 		This method is only called once at the beginning of execution. If
 * 		this method is exited it can not be reentered by the same object.
 *		Therefore, implementations should contains a for(;;) loop.
 *
 *		This implementation does nothing - it is up to the subclasses to
 *		override it.
 * @remark
 * 		Subclasses of Supervisor must override this method to define what
 * 		their FreeRTOS task does.
 * @attention
 * 		This is a protected method. The application must never call it.
 * @param obj
 * 		A pointer to the object invoking this method.
 * @protected @memberof Supervisor
 */
static void _supervisorTask(Supervisor *obj)
{
	return;
}

/**
 * @brief
 * 		The actual FreeRTOS task formated such that the FreeRTOS API will accept
 * 		it.
 * @private @memberof Supervisor
 */
static void freeRTOSTask(void *arg)
{
	Supervisor *obj = (Supervisor *) arg;
	for(;;)
	{
		obj->_supervisorTask(obj);
		vTaskDelay( obj->data._period / portTICK_RATE_MS );
	}
	for(;;);
}

/**
 * @brief
 * 		Creates the Supervisor FreeRTOS task (which will start executing).
 * @param obj
 * 		A pointer to the object invoking the method
 * @param name
 * 		The name of the task - for debugging.
 * @param stackSize
 * 		The size of the tasks stack. Size in bytes equals stackSize * stack width.
 * @param prio
 * 		Priority to set the task to.
 * @remark
 * 		Thread safe.
 * 		Non-reentrant.
 * @attention
 * 		This method must be called before the first call to start().
 * @public @memberof Supervisor
 */
static void initSupervisorTask( Supervisor *obj,
								char const * const name,
								unsigned short stackSize,
								unsigned portBASE_TYPE prio)
{
	/* Prevent multiple calls of this method from creating more than one task. */
	xSemaphoreTake(obj->data._createdMutex, portMAX_DELAY);
	if( obj->data._isTaskCreated == TASK_NOT_CREATED )
	{
		xTaskCreate(freeRTOSTask,
					(signed char const * const) name,
					stackSize,
					(void *) obj,
					prio,
					&(obj->data._taskHandle));

		obj->data._isTaskCreated = TASK_CREATED;
	}
	xSemaphoreGive(obj->data._createdMutex);
}
/*****************************************************************************/
/* Constructor																 */
/*****************************************************************************/
/**
 * @brief
 * 		Constructor for Supervisor class
 * @public @memberof Supervisor
 */
Constructor(Supervisor)
{
	LinkMethod(setPeriod);
	LinkMethod(setSatellite);
	LinkMethod(getSatellite);
	LinkMethod(getTaskHandle);
	LinkMethod(start);
	LinkMethod(stop);
	LinkMethod(_supervisorTask);
	LinkMethod(initSupervisorTask);

	obj->data._period = DEFAULT_PERIOD;
	obj->data._isTaskCreated = TASK_NOT_CREATED;
	obj->data._taskHandle = NULL;

	obj->data._createdMutex = xSemaphoreCreateMutex();
	if( obj->data._createdMutex == NULL )
	{
		/* TODO: Handle failed mutex creation. */
	}
	else
	{
		xSemaphoreGive( obj->data._createdMutex );
	}

	obj->data._setMutex = xSemaphoreCreateMutex();
	if( obj->data._setMutex == NULL )
	{
		/* TODO: Handle failed mutex creation. */
	}
	else
	{
		xSemaphoreGive( obj->data._setMutex );
	}
}
