/*
 * Test_Satellite.c
 *
 *  Created on: 2015-01-03
 *      Author: brendan
 */

#include "unit.h"
#include "Test_Satellite.h"
#include "Satellite.h"

TEST(set_state)
{
	Satellite sat;
	State *testState;

	newSatellite(&sat);
	testState = (State *) 0x75;

	sat.setCurrentState(&sat, testState);

	/* Two asserts ensure mutex is behaving properly. */
	ASSERT("Satellite: set_state 1", testState == sat.getCurrentState(&sat));
	ASSERT("Satellite: set_state 2", testState == sat.getCurrentState(&sat));

	return TEST_END;
}

TEST(set_drivers)
{
	Satellite sat;
	EPS eps;
	COMM comm;

	newSatellite(&sat);
	newEPS(&eps);

	sat.setEPSDriver(&sat, &eps);
	sat.setCOMMDriver(&sat, &comm);

	/* Two asserts ensure mutex is behaving properly. */
	ASSERT("Satellite: set_driver EPS 1", &eps == sat.getEPSDriver(&sat));
	ASSERT("Satellite: set_driver EPS 2", &eps == sat.getEPSDriver(&sat));

	ASSERT("set_drivers COMM 1", &comm == sat.getCOMMDriver(&sat));
	ASSERT("set_drivers COMM 2", &comm == sat.getCOMMDriver(&sat));

	return TEST_END;
}

TEST(get_aggregate_state_objects)
{
	Satellite sat;

	newSatellite(&sat);

	ASSERT("Satellite: get_aggregate_state_objects 1",
			sat.getDetumbleState(&sat)->data._testID == DETUMBLE_TEST_ID);
	ASSERT("Satellite: get_aggregate_state_objects 2",
			sat.getPowerOnState(&sat)->data._testID == POWER_ON_TEST_ID);
	ASSERT("Satellite: get_aggregate_state_objects 3",
			sat.getPowerSafeState(&sat)->data._testID == POWER_SAFE_TEST_ID);
	ASSERT("Satellite: get_aggregate_state_objects 4",
			sat.getScienceState(&sat)->data._testID == SCIENCE_TEST_ID);

	return TEST_END;
}

TEST(delegate_collect_payload_data)
{
	Satellite sat;
	StateMockUp testState;

	newSatellite(&sat);
	newStateMockUp(&testState);

	sat.setCurrentState(&sat, (State *) &testState);

	ASSERT("Satellite: delegate_collect_payload_data 1",
			testState.collectPayloadDataCount(&testState) == 0);
	sat.collectPayloadData(&sat);
	ASSERT("Satellite: delegate_collect_payload_data 2",
			testState.collectPayloadDataCount(&testState) == 1);
	sat.collectPayloadData(&sat);
	ASSERT("Satellite: delegate_collect_payload_data 3",
			testState.collectPayloadDataCount(&testState) == 2);

	return TEST_END;
}

TEST(delegate_collect_WOD)
{
	Satellite sat;
	StateMockUp testState;

	newSatellite(&sat);
	newStateMockUp(&testState);

	sat.setCurrentState(&sat, (State *) &testState);

	ASSERT("Satellite: delegate_collect_WOD 1",
			testState.collectWODCount(&testState) == 0);
	sat.collectWOD(&sat);
	ASSERT("Satellite: deletage_collect_WOD 2",
			testState.collectWODCount(&testState) == 1);
	sat.collectWOD(&sat);
	ASSERT("Satellite: deletage_collect_WOD 3",
			testState.collectWODCount(&testState) == 2);

	return TEST_END;
}

TEST(delegate_transmit_data)
{
	Satellite sat;
	StateMockUp testState;

	newSatellite(&sat);
	newStateMockUp(&testState);

	sat.setCurrentState(&sat, (State *) &testState);

	ASSERT("Satellite: delegate_transmit_data 1",
			testState.transmitDataCount(&testState) == 0);
	sat.transmitData(&sat);
	ASSERT("Satellite: delegate_transmit_data 2",
			testState.transmitDataCount(&testState) == 1);
	sat.transmitData(&sat);
	ASSERT("Satellite: delegate_transmit_data 3",
			testState.transmitDataCount(&testState) == 2);

	return TEST_END;
}

TEST(delegate_execute_soft_telecommand)
{
	Satellite sat;
	StateMockUp testState;

	newSatellite(&sat);
	newStateMockUp(&testState);

	sat.setCurrentState(&sat, (State *) &testState);

	ASSERT("Satellite: delegate_execute_soft_telecommand 1",
			testState.executeSoftTelecommandCount(&testState) == 0);
	sat.executeSoftTelecommand(&sat, (Command *) NULL, NULL);
	ASSERT("Satellite: delegate_execute_soft_telecommand 2",
			testState.executeSoftTelecommandCount(&testState) == 1);
	sat.executeSoftTelecommand(&sat, (Command *) NULL, NULL);
	ASSERT("Satellite: delegate_execute_soft_telecommand 3",
			testState.executeSoftTelecommandCount(&testState) == 2);


	return TEST_END;
}

TEST(delegate_execute_hard_telecommand)
{
	Satellite sat;
	StateMockUp testState;

	newSatellite(&sat);
	newStateMockUp(&testState);

	sat.setCurrentState(&sat, (State *) &testState);

	ASSERT("Satellite: delegate_execute_hard_telecommand 1",
			testState.executeHardTelecommandCount(&testState) == 0);
	sat.executeHardTelecommand(&sat, (Command *) NULL, NULL);
	ASSERT("Satellite: delegate_execute_hard_telecommand 2",
			testState.executeHardTelecommandCount(&testState) == 1);
	sat.executeHardTelecommand(&sat, (Command *) NULL, NULL);
	ASSERT("Satellite: delegate_execute_hard_telecommand 3",
			testState.executeHardTelecommandCount(&testState) == 2);

	return TEST_END;
}

TEST_SUITE(Satellite)
{
	ADD_TEST(set_state);
	ADD_TEST(set_drivers);
	ADD_TEST(get_aggregate_state_objects);
	ADD_TEST(delegate_collect_payload_data);
	ADD_TEST(delegate_collect_WOD);
	ADD_TEST(delegate_transmit_data);
	ADD_TEST(delegate_execute_soft_telecommand);
	ADD_TEST(delegate_execute_hard_telecommand);
	return TEST_END;
}
