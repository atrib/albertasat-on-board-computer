/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file Test_Commands.c
 * @author Brendan Bruner
 * @date 2015-01-10
 */

#include "Test_Commands.h"
#include "Command.h"
#include "MacroCommand.h"
#include "CommandSetStatePowerSafe.h"
#include "CommandCollectWOD.h"
#include "Satellite.h"
#include "State.h"
#include "StateMockUp.h"
#include "EPS.h"

TEST(command_set_state_power_safe)
{
	Satellite sat;
	CommandSetStatePowerSafe command;
	State *testState;

	newSatellite(&sat);
	newCommandSetStatePowerSafe(&command);
	/* Ensure the testState and PowerSafe state pointers are different. */
	testState = (State *)(sat.getPowerSafeState(&sat) + 0x1);

	sat.setCurrentState(&sat, testState);
	((Command *) &command)->execute((Command *) &command, &sat, NULL);

	ASSERT("command_set_state_power_safe",
			sat.getCurrentState(&sat) == (State *) sat.getPowerSafeState(&sat));

	return TEST_END;
}

TEST(command_collect_wod)
{
	CommandCollectWOD command;
	StateMockUp testState;
	Satellite sat;

	newCommandCollectWOD(&command);
	newStateMockUp(&testState);
	newSatellite(&sat);

	sat.setCurrentState(&sat, (State *) &testState);

	ASSERT("command_collect_wod 1", testState.collectWODCount(&testState) == 0);
	((Command *) &command)->execute((Command *) &command, &sat, NULL);
	ASSERT("command_collect_wod 2", testState.collectWODCount(&testState) == 1);

	return TEST_END;
}

TEST(macro_command)
{
	MacroCommand macro;
	CommandCollectWOD command_a;
	CommandSetStatePowerSafe command_b;
	StateMockUp testState;
	Satellite sat;
	EPS eps;

	newMacroCommand(&macro);
	newCommandSetStatePowerSafe(&command_b);
	newCommandCollectWOD(&command_a);
	newStateMockUp(&testState);
	newSatellite(&sat);
	newEPS(&eps);

	/* Executing the macro command will collect wod then set the
	 * state to the PowerSafe state.
	 */
	macro.addCommand(&macro, (Command *) &command_b);
	macro.addCommand(&macro, (Command *) &command_a);

	/* Making the initial state the StateMockUp state gives us a hook
	 * to test if the collectWOD method was called when the macro command
	 * is executed.
	 */
	sat.setCurrentState(&sat, (State *) &testState);
	sat.setEPSDriver(&sat, &eps);

	/* Test the MacroCommand executes correctly. */
	ASSERT("macro_command 1", testState.collectWODCount(&testState) == 0);
	((Command *) &macro)->execute((Command *) &macro, &sat, NULL);
	/* Assert wod was collect once. */
	ASSERT("macro_command 2", testState.collectWODCount(&testState) == 1);
	/* Assert the current state changed to the power safe state */
	ASSERT("macro_command 3", sat.getCurrentState(&sat) == sat.getPowerSafeState(&sat));
	/* Assert command buffer counter is empty */
	ASSERT("macro_command 3.1", macro.data._numBufferedCommands == 0);


	/* Ensure MacroCommand buffer is emptied after execution */
	sat.setCurrentState(&sat, (State *) &testState);
	((Command *) &macro)->execute((Command *) &macro, &sat, NULL);
	ASSERT("macro_command 4", testState.collectWODCount(&testState) == 1);
	ASSERT("macro_command 5", sat.getCurrentState(&sat) == (State *) &testState);

	return TEST_END;
}

TEST_SUITE(Commands)
{
	ADD_TEST(command_set_state_power_safe);
	ADD_TEST(command_collect_wod);
	ADD_TEST(macro_command);
	return TEST_END;
}
