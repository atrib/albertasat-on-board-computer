/*
 * unit.h
 *
 *  Created on: 2015-01-03
 *      Author: brendan
 *
 * http://www.jera.com/techinfo/jtns/jtn002.html - January 2015
 */

#ifndef UNIT_H_
#define UNIT_H_

#include <stdio.h>

#define TEST_END 0
unsigned int unit_tests_run;

#define TEST(name)				static char * test_##name (void)
#define TEST_SUITE(suite) 		char * all_tests_##suite (void)

#define ASSERT(message, test)	do {										\
								if( !(test) ) { return message; }			\
								} while(0)

#define ADD_TEST(name)			do {										\
								char * msg = test_##name (); 				\
								++unit_tests_run;							\
								if( msg != TEST_END ) {						\
									return msg;								\
								}											\
								} while(0)

#define RUN_TEST_SUITE(suite)	do {										\
								printf("Running test suite: %s\n", #suite);	\
								char * result = all_tests_##suite ();		\
								if( result != TEST_END ) {					\
									printf("\nFAILURE in suite %s:\n%s\n\n",\
											#suite , result);				\
								} else {									\
									printf("ALL TESTS PASSED\n");			\
								}											\
								printf("Total tests run: %d\n\n", 			\
										unit_tests_run);					\
								} while(0)

#define INIT_TESTING()			unit_tests_run = 0

#endif /* UNIT_H_ */
