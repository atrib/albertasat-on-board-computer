/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file COMMHardMockUp.h
 * @author Brendan Bruner
 * @date Jan 19, 2015
 */
#ifndef COMMHARDMOCKUP_H_
#define COMMHARDMOCKUP_H_

#include "Class.h"
#include "COMM.h"

Class(COMMHardMockUp) Extends(COMM)
	Data
		/* TODO: data members? */
	Methods
		/* TODO: methods? */
EndClass;

#endif /* COMMHARDMOCKUP_H_ */
