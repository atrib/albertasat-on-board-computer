/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_command_locks.c
 * @author Brendan Bruner
 * @date Aug 24, 2015
 */

#include "test_suites.h"
#include "dependency_injection.h"
#include <states/state.h>
#include <telecommands/telemetry_alteration/telecommand_lock_config.h>
#include <telecommands/telemetry_alteration/telecommand_unlock_config.h>



TEST_SETUP( )
{

}
TEST_TEARDOWN( )
{

}

TEST( state_config_lock )
{
	state_t 							state;
	state_relay_t						relay;
	state_config_t						config;
	telecommand_lock_config_t			lock_command;
	telecommand_unlock_config_t			unlock_command;
	uint8_t								err;

	/* Initialize lock command. */
	err = initialize_telecommand_lock_config( &lock_command, kitp );
	if( err != TELECOMMAND_SUCCESS )
	{
		ABORT_TEST( "Failed to initialize lock command under test" );
	}

	/* Set up default configuration and initialize state. */
	state_config_set_hk( &config, STATE_DISABLES_EXECUTION );
	state_config_set_transmit( &config, STATE_DISABLES_EXECUTION );
	state_config_set_response( &config, STATE_DISABLES_EXECUTION );
	state_config_set_dfgm( &config, STATE_DISABLES_EXECUTION );
	state_config_set_mnlp( &config, STATE_DISABLES_EXECUTION );
	err = initialize_state( &state, "mcderp.txt", kitp->fs, &config );
	if( err == STATE_FAILURE )
	{
		((telecommand_t *) &lock_command)->destroy( (telecommand_t *) &lock_command );
		ABORT_TEST( "Failed to initialize state under test" );
	}

	/* Initialize state relay. */
	err = initialize_state_relay_simple( &relay, kitp );
	if( err == STATE_FAILURE )
	{
		((telecommand_t *) &lock_command)->destroy( (telecommand_t *) &lock_command );
		ABORT_TEST( "Failed to initialize relay under test" );
	}

	/* Initialize unlock command. */
	err = initialize_telecommand_unlock_config( &unlock_command, &relay );
	if( err != TELECOMMAND_SUCCESS )
	{
		((telecommand_t *) &lock_command)->destroy( (telecommand_t *) &lock_command );
		state.destroy( &state );
		destroy_state_relay( &relay );
		ABORT_TEST( "Failed to initialize unlock command under test" );
	}

	/* Test lock command, in fact, locks config files. */
	telecommand_execute( (telecommand_t *) &lock_command );
	base_t sem_status;
	sem_status = state_lock_config_globally( USE_POLLING );
	ASSERT( "state semaphore should not have been acquired", sem_status == SEMAPHORE_BUSY );
	sem_status = telemetry_priority_lock_config_globally( USE_POLLING );
	ASSERT( "telemtry priority semaphore should not have been acquired", sem_status == SEMAPHORE_BUSY );

	/* Test command unlock. */
	telecommand_execute( (telecommand_t *) &unlock_command );
	sem_status = state_lock_config_globally( USE_POLLING );
	state_unlock_config_globally( );
	ASSERT( "state semaphore should have been acquired", sem_status == SEMAPHORE_ACQUIRED );
	sem_status = telemetry_priority_lock_config_globally( USE_POLLING );
	telemetry_priority_unlock_config_globally( );
	ASSERT( "telemtry priority semaphore should have been acquired", sem_status == SEMAPHORE_ACQUIRED );

	/* Destroy objects. */
	state.destroy( &state );
	destroy_state_relay( &relay );
	((telecommand_t *) &lock_command)->destroy( (telecommand_t *) &lock_command );
	((telecommand_t *) &unlock_command)->destroy( (telecommand_t *) &unlock_command );
}

TEST_SUITE( command_locks )
{
	ADD_TEST( state_config_lock );
}
