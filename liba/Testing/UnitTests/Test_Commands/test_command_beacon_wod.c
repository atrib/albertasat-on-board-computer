/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_command_beacon_wod.c
 * @author Brendan Bruner
 * @date Oct 22, 2015
 */

#include "test_suites.h"
#include "dependency_injection.h"
#include <telecommands/telemetry_alteration/telecommand_beacon_wod.h>
#include <telecommands/telemetry_alteration/telecommand_log_wod.h> /* For wod size. */

TEST_SETUP( )
{

}
TEST_TEARDOWN( )
{

}

TEST( wod_beaconds )
{
	telecommand_beacon_wod_t 	command;
	uint8_t 					err;
	file_t* 					packet;
	logger_error_t				logger_err;
	fs_error_t					file_err;
	uint32_t					file_size;
	uint8_t						wod[PACKET_RAW_WOD_SIZE];
	uint8_t						wod_beaconed[PACKET_RAW_WOD_SIZE];

	/* Get WOD packet from wod logger. */
	packet = logger_peek_top_packet( (logger_t*) kitp->wod_logger, &logger_err );
	if( logger_err != LOGGER_OK )
	{
		ABORT_TEST( "Failed to open file containing wod to be beaconed" );
	}

	/* Get size of the packet. */
	file_size = packet->size( packet, &file_err );
	if( file_err != FS_OK || file_size < PACKET_RAW_WOD_SIZE )
	{
		packet->close( packet );
		ABORT_TEST( "Failed to query file size" );
	}

	/* Seek to WOD that will be beaconed. */
	file_err = packet->seek( packet, file_size - PACKET_RAW_WOD_SIZE );
	if( file_err != FS_OK )
	{
		packet->close( packet );
		ABORT_TEST( "Failed to seek" );
	}

	/* Read out the WOD. Recycle file_size variable */
	file_err = packet->read( packet, wod, PACKET_RAW_WOD_SIZE, &file_size );
	packet->close( packet );
	if( file_err != FS_OK )
	{
		ABORT_TEST( "Failed to read from wod packet file" );
	}

	/* Initialize command. */
	err = initialize_telecommand_beacon_wod( &command, kitp );
	if( err != TELECOMMAND_SUCCESS )
	{
		ABORT_TEST( "Failed to initialize command under test" );
	}

	/* Execute command. */
	telecommand_execute( (telecommand_t*) &command );
	if( telecommand_status( (telecommand_t*) &command ) != CMND_EXECUTED )
	{
		/* Failed to execute. */
		((telecommand_t*) &command)->destroy( (telecommand_t*) &command );
		ABORT_TEST( "Failed to execute command under test" );
	}
	((telecommand_t*) &command)->destroy( (telecommand_t*) &command );

	/* Confirm ground station contents. */
	kitp->gs->read( kitp->gs, wod_beaconed, PACKET_RAW_WOD_SIZE, 0, 0 );

	uint8_t iter;
	for( iter = 0; iter < PACKET_RAW_WOD_SIZE; ++iter )
	{
		ASSERT( "beaconed wod, element %d, is wrong", wod_beaconed[iter] == wod[iter] );
	}
}

TEST_SUITE( command_beacon_wod )
{
	ground_station_t			mock_gs;
	ground_station_t*			previous_gs;

	/* Dependency inject a ground station. */
	initialize_mock_up_ground_station( &mock_gs );
	previous_gs = kitp->gs;
	kitp->gs = &mock_gs;

	ADD_TEST( wod_beaconds );

	/* Restore to state before dependency injection. */
	kitp->gs = previous_gs;
}
