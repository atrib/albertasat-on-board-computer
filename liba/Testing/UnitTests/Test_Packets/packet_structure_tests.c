/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file packet_structure_tests.c
 * @author Brendan Bruner
 * @date May 4, 2015
 */
#include <test_suites.h>
#include <packets/telecommand_packet.h>
#include "rtc.h"

TEST_SETUP( ){ }
TEST_TEARDOWN( ){ }

TEST( telecommand_packet_extraction )
{
	telecommand_packet_t test_packet;
	rtc_time_t date;
	uint32_t *date_p;

	test_packet.meta_data.data_size = 0;
	ASSERT( "No optional data", extract_options_bool( &test_packet ) == NO_OPTIONS );

	test_packet.meta_data.data_size = 23;
	ASSERT( "Optional data", extract_options_bool( &test_packet ) == OPTIONS_AVAILABLE );

	test_packet.meta_data.priority = 5;
	ASSERT( "priority test", extract_priority( &test_packet ) == 5 );

	date.seconds = 14;
	date.minutes = 23;
	date.hours = 23;
	date.day_of_month = 1;
	date.month = 4;
	date.year = 2015;
	test_packet.meta_data.exec_date = date;
	ASSERT( "seconds incorrect", extract_exec_date( &test_packet).seconds );
	ASSERT( "minutes incorrect", extract_exec_date( &test_packet).minutes );
	ASSERT( "hours incorrect", extract_exec_date( &test_packet).hours );
	ASSERT( "day of month incorrect", extract_exec_date( &test_packet).day_of_month );
	ASSERT( "month incorrect", extract_exec_date( &test_packet).month );
	ASSERT( "year incorrect", extract_exec_date( &test_packet).year );

	date_p = ((uint32_t *) &extract_exec_date( &test_packet ));
	ASSERT( "exec date not packed", date_p[0] == date.seconds &&
									date_p[1] == date.minutes &&
									date_p[2] == date.hours &&
									date_p[3] == date.day_of_month &&
									date_p[4] == date.month &&
									date_p[5] == date.year );

	test_packet.meta_data.type = 312;
	ASSERT( "code test", extract_code( &test_packet ) == 312 );

	test_packet.meta_data.id = 8932;
	ASSERT( "id test", extract_id( &test_packet) == 8932 );
}

TEST_SUITE( packet_structure )
{
	ADD_TEST( telecommand_packet_extraction );
}
