/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_allocator.c
 * @author Brendan Bruner
 * @date Jun 8, 2015
 */

#include <allocator/allocator.h>

#include <test_suites.h>
#include <dependency_injection.h>

#define ALLOC_SIZE 5

static driver_toolkit_t *kit;
static allocator_t alloc;
static telecommand_counter_t telecommands[ALLOC_SIZE];
static allocator_node_t nodes[ALLOC_SIZE];

TEST_SETUP( )
{
	initialize_allocator( &alloc, (void *) telecommands, nodes, sizeof( telecommand_counter_t ), ALLOC_SIZE );
}

TEST_TEARDOWN( )
{
	destroy_allocator( &alloc );
}

TEST( alloc_command )
{
	telecommand_counter_t *commands[ALLOC_SIZE];
	telecommand_counter_t *null_command;
	uint8_t iter;

	for( iter = 0; iter < ALLOC_SIZE; ++iter )
	{
		commands[iter] = (telecommand_counter_t *) alloc_element( &alloc );
		ASSERT( "telecommand %d not allocated correctly", commands[iter] != NULL, iter );
	}
	null_command = (telecommand_counter_t *) alloc_element( &alloc );
	ASSERT( "allocated too many commands", null_command == NULL );
}

TEST( execute_alloc_comand )
{
	telecommand_counter_t *commands[ALLOC_SIZE];
	int counters[ALLOC_SIZE];
	uint8_t iter;

	for( iter = 0; iter < ALLOC_SIZE; ++iter )
	{
		commands[iter] = (telecommand_counter_t *) alloc_element( &alloc );
		initialize_command_counter( commands[iter], &counters[iter], kit );
	}
	for( iter = 0; iter < ALLOC_SIZE; ++iter )
	{
		telecommand_execute( (telecommand_t *) commands[iter] );
		ASSERT( "command %d didn't get executed", counters[iter] == 1, iter );
		((telecommand_t *) commands[iter])->destroy( (telecommand_t *) commands[iter] );
	}
}

TEST( free_command )
{
	telecommand_counter_t *commands[ALLOC_SIZE];
	telecommand_counter_t *null_command;
	uint8_t iter;

	for( iter = 0; iter < ALLOC_SIZE; ++iter )
	{
		commands[iter] = (telecommand_counter_t *) alloc_element( &alloc );
	}
	free_element( &alloc, (void *) commands[0] );
	commands[0] = (telecommand_counter_t *) alloc_element( &alloc );
	ASSERT( "could not alloc after free", commands[0] != NULL );

	for( iter = 0; iter < ALLOC_SIZE; ++iter )
	{
		free_element( &alloc, (void *) commands[iter] );
	}

	for( iter = 0; iter < ALLOC_SIZE; ++iter )
	{
		commands[iter] = (telecommand_counter_t *) alloc_element( &alloc );
		ASSERT( "telecommand %d not allocated correctly after free", commands[iter] != NULL, iter );
	}
	null_command = (telecommand_counter_t *) alloc_element( &alloc );
	ASSERT( "allocated too many commands", null_command == NULL );
}

TEST( remaining )
{
	telecommand_counter_t *commands[ALLOC_SIZE+1];
	uint32_t iter;

	for( iter = 0; iter < (ALLOC_SIZE+1); ++iter )
	{
		ASSERT( "R%d - should be %d remaining, not %d", remaining_elements( &alloc ) == ALLOC_SIZE - iter,
														iter, ALLOC_SIZE - iter, remaining_elements( &alloc ) );
		commands[iter] = (telecommand_counter_t *) alloc_element( &alloc );
	}

	for( iter = 0; iter < ALLOC_SIZE; ++iter )
	{
		free_element( &alloc, commands[iter] );
		ASSERT( "R%d - should be %d remaining, not %d", remaining_elements( &alloc ) == iter+1,
														ALLOC_SIZE + iter, iter, remaining_elements( &alloc ) );
	}
}

TEST_SUITE( allocator )
{
	kit = kitp;
	ADD_TEST( alloc_command );
	ADD_TEST( execute_alloc_comand );
	ADD_TEST( free_command );
	ADD_TEST( remaining );
}
