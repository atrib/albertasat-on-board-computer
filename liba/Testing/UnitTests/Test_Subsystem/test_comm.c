/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/*
 * @file test_comm
 * @author Brendan Bruner
 * @author Jeff Ryan
 * @date Feb 24, 2015
 */

#include "test_suites.h"
#include "dependency_injection.h"
#include "comm.h"

static nanocom_conf_t nanocom_conf_temp1;
static nanocom_conf_t nanocom_conf_temp2;
static comm_t *comm;

TEST_SETUP( )
{
#ifdef __LPC17XX__
	initialize_comm_lpc_local( comm );
#else
	initialize_comm_nanomind( comm );
#endif
}
TEST_TEARDOWN( )
{
	destroy_comm( comm );
}


void set_conf1()
{
	int i;

	nanocom_conf_temp1.do_rs = (uint8_t) 1;
	nanocom_conf_temp1.do_random = (uint8_t) 1;
	nanocom_conf_temp1.do_vitebi = (uint8_t) 1;
	nanocom_conf_temp1.tx_baud = (uint8_t) 24;
	nanocom_conf_temp1.rx_baud = (uint8_t) 24;
	nanocom_conf_temp1.tx_max_temp = (int16_t) 1;
	nanocom_conf_temp1.preamble_length = (uint16_t) 100;
	nanocom_conf_temp1.morse_enable = (uint8_t) 1;
	nanocom_conf_temp1.morse_mode = (uint8_t) 1;
	nanocom_conf_temp1.morse_cycle = (uint8_t) 1;
	nanocom_conf_temp1.morse_en_voltage = (uint8_t) 3;
	nanocom_conf_temp1.morse_en_rx_count = (uint8_t) 20;
	nanocom_conf_temp1.morse_en_tx_count = (uint8_t) 20;
	nanocom_conf_temp1.morse_en_temp_a = (uint8_t) 20;
	nanocom_conf_temp1.morse_en_temp_b = (uint8_t) 20;
	nanocom_conf_temp1.morse_en_rssi = (uint8_t) 20;
	nanocom_conf_temp1.morse_en_rf_err = (uint8_t) 1;
	nanocom_conf_temp1.morse_inter_delay = (uint16_t) 20;
	nanocom_conf_temp1.morse_pospone = (uint16_t) 20;
	nanocom_conf_temp1.morse_wpm = (uint8_t) 20;
	for(i = 0; i<NANOCOM_CONF_MORSE_TEXT_LENGTH; i++)
	{
		nanocom_conf_temp1.morse_text[i] = (uint8_t) 1;
	}
	nanocom_conf_temp1.morse_bat_level = (uint16_t) 300;
	nanocom_conf_temp1.hk_interval = (uint16_t) 20;
}


TEST( comm_lpc_local_bound )
{
	ASSERT( "com_set_conf is null", comm->com_set_conf != NULL );
	ASSERT( "com_get_conf is null", comm->com_get_conf != NULL );
	ASSERT( "com_restore_conf is null", comm->com_restore_conf != NULL );
	ASSERT( "com_get_status is null", comm->com_get_status != NULL );
	ASSERT( "com_get_log_rssi is null", comm->com_get_log_rssi != NULL );
	ASSERT( "com_get_hk is null", comm->com_get_hk != NULL );
}


TEST( comm_lpc_local_setconf )
{
	set_conf1();
	comm->com_set_conf( comm, &nanocom_conf_temp1 );
	comm->com_get_conf( comm, &nanocom_conf_temp2 );

	int i;

	ASSERT( "nancom_conf do_rs not set",nanocom_conf_temp1.do_rs == nanocom_conf_temp2.do_rs );
	ASSERT( "nancom_conf do_random not set",nanocom_conf_temp1.do_random == nanocom_conf_temp2.do_random );
	ASSERT( "nancom_conf do_vitebi not set",nanocom_conf_temp1.do_vitebi == nanocom_conf_temp2.do_vitebi );
	ASSERT( "nancom_conf tx_baud not set",nanocom_conf_temp1.tx_baud == nanocom_conf_temp2.tx_baud );
	ASSERT( "nancom_conf rx_baud not set",nanocom_conf_temp1.rx_baud == nanocom_conf_temp2.rx_baud );
	ASSERT( "nancom_conf tx_max_temp not set",nanocom_conf_temp1.tx_max_temp == nanocom_conf_temp2 .tx_max_temp );
	ASSERT( "nancom_conf preamble_length not set",nanocom_conf_temp1.preamble_length == nanocom_conf_temp2.preamble_length );
	ASSERT( "nancom_conf morse_enable not set",nanocom_conf_temp1.morse_enable == nanocom_conf_temp2.morse_enable );
	ASSERT( "nancom_conf morse_mode not set",nanocom_conf_temp1.morse_mode == nanocom_conf_temp2.morse_mode );
	ASSERT( "nancom_conf morse_cycle not set",nanocom_conf_temp1.morse_cycle == nanocom_conf_temp2.morse_cycle );
	ASSERT( "nancom_conf morse_en_voltage not set",nanocom_conf_temp1.morse_en_voltage == nanocom_conf_temp2.morse_en_voltage );
	ASSERT( "nancom_conf morse_en_rx_count not set",nanocom_conf_temp1.morse_en_rx_count == nanocom_conf_temp2.morse_en_rx_count );
	ASSERT( "nancom_conf morse_en_tx_count not set",nanocom_conf_temp1.morse_en_tx_count == nanocom_conf_temp2.morse_en_tx_count );
	ASSERT( "nancom_conf morse_en_temp_a not set",nanocom_conf_temp1.morse_en_temp_a == nanocom_conf_temp2.morse_en_temp_a );
	ASSERT( "nancom_conf morse_en_temp_b not set",nanocom_conf_temp1.morse_en_temp_b == nanocom_conf_temp2.morse_en_temp_b );
	ASSERT( "nancom_conf morse_en_rssi not set",nanocom_conf_temp1.morse_en_rssi == nanocom_conf_temp2.morse_en_rssi );
	ASSERT( "nancom_conf morse_en_rf_err not set",nanocom_conf_temp1.morse_en_rf_err == nanocom_conf_temp2.morse_en_rf_err );
	ASSERT( "nancom_conf morse_inter_delay not set",nanocom_conf_temp1.morse_inter_delay == nanocom_conf_temp2.morse_inter_delay );
	ASSERT( "nancom_conf morse_pospone not set",nanocom_conf_temp1.morse_pospone == nanocom_conf_temp2.morse_pospone );
	ASSERT( "nancom_conf morse_wpm not set",nanocom_conf_temp1.morse_wpm == nanocom_conf_temp2.morse_wpm );
	for(i = 0; i<NANOCOM_CONF_MORSE_TEXT_LENGTH; i++)
	{
		ASSERT( "nancom_conf morse_text not set",nanocom_conf_temp1.morse_text[i] == nanocom_conf_temp2.morse_text[i] );
	}
	ASSERT( "nancom_conf morse_bat_level not set",nanocom_conf_temp1.morse_bat_level == nanocom_conf_temp2.morse_bat_level );
	ASSERT( "nancom_conf hk_interval not set",nanocom_conf_temp1.hk_interval == nanocom_conf_temp2.hk_interval );
}

TEST_SUITE( comm )
{
	comm_t mcomm;
	comm = &mcomm;
	ADD_TEST( comm_lpc_local_bound );
	ADD_TEST( comm_lpc_local_setconf );
}
