/*
 * Copyright (C) 2015  Brendan Bruner, Stefan Damkjar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_eps
 * @author Brendan Bruner, Stefan Damkjar
 * @date Feb 4, 2015
 */
#include "test_suites.h"
#include "stdint.h"
#include "eps/eps.h"
#include "dependency_injection.h"

static eps_t *eps;

TEST_SETUP( )
{
}
TEST_TEARDOWN( )
{
}

TEST(eps_init_housekeeping)
{
	ASSERT( "Initialize vboost in EPS hk struct to zero",
			eps->_.hk.vboost[0] == 0 );

	ASSERT( "Initialize vboost in EPS hk struct to zero",
			eps->_.hk.vboost[1] == 0 );

	ASSERT( "Initialize vboost in EPS hk struct to zero",
			eps->_.hk.vboost[2] == 0 );

	ASSERT( "Initialize vbatt in EPS hk struct to zero",
			eps->_.hk.vbatt == 0 );

	ASSERT( "Initialize curin in EPS hk struct to zero",
			eps->_.hk.curin[0] == 0 );

	ASSERT( "Initialize curin in EPS hk struct to zero",
			eps->_.hk.curin[1] == 0 );

	ASSERT( "Initialize curin in EPS hk struct to zero",
			eps->_.hk.curin[2] == 0 );

	ASSERT( "Initialize cursun in EPS hk struct to zero",
			eps->_.hk.cursun == 0 );

	ASSERT( "Initialize cursys in EPS hk struct to zero",
			eps->_.hk.cursys == 0 );

	ASSERT( "Initialize reserved in EPS hk struct to zero",
			eps->_.hk.reserved == 0 );

	ASSERT( "Initialize curout in EPS hk struct to zero",
			eps->_.hk.curout[0] == 0 );

	ASSERT( "Initialize curout in EPS hk struct to zero",
			eps->_.hk.curout[1] == 0 );

	ASSERT( "Initialize curout in EPS hk struct to zero",
			eps->_.hk.curout[2] == 0 );

	ASSERT( "Initialize curout in EPS hk struct to zero",
			eps->_.hk.curout[3] == 0 );

	ASSERT( "Initialize curout in EPS hk struct to zero",
			eps->_.hk.curout[4] == 0 );

	ASSERT( "Initialize curout in EPS hk struct to zero",
			eps->_.hk.curout[5] == 0 );

	ASSERT( "Initialize output in EPS hk struct to zero",
			eps->_.hk.output[0] == 0 );

	ASSERT( "Initialize output in EPS hk struct to zero",
			eps->_.hk.output[1] == 0 );

	ASSERT( "Initialize output in EPS hk struct to zero",
			eps->_.hk.output[2] == 0 );

	ASSERT( "Initialize output in EPS hk struct to zero",
			eps->_.hk.output[3] == 0 );

	ASSERT( "Initialize output in EPS hk struct to zero",
			eps->_.hk.output[4] == 0 );

	ASSERT( "Initialize output in EPS hk struct to zero",
			eps->_.hk.output[5] == 0 );

	ASSERT( "Initialize output in EPS hk struct to zero",
			eps->_.hk.output[6] == 0 );

	ASSERT( "Initialize output in EPS hk struct to zero",
			eps->_.hk.output[7] == 0 );

	ASSERT( "Initialize output_on_delta in EPS hk struct to zero",
			eps->_.hk.output_on_delta[0] == 0 );

	ASSERT( "Initialize output_on_delta in EPS hk struct to zero",
			eps->_.hk.output_on_delta[1] == 0 );

	ASSERT( "Initialize output_on_delta in EPS hk struct to zero",
			eps->_.hk.output_on_delta[2] == 0 );

	ASSERT( "Initialize output_on_delta in EPS hk struct to zero",
			eps->_.hk.output_on_delta[3] == 0 );

	ASSERT( "Initialize output_on_delta in EPS hk struct to zero",
			eps->_.hk.output_on_delta[4] == 0 );

	ASSERT( "Initialize output_on_delta in EPS hk struct to zero",
			eps->_.hk.output_on_delta[5] == 0 );

	ASSERT( "Initialize output_on_delta in EPS hk struct to zero",
			eps->_.hk.output_on_delta[6] == 0 );

	ASSERT( "Initialize output_on_delta in EPS hk struct to zero",
			eps->_.hk.output_on_delta[7] == 0 );

	ASSERT( "Initialize output_off_delta in EPS hk struct to zero",
			eps->_.hk.output_off_delta[0] == 0 );

	ASSERT( "Initialize output_off_delta in EPS hk struct to zero",
			eps->_.hk.output_off_delta[1] == 0 );

	ASSERT( "Initialize output_off_delta in EPS hk struct to zero",
			eps->_.hk.output_off_delta[2] == 0 );

	ASSERT( "Initialize output_off_delta in EPS hk struct to zero",
			eps->_.hk.output_off_delta[3] == 0 );

	ASSERT( "Initialize output_off_delta in EPS hk struct to zero",
			eps->_.hk.output_off_delta[4] == 0 );

	ASSERT( "Initialize output_off_delta in EPS hk struct to zero",
			eps->_.hk.output_off_delta[5] == 0 );

	ASSERT( "Initialize output_off_delta in EPS hk struct to zero",
			eps->_.hk.output_off_delta[6] == 0 );

	ASSERT( "Initialize output_off_delta in EPS hk struct to zero",
			eps->_.hk.output_off_delta[7] == 0 );

	ASSERT( "Initialize latchup in EPS hk struct to zero",
			eps->_.hk.latchup[0] == 0 );

	ASSERT( "Initialize latchup in EPS hk struct to zero",
			eps->_.hk.latchup[1] == 0 );

	ASSERT( "Initialize latchup in EPS hk struct to zero",
			eps->_.hk.latchup[2] == 0 );

	ASSERT( "Initialize latchup in EPS hk struct to zero",
			eps->_.hk.latchup[3] == 0 );

	ASSERT( "Initialize latchup in EPS hk struct to zero",
			eps->_.hk.latchup[4] == 0 );

	ASSERT( "Initialize latchup in EPS hk struct to zero",
			eps->_.hk.latchup[5] == 0 );

	ASSERT( "Initialize wdt_i2c_time_left in EPS hk struct to zero",
			eps->_.hk.wdt_i2c_time_left == 0 );

	ASSERT( "Initialize wdt_gnd_time_left in EPS hk struct to zero",
			eps->_.hk.wdt_gnd_time_left == 0 );

	ASSERT( "Initialize wdt_csp_pings_left in EPS hk struct to zero",
			eps->_.hk.wdt_csp_pings_left[0] == 0 );

	ASSERT( "Initialize wdt_csp_pings_left in EPS hk struct to zero",
			eps->_.hk.wdt_csp_pings_left[1] == 0 );

	ASSERT( "Initialize counter_wdt_i2c in EPS hk struct to zero",
			eps->_.hk.counter_wdt_i2c == 0 );

	ASSERT( "Initialize counter_wdt_gnd in EPS hk struct to zero",
			eps->_.hk.counter_wdt_gnd == 0 );

	ASSERT( "Initialize counter_wdt_csp in EPS hk struct to zero",
			eps->_.hk.counter_wdt_csp[0] == 0 );

	ASSERT( "Initialize counter_wdt_csp in EPS hk struct to zero",
			eps->_.hk.counter_wdt_csp[1] == 0 );

	ASSERT( "Initialize counter_boot in EPS hk struct to zero",
			eps->_.hk.counter_boot == 0 );

	ASSERT( "Initialize temp in EPS hk struct to zero",
			eps->_.hk.temp[0] == 0 );

	ASSERT( "Initialize temp in EPS hk struct to zero",
			eps->_.hk.temp[1] == 0 );

	ASSERT( "Initialize temp in EPS hk struct to zero",
			eps->_.hk.temp[2] == 0 );

	ASSERT( "Initialize temp in EPS hk struct to zero",
			eps->_.hk.temp[3] == 0 );

	ASSERT( "Initialize temp in EPS hk struct to zero",
			eps->_.hk.temp[4] == 0 );

	ASSERT( "Initialize temp in EPS hk struct to zero",
			eps->_.hk.temp[5] == 0 );

	ASSERT( "Initialize bootcause in EPS hk struct to zero",
			eps->_.hk.bootcause == 0 );

	ASSERT( "Initialize battmode in EPS hk struct to zero",
			eps->_.hk.battmode == 0 );

	ASSERT( "Initialize pptmode in EPS hk struct to zero",
			eps->_.hk.pptmode == 0 );

	ASSERT( "Initialize reserved2 in EPS hk struct to zero",
			eps->_.hk.reserved2 == 0 );
}

TEST(eps_methods_bound)
{
	ASSERT( "hk_get method not null",
			eps->hk_get != NULL );
	ASSERT( "hk_refresh method not null",
			eps->hk_refresh != NULL );
	ASSERT( "mode method not null",
			eps->mode != NULL );
}

TEST(eps_get_housekeeping)
{
	hk_handler_t hk_handler = {0};

 	hk_handler = eps->hk_get(eps,EPS_VBOOST);
	ASSERT( "Getting vboost from EPS",
			hk_handler.ptr == eps->_.hk.vboost &&
			hk_handler.size == sizeof(eps->_.hk.vboost));

	hk_handler = eps->hk_get(eps,EPS_VBATT);
	ASSERT( "Getting vbatt from EPS",
			hk_handler.ptr == &eps->_.hk.vbatt &&
			hk_handler.size == sizeof(eps->_.hk.vbatt));

	hk_handler = eps->hk_get(eps,EPS_CURIN);
	ASSERT( "Getting curin from EPS",
			hk_handler.ptr == eps->_.hk.curin &&
			hk_handler.size == sizeof(eps->_.hk.curin));

	hk_handler = eps->hk_get(eps,EPS_CURSUN);
	ASSERT( "Getting cursun from EPS",
			hk_handler.ptr == &eps->_.hk.cursun &&
			hk_handler.size == sizeof(eps->_.hk.cursun));

	hk_handler = eps->hk_get(eps,EPS_CURSYS);
	ASSERT( "Getting cursys from EPS",
			hk_handler.ptr == &eps->_.hk.cursys &&
			hk_handler.size == sizeof(eps->_.hk.cursys));

	hk_handler = eps->hk_get(eps,EPS_RESERVED);
	ASSERT( "Getting reserved from EPS",
			hk_handler.ptr == &eps->_.hk.reserved &&
			hk_handler.size == sizeof(eps->_.hk.reserved));

	hk_handler = eps->hk_get(eps,EPS_CUROUT);
	ASSERT( "Getting curout from EPS",
			hk_handler.ptr == eps->_.hk.curout &&
			hk_handler.size == sizeof(eps->_.hk.curout));

	hk_handler = eps->hk_get(eps,EPS_OUTPUT);
	ASSERT( "Getting output from EPS",
			hk_handler.ptr == eps->_.hk.output &&
			hk_handler.size == sizeof(eps->_.hk.output));

	hk_handler = eps->hk_get(eps,EPS_OUTPUT_ON_DELTA);
	ASSERT( "Getting output_on_delta from EPS",
			hk_handler.ptr == eps->_.hk.output_on_delta &&
			hk_handler.size == sizeof(eps->_.hk.output_on_delta));

	hk_handler = eps->hk_get(eps,EPS_OUTPUT_OFF_DELTA);
	ASSERT( "Getting output_off_delta from EPS",
			hk_handler.ptr == eps->_.hk.output_off_delta &&
			hk_handler.size == sizeof(eps->_.hk.output_off_delta));

	hk_handler = eps->hk_get(eps,EPS_LATCHUP);
	ASSERT( "Getting latchup from EPS",
			hk_handler.ptr == eps->_.hk.latchup &&
			hk_handler.size == sizeof(eps->_.hk.latchup));

	hk_handler = eps->hk_get(eps,EPS_WDT_I2C_TIME_LEFT);
	ASSERT( "Getting wdt_i2c_time_left from EPS",
			hk_handler.ptr == &eps->_.hk.wdt_i2c_time_left &&
			hk_handler.size == sizeof(eps->_.hk.wdt_i2c_time_left));

	hk_handler = eps->hk_get(eps,EPS_WDT_GND_TIME_LEFT);
	ASSERT( "Getting wdt_gnd_time_left from EPS",
			hk_handler.ptr == &eps->_.hk.wdt_gnd_time_left &&
			hk_handler.size == sizeof(eps->_.hk.wdt_gnd_time_left));

	hk_handler = eps->hk_get(eps,EPS_WDT_CSP_PINGS_LEFT);
	ASSERT( "Getting wdt_csp_pings_left from EPS",
			hk_handler.ptr == eps->_.hk.wdt_csp_pings_left &&
			hk_handler.size == sizeof(eps->_.hk.wdt_csp_pings_left));

	hk_handler = eps->hk_get(eps,EPS_COUNTER_WDT_I2C);
	ASSERT( "Getting counter_wdt_i2c from EPS",
			hk_handler.ptr == &eps->_.hk.counter_wdt_i2c &&
			hk_handler.size == sizeof(eps->_.hk.counter_wdt_i2c));

	hk_handler = eps->hk_get(eps,EPS_COUNTER_WDT_GND);
	ASSERT( "Getting counter_wdt_gnd from EPS",
			hk_handler.ptr == &eps->_.hk.counter_wdt_gnd &&
			hk_handler.size == sizeof(eps->_.hk.counter_wdt_gnd));

	hk_handler = eps->hk_get(eps,EPS_COUNTER_WDT_CSP);
	ASSERT( "Getting counter_wdt_csp from EPS",
			hk_handler.ptr == eps->_.hk.counter_wdt_csp &&
			hk_handler.size == sizeof(eps->_.hk.counter_wdt_csp));

	hk_handler = eps->hk_get(eps,EPS_COUNTER_BOOT);
	ASSERT( "Getting counter_boot from EPS",
			hk_handler.ptr == &eps->_.hk.counter_boot &&
			hk_handler.size == sizeof(eps->_.hk.counter_boot));

	hk_handler = eps->hk_get(eps,EPS_TEMP);
	ASSERT( "Getting temp from EPS",
			hk_handler.ptr == eps->_.hk.temp &&
			hk_handler.size == sizeof(eps->_.hk.temp));

	hk_handler = eps->hk_get(eps,EPS_BOOTCAUSE);
	ASSERT( "Getting bootcause from EPS",
			hk_handler.ptr == &eps->_.hk.bootcause &&
			hk_handler.size == sizeof(eps->_.hk.bootcause));

	hk_handler = eps->hk_get(eps,EPS_BATTMODE);
	ASSERT( "Getting battmode from EPS",
			hk_handler.ptr == &eps->_.hk.battmode &&
			hk_handler.size == sizeof(eps->_.hk.battmode));

	hk_handler = eps->hk_get(eps,EPS_PPTMODE);
	ASSERT( "Getting pptmode from EPS",
			hk_handler.ptr == &eps->_.hk.pptmode &&
			hk_handler.size == sizeof(eps->_.hk.pptmode));

	hk_handler = eps->hk_get(eps,EPS_RESERVED2);
	ASSERT( "Getting reserved2 from EPS",
			hk_handler.ptr == &eps->_.hk.reserved2 &&
			hk_handler.size == sizeof(eps->_.hk.reserved2));

	hk_handler = eps->hk_get(eps,0xFF);
	ASSERT( "Handling invalid housekeeping item from EPS",
			hk_handler.ptr == TLM_ERR_INVAL.ptr &&
			hk_handler.size == TLM_ERR_INVAL.size);
}


TEST_SUITE(eps)
{
	eps = kitp->eps;
	ADD_TEST(eps_init_housekeeping);
	ADD_TEST(eps_get_housekeeping);
	ADD_TEST(eps_methods_bound);
}
