/*
 * Copyright (C) 2015  Brendan Bruner, Oleg Oleynikov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_adcs
 * @author Brendan Bruner
 * @author Oleg Oleynikov
 * @date Mar 02, 2015
 */

#include "test_suites.h"
#include "dependency_injection.h"
#include <stdlib.h>

TEST_SETUP( )
{
}
TEST_TEARDOWN( )
{
}

TEST_SUITE( adcs )
{
}
