/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_telemetry_priorities.c
 * @author Brendan Bruner
 * @date May 28, 2015
 */

#include <test_suites.h>
#include <telemetry_priority/telemetry_priority.h>
#include <dependency_injection.h>

#define PRIO_LOG_NAME "tpln.txt"

static driver_toolkit_t *kit;
static filesystem_t *fs;
static telemetry_priority_t prio;

TEST_SETUP( )
{
	initialize_telemetry_priority( &prio, fs, PRIO_LOG_NAME );
}
TEST_TEARDOWN( )
{
	destroy_telemetry_priority( &prio );
}

TEST( first_init )
{
	uint8_t data;

	fs->delete( fs, PRIO_LOG_NAME );
	destroy_telemetry_priority( &prio );
	initialize_telemetry_priority( &prio, fs, PRIO_LOG_NAME );

	data = get_telemetry_priority_wod( &prio );
	ASSERT( "default wod = %d, should be %d", data == TELEMETRY_PRIORITY_DEFAULT_WOD,
											  data, TELEMETRY_PRIORITY_DEFAULT_WOD );

	data = get_telemetry_priority_dfgm( &prio );
	ASSERT( "default dfgm = %d, should be %d", data == TELEMETRY_PRIORITY_DEFAULT_DFGM,
											   data, TELEMETRY_PRIORITY_DEFAULT_DFGM );

	data = get_telemetry_priority_mnlp( &prio );
	ASSERT( "default mnlp = %d, should be %d", data == TELEMETRY_PRIORITY_DEFAULT_MNLP,
	  										   data, TELEMETRY_PRIORITY_DEFAULT_MNLP );

	data = get_telemetry_priority_cmnd_status( &prio );
	ASSERT( "default cmnd status = %d, should be %d", data == TELEMETRY_PRIORITY_DEFAULT_CMND_STATUS,
													  data, TELEMETRY_PRIORITY_DEFAULT_CMND_STATUS );

	data = get_telemetry_priority_state( &prio );
	ASSERT( "default state = %d, should be %d", data == TELEMETRY_PRIORITY_DEFAULT_STATE,
											  	data, TELEMETRY_PRIORITY_DEFAULT_STATE );
}

TEST( set_and_get )
{
	uint8_t data, next;
	tp_error_t err;

	next = (1+TELEMETRY_PRIORITY_DEFAULT_WOD)%(1+TELEMETRY_PRIORITY_MAX);
	err = set_telemetry_priority_wod( &prio,  next );
	data = get_telemetry_priority_wod( &prio );
	ASSERT( "error setting = %d, should be %d", err == TP_OK, err, TP_OK );
	ASSERT( "wod = %d, should be %d", data == next,
									  data, next );

	next = (1+TELEMETRY_PRIORITY_DEFAULT_DFGM)%(1+TELEMETRY_PRIORITY_MAX);
	err = set_telemetry_priority_dfgm( &prio,  next );
	data = get_telemetry_priority_dfgm( &prio );
	ASSERT( "error setting = %d, should be %d", err == TP_OK, err, TP_OK );
	ASSERT( "dfgm = %d, should be %d", data == next,
									   data, next );

	next = (1+TELEMETRY_PRIORITY_DEFAULT_MNLP)%(1+TELEMETRY_PRIORITY_MAX);
	err = set_telemetry_priority_mnlp( &prio,  next );
	data = get_telemetry_priority_mnlp( &prio );
	ASSERT( "error setting = %d, should be %d", err == TP_OK, err, TP_OK );
	ASSERT( "mnlp = %d, should be %d", data == next,
									   data, next );

	next = (1+TELEMETRY_PRIORITY_DEFAULT_CMND_STATUS)%(1+TELEMETRY_PRIORITY_MAX);
	err = set_telemetry_priority_cmnd_status( &prio,  next );
	data = get_telemetry_priority_cmnd_status( &prio );
	ASSERT( "error setting = %d, should be %d", err == TP_OK, err, TP_OK );
	ASSERT( "cmnd status = %d, should be %d", data == next,
											  data, next );

	next = (1+TELEMETRY_PRIORITY_DEFAULT_STATE)%(1+TELEMETRY_PRIORITY_MAX);
	err = set_telemetry_priority_state( &prio,  next );
	data = get_telemetry_priority_state( &prio );
	ASSERT( "error setting = %d, should be %d", err == TP_OK, err, TP_OK );
	ASSERT( "state = %d, should be %d", data == next,
					 				   	data, next );\
}

TEST( set_out_of_bounds )
{
	uint8_t data, next, old;
	tp_error_t err;

	old = get_telemetry_priority_wod( &prio );
	next = 1+TELEMETRY_PRIORITY_MAX;
	err = set_telemetry_priority_wod( &prio,  next );
	data = get_telemetry_priority_wod( &prio );
	ASSERT( "error setting = %d, should be %d", err == TP_INV_PRM, err, TP_INV_PRM );
	ASSERT( "wod should not be %d", data == old, data );

	old = get_telemetry_priority_dfgm( &prio );
	next = 1+TELEMETRY_PRIORITY_MAX;
	err = set_telemetry_priority_dfgm( &prio,  next );
	data = get_telemetry_priority_dfgm( &prio );
	ASSERT( "error setting = %d, should be %d", err == TP_INV_PRM, err, TP_INV_PRM );
	ASSERT( "dfgm should not be %d", data == old, data );

	old = get_telemetry_priority_mnlp( &prio );
	next = 1+TELEMETRY_PRIORITY_MAX;
	err = set_telemetry_priority_mnlp( &prio,  next );
	data = get_telemetry_priority_mnlp( &prio );
	ASSERT( "error setting = %d, should be %d", err == TP_INV_PRM, err, TP_INV_PRM );
	ASSERT( "mnlp should not be %d", data == old, data );

	old = get_telemetry_priority_cmnd_status( &prio );
	next = 1+TELEMETRY_PRIORITY_MAX;
	err = set_telemetry_priority_cmnd_status( &prio,  next );
	data = get_telemetry_priority_cmnd_status( &prio );
	ASSERT( "error setting = %d, should be %d", err == TP_INV_PRM, err, TP_INV_PRM );
	ASSERT( "cmnd_status should not be %d", data == old, data );

	old = get_telemetry_priority_state( &prio );
	next = 1+TELEMETRY_PRIORITY_MAX;
	err = set_telemetry_priority_state( &prio,  next );
	data = get_telemetry_priority_state( &prio );
	ASSERT( "error setting = %d, should be %d", err == TP_INV_PRM, err, TP_INV_PRM );
	ASSERT( "state should not be %d", data == old, data );
}

TEST( init_after_destroy )
{
	uint8_t data, next_wod, next_dfgm, next_mnlp, next_cmnd, next_state;
	tp_error_t err;

	/* Set to something other than default. */
	next_wod = (1+TELEMETRY_PRIORITY_DEFAULT_WOD)%(1+TELEMETRY_PRIORITY_MAX);
	next_dfgm = (1+TELEMETRY_PRIORITY_DEFAULT_DFGM)%(1+TELEMETRY_PRIORITY_MAX);
	next_mnlp = (1+TELEMETRY_PRIORITY_DEFAULT_MNLP)%(1+TELEMETRY_PRIORITY_MAX);
	next_cmnd = (1+TELEMETRY_PRIORITY_DEFAULT_CMND_STATUS)%(1+TELEMETRY_PRIORITY_MAX);
	next_state = (1+TELEMETRY_PRIORITY_DEFAULT_STATE)%(1+TELEMETRY_PRIORITY_MAX);
	set_telemetry_priority_wod( &prio, next_wod );
	set_telemetry_priority_dfgm( &prio, next_dfgm );
	set_telemetry_priority_mnlp( &prio, next_mnlp );
	set_telemetry_priority_cmnd_status( &prio, next_cmnd );
	set_telemetry_priority_state( &prio, next_state );

	destroy_telemetry_priority( &prio );
	err = initialize_telemetry_priority( &prio, fs, PRIO_LOG_NAME );

	ASSERT( "err on init = %d", err == TP_OK, err );

	data = get_telemetry_priority_wod( &prio );
	ASSERT( "wod = %d, should be %d", data == next_wod,
									  data, next_wod );

	data = get_telemetry_priority_dfgm( &prio );
	ASSERT( "dfgm = %d, should be %d", data == next_dfgm,
									   data, next_dfgm );

	data = get_telemetry_priority_mnlp( &prio );
	ASSERT( "mnlp = %d, should be %d", data == next_mnlp,
									   data, next_mnlp );

	data = get_telemetry_priority_cmnd_status( &prio );
	ASSERT( "cmnd status = %d, should be %d", data == next_cmnd,
											  data, next_cmnd );

	data = get_telemetry_priority_state( &prio );
	ASSERT( "state = %d, should be %d", data == next_state,
					 				   	data, next_state );
}

TEST( global_lock )
{
	rtc_time_t 					time;
	telemetry_priority_config_t	config;
	uint32_t					bytes_used;
	file_t						*file;
	fs_error_t					fs_err;

	/* Testing refresh. */
	telemetry_priority_lock_config_globally( BLOCK_FOREVER );
	file = fs->open( fs, &fs_err, prio._log_name, FS_OPEN_EXISTING, USE_POLLING );
	if( fs_err != FS_OK )
	{
		fs->close( fs, file );
		ABORT_TEST( "failed to open config file" );
	}
	fs_err = file->read( file, (uint8_t *) &config, sizeof(config), &bytes_used );
	if( fs_err != FS_OK || bytes_used != sizeof(config) )
	{
		fs->close( fs, file );
		ABORT_TEST( "failed to read in config" );
	}
	/* Set to something different. */
	config._wod_priority = TELEMETRY_PRIORITY_DEFAULT_MNLP;
	fs_err = file->seek( file, BEGINNING_OF_FILE );
	if( fs_err != FS_OK )
	{
		fs->close( fs, file );
		ABORT_TEST( "failed to write in config" );
	}
	fs_err = file->write( file, (uint8_t *) &config, sizeof(config), &bytes_used );
	if( fs_err != FS_OK || bytes_used != sizeof(config) )
	{
		fs->close( fs, file );
		ABORT_TEST( "failed to write in config" );
	}
	fs->close( fs, file );
	telemetry_priority_unlock_config_globally( );

	uint8_t wod_prio = get_telemetry_priority_wod( &prio );
	ASSERT( "wod prio should not be updated", wod_prio != TELEMETRY_PRIORITY_DEFAULT_MNLP );
	telemetry_priority_refresh_configuration( &prio );
	wod_prio = get_telemetry_priority_wod( &prio );
	ASSERT( "wod prio should be updated", wod_prio == TELEMETRY_PRIORITY_DEFAULT_MNLP );

	telemetry_priority_lock_config_globally( BLOCK_FOREVER );
	telemetry_priority_unlock_config_globally( );
	time = (rtc_time_t){ .seconds = 0, .minutes = 0, .hours = 0, .day_of_month = 0, .month = 0, .year = 2010 };
	kit->rtc->set_time( kit->rtc, &time );
	set_telemetry_priority_wod( &prio, TELEMETRY_PRIORITY_DEFAULT_WOD );
	kit->rtc->get_time( kit->rtc, &time );
	ASSERT( "Unlock did not work, sec: %d", time.seconds <= 5, time.seconds );

	/* Five minute delay here. */
	time = (rtc_time_t){ .seconds = 0, .minutes = 0, .hours = 0, .day_of_month = 0, .month = 0, .year = 2010 };
	kit->rtc->set_time( kit->rtc, &time );
	telemetry_priority_lock_config_globally( BLOCK_FOREVER );
	set_telemetry_priority_wod( &prio, TELEMETRY_PRIORITY_DEFAULT_WOD );
	kit->rtc->get_time( kit->rtc, &time );
	ASSERT( "Block did not last correct amount of time, min: %d, sec: %d", time.minutes >= 5 && time.seconds <= 5, time.minutes, time.seconds );
}

TEST_SUITE( telemetry_priority )
{
	kit = kitp;
	fs = kit->fs;
	ADD_TEST( first_init );
	ADD_TEST( set_and_get );
	ADD_TEST( set_out_of_bounds );
	ADD_TEST( init_after_destroy );
	ADD_TEST( global_lock );
}
