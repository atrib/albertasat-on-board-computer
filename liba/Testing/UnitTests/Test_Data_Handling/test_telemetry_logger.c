/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_logger.c
 * @author Brendan Bruner
 * @date May 13, 2015
 */

#include <test_suites.h>
#include <dependency_injection.h>
#include <filesystems/filesystem.h>
#include <filesystems/fs_handle.h>
#include <logger/logger.h>
#include <string.h>
#include <packets/packet_base.h>

#define NO_queue "x"
#define NO_NAME_SPACE	"x"
#define ZERO_FILE_SIZE	0
#define PACKET_FILE_SIZE 		MOCK_UP_LOGGER_PACKET_SIZE
#define HALF_SMALL_FILE_SIZE 	(PACKET_FILE_SIZE/2)
#define MEDIUM_FILE_SIZE 		(PACKET_FILE_SIZE - 7)
#define LARGE_FILE_SIZE 		((PACKET_FILE_SIZE*2) + 6)
#define ONE_CONFIRMATIONS 1
#define FILE_ONE 0
#define FILE_THREE 2
#define FILE_SIX 5
#define FILE_SEVEN 6
#define NO_DELETE_BUFFER "x"
#define LOGGER_PRIVATE_QUEUE_TOP 14 /* This is private information to the logger, check logger.c */

static driver_toolkit_t *kit;
static filesystem_t *filesystem, *fs;
static logger_t logger;

TEST_SETUP( )
{
}
TEST_TEARDOWN( )
{
}

TEST( base_methods_bound )
{
	ASSERT( "next name bound", logger.__.next_name != NULL );
	ASSERT( "new entry hook", logger.__.new_entry_hook != NULL );
	ASSERT( "new packet hook", logger.__.new_packet_hook != NULL );

}

TEST( no_queue )
{
	logger_error_t	err;
	file_t*			packet;
	char const*		name;

	initialize_mock_up_logger( &logger );

	/* Push to packets into logger queue. */
	err = logger_push_packet( &logger );
	if( err != LOGGER_OK )
	{
		logger.destroy( &logger );
		ABORT_TEST( "failed to push to logger first time" );
	}
	err = logger_push_packet( &logger );
	if( err != LOGGER_OK )
	{
		logger.destroy( &logger );
		ABORT_TEST( "failed to push to logger second time" );
	}

	/* Corrupt the master table by deleting it. */
	logger._.fs->delete( logger._.fs, logger._.queue_file_name );

	/* Pop the packets off the logger queue. */
	/* First packet. */
	packet = logger_peek_packet( &logger, &err );
	if( err != LOGGER_OK )
	{
		logger.destroy( &logger );
		ABORT_TEST( "failed to peek from logger first time" );
	}
	name = fs_handle_get_name( (fs_handle_t*) packet );
	ASSERT( "first packet incorrect name", 0 == strncmp( "00_n0.txt", name, FILESYSTEM_MAX_NAME_LENGTH ) );
	packet->close( packet );
	err = logger_pop_packet( &logger );
	if( err != LOGGER_OK )
	{
		logger.destroy( &logger );
		ABORT_TEST( "failed to pop from logger first time" );
	}

	/* Second packet. */
	packet = logger_peek_packet( &logger, &err );
	if( err != LOGGER_OK )
	{
		logger.destroy( &logger );
		ABORT_TEST( "failed to peek from logger" );
	}
	name = fs_handle_get_name( (fs_handle_t*) packet );
	ASSERT( "second packet incorrect name", 0 == strncmp( "01_n0.txt", name, FILESYSTEM_MAX_NAME_LENGTH ) );
	packet->close( packet );
	err = logger_pop_packet( &logger );
	if( err != LOGGER_OK )
	{
		logger.destroy( &logger );
		ABORT_TEST( "failed to pop from logger first time" );
	}

	logger.destroy( &logger );
}

TEST( reset )
{
	logger_error_t	err;
	file_t*			packet;

	initialize_mock_up_logger( &logger );

	/* Rewind to state from previous test. */
	err = logger_reset( &logger );
	if( err != LOGGER_OK )
	{
		logger.destroy( &logger );
		ABORT_TEST( "failed to rewind logger" );
	}

	/* Asset same thing as previous test, no_queue. */
	/* First packet. */
	packet = logger_peek_packet( &logger, &err );
	if( err != LOGGER_OK )
	{
		logger.destroy( &logger );
		ABORT_TEST( "failed to peek from logger first time" );
	}
	ASSERT( "first packet incorrect name", 0 == strncmp( "00_n0.txt", fs_handle_get_name( (fs_handle_t*) packet ), FILESYSTEM_MAX_NAME_LENGTH ) );
	packet->close( packet );
	err = logger_pop_packet( &logger );
	if( err != LOGGER_OK )
	{
		logger.destroy( &logger );
		ABORT_TEST( "failed to pop from logger first time" );
	}

	/* Second packet. */
	packet = logger_peek_packet( &logger, &err );
	if( err != LOGGER_OK )
	{
		logger.destroy( &logger );
		ABORT_TEST( "failed to peek from logger" );
	}
	ASSERT( "first packet incorrect name", 0 == strncmp( "01_n0.txt", fs_handle_get_name( (fs_handle_t*) packet ), FILESYSTEM_MAX_NAME_LENGTH ) );
	packet->close( packet );
	err = logger_pop_packet( &logger );
	if( err != LOGGER_OK )
	{
		logger.destroy( &logger );
		ABORT_TEST( "failed to pop from logger first time" );
	}

	logger.destroy( &logger );
}

TEST( queue_corruption )
{
	logger_error_t	err;
	file_t*			packet;
	char const*		name;

	initialize_mock_up_logger( &logger );

	/* Rewind to state from previous test. */
	err = logger_reset( &logger );

	/* Delete queue tail, then read. */
	logger._.fs->delete( logger._.fs, "00_n0.txt" );

	packet = logger_peek_packet( &logger, &err );
	if( err != LOGGER_OK )
	{
		logger.destroy( &logger );
		ABORT_TEST( "failed to peek from logger" );
	}
	name = fs_handle_get_name( (fs_handle_t*) packet );
	ASSERT( "first packet incorrect name", 0 == strncmp( "01_n0.txt", name, FILESYSTEM_MAX_NAME_LENGTH ) );
	packet->close( packet );
	err = logger_pop_packet( &logger );
	if( err != LOGGER_OK )
	{
		logger.destroy( &logger );
		ABORT_TEST( "failed to pop from logger first time" );
	}

	logger.destroy( &logger );
}

TEST( wod_logger_time_stamp )
{
	logger_t* 		wod_logger;
	logger_error_t	lerr;
	fs_error_t		ferr;
	file_t*			file;
	rtc_t			*rtc;
	uint32_t		current_time;
	uint8_t			file_time_stamp[sizeof(uint32_t)];
	uint32_t		actual_time_stamp;
	uint32_t		read;
	char			file_name[FILESYSTEM_MAX_NAME_LENGTH+1];

	wod_logger = kitp->wod_logger;
	rtc = kitp->rtc;

	/* Make a dummy packet to test the time stamp */
	lerr = logger_push_packet( wod_logger );
	if( lerr != LOGGER_OK )
	{
		ABORT_TEST( "failed to push dummy packet" );
	}
	lerr = logger_push_packet( wod_logger );
	if( lerr != LOGGER_OK )
	{
		ABORT_TEST( "failed to push dummy packet" );
	}
	/* Will be only slightly larger than time stamp. */
	current_time = rtc->seconds_since_epoch( rtc, QB50_EPOCH );

	/* Open queue file and find the name of the newest packet file. */
	file = kitp->fs->open( kitp->fs, &ferr, wod_logger->_.queue_file_name, FS_OPEN_EXISTING, USE_POLLING );
	if( ferr != FS_OK )
	{
		ABORT_TEST( "Failed to open queue file" );
	}
	ferr = file->seek( file, LOGGER_PRIVATE_QUEUE_TOP ); /* This is private information to the logger, check logger.c */
	if( ferr != FS_OK )
	{
		ABORT_TEST( "Failed to seek" );
	}
	ferr = file->read( file, (uint8_t*) file_name, FILESYSTEM_MAX_NAME_LENGTH+1, &read );
	if( ferr != FS_OK || read < FILESYSTEM_MAX_NAME_LENGTH+1 )
	{
		ABORT_TEST( "failed to read packet file name" );
	}
	kitp->fs->close( kitp->fs, file );

	/* Open the packet file and get the time stamp. */
	file = kitp->fs->open( kitp->fs, &ferr, file_name, FS_OPEN_EXISTING, USE_POLLING );	\
	if( ferr != FS_OK )
	{
		ABORT_TEST( "Failed to open packet file" );
	}
	ferr = file->read( file, file_time_stamp, sizeof(uint32_t), &read );
	to32_from_little_endian( &actual_time_stamp, file_time_stamp );
	if( ferr != FS_OK )
	{
		ABORT_TEST( "Failed to read packet file" );
	}

	/* Assert correct time stamp. */
	ASSERT( "failed to assert time stamp", current_time >= actual_time_stamp && (current_time < 2 ? 0 : (current_time - 2)) <= actual_time_stamp );
}

TEST_SUITE( logger )
{
	kit = kitp;
	fs = kit->fs;
	filesystem = fs;

	initialize_mock_up_logger( &logger );

	ADD_TEST( base_methods_bound );
	ADD_TEST( wod_logger_time_stamp );

	/* Tests below this point must be run in the order they are coded. */
	mock_up_logger_reset( );
	ADD_TEST( no_queue );
	ADD_TEST( reset );
	ADD_TEST( queue_corruption );

	((logger_t *) &logger)->destroy( &logger );
}
