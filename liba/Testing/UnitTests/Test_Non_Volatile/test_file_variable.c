/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_file_variable.c
 * @author Brendan Bruner
 * @date Oct 21, 2015
 */

#include "test_suites.h"
#include "dependency_injection.h"
#include <non_volatile/file_variable.h>
#include <string.h>

#define VAR_TEST_VALUE 9104
#define VAR_TEST_NAME "test_var"
#define VAR_TEST_NAME2 "pc_var"

TEST_SETUP( )
{

}
TEST_TEARDOWN( )
{

}

TEST( file_variable )
{
	file_variable_t variable;
	char const* variable_name = VAR_TEST_NAME;
	uint32_t test_value;
	uint32_t retrieved_value;
	bool_t err;

	err = initialize_file_variable( &variable, kitp->fs, variable_name, sizeof( test_value ) );
	if( !err )
	{
		ABORT_TEST( "Failed to construct variable" );
	}

	test_value = VAR_TEST_VALUE;
	err = ((non_volatile_variable_t*) &variable)->set( (non_volatile_variable_t*) &variable, (void*) &test_value );
	if( !err )
	{
		((non_volatile_variable_t*) &variable)->destroy( (non_volatile_variable_t*) &variable );
		ABORT_TEST( "Failed to set variable" );
	}
	((non_volatile_variable_t*) &variable)->get( (non_volatile_variable_t*) &variable, (void*) &retrieved_value );
	if( !err )
	{
		((non_volatile_variable_t*) &variable)->destroy( (non_volatile_variable_t*) &variable );
		ABORT_TEST( "Failed to get variable" );
	}

	ASSERT( "variable is incorrect", test_value == retrieved_value );
	((non_volatile_variable_t*) &variable)->destroy( (non_volatile_variable_t*) &variable );
}

TEST( file_variable_retest )
{
	file_variable_t variable;
	char const* variable_name = VAR_TEST_NAME2;
	uint32_t test_value;
	uint32_t retrieved_value;
	bool_t err;

	test_value = VAR_TEST_VALUE;

	err = initialize_file_variable( &variable, kitp->fs, variable_name, sizeof( test_value ) );
	if( !err )
	{
		ABORT_TEST( "Failed to construct variable" );
	}

	((non_volatile_variable_t*) &variable)->get( (non_volatile_variable_t*) &variable, (void*) &retrieved_value );
	if( !err )
	{
		((non_volatile_variable_t*) &variable)->destroy( (non_volatile_variable_t*) &variable );
		ABORT_TEST( "Failed to get variable" );
	}
	if( retrieved_value == 0 )
	{
		/* First time this variable has been used. Nothing to test. */
		err = ((non_volatile_variable_t*) &variable)->set( (non_volatile_variable_t*) &variable, (void*) &test_value );
		((non_volatile_variable_t*) &variable)->destroy( (non_volatile_variable_t*) &variable );
		if( !err )
		{
			ABORT_TEST( "Failed to set variable" );
		}
		ABORT_TEST( "Cycle power, then run this test again to confirm functionality" );
	}

	/* Second or later time running test. */
	ASSERT( "variable is incorrect", test_value == retrieved_value );
	((non_volatile_variable_t*) &variable)->destroy( (non_volatile_variable_t*) &variable );
}

TEST_SUITE( non_volatile_variable )
{
	ADD_TEST( file_variable_retest );
	ADD_TEST( file_variable );
}
