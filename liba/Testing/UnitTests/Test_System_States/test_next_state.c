/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_next_state.c
 * @author Brendan Bruner
 * @date Oct 7, 2015
 */

#include "test_suites.h"
#include "dependency_injection.h"
#include <states/state_relay.h>
#include <core_defines.h>

static 	state_relay_t*			relay;
static 	filesystem_t*			fs;
static 	eps_mode_t 				eps_mode_state;
#define TEST_LOG_FILE 			"tns.txt"
#define TEST_BLACK_OUT_TIME_OUT	3
#define TEST_DETUMBLE_TIME_OUT 	3

/* Dependency inject, into eps_t, a way to control what mode its in. */
static eps_mode_t eps_mode( eps_t *self )
{
	return eps_mode_state;
}
static void set_eps_mode( eps_mode_t mode )
{
	eps_mode_state = mode;
}

/* Dependency inject, into adcs_t, a way to control what mode its in. */
static void set_adcs_mode( adcs_t* self, adcs_state_enum_t mode )
{
	switch( mode )
	{
		case ADCS_NO_CONTROL:
			self->adcs_state.adcs_modes = 0;
			break;
		case ADCS_DETUMBLING_CONTROL:
			self->adcs_state.adcs_modes = 2;
			break;
		case ADCS_Y_INIT_PITCH:
			self->adcs_state.adcs_modes = 3;
			break;
		case ADCS_Y_STEADY:
			self->adcs_state.adcs_modes = 4;
			break;
		default:
			self->adcs_state.adcs_modes = 0;
			break;
	}
}
static void getframe_adcs_state_injection( adcs_t* self )
{
	return;
}


TEST_SETUP( )
{
	fs_error_t err;

	err = fs->file_exists( fs, TEST_LOG_FILE );
	if( err == FS_OK )
	{
		fs->delete( fs, TEST_LOG_FILE );
	}
}
TEST_TEARDOWN( )
{

}

TEST( leop_check )
{
	state_leop_check_t state;
	state_t* next_state;
	uint8_t err;
	ram_variable_t leop_flag;
	uint8_t leop_flag_value;

	initialize_ram_variable( &leop_flag, (void*) &leop_flag_value, sizeof(leop_flag_value) );
	leop_flag_value = LEOP_FLAG_UNFINISHED;
	((non_volatile_variable_t*) &leop_flag)->set( (non_volatile_variable_t*) &leop_flag, (void*) &leop_flag_value );
	err = initialize_state_leop_check( &state, fs, (non_volatile_variable_t*) &leop_flag );
	if( err == STATE_FAILURE )
	{
		ABORT_TEST( "Failed to construct state object" );
	}

	next_state = ((state_t*) &state)->next_state( (state_t*) &state, relay );
	ASSERT( "next state before finishing leop is wrong", next_state == (state_t*) &relay->black_out );

	leop_flag_value = LEOP_FLAG_FINISHED;
	((non_volatile_variable_t*) &leop_flag)->set( (non_volatile_variable_t*) &leop_flag, (void*) &leop_flag_value );

	next_state = ((state_t*) &state)->next_state( (state_t*) &state, relay );
	ASSERT( "next state after finishing leop is wrong", next_state == (state_t*) &relay->bring_up );

	((state_t*) &state)->destroy( (state_t*) &state );
	((non_volatile_variable_t*) &leop_flag)->destroy( (non_volatile_variable_t*) &leop_flag );
}

TEST( antenna_deployment )
{
	state_antenna_deployment_t state;
	state_t* next_state;
	uint8_t err;
	ram_variable_t leop_flag;
	uint8_t leop_flag_value;

	initialize_ram_variable( &leop_flag, (void*) &leop_flag_value, sizeof(leop_flag_value) );
	err = initialize_state_antenna_deployment( &state, fs, (non_volatile_variable_t*) &leop_flag );
	if( err == STATE_FAILURE )
	{
		ABORT_TEST( "Failed to construct state object" );
	}

	next_state = ((state_t*) &state)->next_state( (state_t*) &state, relay );
	ASSERT( "next state before deployment is wrong", next_state == NULL );

	((state_t*) &state)->enter_state( (state_t*) &state, kitp );
	next_state = ((state_t*) &state)->next_state( (state_t*) &state, relay );
	ASSERT( "next state after deployment is wrong", next_state == (state_t*) &relay->bring_up );

	((state_t*) &state)->destroy( (state_t*) &state );
	((non_volatile_variable_t*) &leop_flag)->destroy( (non_volatile_variable_t*) &leop_flag );
}

TEST( black_out )
{
	state_black_out_t state;
	state_t* next_state;
	uint8_t err;

	err = initialize_state_black_out( &state, fs, TEST_BLACK_OUT_TIME_OUT, TEST_LOG_FILE );
	if( err == STATE_FAILURE )
	{
		ABORT_TEST( "Failed to construct state object" );
	}

	((state_t*) &state)->enter_state( (state_t*) &state, kitp );
	next_state = ((state_t*) &state)->next_state( (state_t*) &state, relay );
	ASSERT( "next state before black out is finished is wrong", next_state == NULL );

	/* Wait for blackout to finish. */
	state._.timer.is_expired( &state._.timer, BLOCK_FOREVER );

	set_eps_mode( EPS_MODE_OPTIMAL );
	((state_t*) &state)->enter_state( (state_t*) &state, kitp );
	next_state = ((state_t*) &state)->next_state( (state_t*) &state, relay );
	ASSERT( "next state with optimal battery is wrong", next_state == (state_t*) &relay->detumble );

	set_eps_mode( EPS_MODE_NORMAL );
	((state_t*) &state)->enter_state( (state_t*) &state, kitp );
	next_state = ((state_t*) &state)->next_state( (state_t*) &state, relay );
	ASSERT( "next state with below optimal battery is wrong", next_state == (state_t*) &relay->power_charge );

	((state_t*) &state)->destroy( (state_t*) &state );
}

TEST( boom_deployment )
{
	state_boom_deployment_t state;
	state_t* next_state;
	uint8_t err;

	err = initialize_state_boom_deployment( &state, fs );
	if( err == STATE_FAILURE )
	{
		ABORT_TEST( "Failed to construct state object" );
	}

	next_state = ((state_t*) &state)->next_state( (state_t*) &state, relay );
	ASSERT( "next state before deployment is wrong", next_state == NULL );

	((state_t*) &state)->enter_state( (state_t*) &state, kitp );
	next_state = ((state_t*) &state)->next_state( (state_t*) &state, relay );
	ASSERT( "next state after deployment is wrong", next_state == (state_t*) &relay->antenna_deploy );

	((state_t*) &state)->destroy( (state_t*) &state );
}

TEST( detumble )
{
	state_detumble_t state;
	state_t* next_state;
	uint8_t err;

	err = initialize_state_detumble( &state, fs, TEST_DETUMBLE_TIME_OUT, TEST_LOG_FILE );
	if( err == STATE_FAILURE )
	{
		ABORT_TEST( "Failed to construct state object" );
	}
	((state_t*) &state)->enter_state( (state_t*) &state, kitp );

	/* Finished detumbling timer not expired. */
	set_adcs_mode( &kitp->adcs, ADCS_Y_STEADY );
	next_state = ((state_t*) &state)->next_state( (state_t*) &state, relay );
	ASSERT( "next state when finished detumbling is wrong", next_state == (state_t*) &relay->boom_deploy );

	/* Not done detumbling, timer not expired, test different power modes. */
	set_adcs_mode( &kitp->adcs, ADCS_DETUMBLING_CONTROL );
	set_eps_mode( EPS_MODE_CRITICAL );
	next_state = ((state_t*) &state)->next_state( (state_t*) &state, relay );
	ASSERT( "next state with critical power is wrong", next_state == (state_t*) &relay->power_charge );

	set_eps_mode( EPS_MODE_POWER_SAFE );
	next_state = ((state_t*) &state)->next_state( (state_t*) &state, relay );
	ASSERT( "next state when power safe is wrong", next_state == (state_t*) &relay->power_charge );

	set_eps_mode( EPS_MODE_NORMAL );
	next_state = ((state_t*) &state)->next_state( (state_t*) &state, relay );
	ASSERT( "next state with normal power is wrong", next_state == NULL );

	set_eps_mode( EPS_MODE_OPTIMAL );
	next_state = ((state_t*) &state)->next_state( (state_t*) &state, relay );
	ASSERT( "next state with optimal power is wrong", next_state == NULL );

	/* timer expired. */
	state.priv_.timer.is_expired( &state.priv_.timer, BLOCK_FOREVER );
	next_state = ((state_t*) &state)->next_state( (state_t*) &state, relay );
	ASSERT( "next state with time out is wrong", next_state == (state_t*) &relay->antenna_deploy );

	((state_t*) &state)->destroy( (state_t*) &state );
}

TEST( power_charge )
{
	state_leop_power_charge_t state;
	state_t* next_state;
	uint8_t err;

	err = initialize_state_leop_power_charge( &state, fs );
	if( err == STATE_FAILURE )
	{
		ABORT_TEST( "Failed to construct state object" );
	}

	set_eps_mode( EPS_MODE_NORMAL );
	next_state = ((state_t*) &state)->next_state( (state_t*) &state, relay );
	ASSERT( "next state with below optimal power is wrong", next_state == NULL );

	set_eps_mode( EPS_MODE_OPTIMAL );
	next_state = ((state_t*) &state)->next_state( (state_t*) &state, relay );
	ASSERT( "next state with optimal power is wrong", next_state == (state_t*) &relay->detumble );

	((state_t*) &state)->destroy( (state_t*) &state );
}

TEST( alignment )
{
	ABORT_TEST( "not implemented" );
}

TEST( bring_up )
{
	ABORT_TEST( "not implemented" );
}

TEST( low_power )
{
	ABORT_TEST( "not implemented" );
}

TEST( science )
{
	ABORT_TEST( "not implemented" );
}

TEST_SUITE( next_state )
{
	typedef eps_mode_t (*eps_mode_method_t)( eps_t* );
	typedef void (*getframe_adcs_state_method_t)( adcs_t* );

	relay = (state_relay_t*) OBCMalloc( sizeof(state_relay_t) );
	fs = kitp->fs;
	if( relay == NULL )
	{
		UNIT_PRINT( "failure to allocate memory for test\n" );
		return;
	}
	uint8_t err = initialize_state_relay_simple( relay, kitp );
	if( err != STATE_RELAY_SUCCESS )
	{
		UNIT_PRINT( "failure to initialize object for testing\n" );
		OBCFree( relay );
		return;
	}

	/* dependency inject, into eps_t, a way to control its mode. */
	eps_mode_method_t old_mode;
	old_mode = kitp->eps->mode;
	kitp->eps->mode = eps_mode;

	getframe_adcs_state_method_t old_adcs_get_frame;
	old_adcs_get_frame = kitp->adcs.getframe_adcs_state;
	kitp->adcs.getframe_adcs_state = getframe_adcs_state_injection;

	ADD_TEST( leop_check );
	ADD_TEST( antenna_deployment );
	ADD_TEST( black_out );
	ADD_TEST( boom_deployment );
	ADD_TEST( detumble );
	ADD_TEST( power_charge );
	ADD_TEST( alignment );
	ADD_TEST( bring_up );
	ADD_TEST( low_power );
	ADD_TEST( science );

	/* Restore operations altered by dependency injection. */
	kitp->eps->mode = old_mode;
	kitp->adcs.getframe_adcs_state = old_adcs_get_frame;

	destroy_state_relay( relay );
	OBCFree( relay );
}
