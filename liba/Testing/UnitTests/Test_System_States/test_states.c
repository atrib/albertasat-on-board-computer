/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_power_on
 * @author Brendan Bruner
 * @date Feb 11, 2015
 */

#include <states/state_relay.h>
#include <states/state.h>
#include <telecommands/telecommand.h>

#include <test_suites.h>
#include <dependency_injection.h>

#define TEST_STATE_CONFIG_NAME "tsln_l.txt"

static driver_toolkit_t *kit;
static state_config_t default_config;
static state_t state;

TEST_SETUP( )
{
	/* Set up default configuration. */
	state_config_set_hk( &default_config, STATE_DISABLES_EXECUTION );
	state_config_set_transmit( &default_config, STATE_DISABLES_EXECUTION );
	state_config_set_response( &default_config, STATE_DISABLES_EXECUTION );
	state_config_set_dfgm( &default_config, STATE_DISABLES_EXECUTION );
	state_config_set_mnlp( &default_config, STATE_DISABLES_EXECUTION );

	initialize_state( &state, TEST_STATE_CONFIG_NAME, kit->fs, &default_config );
}
TEST_TEARDOWN( )
{
	state.destroy( &state );
}

TEST( query_state_id )
{
	state_relay_t relay;

	initialize_state_relay_simple( &relay, kit );

	ASSERT( "bring up id is %d, not %d",
			query_state_id( (state_t *) &relay.bring_up ) == STATE_BRING_UP_ID,
			query_state_id( (state_t *) &relay.bring_up ), STATE_BRING_UP_ID );
	ASSERT( "low power id is %d, not %d",
			query_state_id( (state_t *) &relay.low_power ) == STATE_LOW_POWER_ID,
			query_state_id( (state_t *) &relay.low_power ), STATE_LOW_POWER_ID);
	ASSERT( "alignment id is %d, not %d",
			query_state_id( (state_t *) &relay.alignment ) == STATE_ALIGNMENT_ID,
			query_state_id( (state_t *) &relay.alignment ), STATE_ALIGNMENT_ID );
	ASSERT( "science id is %d, not %d",
			query_state_id( (state_t *) &relay.science ) == STATE_SCIENCE_ID,
			query_state_id( (state_t *) &relay.science ), STATE_SCIENCE_ID );

	destroy_state_relay( &relay );
}

TEST( default_config )
{
	telecommand_counter_t counter;
	int count;

	/* Delete config file before initializing state. */
	state.destroy( &state );
	kit->fs->delete( kit->fs, TEST_STATE_CONFIG_NAME );
	initialize_state( &state, TEST_STATE_CONFIG_NAME, kit->fs, &default_config );
	initialize_command_counter( &counter, &count, kit );

	state_execution_hk( &state, (telecommand_t *) &counter );
	ASSERT( "hk command should not have executed", count == 0 );
	state_execution_transmit( &state, (telecommand_t *) &counter );
	ASSERT( "transmit command should not have executed", count == 0 );
	state_execution_response( &state, (telecommand_t *) &counter );
	ASSERT( "response command should not have executed", count == 0 );
	state_execution_dfgm( &state, (telecommand_t *) &counter );
	ASSERT( "dfgm command should not have executed", count == 0 );
	state_execution_mnlp( &state, (telecommand_t *) &counter );
	ASSERT( "mnlp command should not have executed", count == 0 );

	((telecommand_t *) &counter)->destroy( (telecommand_t *) &counter );
}

TEST( get_config )
{
	state_config_t config;

	state_get_config( &state, &config );

	ASSERT( "hk command should be disabled", config._uses_hk == STATE_DISABLES_EXECUTION );
	ASSERT( "transmit command should be disabled", config._uses_transmit == STATE_DISABLES_EXECUTION );
	ASSERT( "response command should be disabled", config._uses_response == STATE_DISABLES_EXECUTION );
	ASSERT( "dfgm command should be disabled", config._uses_dfgm == STATE_DISABLES_EXECUTION );
	ASSERT( "mnlp command should be disabled", config._uses_mnlp == STATE_DISABLES_EXECUTION );
}

TEST( alter_config )
{
	state_config_t config;
	telecommand_counter_t counter;
	int count;
	uint8_t set_err;

	initialize_command_counter( &counter, &count, kit );

	/* Set up new configuration. */
	state_config_set_hk( &config, STATE_ENABLES_EXECUTION );
	state_config_set_transmit( &config, STATE_ENABLES_EXECUTION );
	state_config_set_response( &config, STATE_ENABLES_EXECUTION );
	state_config_set_dfgm( &config, STATE_ENABLES_EXECUTION );
	state_config_set_mnlp( &config, STATE_ENABLES_EXECUTION );

	set_err = state_set_config( &state, &config );
	if( set_err != STATE_SUCCESS )
	{
		((telecommand_t *) &counter)->destroy( (telecommand_t *) &counter );
		ABORT_TEST( "Failed to set new config" );
	}

	state_execution_hk( &state, (telecommand_t *) &counter );
	ASSERT( "hk command should have executed", count == 1 );
	state_execution_transmit( &state, (telecommand_t *) &counter );
	ASSERT( "transmit command should have executed", count == 2 );
	state_execution_response( &state, (telecommand_t *) &counter );
	ASSERT( "response command should have executed", count == 3 );
	state_execution_dfgm( &state, (telecommand_t *) &counter );
	ASSERT( "dfgm command should have executed", count == 4 );
	state_execution_mnlp( &state, (telecommand_t *) &counter );
	ASSERT( "mnlp command should have executed", count == 5 );

	state_get_config( &state, &config );

	ASSERT( "hk command should be enabled", config._uses_hk == STATE_ENABLES_EXECUTION );
	ASSERT( "transmit command should be enabled", config._uses_transmit == STATE_ENABLES_EXECUTION );
	ASSERT( "response command should be enabled", config._uses_response == STATE_ENABLES_EXECUTION );
	ASSERT( "dfgm command should be enabled", config._uses_dfgm == STATE_ENABLES_EXECUTION );
	ASSERT( "mnlp command should be enabled", config._uses_mnlp == STATE_ENABLES_EXECUTION );

	((telecommand_t *) &counter)->destroy( (telecommand_t *) &counter );
}

TEST( reset_config )
{
	state_config_t config;
	uint8_t set_err;

	set_err = state_reset_config( &state );
	if( set_err != STATE_SUCCESS )
	{
		ABORT_TEST( "Failed to reset config" );
	}
	state_get_config( &state, &config );

	ASSERT( "hk command should be disabled", config._uses_hk == STATE_DISABLES_EXECUTION );
	ASSERT( "transmit command should be disabled", config._uses_transmit == STATE_DISABLES_EXECUTION );
	ASSERT( "response command should be disabled", config._uses_response == STATE_DISABLES_EXECUTION );
	ASSERT( "dfgm command should be disabled", config._uses_dfgm == STATE_DISABLES_EXECUTION );
	ASSERT( "mnlp command should be disabled", config._uses_mnlp == STATE_DISABLES_EXECUTION );
}

TEST( corrupt_config )
{
	/* Since a corrupt config will cause a reset to default, we need to */
	/* change the current config to something other than default. */
	state_config_t config;
	telecommand_counter_t counter;
	uint8_t corruption[5], iter, set_err;
	uint32_t written;
	int count;
	fs_error_t err;
	file_t *file;

	initialize_command_counter( &counter, &count, kit );
	for( iter = 0; iter < 5; ++iter )
	{
		corruption[iter] = iter;
	}

	/* Set up new configuration. */
	state_config_set_hk( &config, STATE_ENABLES_EXECUTION );
	state_config_set_transmit( &config, STATE_ENABLES_EXECUTION );
	state_config_set_response( &config, STATE_ENABLES_EXECUTION );
	state_config_set_dfgm( &config, STATE_ENABLES_EXECUTION );
	state_config_set_mnlp( &config, STATE_ENABLES_EXECUTION );
	set_err = state_set_config( &state, &config );
	if( set_err != STATE_SUCCESS )
	{
		((telecommand_t *) &counter)->destroy( (telecommand_t *) &counter );
		ABORT_TEST( "Failed to set new config" );
	}

	/* Corrupt the file. */
	file = kit->fs->open( kit->fs, &err, TEST_STATE_CONFIG_NAME, FS_OPEN_EXISTING, USE_POLLING );
	if( err != FS_OK )
	{
		((telecommand_t *) &counter)->destroy( (telecommand_t *) &counter );
		ABORT_TEST( "Failed to open config file" );
	}
	err = file->write( file, corruption, 5, &written );
	if( err != FS_OK )
	{
		kit->fs->close( kit->fs, file );
		((telecommand_t *) &counter)->destroy( (telecommand_t *) &counter );
		ABORT_TEST( "Failed to open config file" );
	}
	kit->fs->close( kit->fs, file );

	/* State should continue using ram copy of configuration. */
	state_execution_hk( &state, (telecommand_t *)&counter );
	ASSERT( "hk command should have executed using ram copy", count == 1 );
	state_execution_transmit( &state, (telecommand_t *)&counter );
	ASSERT( "transmit command should have executed using ram copy", count == 2 );
	state_execution_response( &state, (telecommand_t *)&counter );
	ASSERT( "response command should have executed using ram copy", count == 3 );
	state_execution_dfgm( &state, (telecommand_t *)&counter );
	ASSERT( "dfgm command should have executed using ram copy", count == 4 );
	state_execution_mnlp( &state, (telecommand_t *)&counter );
	ASSERT( "mnlp command should have executed using ram copy", count == 5 );

	/* Setting a new config should over write corruption. */
	state_config_set_hk( &config, STATE_ENABLES_EXECUTION );
	state_config_set_transmit( &config, STATE_ENABLES_EXECUTION );
	state_config_set_response( &config, STATE_ENABLES_EXECUTION );
	state_config_set_dfgm( &config, STATE_DISABLES_EXECUTION );
	state_config_set_mnlp( &config, STATE_DISABLES_EXECUTION );
	set_err = state_set_config( &state, &config );
	if( set_err != STATE_SUCCESS )
	{
		((telecommand_t *) &counter)->destroy( (telecommand_t *) &counter );
		ABORT_TEST( "Failed to set new config" );
	}

	/* Destroy and initialize to assert corruption was fixed. */
	state.destroy( &state );
	initialize_state( &state, TEST_STATE_CONFIG_NAME, kit->fs, &default_config );
	state_execution_hk( &state, (telecommand_t *) &counter );
	ASSERT( "hk command should have executed after setting new config", count == 6 );
	state_execution_transmit( &state, (telecommand_t *) &counter );
	ASSERT( "transmit command should have executed after setting new config", count == 7 );
	state_execution_response( &state, (telecommand_t *) &counter );
	ASSERT( "response command should have executed after setting new config", count == 8 );
	state_execution_dfgm( &state, (telecommand_t *) &counter );
	ASSERT( "dfgm command should not have executed after setting new config", count == 8 );
	state_execution_mnlp( &state, (telecommand_t *) &counter );
	ASSERT( "mnlp command should not have executed after setting new config", count == 8 );

	/* Corrupt the file again. This time, do not set a new config. */
	file = kit->fs->open( kit->fs, &err, TEST_STATE_CONFIG_NAME, FS_OPEN_EXISTING, USE_POLLING );
	if( err != FS_OK )
	{
		((telecommand_t *) &counter)->destroy( (telecommand_t *) &counter );
		ABORT_TEST( "Failed to open config file" );
	}
	err = file->write( file, corruption, 5, &written );
	if( err != FS_OK )
	{
		kit->fs->close( kit->fs, file );
		((telecommand_t *) &counter)->destroy( (telecommand_t *) &counter );
		ABORT_TEST( "Failed to open config file" );
	}
	kit->fs->close( kit->fs, file );

	/* Destroy and initialize to check corruption gets fixed. */
	state.destroy( &state );
	set_err = initialize_state( &state, TEST_STATE_CONFIG_NAME, kit->fs, &default_config );
	ASSERT( "error code should not be %d, but should be %d", set_err == STATE_DEFAULTED,
			set_err, STATE_DEFAULTED );
	state_execution_hk( &state, (telecommand_t *) &counter );
	ASSERT( "hk command should not have executed after new initialization", count == 8 );
	state_execution_transmit( &state, (telecommand_t *) &counter );
	ASSERT( "transmit command should not have executed after new initialization", count == 8 );
	state_execution_response( &state, (telecommand_t *) &counter );
	ASSERT( "response command should not have executed after new initialization", count == 8 );
	state_execution_dfgm( &state, (telecommand_t *) &counter );
	ASSERT( "dfgm command should not have executed after new initialization", count == 8 );
	state_execution_mnlp( &state, (telecommand_t *) &counter );
	ASSERT( "mnlp command should not have executed after new initialization", count == 8 );

	((telecommand_t *) &counter)->destroy( (telecommand_t *) &counter );
}

TEST( delete_config )
{
	state_config_t config;
	telecommand_counter_t counter;
	uint8_t set_err;
	int count;
	fs_error_t err;

	initialize_command_counter( &counter, &count, kit );

	/* Set up new configuration. */
	state_config_set_hk( &config, STATE_ENABLES_EXECUTION );
	state_config_set_transmit( &config, STATE_ENABLES_EXECUTION );
	state_config_set_response( &config, STATE_ENABLES_EXECUTION );
	state_config_set_dfgm( &config, STATE_ENABLES_EXECUTION );
	state_config_set_mnlp( &config, STATE_ENABLES_EXECUTION );
	set_err = state_set_config( &state, &config );
	if( set_err != STATE_SUCCESS )
	{
		((telecommand_t *) &counter)->destroy( (telecommand_t *) &counter );
		ABORT_TEST( "Failed to set new config" );
	}

	/* Corrupt the file. */
	err = kit->fs->delete( kit->fs, TEST_STATE_CONFIG_NAME );
	if( err != FS_OK )
	{
		((telecommand_t *) &counter)->destroy( (telecommand_t *) &counter );
		ABORT_TEST( "Failed to delete config file" );
	}

	/* State should continue using ram copy of configuration. */
	state_execution_hk( &state, (telecommand_t *) &counter );
	ASSERT( "hk command should have executed using ram copy", count == 1 );
	state_execution_transmit( &state, (telecommand_t *) &counter );
	ASSERT( "transmit command should have executed using ram copy", count == 2 );
	state_execution_response( &state, (telecommand_t *) &counter );
	ASSERT( "response command should have executed using ram copy", count == 3 );
	state_execution_dfgm( &state, (telecommand_t *) &counter );
	ASSERT( "dfgm command should have executed using ram copy", count == 4 );
	state_execution_mnlp( &state, (telecommand_t *) &counter );
	ASSERT( "mnlp command should have executed using ram copy", count == 5 );

	/* Setting a new config should over write corruption. */
	state_config_set_hk( &config, STATE_ENABLES_EXECUTION );
	state_config_set_transmit( &config, STATE_ENABLES_EXECUTION );
	state_config_set_response( &config, STATE_ENABLES_EXECUTION );
	state_config_set_dfgm( &config, STATE_DISABLES_EXECUTION );
	state_config_set_mnlp( &config, STATE_DISABLES_EXECUTION );
	set_err = state_set_config( &state, &config );
	if( set_err != STATE_SUCCESS )
	{
		((telecommand_t *) &counter)->destroy( (telecommand_t *) &counter );
		ABORT_TEST( "Failed to set new config" );
	}

	/* Destroy and initialize to assert corruption was fixed. */
	state.destroy( &state );
	initialize_state( &state, TEST_STATE_CONFIG_NAME, kit->fs, &default_config );
	state_execution_hk( &state, (telecommand_t *) &counter );
	ASSERT( "hk command should have executed after setting new config", count == 6 );
	state_execution_transmit( &state, (telecommand_t *) &counter );
	ASSERT( "transmit command should have executed after setting new config", count == 7 );
	state_execution_response( &state, (telecommand_t *) &counter );
	ASSERT( "response command should have executed after setting new config", count == 8 );
	state_execution_dfgm( &state, (telecommand_t *) &counter );
	ASSERT( "dfgm command should not have executed after setting new config", count == 8 );
	state_execution_mnlp( &state, (telecommand_t *) &counter );
	ASSERT( "mnlp command should not have executed after setting new config", count == 8 );

	/* Corrupt the file again. This time, do not set a new config. */
	err = kit->fs->delete( kit->fs, TEST_STATE_CONFIG_NAME );
	if( err != FS_OK )
	{
		((telecommand_t *) &counter)->destroy( (telecommand_t *) &counter );
		ABORT_TEST( "Failed to delete config file" );
	}

	/* Destroy and initialize to check corruption gets fixed. */
	state.destroy( &state );
	set_err = initialize_state( &state, TEST_STATE_CONFIG_NAME, kit->fs, &default_config );
	ASSERT( "error code should not be %d, but should be %d", set_err == STATE_DEFAULTED,
			set_err, STATE_DEFAULTED );
	state_execution_hk( &state, (telecommand_t *)&counter );
	ASSERT( "hk command should not have executed after new initialization", count == 8 );
	state_execution_transmit( &state, (telecommand_t *) &counter );
	ASSERT( "transmit command should not have executed after new initialization", count == 8 );
	state_execution_response( &state, (telecommand_t *) &counter );
	ASSERT( "response command should not have executed after new initialization", count == 8 );
	state_execution_dfgm( &state, (telecommand_t *) &counter );
	ASSERT( "dfgm command should not have executed after new initialization", count == 8 );
	state_execution_mnlp( &state, (telecommand_t *) &counter );
	ASSERT( "mnlp command should not have executed after new initialization", count == 8 );

	((telecommand_t *) &counter)->destroy( (telecommand_t *) &counter );
}

TEST( global_lock )
{
	state_config_t 	config;
	rtc_time_t 		time;
	file_t			*file;
	uint32_t		bytes_used;
	fs_error_t		fs_err;

	/* Set up arbitrary configuration. */
	state_config_set_hk( &config, STATE_ENABLES_EXECUTION );
	state_config_set_transmit( &config, STATE_ENABLES_EXECUTION );
	state_config_set_response( &config, STATE_ENABLES_EXECUTION );
	state_config_set_dfgm( &config, STATE_ENABLES_EXECUTION );
	state_config_set_mnlp( &config, STATE_ENABLES_EXECUTION );

	/* Test refreshing works. */
	state_lock_config_globally( BLOCK_FOREVER );
	state_config_set_hk( &config, STATE_DISABLES_EXECUTION );
	file = kit->fs->open( kit->fs, &fs_err, state._config_name_, FS_OPEN_EXISTING, USE_POLLING );
	if( fs_err != FS_OK )
	{
		ABORT_TEST( "failed to open config file" );
	}
	fs_err = file->write( file, (uint8_t *) &config, sizeof(config), &bytes_used );
	if( fs_err != FS_OK )
	{
		kit->fs->close( kit->fs, file );
		ABORT_TEST( "failed to open config file" );
	}
	kit->fs->close( kit->fs, file );
	state_unlock_config_globally( );
	state_refresh( &state, kit );
	state_get_config( &state, &config );
	ASSERT( "config did not get refreshed", config._uses_hk == STATE_DISABLES_EXECUTION );

	/* Test lock and unlock work */
	state_lock_config_globally( BLOCK_FOREVER );
	state_unlock_config_globally( );
	time = (rtc_time_t){ .seconds = 0, .minutes = 0, .hours = 0, .day_of_month = 0, .month = 0, .year = 2010 };
	kit->rtc->set_time( kit->rtc, &time );
	state_set_config( &state, &config );
	kit->rtc->get_time( kit->rtc, &time );
	ASSERT( "Unlock did not work, sec: %d", time.seconds <= 5, time.seconds );

	/* Test time out. Five minute delay here. */
	time = (rtc_time_t){ .seconds = 0, .minutes = 0, .hours = 0, .day_of_month = 0, .month = 0, .year = 2010 };
	kit->rtc->set_time( kit->rtc, &time );
	state_lock_config_globally( BLOCK_FOREVER );
	state_set_config( &state, &config );
	kit->rtc->get_time( kit->rtc, &time );
	ASSERT( "Block did not last correct amount of time, min: %d, sec: %d", time.minutes >= 5 && time.seconds <= 5, time.minutes, time.seconds );
}


TEST_SUITE( system_states )
{
	kit = kitp;

	ADD_TEST( global_lock );
	ADD_TEST( query_state_id );
	ADD_TEST( default_config );
	ADD_TEST( get_config );
	ADD_TEST( alter_config );
	ADD_TEST( reset_config );
	ADD_TEST( corrupt_config );
	ADD_TEST( delete_config );
}
