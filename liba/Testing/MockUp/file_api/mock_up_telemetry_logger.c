/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file mock_up_logger.c
 * @author Brendan Bruner
 * @date May 22, 2015
 */

#include "dependency_injection.h"

char *del_buff = "mtldb.txt";
char *mas_tbl = "mmt0.txt";
char *nm_spc = "00_n0.txt";

#define NM_SPC_SIZE 9
#define nm_spc1 "00_n1.txt"
#define nm_spc2 "00_n2.txt"
#define nm_spc3 "00_n3.txt"
#define nm_spc4 "00_n4.txt"
#define nm_spc5 "00_n5.txt"
#define nm_spc6 "00_n6.txt"
char *switch_nm_spc[] = {nm_spc1, nm_spc2, nm_spc3, nm_spc4, nm_spc5, nm_spc6};

#define mas_tbl1 "mmt1.txt"
#define mas_tbl2 "mmt2.txt"
#define mas_tbl3 "mmt3.txt"
#define mas_tbl4 "mmt4.txt"
#define mas_tbl5 "mmt5.txt"
#define mas_tbl6 "mmt6.txt"
char *switch_mas_tbl[] = {mas_tbl1, mas_tbl2, mas_tbl3, mas_tbl4, mas_tbl5, mas_tbl6};



void mock_up_logger_reset( void )
{
	filesystem_t *fs;
	fs = kitp->fs;

	if( fs->file_exists( fs, mas_tbl ) == FS_OK ) fs->delete( fs, mas_tbl );
	if( fs->file_exists( fs, mas_tbl1 ) == FS_OK ) fs->delete( fs, mas_tbl1 );
	if( fs->file_exists( fs, mas_tbl2 ) == FS_OK ) fs->delete( fs, mas_tbl2 );
	if( fs->file_exists( fs, mas_tbl3 ) == FS_OK ) fs->delete( fs, mas_tbl3 );
	if( fs->file_exists( fs, mas_tbl4 ) == FS_OK ) fs->delete( fs, mas_tbl4 );
	if( fs->file_exists( fs, mas_tbl5 ) == FS_OK ) fs->delete( fs, mas_tbl5 );
	if( fs->file_exists( fs, mas_tbl6 ) == FS_OK ) fs->delete( fs, mas_tbl6 );

	if( fs->file_exists( fs, "00_n0.txt" ) == FS_OK ) fs->delete( fs, "00_n0.txt" );
	if( fs->file_exists( fs, "01_n0.txt" ) == FS_OK ) fs->delete( fs, "01_n0.txt" );
	if( fs->file_exists( fs, "02_n0.txt" ) == FS_OK ) fs->delete( fs, "02_n0.txt" );
	if( fs->file_exists( fs, "03_n0.txt" ) == FS_OK ) fs->delete( fs, "03_n0.txt" );
	if( fs->file_exists( fs, "04_n0.txt" ) == FS_OK ) fs->delete( fs, "04_n0.txt" );
	if( fs->file_exists( fs, "05_n0.txt" ) == FS_OK ) fs->delete( fs, "05_n0.txt" );
	if( fs->file_exists( fs, "06_n0.txt" ) == FS_OK ) fs->delete( fs, "06_n0.txt" );

	if( fs->file_exists( fs, "00_n1.txt" ) == FS_OK ) fs->delete( fs, "00_n1.txt" );
	if( fs->file_exists( fs, "01_n1.txt" ) == FS_OK ) fs->delete( fs, "01_n1.txt" );
	if( fs->file_exists( fs, "02_n1.txt" ) == FS_OK ) fs->delete( fs, "02_n1.txt" );
	if( fs->file_exists( fs, "03_n1.txt" ) == FS_OK ) fs->delete( fs, "03_n1.txt" );

	if( fs->file_exists( fs, "00_n2.txt" ) == FS_OK ) fs->delete( fs, "00_n2.txt" );
	if( fs->file_exists( fs, "01_n2.txt" ) == FS_OK ) fs->delete( fs, "01_n2.txt" );
	if( fs->file_exists( fs, "02_n2.txt" ) == FS_OK ) fs->delete( fs, "02_n2.txt" );
	if( fs->file_exists( fs, "03_n2.txt" ) == FS_OK ) fs->delete( fs, "03_n2.txt" );

	if( fs->file_exists( fs, "00_n3.txt" ) == FS_OK ) fs->delete( fs, "00_n3.txt" );
	if( fs->file_exists( fs, "01_n3.txt" ) == FS_OK ) fs->delete( fs, "01_n3.txt" );
	if( fs->file_exists( fs, "02_n3.txt" ) == FS_OK ) fs->delete( fs, "02_n3.txt" );
	if( fs->file_exists( fs, "03_n3.txt" ) == FS_OK ) fs->delete( fs, "03_n3.txt" );

	if( fs->file_exists( fs, "00_n4.txt" ) == FS_OK ) fs->delete( fs, "00_n4.txt" );
	if( fs->file_exists( fs, "01_n4.txt" ) == FS_OK ) fs->delete( fs, "01_n4.txt" );
	if( fs->file_exists( fs, "02_n4.txt" ) == FS_OK ) fs->delete( fs, "02_n4.txt" );
	if( fs->file_exists( fs, "03_n4.txt" ) == FS_OK ) fs->delete( fs, "03_n4.txt" );

	if( fs->file_exists( fs, "00_n6.txt" ) == FS_OK ) fs->delete( fs, "00_n6.txt" );
	if( fs->file_exists( fs, "01_n6.txt" ) == FS_OK ) fs->delete( fs, "01_n6.txt" );
	if( fs->file_exists( fs, "02_n6.txt" ) == FS_OK ) fs->delete( fs, "02_n6.txt" );
	if( fs->file_exists( fs, "03_n6.txt" ) == FS_OK ) fs->delete( fs, "03_n6.txt" );

	if( fs->file_exists( fs, "00_n5.txt" ) == FS_OK ) fs->delete( fs, "00_n5.txt" );
	if( fs->file_exists( fs, "01_n5.txt" ) == FS_OK ) fs->delete( fs, "01_n5.txt" );
	if( fs->file_exists( fs, "02_n5.txt" ) == FS_OK ) fs->delete( fs, "02_n5.txt" );
	if( fs->file_exists( fs, "03_n5.txt" ) == FS_OK ) fs->delete( fs, "03_n5.txt" );
}

static size_t next_name( logger_t *logger, char *name )
{
	if ( name[1] == '9' )
	{
		name[0] = name[0] + 1;
		name[1] = '0';
	}
	else if (name[0] == '9' )
	{
		name[1] = name[1] + 1;
		name[0] = '0';
	}
	else
	{
		name[1] += 1;
	}
	return NM_SPC_SIZE;
}

void initialize_mock_up_logger( logger_t *logger )
{
	_initialize_logger( logger, kitp->fs, del_buff, mas_tbl, nm_spc );
	logger->__.next_name = next_name;
}

void initialize_mock_up_logger_custom( logger_t *logger, uint8_t ns_switch )
{
	char *nm_spc = switch_nm_spc[ ns_switch ];
	char *master = switch_mas_tbl[ ns_switch ];

	_initialize_logger( logger, kitp->fs, del_buff, master, nm_spc );
	logger->__.next_name = next_name;
}
