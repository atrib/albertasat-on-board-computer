/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file ram_variable.c
 * @author Brendan Bruner
 * @date Oct 21, 2015
 */
#include <dependency_injection.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static bool_t ram_set( non_volatile_variable_t* self_, void* var )
{
	ram_variable_t* self = (ram_variable_t*) self_;
	DEV_ASSERT( self );
	DEV_ASSERT( var );

	uint32_t iter;

	for( iter = 0; iter < self->size; ++iter )
	{
		((uint8_t*) self->var)[iter] = ((uint8_t*) var)[iter];
	}
	return true;
}
static bool_t ram_get( non_volatile_variable_t* self_, void* var )
{
	ram_variable_t* self = (ram_variable_t*) self_;
	DEV_ASSERT( self );
	DEV_ASSERT( var );

	uint32_t iter;

	for( iter = 0; iter < self->size; ++iter )
	{
		((uint8_t*) var)[iter] = ((uint8_t*) self->var)[iter];
	}
	return true;
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
void initialize_ram_variable( ram_variable_t* self, void* var, uint32_t size )
{
	extern void initialize_non_volatile_variable( non_volatile_variable_t* );
	initialize_non_volatile_variable( (non_volatile_variable_t*) self );
	((non_volatile_variable_t*) self)->set = ram_set;
	((non_volatile_variable_t*) self)->get = ram_get;
	self->var = var;
	self->size = size;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


