################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../portable/heap_3.c \
../portable/port.c 

OBJS += \
./portable/heap_3.o \
./portable/port.o 

C_DEPS += \
./portable/heap_3.d \
./portable/port.d 


# Each subdirectory must supply rules for building sources it contributes
portable/%.o: ../portable/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DFREERTOS_LPC_V -I"/home/bbruner/AlbertaSat/Software/Git/OBC/lib3rdParty/FreeRTOS_LPC1769/include" -I"/home/bbruner/AlbertaSat/Software/Git/OBC/lib3rdParty/CMSISv1p30_LPC17xx/inc" -I"/home/bbruner/AlbertaSat/Software/Git/OBC/lib3rdParty/FreeRTOS_LPC1769/portable" -O1 -g3 -Wall -c -fmessage-length=0 -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


