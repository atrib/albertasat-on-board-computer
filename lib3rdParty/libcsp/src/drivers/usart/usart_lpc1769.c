#include <csp/drivers/usart.h>
#include <csp/csp.h>

/* Transmit and receive ring buffers */
STATIC RINGBUFF_T txring, rxring;

/* Transmit and receive ring buffer sizes */
#define UART_SRB_SIZE 256	/* Send */
#define UART_RRB_SIZE 32	/* Receive */

/* Transmit and receive buffers */
static uint8_t rxbuff[UART_RRB_SIZE], txbuff[UART_SRB_SIZE];

// callback function pointer
usart_callback_t usart_callback = NULL;

/**
 * @brief	UART 3 interrupt handler using ring buffers
 * @return	Nothing
 */
void UART3_IRQHandler(void)
{
	int bytes;
	char buf[UART_RRB_SIZE];
	/* Want to handle any errors? Do it here. */

	/* Use default ring buffer handler. Override this with your own
	   code if you need more capability. */
	Chip_UART_IRQRBHandler(LPC_UART3, &rxring, &txring);
	bytes = Chip_UART_ReadRB(LPC_UART3, &rxring, &buf, UART_RRB_SIZE);
	if (bytes > 0) {
		usart_callback(buf, bytes, NULL);
	}
}

/*
 * Set the uart line control register
 */
static void set_uart_lcr(LPC_USART_T *pUART,
		uint8_t databits,
		uint8_t stopbits,
		uint8_t paritysetting,
		uint8_t checkparity)
{
	// Set up line control register with settings for databits, stopbits and parity setting
	uint32_t lcr_config = 0;
	switch (databits){
	case 5:
		lcr_config |= UART_LCR_WLEN5;
		break;
	case 6:
		lcr_config |= UART_LCR_WLEN6;
		break;
	case 7:
		lcr_config |= UART_LCR_WLEN7;
		break;
	case 8:
		lcr_config |= UART_LCR_WLEN8;
		break;
	default:
		lcr_config |= UART_LCR_WLEN8;
		break;
	}

	switch(stopbits){
	case 1:
		lcr_config |= UART_LCR_SBS_1BIT;
		break;
	case 2:
		lcr_config |= UART_LCR_SBS_2BIT;
		break;
	default:
		lcr_config |= UART_LCR_SBS_1BIT;
	}

	if (paritysetting == 1){
		lcr_config |= UART_LCR_PARITY_ODD;
	} else {
		lcr_config |= UART_LCR_PARITY_EVEN;
	}

	if (checkparity == 1){
		lcr_config |= UART_LCR_PARITY_EN;
	} else {
		lcr_config |= UART_LCR_PARITY_DIS;
	}

	Chip_UART_ConfigData(pUART, lcr_config);

}
/**
 * Initialise UART with the usart_conf data structure
 * @param usart_conf full configuration structure
 */
void usart_init(struct usart_conf *conf){
// ignore device, just assume UART3.
//TODO use device to select UART

	Board_UART_Init(LPC_UART3);
	Board_LED_Set(0, false);

	/* Setup UART based on conf */
	Chip_UART_Init(LPC_UART3);
	Chip_UART_SetBaud(LPC_UART3, conf->baudrate);
	// set the line control register
    set_uart_lcr(LPC_UART3, conf->databits, conf->stopbits, conf->paritysetting, conf->checkparity);

    // set up the FIFO control register: enable, and trigger level 2 (at 8 chars)
    // The trigger level is somewhat arbitrary, it can be levels 0 - 3, for 1, 4, 8 and 14 chars
    // I think this is when it triggers the irq
	Chip_UART_SetupFIFOS(LPC_UART3, (UART_FCR_FIFO_EN | UART_FCR_TRG_LEV2));
	// Enable transmit
	Chip_UART_TXEnable(LPC_UART3);

	/* Before using the ring buffers, initialize them using the ring
	   buffer init function */
	RingBuffer_Init(&rxring, rxbuff, 1, UART_RRB_SIZE);
	RingBuffer_Init(&txring, txbuff, 1, UART_SRB_SIZE);

	/* Reset and enable FIFOs, FIFO trigger level 3 (14 chars) */
	// No idea why I am doing this, just copied this code from an example
	Chip_UART_SetupFIFOS(LPC_UART3, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
							UART_FCR_TX_RS | UART_FCR_TRG_LEV3));

	/* Enable receive data and line status interrupt */
	// RBR: receive buffer register, RLS: receive line status
	Chip_UART_IntEnable(LPC_UART3, (UART_IER_RBRINT | UART_IER_RLSINT));

	/* preemption = 1, sub-priority = 1 */
	NVIC_SetPriority(UART3_IRQn, 1);
	NVIC_EnableIRQ(UART3_IRQn);

}

/**
 * In order to catch incoming chars use the callback.
 * Only one callback per interface.
 * @param handle usart[0,1,2,3]
 * @param callback function pointer
 */
//typedef void (*usart_callback_t) (uint8_t *buf, int len, void *pxTaskWoken);
void usart_set_callback(usart_callback_t callback){
    usart_callback = callback;
}

/**
 * Insert a character to the RX buffer of a usart
 * @param handle usart[0,1,2,3]
 * @param c Character to insert
 */
void usart_insert(char c, void *pxTaskWoken){
    // This is a straight copy from libcsp's usart_linux.c
    // Based on the comments above (from usart.h) it doesn't make much sense.
    // The way it is used in the kiss.c example, this is called when
    // KISS_MODE_NOT_STARTED, so to discard characters when not part of a KISS
    // frame.  So more of a discard function.
	printf("%c", c);
}

/**
 * Polling putchar
 *
 * @param handle usart[0,1,2,3]
 * @param c Character to transmit
 */
void usart_putc(char c){
	if (Chip_UART_SendRB(LPC_UART3, &txring, &c, 1) != 1) {
		Board_LED_Toggle(0);/* Toggle LED if the TX FIFO is full */
	}
}

/**
 * Send char buffer on UART
 *
 * @param handle usart[0,1,2,3]
 * @param buf Pointer to data
 * @param len Length of data
 */
void usart_putstr(char *buf, int len){
	if (Chip_UART_SendRB(LPC_UART3, &txring, buf, len) != 1) {
		Board_LED_Toggle(0);/* Toggle LED if the TX FIFO is full */
	}
}

/**
 * Buffered getchar
 *
 * @param handle usart[0,1,2,3]
 * @return Character received
 */
char usart_getc(void){
	int bytes;
	char c;
	bytes = Chip_UART_ReadRB(LPC_UART3, &rxring, &c, 1);
	if (bytes == 0){
		return 0;
	}
	return c;
}

// I think this is suppose to return 1 if there is something in the receive buffer.
// But I am not sure it is used.
int usart_messages_waiting(int handle){
	printf('usart_messages_waiting called');
	return 0;
}
