/* FreeRTOS.org includes. */
#include "FreeRTOS.h"
#include "task.h"

#include <csp/i2c.h>
#include <csp/csp.h>


csp_packet_t * packet;

/*********************************************************************//**
 * @brief		Interrupt Service Routine for Master Peripheral
 * 				Assigns Handle to interrupt handler and sets flag
 * 				upon completion
 * @param[in]	None
 * @return		None
 **********************************************************************/
/*
void I2C1_IRQHandler() {
	I2C_MasterHandler(I2C_MASTER);
}
*/

int i2c_init(int handle, int mode, uint8_t addr, uint16_t speed, int queue_len_tx, int queue_len_rx, i2c_callback_t callback)
{
	PINSEL_CFG_Type PinCfg;
	PinCfg.Portnum   = 0;
	PinCfg.Pinnum    = 0;
	PinCfg.Funcnum   = PINSEL_FUNC_3;
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Pinmode   = PINSEL_PINMODE_PULLUP;
	PINSEL_ConfigPin ( &PinCfg );
	PinCfg.Pinnum    = 1;
	PINSEL_ConfigPin ( &PinCfg );

	I2C_Init (I2C_MASTER, I2C_CLK_RATE );
	//NVIC_DisableIRQ(I2C1_IRQn);
	//NVIC_SetPriority(I2C1_IRQn, ((0x00<<3)|0x01));
	I2C_Cmd  (I2C_MASTER, ENABLE );

	//i2c_receive();
	return E_NO_ERR;

}
int i2c_send(int handle, i2c_frame_t * frame, uint16_t timeout)
{
	I2C_M_SETUP_Type MasterConfig;

	/* Start I2C master device*/
	MasterConfig.sl_addr7bit = I2C_SLAVE_ADDR;
	MasterConfig.tx_data = frame -> data;
	MasterConfig.tx_length = frame -> len;
	MasterConfig.rx_data = NULL;
	MasterConfig.rx_length = 0;
	MasterConfig.retransmissions_max = 0;
	I2C_MasterTransferData(I2C_MASTER, &MasterConfig, I2C_TRANSFER_POLLING);

	while(I2C_MasterTransferComplete(I2C_MASTER));
	return E_NO_ERR;
}
