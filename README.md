# License
---

The license used to develop this software is the GNU General Public License, v3. We are in the process of changing our license from GNU Genral Public License, v2 to v3. Any soure files written by us which contain a license for v2 are to be ignored and v3 is to be used instead. Any source files written by us containing a non GNU license are not having their license changed.

# AlbertaSat
---

## Required Imports for Development ##

There are four static libraries and two execution projects that need to be imported

* lib3rdParty/FreeRTOS_LPCOpen - The operating system
* liba/Core - The core software of the satellite
* liba/Subsystem - Soon to be deprecated, but still has some required software
* liba/Testing - All unit test code
* main/OBCMain - Executable for main code
* main/OBCTest - Executable for testing code

In addition, there are two more local static libraries needed. 

1. Open the quick start panel. This can be done by going to ```Window -> Show View -> Other..``` Then select the ```Quick Start``` Folder. In that folder is the ```Quickstart Panel```. Open this up.
2. In the quick start panel, click ```Import project(s)```.
3. Click the ```Browse``` button beside the heading ```Project archive (zip)```.
4. Browse to ```LPCOpen->lpcopen_2_10_lpcxpresso_nxp_lpcxpresso_1769.zip]``` and select it.
5. Click ```Next >```
6. Select **only** the following projects:
    * ```lpc_board_nxp_lpcxpresso_1769```
    * ```lpc_chip_175x_6x```
7 Click ```Finish```

All include paths, library paths, and linker settings have been configured. Make sure you clean all the projects before the first build.

##Hardware Configuration##

To run the code you will need, at a minimum, an SD card holder, an SD card formatted as a FAT32 partition, and a UART to USB device (such as an arduino or dedicated chip). Following is a description of the pin out. Pin names reference the "Pin Location" field of this [datasheet](https://bytebucket.org/bbruner0/albertasat-on-board-computer/wiki/1. Resources/1.1. DataSheets/LPC17xx/simple_pinout.pdf).

| Pin location | Description |
| :----------- | :---------- |
| H[1] | Ground |
| H[5] | To SD card, MOSI |
| H[6] | To SD card, MISO |
| H[7] | To SD card, SCK |
| H[11] | Optional, GPIO, pulled high when EPS at low power, low otherwise |
| H[12] | Optional, GPIO, pulled high when EPS at optimal power, low otherwise |
| H[13] | Optional, GPIO, pulled high when EPS at normal power, low otherwise |
| H[14] | To SD card, CS |
| H[15] | Optional, GPIO, simulated power line to COMM |
| H[16] | Optional, GPIO, simulated power line to Nanohub |
| H[17] | Optional, GPIO, simulated power line to DFGM |
| H[18] | Optional, GPIO, simulated power line to MNLP |
| H[19] | Optional, GPIO, simulated power line to Teledyne |
| H[20] | Optional, GPIO, pulled high when DFGM Boom is deployed, low otherwise |
| H[21] | Optional, GPIO, pulled high when antennas are deployed, low otherwise |
| H[27] | Optional, GPIO, ismulated power line to ADCS |
| H[40] | UART Tx |
| H[41] | UART Rx |

##Eclipse Build Configurations##
There are at least two build configurations to each project, some have three, and legacy projects will have a build configuration which targets linux.
 
* Debug: Debug build targetting LPC17XX board.
* Release: Release build targetting an LPC17XX board. Note, board must have power reset before the program starts running.
* Demo: A debug build which includes extra code for demonstrating proof of concepts.

##Running The Code##

* Build the OBCMain project and depedent libraries in master. This is the code which will be flashed to the LPC17XX. 
* To capture UART output, go to the directory /main/Console. Run the script "script.sh" by typing "./script.sh" in a terminal.
    * You may have to install pyserial. Do this by typing ```sudo apt-get install python-serial``` in the terminal.
    * You will be prompted for the serial port. If using an arduino the default may work. If using a dedicated uart-usb chip, the serial port may be /dev/ttyUSB0
    * Once a connection is made, the terminal will echo all incoming characters.

## Contribution guidelines ##

* See section 4. of the [wiki](https://bitbucket.org/bbruner0/albertasat-on-board-computer/wiki/Home) for coding standards.
* Use the LPCxpresso IDE when writing code. 
* All code must have full unit test coverage.
* Create a feature branch when you are working on code, do not work directly off of master. This goes without saying, but first time users of version control may not know. Include your name and your feature in the branch name. For example, make a branch called john-filter if your name is john and you're working on some sort of filter code. See [https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow]
* When your branch is ready to be merged into master, make a pull request.
* If you are not sure, ask. Brendan, Stefan, and Collin will help make sure your code is included in the repo and can address most first time user Eclipse bugs.

## Repository Structure ##
The structure of the repo is as follows. Currently, the majority of work is done within liba.

* **lib3rdParty**
    * Contains 3rd party libraries to support our development like FreeRTOS, csp, and LPC board files
* **liba**
    * Contains AlbertaSat's static libraries under development

* ** core **
    * For On-board computer (OBC) **production** code  NOT specific for each target architecture. Finished code from liba will be moved to core.
    * This should only be committed to master on approval - make a pull request to Brendan when your feature branch is ready (fully unit tested!)
    * Code in this folder is considered "flight code" and should be, in every way possible, fit to run the satellite.
* **ports**
    * Same as core, except the code targets specific hardware. 
* **prototypes**
    * This is where we develop code and work on features
    * Code here may or may not be used in the final product, this area is for testing and experimenting
    * Feel free to make a new folder and begin developing here.

## How do I get set up? ##

* Add your name to the contributors.txt file in this repo
* Check out an LPCexpresso board or buy your own (LPC1769 is the board we're using)
* Download LPCexpresso eclipse
* The repo is primarily set up for developing on Linux, so expect that environment to work best. For example, the FreeRTOS posix simulator does not work on OSX (confirmed) and the python ground station script was written using linux libraries.