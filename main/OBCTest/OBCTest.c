/*
===============================================================================
 Name        : OBCTest.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "board.h"
#include <cr_section_macros.h>
#include "unit.h"
#include <stdio.h>
#include "test_suites.h"
#include "portable_types.h"
#include <printing.h>
#include <core_cm3.h>

//#define RUN_TEST_GROUP_1
//#define RUN_TEST_GROUP_2
#define RUN_TEST_GROUP_3


#ifdef RUN_TEST_GROUP_1
#define RUN_ALLOCATOR_TEST
#define RUN_FILESYSTEM_API_TEST
#define RUN_DATA_TEST
#endif

#ifdef RUN_TEST_GROUP_2
#define RUN_STATE_TEST
//#define RUN_PACKET_TEST
//#define RUN_SUBSYSTEM_TEST
#endif

#ifdef RUN_TEST_GROUP_3
//#define RUN_COMMAND_TEST
//#define RUN_TIMER_TEST
#define RUN_INTERPRETER_TEST
//#define RUN_DAEMON_TEST
//#define RUN_NV_TEST
#endif

static void test_task( void * );
static void init_testing( void* );

int main(void)
{
	SystemCoreClockUpdate();
	create_task( init_testing, "test init", 400/*TEST_TSK_STACK*/, NO_PARAMETERS, BASE_PRIORITY+1, NO_HANDLE);
	vTaskStartScheduler();
	return 0;
}

static void init_testing( void* param )
{
	INIT_TEST_SUITE_DRIVER_TOOLKIT( );
	create_task( test_task, "test task", 1800/*TEST_TSK_STACK*/, NO_PARAMETERS, BASE_PRIORITY+1, NO_HANDLE);
	delete_task( NULL );
}

static void test_task( void *param )
{
	INIT_TESTING( );
#ifdef RUN_DAEMON_TEST
	RUN_TEST_SUITE( script_daemon );
#endif
#ifdef RUN_DATA_TEST
	RUN_TEST_SUITE( telemetry_priority );
	RUN_TEST_SUITE( logger );
#endif
#ifdef RUN_STATE_TEST
	RUN_TEST_SUITE( system_state_relay );
	RUN_TEST_SUITE( next_state );
	RUN_TEST_SUITE( system_states );
#endif
#ifdef RUN_SUBSYSTEM_TEST
	RUN_TEST_SUITE( eps );
#endif
#ifdef RUN_INTERPRETER_TEST
	RUN_TEST_SUITE( interpreter );
#endif
#ifdef RUN_COMMAND_TEST
	RUN_TEST_SUITE( command_api );
	RUN_TEST_SUITE( command_beacon_wod );
	RUN_TEST_SUITE( command_log_wod );
//	RUN_TEST_SUITE( command_downlink_confirm_telemetry );
	RUN_TEST_SUITE( command_set_state );
	RUN_TEST_SUITE( command_locks );
#endif
#ifdef RUN_ALLOCATOR_TEST
	RUN_TEST_SUITE( allocator );
#endif
#ifdef RUN_FILESYSTEM_API_TEST
	RUN_TEST_SUITE( file_handle_api );
	RUN_TEST_SUITE( filesystem_api );
#endif
#ifdef RUN_TIMER_TEST
	RUN_TEST_SUITE( rtc_api );
#endif
#ifdef RUN_NV_TEST
	RUN_TEST_SUITE( non_volatile_variable );
	RUN_TEST_SUITE( persistent_timer );
#endif
#ifdef RUN_PACKET_TEST
	RUN_TEST_SUITE( packet_structure );
#endif
	PRINT_DIAG( );
	for( ;; )
	{
		task_delay( ONE_SECOND );
	}
}
