#include <portable_types.h>

#include <states/state_relay.h>
#include <driver_toolkit/driver_toolkit_lpc.h>

#include <parser/parser.h>
#include <script_daemon.h>

#include <telecommands/telecommand_prototype_manager.h>
#include <telecommands/telemetry_alteration/telecommand_log_wod.h>
#include <telecommands/self_alteration/telecommand_blink.h>
#include <telecommands/subsystem_alteration/telecommand_deploy_boom.h>

#include <ftp.h>

#include <gpio/gpio_lpc.h>
#include <printing.h>

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static driver_toolkit_t* 				kit;
static driver_toolkit_lpc_t* 			lpc_kit;
static state_relay_t* 					relay;
static telecommand_log_wod_t			log_wod_command;
static script_daemon_t 					daemon;
static telecommand_deploy_boom_t		deploy_command;
static telecommand_blink_t				blink_command;
static ftp_t 							ftp;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static void wod_demo( void* );
static void script_demo( );
static void ftp_demo( );
static void state_demo( );
static void vLEDTask1( void* );

static void demo_task( void* inputs )
{
	bool_t err_kit;
	bool_t err_relay;
	(void) inputs;

	err_kit = initialize_driver_toolkit_lpc( lpc_kit );
	if( !err_kit )
	{
		DEBUG_PRINTF( "Failed to create kit\n" );
		create_task(vLEDTask1, (signed char *) "vTaskLed1",
					128, NULL, (tskIDLE_PRIORITY + 1UL),
					(xTaskHandle *) NULL);
		suspend_task( NULL );
	}
	kit = (driver_toolkit_t*) lpc_kit;

	kit->fs->delete( kit->fs, "leopf.txt" );
	kit->fs->delete( kit->fs, "bolog.txt" );
	kit->fs->delete( kit->fs, "dtlog.txt" );
	err_relay = initialize_state_relay( relay, kit,
										"leopf.txt", 5, "bolog.txt",
										15, "dtlog.txt" );
	if( err_relay != STATE_RELAY_SUCCESS )
	{
		DEBUG_PRINTF( "Failed to create relay\n" );
		create_task(vLEDTask1, (signed char *) "vTaskLed1",
					128, NULL, (tskIDLE_PRIORITY + 1UL),
					(xTaskHandle *) NULL);
		suspend_task( NULL );
	}

	script_demo( );
	ftp_demo( );

	create_task( wod_demo, (signed char*) "WOD Task",
				 700, NULL, (tskIDLE_PRIORITY + 1),
				 NULL );

	state_demo( );

	suspend_task( NULL );
	for( ;; );
}


/*****************************************************************************
 * Public functions
 ****************************************************************************/
static const PINMUX_GRP_T gpio0_mux[] = {
	{2,  13,   IOCON_MODE_INACT | IOCON_FUNC0},	/* gpio */
};

int main(void)
{
	SystemCoreClockUpdate();

	lpc_kit = (driver_toolkit_lpc_t*) malloc( sizeof(driver_toolkit_lpc_t) );
	relay = (state_relay_t*) malloc( sizeof(state_relay_t) );

	if( lpc_kit != NULL && relay != NULL )
	{
		create_task( demo_task, (signed char*) "Demo Task",
					 400, NULL, (tskIDLE_PRIORITY + 1),
					 NULL );
	}
	else
	{
		/* LED1 toggle thread */
		create_task(vLEDTask1, (signed char *) "vTaskLed1",
					128, NULL, (tskIDLE_PRIORITY + 1UL),
					(xTaskHandle *) NULL);
	}

	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}

static void ftp_demo( )
{
	uint8_t err;
	err = initialize_ftp_service( &ftp, kit );
	if( err != FTP_SUCCESS )
	{
		DEBUG_PRINTF( "Failed to create kit\n" );
		create_task(vLEDTask1, (signed char *) "vTaskLed1",
					128, NULL, (tskIDLE_PRIORITY + 1UL),
					(xTaskHandle *) NULL);
		suspend_task( NULL );
	}
	start_ftp_service( &ftp );
}

static void state_demo( )
{
	for( ;; )
	{
		relay_next_state( relay );
		task_delay( 1000 );
	}
}

static void script_demo( )
{
	bool_t								err;
	telecommand_prototype_manager_t*	prototypes;

	/* Get the prototype manager. */
	prototypes = get_telecommand_prototype_manager( );

	/* REGISTER COMMANDS */
	err = initialize_telecommand_blink( &blink_command, kit );
	if( !err )
	{
		DEBUG_PRINTF( "Failed to create kit\n" );
		create_task(vLEDTask1, (signed char *) "vTaskLed1",
					128, NULL, (tskIDLE_PRIORITY + 1UL),
					(xTaskHandle *) NULL);
		suspend_task( NULL );
	}
	err = initialize_telecommand_deploy_boom( &deploy_command, kit );
	if( !err )
	{
		DEBUG_PRINTF( "Failed to create kit\n" );
		create_task(vLEDTask1, (signed char *) "vTaskLed1",
					128, NULL, (tskIDLE_PRIORITY + 1UL),
					(xTaskHandle *) NULL);
		suspend_task( NULL );
	}
	err = prototypes->register_prototype( prototypes, "blink", (telecommand_t*) &blink_command );
	if( !err )
	{
		DEBUG_PRINTF( "Failed to create kit\n" );
		create_task(vLEDTask1, (signed char *) "vTaskLed1",
					128, NULL, (tskIDLE_PRIORITY + 1UL),
					(xTaskHandle *) NULL);
		suspend_task( NULL );
	}
	err = prototypes->register_prototype( prototypes, "deploy", (telecommand_t*) &deploy_command );
	if( !err )
	{
		DEBUG_PRINTF( "Failed to create kit\n" );
		create_task(vLEDTask1, (signed char *) "vTaskLed1",
					128, NULL, (tskIDLE_PRIORITY + 1UL),
					(xTaskHandle *) NULL);
		suspend_task( NULL );
	}

	err = initialize_script_daemon( &daemon, kit->gs );
	if( !err )
	{
		DEBUG_PRINTF( "Failed to create kit\n" );
		create_task(vLEDTask1, (signed char *) "vTaskLed1",
					128, NULL, (tskIDLE_PRIORITY + 1UL),
					(xTaskHandle *) NULL);
		suspend_task( NULL );
	}
}

static void wod_demo( void* args )
{
	uint8_t err_command;
	(void) args;

	err_command = initialize_telecommand_log_wod( &log_wod_command, relay );
	if( err_command != TELECOMMAND_SUCCESS )
	{
		DEBUG_PRINTF( "Failed to create telecommand\n" );
		create_task(vLEDTask1, (signed char *) "vTaskLed1",
					128, NULL, (tskIDLE_PRIORITY + 1UL),
					(xTaskHandle *) NULL);
		suspend_task( NULL );
	}

	for( ;; )
	{
		relay_command_collect_housekeeping( relay, (telecommand_t*) &log_wod_command );
		task_delay( 60 * ONE_SECOND );
	}
}

/* LED1 toggle thread */
static void vLEDTask1(void *pvParameters) {
	bool LedState = false;

	while (1) {
		Chip_GPIO_WritePortBit( LPC_GPIO, 2, 13, LedState );

		LedState = (bool) !LedState;
		/* About a 3s on/off toggle rate */
		vTaskDelay(configTICK_RATE_HZ * 3);
	}
}
