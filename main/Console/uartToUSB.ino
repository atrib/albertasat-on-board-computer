

void setup()
{
  Serial.begin(115200);
  Serial1.begin(115200);
}

void loop( )
{
  // If data is on uart1...
  if ( Serial1.available( ) > 0 ) 
  {
    // write it to uart0 (usb to pc)...
    Serial.write( Serial1.read( ) );  
  }
  // If data is on uart0...
  if ( Serial.available( ) > 0 )
  {
    Serial1.write( Serial.read( ) );
  }
}
