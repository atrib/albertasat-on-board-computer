 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #

import readline
from SerialInterface import SerialInterface
import binascii
import sys
import time

# Start up message
CONSOLE_START_MESSAGE = "\nAlbertaSat console. ( version 2.0 )\n" \
                               "copyright (C) 2015 Brendan Bruner. GNU GPL V2\n"\
                               "type 'help' to recieve help\n"
CONSOLE_PROMPT = "port$ "

# Messages that accompany the commands above
CONSOLE_FATAL = "console: fatal error: " 
CONSOLE_NO_SERIAL_PORT = "No serial port found. Try again."

# Constants used when initializing serial interface
CONSOLE_BAUD = 115200
CONSOLE_DEFAULT_PORT = "/dev/ttyACM0"

class ConsoleShell:

    def __init__( self ):
        self._usbInterface = SerialInterface( )
        
        # Run the set
        self._setUp( )


    def _setUp( self ):        
        print CONSOLE_START_MESSAGE 
        
        # Set port and baud in self._usbInterface
        port = raw_input( "Please enter serial port. Default is " + CONSOLE_DEFAULT_PORT + "\n" \
                          "if nothing is entered.\n" + CONSOLE_PROMPT )
        while 1:
            if( port == '' ):
                self._usbInterface.initSerial( CONSOLE_DEFAULT_PORT, CONSOLE_BAUD )
            else:
                self._usbInterface.initSerial( port, CONSOLE_BAUD )

            if self._usbInterface.portOpen( ):
                break
            print CONSOLE_FATAL + CONSOLE_NO_SERIAL_PORT 
            port = raw_input( CONSOLE_PROMPT )
        print "port connected" 

    def run( self ):
        while True:
            if self._usbInterface.inWaiting( ) > 0:
                rawRead = self._usbInterface.read( self._usbInterface.inWaiting( ) )
                sys.stdout.write( binascii.unhexlify( binascii.hexlify( rawRead ) ) )
#            time.sleep(0.1)
            


