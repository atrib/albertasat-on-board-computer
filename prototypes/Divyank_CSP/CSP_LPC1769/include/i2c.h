/* Includes */
#include "LPC17xx.h"
#include "lpc_types.h"
#include "lpc17xx_i2c.h"
#include "lpc17xx_clkpwr.h"
#include "lpc17xx_pinsel.h"



/*I2C constants */
#define I2C_SLAVE     (LPC_I2C1)
#define I2C_MASTER	  (LPC_I2C2)
#define I2C_CLK_RATE   (100000)
#define I2C_SLAVE_ADDR (0x04)
#define BUFFER_SIZE    (16)

void I2C1_IRQHandler(void);
void I2C2_IRQHandler(void);
void buffer_init();
void setupI2C();
void I2C_test(void *pvParameters);
void I2C_rx(void *pvParameters);
