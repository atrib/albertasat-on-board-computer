/* FreeRTOS.org includes. */
#include "FreeRTOS.h"
#include "task.h"
#include <stdio.h>
#include "include/i2c.h"

/* Global variables to keep track of transfers */
__IO FlagStatus complete_S;
__IO FlagStatus complete_M;

/* Transmit and receive buffers for Master and Slave */
static uint8_t Slave_Buf[BUFFER_SIZE];
static uint8_t Master_Buf[BUFFER_SIZE];
/*********************************************************************//**
 * @brief		Interrupt Service Routine for Slave Peripheral
 * 				Assigns Handle to interrupt handler and sets flag
 * 				upon completion
 * @param[in]	None
 * @return		None
 **********************************************************************/
void I2C1_IRQHandler() {
	I2C_SlaveHandler(I2C_SLAVE);
}

void I2C2_IRQHandler() {
	I2C_SlaveHandler(I2C_MASTER);
	if(I2C_MasterTransferComplete(I2C_MASTER)) {
		complete_S = SET;
	}
}

/*********************************************************************//**
 * @brief		I2C initialization function
 * @param[in]	None
 * @return		None
 **********************************************************************/
void setupI2C ()
{
	I2C_OWNSLAVEADDR_CFG_Type SlaveAddrConfig;
	PINSEL_CFG_Type PinCfg;
	int i;
	for( i = 0; i < BUFFER_SIZE; ++i )
	{
		Slave_Buf[i] = i;
	}

	/* Init I2C pins */
	PinCfg.Portnum   = 0;
	PinCfg.Pinnum    = 0;
	PinCfg.Funcnum   = PINSEL_FUNC_3;
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Pinmode   = PINSEL_PINMODE_PULLUP;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum    = 1;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Funcnum   = PINSEL_FUNC_3;
	PinCfg.Pinnum 	 = 10;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum 	 = 11;
	PINSEL_ConfigPin(&PinCfg);

	/* Initialize Master and Slave I2C peripherals */
	I2C_Init(I2C_SLAVE, I2C_CLK_RATE);

	SlaveAddrConfig.GeneralCallState = DISABLE;
	SlaveAddrConfig.SlaveAddrChannel = 0;
	SlaveAddrConfig.SlaveAddrMaskValue = 0xFF;
	SlaveAddrConfig.SlaveAddr_7bit = I2C_SLAVE_ADDR;
	I2C_SetOwnSlaveAddr(I2C_SLAVE, &SlaveAddrConfig);

	/* Set up interrupts
	 * Note: Master has higher priority
	 */
	NVIC_DisableIRQ(I2C1_IRQn);
	NVIC_SetPriority(I2C1_IRQn, ((0x00<<3)|0x01));

	/*Enable Master and Slave I2C peripherals */
	I2C_Cmd(I2C_SLAVE, ENABLE);

}

static void SlaveCallback( void )
{
	complete_S = SET;
}

/*********************************************************************//**
 * @brief		Task to test Master-Slave Communication
 * @param[in]	None
 * @return		None
 **********************************************************************/
void I2C_test ( void *pvParameters )
{
	I2C_S_SETUP_Type SlaveConfig;
	uint32_t i;
	char buf;

	/* this loop prevents the task from ending */
	for (i = 0 ;i < BUFFER_SIZE ; i++) {
		/* MASTER SEND DATA TO SLAVE -------------------------------------------------------- */
		/* Force complete flag for the first time of running */
		complete_S =  RESET;

		printf(" slave...\n");
		SlaveConfig.tx_data = NULL;
		SlaveConfig.tx_length = 0;
		SlaveConfig.rx_data = Slave_Buf;
		SlaveConfig.rx_length = 6;
		SlaveConfig.callback = SlaveCallback;
		I2C_SlaveTransferData(I2C_SLAVE, &SlaveConfig, I2C_TRANSFER_INTERRUPT);
		/* Wait until complete */
		while ((complete_S == RESET));
		printf("Received: %s", (char *) Slave_Buf);

		vTaskDelay ( 1000 / portTICK_RATE_MS );
	}
}



