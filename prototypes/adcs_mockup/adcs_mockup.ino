/*
 * Copyright (C) 2015  Brendan Bruner, Oleg Oleynikov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file adcs_lpc_local
 * @author Brendan Bruner
 * @author Oleg Oleynikov
 * @date Mar 04, 2015
 */

// Wire Slave Receiver
// by Nicholas Zambetti <http://www.zambetti.com>

// Demonstrates use of the Wire library
// Receives data as an I2C/TWI slave device
// Refer to the "Wire Master Writer" example for use with this

// Created 29 March 2006

// This example code is in the public domain.

#include "adcs.h"
extern "C" {
#include "gps_module.h"
}
#include <stdlib.h>
#include <string.h>
#include <Wire.h>
#define MODE_CYCLE_SIZE 33
/*static adcs_mode_t mode_cycles[] =	{
		ATTITUDE_CONTROL_NO_CONTROL, ATTITUDE_CONTROL_NO_CONTROL, ATTITUDE_CONTROL_NO_CONTROL,
		ATTITUDE_CONTROL_NO_CONTROL, ATTITUDE_CONTROL_DETUMBLING_CONTROL, ATTITUDE_CONTROL_DETUMBLING_CONTROL,
		ATTITUDE_CONTROL_DETUMBLING_CONTROL, ATTITUDE_CONTROL_DETUMBLING_CONTROL,
		ATTITUDE_CONTROL_INIT_PITCH, ATTITUDE_CONTROL_STEADY_STATE, ATTITUDE_CONTROL_STEADY_STATE,
		ATTITUDE_CONTROL_STEADY_STATE, ATTITUDE_CONTROL_STEADY_STATE, ATTITUDE_CONTROL_STEADY_STATE,
		ATTITUDE_CONTROL_NO_CONTROL, ATTITUDE_CONTROL_NO_CONTROL, ATTITUDE_CONTROL_DETUMBLING_CONTROL,
		ATTITUDE_CONTROL_DETUMBLING_CONTROL, ATTITUDE_CONTROL_DETUMBLING_CONTROL, ATTITUDE_CONTROL_DETUMBLING_CONTROL,
		ATTITUDE_CONTROL_DETUMBLING_CONTROL, ATTITUDE_CONTROL_INIT_PITCH, ATTITUDE_CONTROL_STEADY_STATE,
		ATTITUDE_CONTROL_STEADY_STATE, ATTITUDE_CONTROL_STEADY_STATE, ATTITUDE_CONTROL_STEADY_STATE, ATTITUDE_CONTROL_STEADY_STATE,
		ATTITUDE_CONTROL_STEADY_STATE, ATTITUDE_CONTROL_DETUMBLING_CONTROL, ATTITUDE_CONTROL_DETUMBLING_CONTROL,
		ATTITUDE_CONTROL_INIT_PITCH, ATTITUDE_CONTROL_STEADY_STATE, ATTITUDE_CONTROL_STEADY_STATE
};*/

static adcs_mode_t mode_cycles[] =	{
  ADCS_MODE_IDLE, ADCS_MODE_IDLE, ADCS_MODE_IDLE,
  ADCS_MODE_IDLE, ADCS_MODE_DETUMBLE, ADCS_MODE_DETUMBLE,
  ADCS_MODE_DETUMBLE, ADCS_MODE_DETUMBLE, ADCS_MODE_DETUMBLE,
  ADCS_MODE_STABILIZED, ADCS_MODE_STABILIZED, ADCS_MODE_STABILIZED,
  ADCS_MODE_STABILIZED, ADCS_MODE_STABILIZED, ADCS_MODE_STABILIZED,
  ADCS_MODE_STABILIZED, ADCS_MODE_STABILIZED, ADCS_MODE_STABILIZED,
  ADCS_MODE_STABILIZED, ADCS_MODE_DETUMBLE, ADCS_MODE_DETUMBLE,
  ADCS_MODE_DETUMBLE, ADCS_MODE_DETUMBLE, ADCS_MODE_DETUMBLE,
  ADCS_MODE_STABILIZED, ADCS_MODE_STABILIZED, ADCS_MODE_STABILIZED,
  ADCS_MODE_STABILIZED, ADCS_MODE_STABILIZED, ADCS_MODE_DETUMBLE,
  ADCS_MODE_DETUMBLE, ADCS_MODE_DETUMBLE, ADCS_MODE_DETUMBLE
};

/* Arduino pinread values */
int buttonState1 = 0;
int buttonState2 = 0;
int buttonState3 = 0;
int buttonState4 = 0;

int rheostatValue1 = 0;
int rheostatValue2 = 0;

int joyH = 0;
int joyV = 0;
int joyButton = 0;

int photoResValue1 = 0;
int photoResValue2 = 0;


/************************************************************************/
/* Virtual method implementations										*/
/************************************************************************/

int tlm_size;

int time(int param)
{
  return millis();
}

static void* adcs_handle_command_id(adcs_t *self, uint8_t command_id, void *data)
{
  // all telecommand ids are from 0 to 127
  // all telemetry request ids are from 128 to 255
  // therefore, to check for the type of command, shift command id right by 7 bits
  int command_type = command_id >> 7;
  if (command_type == 0)
  {
    // this is a telecommand
    // so we need to supply the input data to the relevant function
    // but there is nothing to return
    switch (command_id)
    {
      case 1:
        self->telecommands_received++;
        self->adcs_reset(self, (adcs_reset_t *) data);
        self->last_telecommand_id = 1;
        break;
      case 2:
        self->telecommands_received++;
        self->adcs_set_unix_time(self, (adcs_unix_time_t *) data);
        self->last_telecommand_id = 2;
        break;
      case 3:
        self->telecommands_received++;
        self->adcs_set_run_mode(self, (adcs_run_mode_t *) data);
        self->last_telecommand_id = 3;
        break;
      case 4:
        self->telecommands_received++;
        self->adcs_select_logged_data(self, (adcs_logged_data_t *) data);
        self->last_telecommand_id = 4;
        break;
      case 5:
        self->telecommands_received++;
        self->adcs_select_logged_data(self, (adcs_logged_data_t *) data);
        self->last_telecommand_id = 5;
        break;
      case 6:
        self->telecommands_received++;
        self->adcs_deploy_magnetometer_boom(self, (adcs_deploy_magnetometer_boom_timeout_t *) data);
        self->last_telecommand_id = 6;
        break;
      case 7:
        self->telecommands_received++;
        self->adcs_trigger_adcs_loop(self);
        self->last_telecommand_id = 7;
        break;
      case 17:
        self->telecommands_received++;
        self->adcs_set_attitude_estimation_mode(self, (adcs_attitude_estimation_mode_t *) data);
        self->last_telecommand_id = 17;
        break;
      case 18:
        self->telecommands_received++;
        self->adcs_set_attitude_control_mode(self, (adcs_attitude_control_mode_t *) data);
        self->last_telecommand_id = 18;
        break;
      case 19:
        self->telecommands_received++;
        self->adcs_set_commanded_attitude_angles(self, (adcs_commanded_attitude_angles_t *) data);
        self->last_telecommand_id = 19;
        break;
      case 32:
        self->telecommands_received++;
        self->adcs_set_wheel_speed(self, (adcs_wheel_speed_t *) data);
        self->last_telecommand_id = 32;
        break;
      case 33:
        self->telecommands_received++;
        self->adcs_set_magnetorquer_output(self, (adcs_magnetorquer_output_t *) data);
        self->last_telecommand_id = 33;
        break;
      case 102:
        self->telecommands_received++;
        self->adcs_set_start_up_mode(self, (adcs_start_up_mode_t *) data);
        self->last_telecommand_id = 102;
        break;
      case 64:
        self->telecommands_received++;
        self->adcs_set_sgp4_orbit_parameters(self, (adcs_sgp4_orbit_parameters_t *) data);
        self->last_telecommand_id = 64;
        break;
      case 80:
        self->telecommands_received++;
        self->adcs_set_curr_config(self, (adcs_curr_config_t *) data);
        self->last_telecommand_id = 80;
        break;
      case 81:
        self->telecommands_received++;
        self->adcs_set_magnetorquer_config(self, (adcs_magnetorquer_config_t *) data);
        self->last_telecommand_id = 81;
        break;
      case 82:
        self->telecommands_received++;
        self->adcs_set_wheel_config(self, (adcs_wheel_config_t *) data);
        self->last_telecommand_id = 82;
        break;
      case 83:
        self->telecommands_received++;
        self->adcs_set_css_config(self, (adcs_css_config_t *) data);
        self->last_telecommand_id = 83;
        break;
      case 84:
        self->telecommands_received++;
        self->adcs_set_sun_sensor_config(self, (adcs_sun_sensor_config_t *) data);
        self->last_telecommand_id = 84;
        break;
      case 85:
        self->telecommands_received++;
        self->adcs_set_nadir_sensor_config(self, (adcs_nadir_sensor_config_t *) data);
        self->last_telecommand_id = 85;
        break;
      case 86:
        self->telecommands_received++;
        self->adcs_set_magnetometer_config(self, (adcs_magnetometer_config_t *) data);
        self->last_telecommand_id = 86;
        break;
      case 87:
        self->telecommands_received++;
        self->adcs_set_rate_sensor_config(self, (adcs_rate_sensor_config_t *) data);
        self->last_telecommand_id = 87;
        break;
      case 88:
        self->telecommands_received++;
        self->adcs_set_detumbling_control(self, (adcs_detumbling_control_t *) data);
        self->last_telecommand_id = 88;
        break;
      case 89:
        self->telecommands_received++;
        self->adcs_set_Y_momentum_control(self, (adcs_Y_momentum_control_t *) data);
        self->last_telecommand_id = 89;
        break;
      case 90:
        self->telecommands_received++;
        self->adcs_set_moment_of_inertia(self, (adcs_moment_of_inertia_t *) data);
        self->last_telecommand_id = 90;
        break;
      case 91:
        self->telecommands_received++;
        self->adcs_set_estimation_parameters(self, (adcs_estimation_parameters_t *) data);
        self->last_telecommand_id = 91;
        break;
      case 100:
        self->telecommands_received++;
        self->adcs_save_config(self);
        self->last_telecommand_id = 100;
        break;
      case 101:
        self->telecommands_received++;
        self->adcs_save_orbit_params(self);
        self->last_telecommand_id = 101;
        break;
      case 110:
        self->telecommands_received++;
        self->adcs_capture_and_save_image(self, (adcs_capture_and_save_image_t *) data);
        self->last_telecommand_id = 110;
        break;
      case 114:
        self->telecommands_received++;
        self->adcs_reset_file_list(self);
        self->last_telecommand_id = 114;
        break;
      case 115:
        self->telecommands_received++;
        self->adcs_advance_file_list_index(self);
        self->last_telecommand_id = 115;
        break;
      case 116:
        self->telecommands_received++;
        //char* filename = (char*) data;
        //int length = strlen(filename);
        self->adcs_init_download(self, (char *) data, strlen((char*) data));//(self, filename, length);
        self->last_telecommand_id = 116;
        break;
      case 117:
        self->telecommands_received++;
        self->adcs_advance_file_read_pointer(self, (adcs_next_block_num_to_read_t *) data);
        self->last_telecommand_id = 117;
        break;
      case 118:
        self->telecommands_received++;
        //char* filename = (char*) data;
        //int length = strlen(filename);
        self->adcs_erase_file(self, (char *) data, strlen((char*) data));//(self, filename, length);
        self->last_telecommand_id = 118;
        break;
      case 119:
        self->telecommands_received++;
        self->adcs_erase_all_files(self);
        self->last_telecommand_id = 119;
        break;
      case 120:
        self->telecommands_received++;
        self->adcs_set_boot_index(self, (adcs_boot_index_t *) data);
        self->last_telecommand_id = 120;
        break;
      case 121:
        self->telecommands_received++;
        self->adcs_erase_program(self, (adcs_boot_index_t *) data);
        self->last_telecommand_id = 121;
        break;
      case 122:
        self->telecommands_received++;
        self->adcs_upload_program_block(self, (adcs_upload_program_block_t *) data);
        self->last_telecommand_id = 122;
        break;
      case 123:
        self->telecommands_received++;
        self->adcs_finalize_program_upload(self, (adcs_finalize_program_upload_t *) data);
        self->last_telecommand_id = 123;
        break;
    }
  }
  else if (command_type == 1)
  {
    // this is a telemetry request
    // so we don't need the data
    // but there is a structure that needs to be returned
    switch (command_id)
    {
      case 128:
        self->telemetry_requests_received++;

        return (void*) self->adcs_send_identification(self);

        break;
      case 130:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_comm_status(self);

        break;
      case 131:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_ack(self);

        break;
      case 133:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_reset_cause(self);

        break;
      case 138:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_actuator_command(self);

        break;
      case 189:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_ACP_execution_state(self);

        break;
      case 190:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_ACP_execution_times(self);

        break;
      case 193:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_EDAC_and_latchup_counters(self);

        break;
      case 194:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_start_up_mode(self);

        break;
      case 240:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_file_info(self);

        break;
      case 241:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_file_block_CRC(self);

        break;
      case 242:
        self->telemetry_requests_received++;
        //= 256;
        return (void*) self->adcs_send_data_block(self);

        break;
      case 134:
        self->telemetry_requests_received++;
        //= 5;
        return (void*) self->adcs_send_selected_element_power_control(self);

        break;
      case 135:
        self->telemetry_requests_received++;
        //= 18;
        return (void*) self->adcs_send_power_and_temperature_meas(self);

        break;
      case 136:
        self->telemetry_requests_received++;
        //= 48;
        return (void*) self->adcs_send_state(self);

        break;
      case 137:
        self->telemetry_requests_received++;
        //= 36;
        return (void*) self->adcs_send_measurements(self);

        break;
      case 143:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_curr_time(self);

        break;
      case 144:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_curr_state(self);

        break;
      case 145:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_estimated_attitude_angles(self);

        break;
      case 146:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_estimated_angular_rates(self);

        break;
      case 147:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_satellite_position(self);

        break;
      case 148:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_satellite_velocity(self);

        break;
      case 149:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_magnetic_field_vector(self);

        break;
      case 150:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_coarse_sun_vector(self);

        break;
      case 151:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_fine_sun_vector(self);

        break;
      case 152:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_nadir_vector(self);

        break;
      case 153:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_rate_sensor_rates(self);

        break;
      case 154:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_wheel_speed_meas(self);

        break;
      case 173:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_cubesense_current_measurement(self);

        break;
      case 174:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_cubecontrol_current_measurements(self);

        break;
      case 175:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_peripheral_current_and_temperature_meas(self);

        break;
      case 139:
        self->telemetry_requests_received++;
        //= 60;
        return (void*) self->adcs_send_raw_sensor_measurements(self);

        break;
      case 163:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_angular_rate_covariance(self);

        break;
      case 164:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_raw_nadir_sensor(self);

        break;
      case 165:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_raw_sun_sensor(self);

        break;
      case 166:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_raw_css(self);

        break;
      case 167:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_raw_magnetometer(self);

        break;
      case 168:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_raw_GPS_status(self);

        break;
      case 169:
        self->telemetry_requests_received++;
        //= 6;
        return (void*) self->adcs_send_raw_GPS_time(self);

        break;
      case 170:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_raw_GPS_x(self);
        //= 6;
        break;
      case 171:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_raw_GPS_y(self);
        //= 6;
        break;
      case 172:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_raw_GPS_z(self);
        //= 6;
        break;
      case 140:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_estimation_data(self);
        //= 42;
        break;
      case 157:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_IGRF_modelled_magnetic_field_vector(self);
        //= 6;
        break;
      case 158:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_modelled_sun_vector(self);
        //= 6;
        break;
      case 159:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_estimated_gyro_bias(self);
        //= 6;
        break;
      case 160:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_estimated_innovation_vector(self);
        //= 6;
        break;
      case 161:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_quaternion_error_vector(self);
        //= 6;
        break;
      case 162:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_quaternion_covariance(self);
        //= 6;
        break;
      case 155:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_magnetorquer_command(self);
        //= 6;
        break;
      case 156:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_wheel_speed_command(self);
        //= 6;
        break;
      case 191:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_sgp4_orbit_parameters(self);
        //= 64;
        break;
      case 192:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_curr_config(self);
        //= 236;
        break;
      case 230:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_image_capture_and_save_operations_statuses(self);
        //= 16;
        break;
      case 250:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_uploaded_program_status(self);
        //= 3;
        break;
      case 251:
        self->telemetry_requests_received++;
        return (void*) self->adcs_send_flash_program_list(self);
        //= 497;
        break;
      case 255:
        return (void *) self->mode( self );
        break;
    }
  }
  return (void*) - 1;
}

static int mode_index = 0;
static adcs_mode_t mode_mock_up( adcs_t *adcs )
{
  //int index;

  //index = (int) adcs->data;
  //adcs->data = (void*)index++;
  //self->control_mode = mode_cycles[index % MODE_CYCLE_SIZE];
  return mode_cycles[mode_index++ % MODE_CYCLE_SIZE];
  adcs_attitude_control_mode_value_t control_mode = adcs->control_mode;
  switch (control_mode) {
    case ATTITUDE_CONTROL_NO_CONTROL:
      return ADCS_MODE_IDLE;
      break;
    case ATTITUDE_CONTROL_DETUMBLING_CONTROL:
      return ADCS_MODE_DETUMBLE;
      break;
    case ATTITUDE_CONTROL_INIT_PITCH:
      return ADCS_MODE_STABILIZED;
      break;
    case ATTITUDE_CONTROL_STEADY_STATE:
      return ADCS_MODE_STABILIZED;
      break;
  }

}

/* General telecommands */
static void adcs_reset(adcs_t *self, adcs_reset_t *reset_type )
{
  ++self->reset_count;
}

static void adcs_set_unix_time(adcs_t *self, adcs_unix_time_t *time)
{
  self->unix_time = time->unix_time;
  self->millis = time->millis;

}

static void adcs_set_run_mode(adcs_t *self, adcs_run_mode_t *run_mode)
{
  self->run_mode = *run_mode;
}

static void adcs_select_logged_data(adcs_t *self, adcs_logged_data_t *logged_data)
{


}

static void adcs_select_element_power_control(adcs_t *self, adcs_power_control_t *power_state)
{
  self->cube_control_signal_power_selection = power_state->cube_control_signal_power_selection;
  self->cube_control_motor_power_selection = power_state->cube_control_motor_power_selection;
  self->cube_sense_power_selection = power_state->cube_sense_power_selection;
  self->cube_motor_power_selection = power_state->cube_motor_power_selection;
  self->gps_lna_power_selection = power_state->gps_lna_power_selection;
}

static void adcs_deploy_magnetometer_boom(adcs_t *self, adcs_deploy_magnetometer_boom_timeout_t *timeout)
{
  self->boom_timeout = *timeout;

}

static void adcs_trigger_adcs_loop(adcs_t *self)
{


}

static void adcs_set_attitude_estimation_mode(adcs_t *self, adcs_attitude_estimation_mode_t *estimation_mode)
{
  self->attitude_estimation_mode = *estimation_mode;

}

static void adcs_set_attitude_control_mode(adcs_t *self, adcs_attitude_control_mode_t *control_mode)
{

  self->control_mode = control_mode->control_mode;
  self->control_mode_override = control_mode->override;
  self->control_mode_timeout = control_mode->timeout;
}

static void adcs_set_commanded_attitude_angles(adcs_t *self, adcs_commanded_attitude_angles_t *angles)
{
  self->commanded_attitude_angles_roll = angles->roll;
  self->commanded_attitude_angles_pitch = angles->pitch;
  self->commanded_attitude_angles_yaw = angles->yaw;

}

static void adcs_set_wheel_speed(adcs_t *self, adcs_wheel_speed_t *speed)
{
  self->wheel_x_speed = speed->x_speed;
  self->wheel_y_speed = speed->y_speed;
  self->wheel_z_speed = speed->z_speed;
}

static void adcs_set_magnetorquer_output(adcs_t *self, adcs_magnetorquer_output_t *output)
{
  self->magnetorquer_x_duty_cycle = output->x_duty_cycle;
  self->magnetorquer_y_duty_cycle = output->y_duty_cycle;
  self->magnetorquer_z_duty_cycle = output->z_duty_cycle;
}

static void adcs_set_sgp4_orbit_parameters(adcs_t *self, adcs_sgp4_orbit_parameters_t *params)
{
  self->sgp4_inclination = params->inclination;
  self->sgp4_eccentricity = params->eccentricity;
  self->sgp4_right_ascension = params->right_ascension;
  self->sgp4_perigee = params->perigee;
  self->sgp4_b_star_drag_term = params->b_star_drag_term;
  self->sgp4_mean_motion = params->mean_motion;
  self->sgp4_mean_anomaly = params->mean_anomaly;
  self->sgp4_epoch = params->epoch;
}

static void adcs_set_curr_config(adcs_t *self, adcs_curr_config_t *config)
{
  self->magnetorquer_1_configuration = config->magnetorquer_1_configuration;
  self->magnetorquer_2_configuration = config->magnetorquer_2_configuration;
  self->magnetorquer_3_configuration = config->magnetorquer_3_configuration;
  self->magnetorquer_max_x_dipole_moment = config->magnetorquer_max_x_dipole_moment;
  self->magnetorquer_max_y_dipole_moment = config->magnetorquer_max_y_dipole_moment;
  self->magnetorquer_max_z_dipole_moment = config->magnetorquer_max_z_dipole_moment;
  self->magnetorquer_max_on_time = config->magnetorquer_max_on_time;
  self->magnetic_control_selection = config->magnetic_control_selection;

  self->wheel_transform_angle_alpha = config->wheel_transform_angle_alpha;
  self->wheel_transform_angle_gamma = config->wheel_transform_angle_gamma;
  self->wheel_polarity = config->wheel_polarity;
  self->wheel_inertia = config->wheel_inertia;
  self->wheel_max_torque = config->wheel_max_torque;
  self->wheel_max_speed = config->wheel_max_speed;
  self->wheel_control_gain = config->wheel_control_gain;
  self->wheel_backup_control_mode = config->wheel_backup_control_mode;

  self->css1_config = config->css1_config;
  self->css2_config = config->css2_config;
  self->css3_config = config->css3_config;
  self->css4_config = config->css4_config;
  self->css5_config = config->css5_config;
  self->css6_config = config->css6_config;
  self->css1_rel_scale = config->css1_rel_scale;
  self->css2_rel_scale = config->css2_rel_scale;
  self->css3_rel_scale = config->css3_rel_scale;
  self->css4_rel_scale = config->css4_rel_scale;
  self->css5_rel_scale = config->css5_rel_scale;
  self->css6_rel_scale = config->css6_rel_scale;
  self->css_autofill_missing_facets = config->css_autofill_missing_facets;
  self->css_threshold = config->css_threshold;

  self->sun_sensor_transform_angle_alpha = config->sun_sensor_transform_angle_alpha;
  self->sun_sensor_transform_angle_beta = config->sun_sensor_transform_angle_beta;
  self->sun_sensor_transform_angle_gamma = config->sun_sensor_transform_angle_gamma;
  self->sun_detection_threshold = config->sun_detection_threshold;
  self->sun_sensor_auto_adjust_mode = config->sun_sensor_auto_adjust_mode;
  self->sun_sensor_exposure_time = config->sun_sensor_exposure_time;
  self->sun_sensor_AGC = config->sun_sensor_AGC;
  self->sun_blue_gain = config->sun_blue_gain;
  self->sun_red_gain = config->sun_red_gain;
  self->sun_boresight_x = config->sun_boresight_x;
  self->sun_boresight_y = config->sun_boresight_y;
  self->sun_shift = config->sun_shift;

  self->nadir_sensor_transform_angle_alpha = config->nadir_sensor_transform_angle_alpha;
  self->nadir_sensor_transform_angle_beta = config->nadir_sensor_transform_angle_beta;
  self->nadir_sensor_transform_angle_gamma = config->nadir_sensor_transform_angle_gamma;
  self->nadir_detection_threshold = config->nadir_detection_threshold;
  self->nadir_sensor_auto_adjust = config->nadir_sensor_auto_adjust;
  self->nadir_sensor_exposure_time = config->nadir_sensor_exposure_time;
  self->nadir_sensor_AGC = config->nadir_sensor_AGC;
  self->nadir_blue_gain = config->nadir_blue_gain;
  self->nadir_red_gain = config->nadir_red_gain;
  self->nadir_boresight_x = config->nadir_boresight_x;
  self->nadir_boresight_y = config->nadir_boresight_y;
  self->nadir_shift = config->nadir_shift;
  self->min_x_of_area_1 = config->min_x_of_area_1;
  self->max_x_of_area_1 = config->max_x_of_area_1;
  self->min_y_of_area_1 = config->min_y_of_area_1;
  self->max_y_of_area_1 = config->max_y_of_area_1;
  self->min_x_of_area_2 = config->min_x_of_area_2;
  self->max_x_of_area_2 = config->max_x_of_area_2;
  self->min_y_of_area_2 = config->min_y_of_area_2;
  self->max_y_of_area_2 = config->max_y_of_area_2;
  self->min_x_of_area_3 = config->min_x_of_area_3;
  self->max_x_of_area_3 = config->max_x_of_area_3;
  self->min_y_of_area_3 = config->min_y_of_area_3;
  self->max_y_of_area_3 = config->max_y_of_area_3;
  self->min_x_of_area_4 = config->min_x_of_area_4;
  self->max_x_of_area_4 = config->max_x_of_area_4;
  self->min_y_of_area_4 = config->min_y_of_area_4;
  self->max_y_of_area_4 = config->max_y_of_area_4;
  self->min_x_of_area_5 = config->min_x_of_area_5;
  self->max_x_of_area_5 = config->max_x_of_area_5;
  self->min_y_of_area_5 = config->min_y_of_area_5;
  self->max_y_of_area_5 = config->max_y_of_area_5;

  self->magnetometer_transform_angle_alpha = config->magnetometer_transform_angle_alpha;
  self->magnetometer_transform_angle_beta = config->magnetometer_transform_angle_beta;
  self->magnetometer_transform_angle_gamma = config->magnetometer_transform_angle_gamma;
  self->magnetometer_channel1_offset = config->magnetometer_channel1_offset;
  self->magnetometer_channel2_offset = config->magnetometer_channel2_offset;
  self->magnetometer_channel3_offset = config->magnetometer_channel3_offset;
  self->magnetometer_sens_mat_s11 = config->magnetometer_sens_mat_s11;
  self->magnetometer_sens_mat_s12 = config->magnetometer_sens_mat_s12;
  self->magnetometer_sens_mat_s13 = config->magnetometer_sens_mat_s13;
  self->magnetometer_sens_mat_s21 = config->magnetometer_sens_mat_s21;
  self->magnetometer_sens_mat_s22 = config->magnetometer_sens_mat_s22;
  self->magnetometer_sens_mat_s23 = config->magnetometer_sens_mat_s23;
  self->magnetometer_sens_mat_s31 = config->magnetometer_sens_mat_s31;
  self->magnetometer_sens_mat_s32 = config->magnetometer_sens_mat_s32;
  self->magnetometer_sens_mat_s33 = config->magnetometer_sens_mat_s33;

  self->y_rate_sensor_offset = config->y_rate_sensor_offset;
  self->y_rate_sensor_transform_angle_alpha = config->y_rate_sensor_transform_angle_alpha;
  self->y_rate_sensor_transform_angle_gamma = config->y_rate_sensor_transform_angle_gamma;

  self->detumbling_spin_gain = config->detumbling_spin_gain;
  self->detumbling_damping_gain = config->detumbling_damping_gain;
  self->ref_spin_rate = config->ref_spin_rate;

  self->y_momentum_control_gain = config->y_momentum_control_gain;
  self->y_momentum_damping_gain = config->y_momentum_damping_gain;
  self->y_momentum_proportional_gain = config->y_momentum_proportional_gain;
  self->y_momentum_derivative_gain = config->y_momentum_derivative_gain;
  self->ref_wheel_momentum = config->ref_wheel_momentum;
  self->ref_wheel_momentum_init_pitch = config->ref_wheel_momentum_init_pitch;
  self->ref_wheel_torque_init_pitch = config->ref_wheel_torque_init_pitch;

  self->moment_of_inertia_xx = config->moment_of_inertia_xx;
  self->moment_of_inertia_yy = config->moment_of_inertia_yy;
  self->moment_of_inertia_zz = config->moment_of_inertia_zz;
  self->moment_of_inertia_xy = config->moment_of_inertia_xy;
  self->moment_of_inertia_xz = config->moment_of_inertia_xz;
  self->moment_of_inertia_yz = config->moment_of_inertia_yz;

  self->magnetometer_rate_filter_system_noise = config->magnetometer_rate_filter_system_noise;
  self->EKF_system_noise = config->EKF_system_noise;
  self->css_meas_noise = config->css_meas_noise;
  self->sun_sensor_meas_noise = config->sun_sensor_meas_noise;
  self->nadir_sensor_meas_noise = config->nadir_sensor_meas_noise;
  self->magnetometer_sensor_meas_noise = config->magnetometer_sensor_meas_noise;
  self->mask_senors_usage = config->mask_senors_usage;
  self->sun_and_nadir_sampling_period = config->sun_and_nadir_sampling_period;
}

static void adcs_set_magnetorquer_config(adcs_t *self, adcs_magnetorquer_config_t *magnetorquer_config)
{
  self->magnetorquer_1_configuration = magnetorquer_config->magnetorquer_1_configuration;
  self->magnetorquer_2_configuration = magnetorquer_config->magnetorquer_2_configuration;
  self->magnetorquer_3_configuration = magnetorquer_config->magnetorquer_3_configuration;
  self->magnetorquer_max_x_dipole_moment = magnetorquer_config->magnetorquer_max_x_dipole_moment;
  self->magnetorquer_max_y_dipole_moment = magnetorquer_config->magnetorquer_max_y_dipole_moment;
  self->magnetorquer_max_z_dipole_moment = magnetorquer_config->magnetorquer_max_z_dipole_moment;
  self->magnetorquer_max_on_time = magnetorquer_config->magnetorquer_max_on_time;
  self->magnetic_control_selection = magnetorquer_config->magnetic_control_selection;
}
static void adcs_set_wheel_config(adcs_t *self, adcs_wheel_config_t *wheel_config)
{
  self->wheel_transform_angle_alpha = wheel_config->wheel_transform_angle_alpha;
  self->wheel_transform_angle_gamma = wheel_config->wheel_transform_angle_gamma;
  self->wheel_polarity = wheel_config->wheel_polarity;
  self->wheel_inertia = wheel_config->wheel_inertia;
  self->wheel_max_torque = wheel_config->wheel_max_torque;
  self->wheel_max_speed = wheel_config->wheel_max_speed;
  self->wheel_control_gain = wheel_config->wheel_control_gain;
  self->wheel_backup_control_mode = wheel_config->wheel_backup_control_mode;
}
static void adcs_set_css_config(adcs_t *self, adcs_css_config_t *css_config)
{
  self->css1_config = css_config->css1_config;
  self->css2_config = css_config->css2_config;
  self->css3_config = css_config->css3_config;
  self->css4_config = css_config->css4_config;
  self->css5_config = css_config->css5_config;
  self->css6_config = css_config->css6_config;
  self->css1_rel_scale = css_config->css1_rel_scale;
  self->css2_rel_scale = css_config->css2_rel_scale;
  self->css3_rel_scale = css_config->css3_rel_scale;
  self->css4_rel_scale = css_config->css4_rel_scale;
  self->css5_rel_scale = css_config->css5_rel_scale;
  self->css6_rel_scale = css_config->css6_rel_scale;
  self->css_autofill_missing_facets = css_config->css_autofill_missing_facets;
  self->css_threshold = css_config->css_threshold;
}
static void adcs_set_sun_sensor_config(adcs_t *self, adcs_sun_sensor_config_t *sun_sensor_config)
{
  self->sun_sensor_transform_angle_alpha = sun_sensor_config->sun_sensor_transform_angle_alpha;
  self->sun_sensor_transform_angle_beta = sun_sensor_config->sun_sensor_transform_angle_beta;
  self->sun_sensor_transform_angle_gamma = sun_sensor_config->sun_sensor_transform_angle_gamma;
  self->sun_detection_threshold = sun_sensor_config->sun_detection_threshold;
  self->sun_sensor_auto_adjust_mode = sun_sensor_config->sun_sensor_auto_adjust_mode;
  self->sun_sensor_exposure_time = sun_sensor_config->sun_sensor_exposure_time;
  self->sun_sensor_AGC = sun_sensor_config->sun_sensor_AGC;
  self->sun_blue_gain = sun_sensor_config->sun_blue_gain;
  self->sun_red_gain = sun_sensor_config->sun_red_gain;
  self->sun_boresight_x = sun_sensor_config->sun_boresight_x;
  self->sun_boresight_y = sun_sensor_config->sun_boresight_y;
  self->sun_shift = sun_sensor_config->sun_shift;
}
static void adcs_set_nadir_sensor_config(adcs_t *self, adcs_nadir_sensor_config_t *nadir_sensor_config)
{
  self->nadir_sensor_transform_angle_alpha = nadir_sensor_config->nadir_sensor_transform_angle_alpha;
  self->nadir_sensor_transform_angle_beta = nadir_sensor_config->nadir_sensor_transform_angle_beta;
  self->nadir_sensor_transform_angle_gamma = nadir_sensor_config->nadir_sensor_transform_angle_gamma;
  self->nadir_detection_threshold = nadir_sensor_config->nadir_detection_threshold;
  self->nadir_sensor_auto_adjust = nadir_sensor_config->nadir_sensor_auto_adjust;
  self->nadir_sensor_exposure_time = nadir_sensor_config->nadir_sensor_exposure_time;
  self->nadir_sensor_AGC = nadir_sensor_config->nadir_sensor_AGC;
  self->nadir_blue_gain = nadir_sensor_config->nadir_blue_gain;
  self->nadir_red_gain = nadir_sensor_config->nadir_red_gain;
  self->nadir_boresight_x = nadir_sensor_config->nadir_boresight_x;
  self->nadir_boresight_y = nadir_sensor_config->nadir_boresight_y;
  self->nadir_shift = nadir_sensor_config->nadir_shift;
  self->min_x_of_area_1 = nadir_sensor_config->min_x_of_area_1;
  self->max_x_of_area_1 = nadir_sensor_config->max_x_of_area_1;
  self->min_y_of_area_1 = nadir_sensor_config->min_y_of_area_1;
  self->max_y_of_area_1 = nadir_sensor_config->max_y_of_area_1;
  self->min_x_of_area_2 = nadir_sensor_config->min_x_of_area_2;
  self->max_x_of_area_2 = nadir_sensor_config->max_x_of_area_2;
  self->min_y_of_area_2 = nadir_sensor_config->min_y_of_area_2;
  self->max_y_of_area_2 = nadir_sensor_config->max_y_of_area_2;
  self->min_x_of_area_3 = nadir_sensor_config->min_x_of_area_3;
  self->max_x_of_area_3 = nadir_sensor_config->max_x_of_area_3;
  self->min_y_of_area_3 = nadir_sensor_config->min_y_of_area_3;
  self->max_y_of_area_3 = nadir_sensor_config->max_y_of_area_3;
  self->min_x_of_area_4 = nadir_sensor_config->min_x_of_area_4;
  self->max_x_of_area_4 = nadir_sensor_config->max_x_of_area_4;
  self->min_y_of_area_4 = nadir_sensor_config->min_y_of_area_4;
  self->max_y_of_area_4 = nadir_sensor_config->max_y_of_area_4;
  self->min_x_of_area_5 = nadir_sensor_config->min_x_of_area_5;
  self->max_x_of_area_5 = nadir_sensor_config->max_x_of_area_5;
  self->min_y_of_area_5 = nadir_sensor_config->min_y_of_area_5;
  self->max_y_of_area_5 = nadir_sensor_config->max_y_of_area_5;
}
static void adcs_set_magnetometer_config(adcs_t *self, adcs_magnetometer_config_t *magnetometer_config)
{
  self->magnetometer_transform_angle_alpha = magnetometer_config->magnetometer_transform_angle_alpha;
  self->magnetometer_transform_angle_beta = magnetometer_config->magnetometer_transform_angle_beta;
  self->magnetometer_transform_angle_gamma = magnetometer_config->magnetometer_transform_angle_gamma;
  self->magnetometer_channel1_offset = magnetometer_config->magnetometer_channel1_offset;
  self->magnetometer_channel2_offset = magnetometer_config->magnetometer_channel2_offset;
  self->magnetometer_channel3_offset = magnetometer_config->magnetometer_channel3_offset;
  self->magnetometer_sens_mat_s11 = magnetometer_config->magnetometer_sens_mat_s11;
  self->magnetometer_sens_mat_s12 = magnetometer_config->magnetometer_sens_mat_s12;
  self->magnetometer_sens_mat_s13 = magnetometer_config->magnetometer_sens_mat_s13;
  self->magnetometer_sens_mat_s21 = magnetometer_config->magnetometer_sens_mat_s21;
  self->magnetometer_sens_mat_s22 = magnetometer_config->magnetometer_sens_mat_s22;
  self->magnetometer_sens_mat_s23 = magnetometer_config->magnetometer_sens_mat_s23;
  self->magnetometer_sens_mat_s31 = magnetometer_config->magnetometer_sens_mat_s31;
  self->magnetometer_sens_mat_s32 = magnetometer_config->magnetometer_sens_mat_s32;
  self->magnetometer_sens_mat_s33 = magnetometer_config->magnetometer_sens_mat_s33;
}
static void adcs_set_rate_sensor_config(adcs_t *self, adcs_rate_sensor_config_t *rate_sensor_config)
{
  self->y_rate_sensor_offset = rate_sensor_config->y_rate_sensor_offset;
  self->y_rate_sensor_transform_angle_alpha = rate_sensor_config->y_rate_sensor_transform_angle_alpha;
  self->y_rate_sensor_transform_angle_gamma = rate_sensor_config->y_rate_sensor_transform_angle_gamma;
}
static void adcs_set_detumbling_control(adcs_t *self, adcs_detumbling_control_t *detumbling_control)
{
  self->detumbling_spin_gain = detumbling_control->detumbling_spin_gain;
  self->detumbling_damping_gain = detumbling_control->detumbling_damping_gain;
  self->ref_spin_rate = detumbling_control->ref_spin_rate;
}
static void adcs_set_Y_momentum_control(adcs_t *self, adcs_Y_momentum_control_t *Y_momentum_control_control)
{
  self->y_momentum_control_gain = Y_momentum_control_control->y_momentum_control_gain;
  self->y_momentum_damping_gain = Y_momentum_control_control->y_momentum_damping_gain;
  self->y_momentum_proportional_gain = Y_momentum_control_control->y_momentum_proportional_gain;
  self->y_momentum_derivative_gain = Y_momentum_control_control->y_momentum_derivative_gain;
  self->ref_wheel_momentum = Y_momentum_control_control->ref_wheel_momentum;
  self->ref_wheel_momentum_init_pitch = Y_momentum_control_control->ref_wheel_momentum_init_pitch;
  self->ref_wheel_torque_init_pitch = Y_momentum_control_control->ref_wheel_torque_init_pitch;
}
static void adcs_set_moment_of_inertia(adcs_t *self, adcs_moment_of_inertia_t *moment_of_inertia)
{
  self->moment_of_inertia_xx = moment_of_inertia->moment_of_inertia_xx;
  self->moment_of_inertia_yy = moment_of_inertia->moment_of_inertia_yy;
  self->moment_of_inertia_zz = moment_of_inertia->moment_of_inertia_zz;
  self->moment_of_inertia_xy = moment_of_inertia->moment_of_inertia_xy;
  self->moment_of_inertia_xz = moment_of_inertia->moment_of_inertia_xz;
  self->moment_of_inertia_yz = moment_of_inertia->moment_of_inertia_yz;
}
static void adcs_set_estimation_parameters(adcs_t *self, adcs_estimation_parameters_t *estimation_parameters)
{
  self->magnetometer_rate_filter_system_noise = estimation_parameters->magnetometer_rate_filter_system_noise;
  self->EKF_system_noise = estimation_parameters->EKF_system_noise;
  self->css_meas_noise = estimation_parameters->css_meas_noise;
  self->sun_sensor_meas_noise = estimation_parameters->sun_sensor_meas_noise;
  self->nadir_sensor_meas_noise = estimation_parameters->nadir_sensor_meas_noise;
  self->magnetometer_sensor_meas_noise = estimation_parameters->magnetometer_sensor_meas_noise;
  self->mask_senors_usage = estimation_parameters->mask_senors_usage;
  self->sun_and_nadir_sampling_period = estimation_parameters->sun_and_nadir_sampling_period;
}

static void adcs_save_config(adcs_t *self)
{


}

static void adcs_save_orbit_params(adcs_t *self)
{


}

static void adcs_set_start_up_mode(adcs_t *self, adcs_start_up_mode_t *start_up_mode)
{

  self->start_up_mode = *start_up_mode;
}

static void adcs_capture_and_save_image(adcs_t *self, adcs_capture_and_save_image_t *settings)
{


}

static void adcs_reset_file_list(adcs_t *self)
{


}

static void adcs_advance_file_list_index(adcs_t *self)
{


}

static void adcs_init_download(adcs_t *self, char *filename, int length)
{
  if (length != 13) {
    return;
  }


}

static void adcs_advance_file_read_pointer(adcs_t *self, adcs_next_block_num_to_read_t *block_number)
{


}

static void adcs_erase_file(adcs_t *self, char *filename, int length)
{
  if (length != 13) {
    return;
  }


}

static void adcs_erase_all_files(adcs_t *self)
{


}

static void adcs_set_boot_index(adcs_t *self, adcs_boot_index_t *index)
{
  self->boot_index = *index;

}

static void adcs_erase_program(adcs_t *self, adcs_boot_index_t *index)
{


}

static void adcs_upload_program_block(adcs_t *self, adcs_upload_program_block_t *program_block)
{


}

static void adcs_finalize_program_upload(adcs_t *self, adcs_finalize_program_upload_t *finalize_upload_command)
{


}



/*General telemetry*/
static adcs_identification_t* adcs_send_identification(adcs_t *self)
{

  adcs_identification_t* id = (adcs_identification_t*) malloc(sizeof(adcs_identification_t));
  id->node_type = 10;
  id->interface_version = 1;
  id->firmvare_version_major = 1;
  id->firmvare_version_minor = 0;
  id->runtime_seconds = time(NULL) - self->millis / 1000;
  id->runtime_millis = time(NULL) * 1000 - self->millis;
  tlm_size = 8;
  return id;
}

static adcs_comm_status_t* adcs_send_comm_status(adcs_t *self)
{
  /* keep track of this while executing */
  adcs_comm_status_t* comm_status = (adcs_comm_status_t*)malloc(sizeof(adcs_comm_status_t));
  comm_status->telecommand_counter = self->telecommands_received;
  comm_status->telemetry_request_counter = self->telemetry_requests_received;
  comm_status->comm_status_booleans = 0;
  tlm_size = 6;
  return comm_status;
}

static adcs_ack_t* adcs_send_ack(adcs_t *self)
{
  /* keep track of it while executing */
  adcs_ack_t* ack = (adcs_ack_t*)malloc(sizeof(adcs_ack_t));
  ack->last_telecommand_id = self->last_telecommand_id;
  if (!self->telecommand_processed)
  {
    ack->processed = 0;
  }
  else
  {
    ack->processed = 1;
    self->telecommand_processed = 0;
  }
  ack->telecommand_error_status = 0;
  ack->telecommand_parameter_error_index = 0;
  tlm_size = 4;
  return ack;
}

static adcs_reset_cause_t* adcs_send_reset_cause(adcs_t *self)
{
  /* generated */
  adcs_reset_cause_t* reset_cause = (adcs_reset_cause_t*)malloc(sizeof(adcs_reset_cause_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    uint8_t rand_val = rand() % 10;
    *(reset_cause) = (adcs_reset_cause_t)rand_val;

  }
  tlm_size = 1;
  return reset_cause;
}

static adcs_power_control_t* adcs_send_selected_element_power_control(adcs_t *self)
{
  adcs_power_control_t* power_control = (adcs_power_control_t*)malloc(sizeof(adcs_power_control_t));
  power_control->cube_control_signal_power_selection = self->cube_control_signal_power_selection;
  power_control->cube_control_motor_power_selection = self->cube_control_motor_power_selection;
  power_control->cube_sense_power_selection = self->cube_sense_power_selection;
  power_control->cube_motor_power_selection = self->cube_motor_power_selection;
  power_control->gps_lna_power_selection = self->gps_lna_power_selection;
  tlm_size = 5;
  return power_control;
}

static adcs_power_and_temperature_meas_t* adcs_send_power_and_temperature_meas(adcs_t *self)
{
  /* generated */
  adcs_power_and_temperature_meas_t* power_and_temperature_meas = (adcs_power_and_temperature_meas_t*)malloc(sizeof(adcs_power_and_temperature_meas_t));
  /*if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));
    int i;
    for (i = 0; i < sizeof(adcs_power_and_temperature_meas_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)power_and_temperature_meas + i;
      *pos = rand_val;
    }
  }*/
  
  power_and_temperature_meas->cubesense_3V3_current = rheostatValue1 + ( ( (rheostatValue2 << 4) >> 4 ) << 10);
  power_and_temperature_meas->cubesense_nadir_sram_current = (rheostatValue2 >> 6) + (joyH << 4) + ( ( (joyV << 8) >> 8) << 14 );
  power_and_temperature_meas->cubesense_sun_sram_current = (joyV >> 2) + ( ( (photoResValue1 << 8) >> 8 ) << 8);
  
  power_and_temperature_meas->cubecontrol_3V3_current = (rheostatValue1 >> 6) + (rheostatValue2 << 4) + ( ( (joyH << 8) >> 8 ) << 14 );
  power_and_temperature_meas->cubecontrol_5V_current = (joyH >> 2) + ( ( (joyV << 8) >> 8 ) << 8);
  power_and_temperature_meas->cubecontrol_Vbat_current = (joyV >> 8) + (photoResValue1 << 2) + ( ( (photoResValue2 << 6) >> 6 ) << 12);
  
  power_and_temperature_meas->magnetorquer_current = (rheostatValue2 >> 2) + ( ( (joyH << 8) >> 8 ) << 8);
  power_and_temperature_meas->momentum_wheel_current = (joyH >> 8) + (joyV << 2) + ( ( (photoResValue1 << 6) >> 6 ) << 12);
  power_and_temperature_meas->rate_sensor_temperature = (photoResValue1 >> 4) + ( ( (photoResValue2 << 8) >> 8 ) << 6 );
  power_and_temperature_meas->ARM_CPU_temperature = photoResValue2 >> 2;
  
  tlm_size = 18;
  return power_and_temperature_meas;
}

static adcs_cubesense_current_measurements_t* adcs_send_cubesense_current_measurement(adcs_t *self)
{
  /* generated */
  adcs_cubesense_current_measurements_t* cubesense_current_measurements = (adcs_cubesense_current_measurements_t*) malloc(sizeof(adcs_cubesense_current_measurements_t));
  /*if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));
    int i;
    for (i = 0; i < sizeof(adcs_cubesense_current_measurements_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)cubesense_current_measurements + i;
      *pos = rand_val;
    }
  }*/
  
  cubesense_current_measurements->cubesense_3V3_current = rheostatValue1 + ( ( (rheostatValue2 << 4) >> 4 ) << 10);
  cubesense_current_measurements->cubesense_nadir_sram_current = (rheostatValue2 >> 6) + (joyH << 4) + ( ( (joyV << 8) >> 8) << 14 );
  cubesense_current_measurements->cubesense_sun_sram_current = (joyV >> 2) + ( ( (photoResValue1 << 8) >> 8 ) << 8);
  
  tlm_size = 6;
  return cubesense_current_measurements;
}
static adcs_cubecontrol_current_measurements_t* adcs_send_cubecontrol_current_measurements(adcs_t *self)
{
  /* generated */
  adcs_cubecontrol_current_measurements_t* cubecontrol_current_measurements = (adcs_cubecontrol_current_measurements_t*)malloc(sizeof(adcs_cubecontrol_current_measurements_t));
  /*if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));
    int i;
    for (i = 0; i < sizeof(adcs_cubecontrol_current_measurements_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)cubecontrol_current_measurements + i;
      *pos = rand_val;
    }
  }*/
  
  cubecontrol_current_measurements->cubecontrol_3V3_current = (rheostatValue1 >> 6) + (rheostatValue2 << 4) + ( ( (joyH << 8) >> 8 ) << 14 );
  cubecontrol_current_measurements->cubecontrol_5V_current = (joyH >> 2) + ( ( (joyV << 8) >> 8 ) << 8);
  cubecontrol_current_measurements->cubecontrol_Vbat_current = (joyV >> 8) + (photoResValue1 << 2) + ( ( (photoResValue2 << 6) >> 6 ) << 12);
  
  tlm_size = 6;
  return cubecontrol_current_measurements;
}
static adcs_peripheral_current_and_temperature_meas_t* adcs_send_peripheral_current_and_temperature_meas(adcs_t *self)
{
  /* generated */
  adcs_peripheral_current_and_temperature_meas_t* peripheral_current_and_temperature_meas = (adcs_peripheral_current_and_temperature_meas_t*)malloc(sizeof(adcs_peripheral_current_and_temperature_meas_t));
  /*if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));
    int i;
    for (i = 0; i < sizeof(adcs_peripheral_current_and_temperature_meas_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)peripheral_current_and_temperature_meas + i;
      *pos = rand_val;
    }
  }*/
  
  peripheral_current_and_temperature_meas->magnetorquer_current = (rheostatValue2 >> 2) + ( ( (joyH << 8) >> 8 ) << 8);
  peripheral_current_and_temperature_meas->momentum_wheel_current = (joyH >> 8) + (joyV << 2) + ( ( (photoResValue1 << 6) >> 6 ) << 12);
  peripheral_current_and_temperature_meas->rate_sensor_temperature = (photoResValue1 >> 4) + ( ( (photoResValue2 << 8) >> 8 ) << 6 );
  peripheral_current_and_temperature_meas->ARM_CPU_temperature = photoResValue2 >> 2;
  
  tlm_size = 6;
  return peripheral_current_and_temperature_meas;
}

static adcs_state_t* adcs_send_state(adcs_t *self)
{
  adcs_state_t* state = (adcs_state_t*)malloc(sizeof(adcs_state_t));
  state->current_unix_time = self->unix_time;
  state->millis = self->millis;
  uint8_t state_flags = (self->control_mode << 5) + (self->attitude_estimation_mode << 2) + self->run_mode;
  state->adcs_modes = state_flags;
  state->adcs_state_bool_flags_set1 = 0b1111110;
  state->adcs_state_bool_flags_set2 = 0;
  state->adcs_state_bool_flags_set3 = 0;
  state->adcs_state_bool_flags_set4 = 0;
  state->adcs_state_bool_flags_set5 = 0;
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 6; i < sizeof(adcs_state_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)state + i;
      *pos = rand_val;
    }
  }
  tlm_size = 48;
  return state;
}

static adcs_unix_time_t* adcs_send_curr_time(adcs_t *self)
{
  adcs_unix_time_t* curr_time = (adcs_unix_time_t*)malloc(sizeof(adcs_unix_time_t));
  curr_time->unix_time = self->unix_time;
  curr_time->millis = self->millis;
  tlm_size = 6;
  return curr_time;
}
static adcs_curr_state_t* adcs_send_curr_state(adcs_t *self)
{
  adcs_curr_state_t* curr_state = (adcs_curr_state_t*)malloc(sizeof(adcs_curr_state_t));
  uint8_t state_flags = (self->control_mode << 5) + (self->attitude_estimation_mode << 2) + self->run_mode;
  curr_state->adcs_modes = state_flags;
  curr_state->adcs_state_bool_flags_set1 = 0b1111110;
  curr_state->adcs_state_bool_flags_set2 = 0;
  curr_state->adcs_state_bool_flags_set3 = 0;
  curr_state->adcs_state_bool_flags_set4 = 0;
  curr_state->adcs_state_bool_flags_set5 = 0;
  tlm_size = 6;
  return curr_state;
}
static adcs_estimated_attitude_angles_t* adcs_send_estimated_attitude_angles(adcs_t *self)
{
  /* generated */
  adcs_estimated_attitude_angles_t* estimated_attitude_angles = (adcs_estimated_attitude_angles_t*)malloc(sizeof(adcs_estimated_attitude_angles_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_estimated_attitude_angles_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)estimated_attitude_angles + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return estimated_attitude_angles;
}
static adcs_estimated_angular_rates_t* adcs_send_estimated_angular_rates(adcs_t *self)
{
  /* generated */
  adcs_estimated_angular_rates_t* estimated_angular_rates = (adcs_estimated_angular_rates_t*)malloc(sizeof(adcs_estimated_angular_rates_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));
    int i;
    for (i = 0; i < sizeof(adcs_estimated_angular_rates_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)estimated_angular_rates + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return estimated_angular_rates;
}
static adcs_satellite_velocity_t* adcs_send_satellite_velocity(adcs_t *self)
{
  /* generated */
  adcs_satellite_velocity_t* satellite_velocity = (adcs_satellite_velocity_t*)malloc(sizeof(adcs_satellite_velocity_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_satellite_velocity_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)satellite_velocity + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return satellite_velocity;
}
static adcs_satellite_position_t* adcs_send_satellite_position(adcs_t *self)
{
  /* generated */
  adcs_satellite_position_t* satellite_position = (adcs_satellite_position_t*)malloc(sizeof(adcs_satellite_position_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));
    int i;
    for (i = 0; i < sizeof(adcs_satellite_position_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)satellite_position + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return satellite_position;
}

static adcs_measurements_t* adcs_send_measurements(adcs_t *self)
{
  /* generated */
  adcs_measurements_t* measurements = (adcs_measurements_t*)malloc(sizeof(adcs_measurements_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));
    int i;
    for (i = 0; i < sizeof(adcs_measurements_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)measurements + i;
      *pos = rand_val;
    }
  }
  tlm_size = 36;
  return measurements;
}

static adcs_magnetic_field_vector_t* adcs_send_magnetic_field_vector(adcs_t *self)
{
  /* generated */
  adcs_magnetic_field_vector_t* magnetic_field_vector = (adcs_magnetic_field_vector_t*)malloc(sizeof(adcs_magnetic_field_vector_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));
    int i;
    for (i = 0; i < sizeof(adcs_magnetic_field_vector_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)magnetic_field_vector + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return magnetic_field_vector;
}
static adcs_coarse_sun_vector_t* adcs_send_coarse_sun_vector(adcs_t *self)
{
  /* generated */
  adcs_coarse_sun_vector_t* coarse_sun_vector = (adcs_coarse_sun_vector_t*)malloc(sizeof(adcs_coarse_sun_vector_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_coarse_sun_vector_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)coarse_sun_vector + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return coarse_sun_vector;
}
static adcs_fine_sun_vector_t* adcs_send_fine_sun_vector(adcs_t *self)
{
  /* generated */
  adcs_fine_sun_vector_t* fine_sun_vector = (adcs_fine_sun_vector_t*)malloc(sizeof(adcs_fine_sun_vector_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_fine_sun_vector_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)fine_sun_vector + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return fine_sun_vector;
}
static adcs_nadir_vector_t* adcs_send_nadir_vector(adcs_t *self)
{
  /* generated */
  adcs_nadir_vector_t* nadir_vector = (adcs_nadir_vector_t*)malloc(sizeof(adcs_nadir_vector_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_nadir_vector_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)nadir_vector + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return nadir_vector;
}
static adcs_rate_sensor_rates_t* adcs_send_rate_sensor_rates(adcs_t *self)
{
  /* generated */
  adcs_rate_sensor_rates_t* rate_sensor_rates = (adcs_rate_sensor_rates_t*)malloc(sizeof(adcs_rate_sensor_rates_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_rate_sensor_rates_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)rate_sensor_rates + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return rate_sensor_rates;
}
static adcs_wheel_speed_meas_t* adcs_send_wheel_speed_meas(adcs_t *self)
{
  /* generated */
  adcs_wheel_speed_meas_t* wheel_speed_meas = (adcs_wheel_speed_meas_t*)malloc(sizeof(adcs_wheel_speed_meas_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_wheel_speed_meas_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)wheel_speed_meas + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return wheel_speed_meas;
}

static adcs_actuator_command_t* adcs_send_actuator_command(adcs_t *self)
{
  /* generated */
  adcs_actuator_command_t* actuator_command = (adcs_actuator_command_t*)malloc(sizeof(adcs_actuator_command_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < (sizeof(adcs_actuator_command_t) - sizeof(adcs_wheel_speed_command_t)); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)actuator_command + i;
      *pos = rand_val;
    }
  }
  actuator_command->wheel_speed_x = self->wheel_x_speed;
  actuator_command->wheel_speed_y = self->wheel_y_speed;
  actuator_command->wheel_speed_z = self->wheel_z_speed;
  tlm_size = 6;
  return actuator_command;
}

static adcs_magnetorquer_command_t* adcs_send_magnetorquer_command(adcs_t *self)
{
  /* generated */
  adcs_magnetorquer_command_t* magnetorquer_command = (adcs_magnetorquer_command_t*)malloc(sizeof(adcs_magnetorquer_command_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_magnetorquer_command_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)magnetorquer_command + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return magnetorquer_command;
}
static adcs_wheel_speed_command_t* adcs_send_wheel_speed_command(adcs_t *self)
{
  adcs_wheel_speed_command_t* wheel_speed_command = (adcs_wheel_speed_command_t*)malloc(sizeof(adcs_wheel_speed_command_t));
  wheel_speed_command->wheel_speed_x = self->wheel_x_speed;
  wheel_speed_command->wheel_speed_y = self->wheel_y_speed;
  wheel_speed_command->wheel_speed_z = self->wheel_z_speed;
  tlm_size = 6;
  return wheel_speed_command;
}

static adcs_raw_sensor_measurements_t* adcs_send_raw_sensor_measurements(adcs_t *self)
{
  /* generated */
  adcs_raw_sensor_measurements_t* raw_sensor_measurements = (adcs_raw_sensor_measurements_t*)malloc(sizeof(adcs_raw_sensor_measurements_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_raw_sensor_measurements_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)raw_sensor_measurements + i;
      *pos = rand_val;
    }
  }
  tlm_size = 60;
  return raw_sensor_measurements;
}

static adcs_raw_nadir_sensor_t* adcs_send_raw_nadir_sensor(adcs_t *self)
{
  /* generated */
  adcs_raw_nadir_sensor_t* raw_nadir_sensor = (adcs_raw_nadir_sensor_t*)malloc(sizeof(adcs_raw_nadir_sensor_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_raw_nadir_sensor_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)raw_nadir_sensor + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return raw_nadir_sensor;
}
static adcs_raw_sun_sensor_t* adcs_send_raw_sun_sensor(adcs_t *self)
{
  /* generated */
  adcs_raw_sun_sensor_t* raw_sun_sensor = (adcs_raw_sun_sensor_t*)malloc(sizeof(adcs_raw_sun_sensor_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_raw_sun_sensor_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)raw_sun_sensor + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return raw_sun_sensor;
}
static adcs_raw_css_t* adcs_send_raw_css(adcs_t *self)
{
  /* generated */
  adcs_raw_css_t* raw_css = (adcs_raw_css_t*)malloc(sizeof(adcs_raw_css_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_raw_css_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)raw_css + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return raw_css;
}
static adcs_raw_magnetometer_t* adcs_send_raw_magnetometer(adcs_t *self)
{
  /* generated */
  adcs_raw_magnetometer_t* raw_magnetometer = (adcs_raw_magnetometer_t*)malloc(sizeof(adcs_raw_magnetometer_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_raw_magnetometer_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)raw_magnetometer + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return raw_magnetometer;
}
static adcs_raw_GPS_status_t* adcs_send_raw_GPS_status(adcs_t *self)
{
  /* generated */
  /*adcs_raw_GPS_status_t* raw_GPS_status = (adcs_raw_GPS_status_t*)malloc(sizeof(adcs_raw_GPS_status_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL){
  	srand(time(NULL));

  	int i;
  	for (i = 0; i < sizeof(adcs_raw_GPS_status_t); i++){
  		uint8_t rand_val = rand() % 255;
  		uint8_t* pos = (uint8_t *)raw_GPS_status + i;
  		*pos = rand_val;
  	}
  }
        tlm_size = 6;*/
  return gps_status();//raw_GPS_status;
}
static adcs_raw_GPS_time_t* adcs_send_raw_GPS_time(adcs_t *self)
{
  /* generated */
  /*adcs_raw_GPS_time_t* raw_GPS_time = (adcs_raw_GPS_time_t*)malloc(sizeof(adcs_raw_GPS_time_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL){
  	srand(time(NULL));

  	int i;
  	for (i = 0; i < sizeof(adcs_raw_GPS_time_t); i++){
  		uint8_t rand_val = rand() % 255;
  		uint8_t* pos = (uint8_t *)raw_GPS_time + i;
  		*pos = rand_val;
  	}
  }
        tlm_size = 6;*/
  return gps_time(millis());//raw_GPS_time;
}
static adcs_raw_GPS_x_t* adcs_send_raw_GPS_x(adcs_t *self)
{
  /* generated */
  /*adcs_raw_GPS_x_t* raw_GPS_x = (adcs_raw_GPS_x_t*)malloc(sizeof(adcs_raw_GPS_x_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL){
  	srand(time(NULL));

  	int i;
  	for (i = 0; i < sizeof(adcs_raw_GPS_x_t); i++){
  		uint8_t rand_val = rand() % 255;
  		uint8_t* pos = (uint8_t *)raw_GPS_x + i;
  		*pos = rand_val;
  	}
  }
        tlm_size = 6;*/
  return gps_x();//raw_GPS_x;
}
static adcs_raw_GPS_y_t* adcs_send_raw_GPS_y(adcs_t *self)
{
  /* generated */
  /*adcs_raw_GPS_y_t* raw_GPS_y = (adcs_raw_GPS_y_t*)malloc(sizeof(adcs_raw_GPS_y_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL){
  	srand(time(NULL));

  	int i;
  	for (i = 0; i < sizeof(adcs_raw_GPS_y_t); i++){
  		uint8_t rand_val = rand() % 255;
  		uint8_t* pos = (uint8_t *)raw_GPS_y + i;
  		*pos = rand_val;
  	}
  }
        tlm_size = 6;*/
  return gps_y();//raw_GPS_y;
}
static adcs_raw_GPS_z_t* adcs_send_raw_GPS_z(adcs_t *self)
{
  /* generated */
  /*adcs_raw_GPS_z_t* raw_GPS_z = (adcs_raw_GPS_z_t*)malloc(sizeof(adcs_raw_GPS_z_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL){
  	srand(time(NULL));

  	int i;
  	for (i = 0; i < sizeof(adcs_raw_GPS_z_t); i++){
  		uint8_t rand_val = rand() % 255;
  		uint8_t* pos = (uint8_t *)raw_GPS_z + i;
  		*pos = rand_val;
  	}
  }
        tlm_size = 6;*/
  return gps_z();//raw_GPS_z;
}

static adcs_estimation_data_t* adcs_send_estimation_data(adcs_t *self)
{
  /* generated */
  adcs_estimation_data_t* estimation_data = (adcs_estimation_data_t*)malloc(sizeof(adcs_estimation_data_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_estimation_data_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)estimation_data + i;
      *pos = rand_val;
    }
  }
  tlm_size = 42;
  return estimation_data;
}

static adcs_IGRF_modelled_magnetic_field_vector_t* adcs_send_IGRF_modelled_magnetic_field_vector(adcs_t *self)
{
  /* generated */
  adcs_IGRF_modelled_magnetic_field_vector_t* IGRF_modelled_magnetic_field_vector = (adcs_IGRF_modelled_magnetic_field_vector_t*)malloc(sizeof(adcs_IGRF_modelled_magnetic_field_vector_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_IGRF_modelled_magnetic_field_vector_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)IGRF_modelled_magnetic_field_vector + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return IGRF_modelled_magnetic_field_vector;
}
static adcs_modelled_sun_vector_t* adcs_send_modelled_sun_vector(adcs_t *self)
{
  /* generated */
  adcs_modelled_sun_vector_t* modelled_sun_vector = (adcs_modelled_sun_vector_t*)malloc(sizeof(adcs_modelled_sun_vector_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_modelled_sun_vector_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)modelled_sun_vector + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return modelled_sun_vector;
}
static adcs_estimated_gyro_bias_t* adcs_send_estimated_gyro_bias(adcs_t *self)
{
  /* generated */
  adcs_estimated_gyro_bias_t* estimated_gyro_bias = (adcs_estimated_gyro_bias_t*)malloc(sizeof(adcs_estimated_gyro_bias_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_estimated_gyro_bias_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)estimated_gyro_bias + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return estimated_gyro_bias;
}
static adcs_estimated_innovation_vector_t* adcs_send_estimated_innovation_vector(adcs_t *self)
{
  /* generated */
  adcs_estimated_innovation_vector_t* estimated_innovation_vector = (adcs_estimated_innovation_vector_t*)malloc(sizeof(adcs_estimated_innovation_vector_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_estimated_innovation_vector_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)estimated_innovation_vector + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return estimated_innovation_vector;
}
static adcs_quaternion_error_vector_t* adcs_send_quaternion_error_vector(adcs_t *self)
{
  /* generated */
  adcs_quaternion_error_vector_t* quaternion_error_vector = (adcs_quaternion_error_vector_t*)malloc(sizeof(adcs_quaternion_error_vector_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_quaternion_error_vector_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)quaternion_error_vector + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return quaternion_error_vector;
}
static adcs_quaternion_covariance_t* adcs_send_quaternion_covariance(adcs_t *self)
{
  /* generated */
  adcs_quaternion_covariance_t* quaternion_covariance = (adcs_quaternion_covariance_t*)malloc(sizeof(adcs_quaternion_covariance_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_quaternion_covariance_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)quaternion_covariance + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return quaternion_covariance;
}
static adcs_angular_rate_covariance_t* adcs_send_angular_rate_covariance(adcs_t *self)
{
  /* generated */
  adcs_angular_rate_covariance_t* angular_rate_covariance = (adcs_angular_rate_covariance_t*)malloc(sizeof(adcs_angular_rate_covariance_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_angular_rate_covariance_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)angular_rate_covariance + i;
      *pos = rand_val;
    }
  }
  tlm_size = 6;
  return angular_rate_covariance;
}

static adcs_ACP_execution_state_t* adcs_send_ACP_execution_state(adcs_t *self)
{
  /* have to keep track of this in the model */
  adcs_ACP_execution_state_t* ACP_execution_state = (adcs_ACP_execution_state_t*)malloc(sizeof(adcs_ACP_execution_state_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_ACP_execution_state_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)ACP_execution_state + i;
      *pos = rand_val;
    }
  }
  tlm_size = 3;
  return ACP_execution_state;
}

static adcs_ACP_execution_times_t* adcs_send_ACP_execution_times(adcs_t *self)
{
  /* generated */
  adcs_ACP_execution_times_t* ACP_execution_times = (adcs_ACP_execution_times_t*)malloc(sizeof(adcs_ACP_execution_times_t));
  ACP_execution_times->time_to_perform_ADCS_update = time(NULL) + 10000;
  ACP_execution_times->time_to_perform_sensor_or_actuator_comms = time(NULL) + 20000;
  ACP_execution_times->time_to_execute_SGP4_propagator = time(NULL) + 30000;
  ACP_execution_times->time_to_execute_IGRF_model = time(NULL) + 40000;
  tlm_size = 8;
  return ACP_execution_times;
}

static adcs_sgp4_orbit_parameters_t* adcs_send_sgp4_orbit_parameters(adcs_t *self)
{
  adcs_sgp4_orbit_parameters_t* sgp4_orbit_parameters = (adcs_sgp4_orbit_parameters_t*)malloc(sizeof(adcs_sgp4_orbit_parameters_t));
  sgp4_orbit_parameters->inclination = self->sgp4_inclination;
  sgp4_orbit_parameters->eccentricity = self->sgp4_eccentricity;
  sgp4_orbit_parameters->right_ascension = self->sgp4_right_ascension;
  sgp4_orbit_parameters->perigee = self->sgp4_perigee;
  sgp4_orbit_parameters->b_star_drag_term = self->sgp4_b_star_drag_term;
  sgp4_orbit_parameters->mean_motion = self->sgp4_mean_motion;
  sgp4_orbit_parameters->mean_anomaly = self->sgp4_mean_anomaly;
  sgp4_orbit_parameters->epoch = self->sgp4_epoch;
  tlm_size = 64;
  return sgp4_orbit_parameters;
}

static adcs_curr_config_t* adcs_send_curr_config(adcs_t *self)
{
  adcs_curr_config_t* curr_config = (adcs_curr_config_t*)malloc(sizeof(adcs_curr_config_t));
  curr_config->magnetorquer_1_configuration = self->magnetorquer_1_configuration;
  curr_config->magnetorquer_2_configuration = self->magnetorquer_2_configuration;
  curr_config->magnetorquer_3_configuration = self->magnetorquer_3_configuration;
  curr_config->magnetorquer_max_x_dipole_moment = self->magnetorquer_max_x_dipole_moment;
  curr_config->magnetorquer_max_y_dipole_moment = self->magnetorquer_max_y_dipole_moment;
  curr_config->magnetorquer_max_z_dipole_moment = self->magnetorquer_max_z_dipole_moment;
  curr_config->magnetorquer_max_on_time = self->magnetorquer_max_on_time;
  curr_config->magnetic_control_selection = self->magnetic_control_selection;

  curr_config->wheel_transform_angle_alpha = self->wheel_transform_angle_alpha;
  curr_config->wheel_transform_angle_gamma = self->wheel_transform_angle_gamma;
  curr_config->wheel_polarity = self->wheel_polarity;
  curr_config->wheel_inertia = self->wheel_inertia;
  curr_config->wheel_max_torque = self->wheel_max_torque;
  curr_config->wheel_max_speed = self->wheel_max_speed;
  curr_config->wheel_control_gain = self->wheel_control_gain;
  curr_config->wheel_backup_control_mode = self->wheel_backup_control_mode;

  curr_config->css1_config = self->css1_config;
  curr_config->css2_config = self->css2_config;
  curr_config->css3_config = self->css3_config;
  curr_config->css4_config = self->css4_config;
  curr_config->css5_config = self->css5_config;
  curr_config->css6_config = self->css6_config;
  curr_config->css1_rel_scale = self->css1_rel_scale;
  curr_config->css2_rel_scale = self->css2_rel_scale;
  curr_config->css3_rel_scale = self->css3_rel_scale;
  curr_config->css4_rel_scale = self->css4_rel_scale;
  curr_config->css5_rel_scale = self->css5_rel_scale;
  curr_config->css6_rel_scale = self->css6_rel_scale;
  curr_config->css_autofill_missing_facets = self->css_autofill_missing_facets;
  curr_config->css_threshold = self->css_threshold;

  curr_config->sun_sensor_transform_angle_alpha = self->sun_sensor_transform_angle_alpha;
  curr_config->sun_sensor_transform_angle_beta = self->sun_sensor_transform_angle_beta;
  curr_config->sun_sensor_transform_angle_gamma = self->sun_sensor_transform_angle_gamma;
  curr_config->sun_detection_threshold = self->sun_detection_threshold;
  curr_config->sun_sensor_auto_adjust_mode = self->sun_sensor_auto_adjust_mode;
  curr_config->sun_sensor_exposure_time = self->sun_sensor_exposure_time;
  curr_config->sun_sensor_AGC = self->sun_sensor_AGC;
  curr_config->sun_blue_gain = self->sun_blue_gain;
  curr_config->sun_red_gain = self->sun_red_gain;
  curr_config->sun_boresight_x = self->sun_boresight_x;
  curr_config->sun_boresight_y = self->sun_boresight_y;
  curr_config->sun_shift = self->sun_shift;

  curr_config->nadir_sensor_transform_angle_alpha = self->nadir_sensor_transform_angle_alpha;
  curr_config->nadir_sensor_transform_angle_beta = self->nadir_sensor_transform_angle_beta;
  curr_config->nadir_sensor_transform_angle_gamma = self->nadir_sensor_transform_angle_gamma;
  curr_config->nadir_detection_threshold = self->nadir_detection_threshold;
  curr_config->nadir_sensor_auto_adjust = self->nadir_sensor_auto_adjust;
  curr_config->nadir_sensor_exposure_time = self->nadir_sensor_exposure_time;
  curr_config->nadir_sensor_AGC = self->nadir_sensor_AGC;
  curr_config->nadir_blue_gain = self->nadir_blue_gain;
  curr_config->nadir_red_gain = self->nadir_red_gain;
  curr_config->nadir_boresight_x = self->nadir_boresight_x;
  curr_config->nadir_boresight_y = self->nadir_boresight_y;
  curr_config->nadir_shift = self->nadir_shift;
  curr_config->min_x_of_area_1 = self->min_x_of_area_1;
  curr_config->max_x_of_area_1 = self->max_x_of_area_1;
  curr_config->min_y_of_area_1 = self->min_y_of_area_1;
  curr_config->max_y_of_area_1 = self->max_y_of_area_1;
  curr_config->min_x_of_area_2 = self->min_x_of_area_2;
  curr_config->max_x_of_area_2 = self->max_x_of_area_2;
  curr_config->min_y_of_area_2 = self->min_y_of_area_2;
  curr_config->max_y_of_area_2 = self->max_y_of_area_2;
  curr_config->min_x_of_area_3 = self->min_x_of_area_3;
  curr_config->max_x_of_area_3 = self->max_x_of_area_3;
  curr_config->min_y_of_area_3 = self->min_y_of_area_3;
  curr_config->max_y_of_area_3 = self->max_y_of_area_3;
  curr_config->min_x_of_area_4 = self->min_x_of_area_4;
  curr_config->max_x_of_area_4 = self->max_x_of_area_4;
  curr_config->min_y_of_area_4 = self->min_y_of_area_4;
  curr_config->max_y_of_area_4 = self->max_y_of_area_4;
  curr_config->min_x_of_area_5 = self->min_x_of_area_5;
  curr_config->max_x_of_area_5 = self->max_x_of_area_5;
  curr_config->min_y_of_area_5 = self->min_y_of_area_5;
  curr_config->max_y_of_area_5 = self->max_y_of_area_5;

  curr_config->magnetometer_transform_angle_alpha = self->magnetometer_transform_angle_alpha;
  curr_config->magnetometer_transform_angle_beta = self->magnetometer_transform_angle_beta;
  curr_config->magnetometer_transform_angle_gamma = self->magnetometer_transform_angle_gamma;
  curr_config->magnetometer_channel1_offset = self->magnetometer_channel1_offset;
  curr_config->magnetometer_channel2_offset = self->magnetometer_channel2_offset;
  curr_config->magnetometer_channel3_offset = self->magnetometer_channel3_offset;
  curr_config->magnetometer_sens_mat_s11 = self->magnetometer_sens_mat_s11;
  curr_config->magnetometer_sens_mat_s12 = self->magnetometer_sens_mat_s12;
  curr_config->magnetometer_sens_mat_s13 = self->magnetometer_sens_mat_s13;
  curr_config->magnetometer_sens_mat_s21 = self->magnetometer_sens_mat_s21;
  curr_config->magnetometer_sens_mat_s22 = self->magnetometer_sens_mat_s22;
  curr_config->magnetometer_sens_mat_s23 = self->magnetometer_sens_mat_s23;
  curr_config->magnetometer_sens_mat_s31 = self->magnetometer_sens_mat_s31;
  curr_config->magnetometer_sens_mat_s32 = self->magnetometer_sens_mat_s32;
  curr_config->magnetometer_sens_mat_s33 = self->magnetometer_sens_mat_s33;

  curr_config->y_rate_sensor_offset = self->y_rate_sensor_offset;
  curr_config->y_rate_sensor_transform_angle_alpha = self->y_rate_sensor_transform_angle_alpha;
  curr_config->y_rate_sensor_transform_angle_gamma = self->y_rate_sensor_transform_angle_gamma;

  curr_config->detumbling_spin_gain = self->detumbling_spin_gain;
  curr_config->detumbling_damping_gain = self->detumbling_damping_gain;
  curr_config->ref_spin_rate = self->ref_spin_rate;

  curr_config->y_momentum_control_gain = self->y_momentum_control_gain;
  curr_config->y_momentum_damping_gain = self->y_momentum_damping_gain;
  curr_config->y_momentum_proportional_gain = self->y_momentum_proportional_gain;
  curr_config->y_momentum_derivative_gain = self->y_momentum_derivative_gain;
  curr_config->ref_wheel_momentum = self->ref_wheel_momentum;
  curr_config->ref_wheel_momentum_init_pitch = self->ref_wheel_momentum_init_pitch;
  curr_config->ref_wheel_torque_init_pitch = self->ref_wheel_torque_init_pitch;

  curr_config->moment_of_inertia_xx = self->moment_of_inertia_xx;
  curr_config->moment_of_inertia_yy = self->moment_of_inertia_yy;
  curr_config->moment_of_inertia_zz = self->moment_of_inertia_zz;
  curr_config->moment_of_inertia_xy = self->moment_of_inertia_xy;
  curr_config->moment_of_inertia_xz = self->moment_of_inertia_xz;
  curr_config->moment_of_inertia_yz = self->moment_of_inertia_yz;

  curr_config->magnetometer_rate_filter_system_noise = self->magnetometer_rate_filter_system_noise;
  curr_config->EKF_system_noise = self->EKF_system_noise;
  curr_config->css_meas_noise = self->css_meas_noise;
  curr_config->sun_sensor_meas_noise = self->sun_sensor_meas_noise;
  curr_config->nadir_sensor_meas_noise = self->nadir_sensor_meas_noise;
  curr_config->magnetometer_sensor_meas_noise = self->magnetometer_sensor_meas_noise;
  curr_config->mask_senors_usage = self->mask_senors_usage;
  curr_config->sun_and_nadir_sampling_period = self->sun_and_nadir_sampling_period;
  tlm_size = 236;
  return curr_config;
}

static adcs_EDAC_and_latchup_counters_t* adcs_send_EDAC_and_latchup_counters(adcs_t *self)
{
  /* generated */
  adcs_EDAC_and_latchup_counters_t* EDAC_and_latchup_counters = (adcs_EDAC_and_latchup_counters_t*)malloc(sizeof(adcs_EDAC_and_latchup_counters_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_EDAC_and_latchup_counters_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)EDAC_and_latchup_counters + i;
      *pos = rand_val;
    }
  }
  tlm_size = 10;
  return EDAC_and_latchup_counters;
}

static adcs_start_up_mode_t* adcs_send_start_up_mode(adcs_t *self)
{
  adcs_start_up_mode_t* start_up_mode = (adcs_start_up_mode_t*)malloc(sizeof(adcs_start_up_mode_t));
  *start_up_mode = self->start_up_mode;
  tlm_size = 1;
  return start_up_mode;
}

static adcs_image_capture_and_save_operations_statuses_t* adcs_send_image_capture_and_save_operations_statuses(adcs_t *self)
{
  /* generated */
  adcs_image_capture_and_save_operations_statuses_t* image_capture_and_save_operations_statuses = (adcs_image_capture_and_save_operations_statuses_t*)malloc(sizeof(adcs_image_capture_and_save_operations_statuses_t));
  if (self->control_mode == ATTITUDE_CONTROL_DETUMBLING_CONTROL) {
    srand(time(NULL));

    int i;
    for (i = 0; i < sizeof(adcs_image_capture_and_save_operations_statuses_t); i++) {
      uint8_t rand_val = rand() % 255;
      uint8_t* pos = (uint8_t *)image_capture_and_save_operations_statuses + i;
      *pos = rand_val;
    }
  }
  tlm_size = 16;
  return image_capture_and_save_operations_statuses;
}

static adcs_file_info_t* adcs_send_file_info(adcs_t *self)
{
  /* keep track of it in the model */
  adcs_file_info_t* file_info = (adcs_file_info_t*)malloc(sizeof(adcs_file_info_t));
  tlm_size = 22;
  return file_info;
}

static adcs_file_block_CRC_t* adcs_send_file_block_CRC(adcs_t *self)
{
  /* keep track of it in the model */
  adcs_file_block_CRC_t* file_block_CRC = (adcs_file_block_CRC_t*)malloc(sizeof(adcs_file_block_CRC_t));
  tlm_size = 6;
  return file_block_CRC;
}

//static unsigned char block2048[8*256];
static void* adcs_send_data_block(adcs_t *self)
{
  /* keep track of it during execution */
  tlm_size = 256;
  return (void*) malloc(256);
}

static adcs_uploaded_program_status_t* adcs_send_uploaded_program_status(adcs_t *self)
{
  /* keep track of it during execution */
  adcs_uploaded_program_status_t* uploaded_program_status = (adcs_uploaded_program_status_t*)malloc(sizeof(adcs_uploaded_program_status_t));
  tlm_size = 3;
  return uploaded_program_status;
}

static adcs_flash_program_list_t* adcs_send_flash_program_list(adcs_t *self)
{
  /* keep track of it during execution */
  adcs_flash_program_list_t* flash_program_list = (adcs_flash_program_list_t*)malloc(sizeof(adcs_flash_program_list_t));
  tlm_size = 497;
  return flash_program_list;
}

/************************************************************************/
/* Initialization														*/
/************************************************************************/
void initialize_adcs_lpc_local( adcs_t *adcs )
{
  adcs->data = (void *) 0;
  adcs->reset_count = 0;
  adcs->mode = &mode_mock_up;
  //adcs->adcs_handle_command_id = &adcs_handle_command_id;
  adcs->adcs_reset = &adcs_reset;
  adcs->adcs_set_unix_time = &adcs_set_unix_time;
  adcs->adcs_set_run_mode = &adcs_set_run_mode;
  adcs->adcs_select_logged_data = &adcs_select_logged_data;
  adcs->adcs_select_element_power_control = &adcs_select_element_power_control;
  adcs->adcs_deploy_magnetometer_boom = &adcs_deploy_magnetometer_boom;
  adcs->adcs_trigger_adcs_loop = &adcs_trigger_adcs_loop;
  adcs->adcs_set_attitude_estimation_mode = &adcs_set_attitude_estimation_mode;
  adcs->adcs_set_attitude_control_mode = &adcs_set_attitude_control_mode;
  adcs->adcs_set_commanded_attitude_angles = &adcs_set_commanded_attitude_angles;
  adcs->adcs_set_wheel_speed = &adcs_set_wheel_speed;
  adcs->adcs_set_magnetorquer_output = &adcs_set_magnetorquer_output;
  adcs->adcs_set_sgp4_orbit_parameters = &adcs_set_sgp4_orbit_parameters;
  adcs->adcs_set_curr_config = &adcs_set_curr_config;
  adcs->adcs_set_magnetorquer_config = &adcs_set_magnetorquer_config;
  adcs->adcs_set_wheel_config = &adcs_set_wheel_config;
  adcs->adcs_set_css_config = &adcs_set_css_config;
  adcs->adcs_set_sun_sensor_config = &adcs_set_sun_sensor_config;
  adcs->adcs_set_nadir_sensor_config = &adcs_set_nadir_sensor_config;
  adcs->adcs_set_magnetometer_config = &adcs_set_magnetometer_config;
  adcs->adcs_set_rate_sensor_config = &adcs_set_rate_sensor_config;
  adcs->adcs_set_detumbling_control = &adcs_set_detumbling_control;
  adcs->adcs_set_Y_momentum_control = &adcs_set_Y_momentum_control;
  adcs->adcs_set_moment_of_inertia = &adcs_set_moment_of_inertia;
  adcs->adcs_set_estimation_parameters = &adcs_set_estimation_parameters;
  adcs->adcs_save_config = &adcs_save_config;
  adcs->adcs_save_orbit_params = &adcs_save_orbit_params;
  adcs->adcs_set_start_up_mode = &adcs_set_start_up_mode;
  adcs->adcs_capture_and_save_image = &adcs_capture_and_save_image;
  adcs->adcs_reset_file_list = &adcs_reset_file_list;
  adcs->adcs_advance_file_list_index = &adcs_advance_file_list_index;
  adcs->adcs_init_download = &adcs_init_download;
  adcs->adcs_advance_file_read_pointer = &adcs_advance_file_read_pointer;
  adcs->adcs_erase_file = &adcs_erase_file;
  adcs->adcs_erase_all_files = &adcs_erase_all_files;
  adcs->adcs_set_boot_index = &adcs_set_boot_index;
  adcs->adcs_erase_program = &adcs_erase_program;
  adcs->adcs_upload_program_block = &adcs_upload_program_block;
  adcs->adcs_finalize_program_upload = &adcs_finalize_program_upload;
  adcs->adcs_send_identification = &adcs_send_identification;
  adcs->adcs_send_comm_status = &adcs_send_comm_status;
  adcs->adcs_send_ack = &adcs_send_ack;
  adcs->adcs_send_reset_cause = &adcs_send_reset_cause;
  adcs->adcs_send_selected_element_power_control = &adcs_send_selected_element_power_control;
  adcs->adcs_send_power_and_temperature_meas = &adcs_send_power_and_temperature_meas;
  adcs->adcs_send_cubesense_current_measurement = &adcs_send_cubesense_current_measurement;
  adcs->adcs_send_cubecontrol_current_measurements = &adcs_send_cubecontrol_current_measurements;
  adcs->adcs_send_peripheral_current_and_temperature_meas = &adcs_send_peripheral_current_and_temperature_meas;
  adcs->adcs_send_state = &adcs_send_state;
  adcs->adcs_send_curr_time = &adcs_send_curr_time;
  adcs->adcs_send_curr_state = &adcs_send_curr_state;
  adcs->adcs_send_estimated_attitude_angles = &adcs_send_estimated_attitude_angles;
  adcs->adcs_send_estimated_angular_rates = &adcs_send_estimated_angular_rates;
  adcs->adcs_send_satellite_velocity = &adcs_send_satellite_velocity;
  adcs->adcs_send_satellite_position = &adcs_send_satellite_position;
  adcs->adcs_send_measurements = &adcs_send_measurements;
  adcs->adcs_send_magnetic_field_vector = &adcs_send_magnetic_field_vector;
  adcs->adcs_send_coarse_sun_vector = &adcs_send_coarse_sun_vector;
  adcs->adcs_send_fine_sun_vector = &adcs_send_fine_sun_vector;
  adcs->adcs_send_nadir_vector = &adcs_send_nadir_vector;
  adcs->adcs_send_rate_sensor_rates = &adcs_send_rate_sensor_rates;
  adcs->adcs_send_wheel_speed_meas = &adcs_send_wheel_speed_meas;
  adcs->adcs_send_actuator_command = &adcs_send_actuator_command;
  adcs->adcs_send_magnetorquer_command = &adcs_send_magnetorquer_command;
  adcs->adcs_send_wheel_speed_command = &adcs_send_wheel_speed_command;
  adcs->adcs_send_raw_sensor_measurements = &adcs_send_raw_sensor_measurements;
  adcs->adcs_send_raw_nadir_sensor = &adcs_send_raw_nadir_sensor;
  adcs->adcs_send_raw_sun_sensor = &adcs_send_raw_sun_sensor;
  adcs->adcs_send_raw_css = &adcs_send_raw_css;
  adcs->adcs_send_raw_magnetometer = &adcs_send_raw_magnetometer;
  adcs->adcs_send_raw_GPS_status = &adcs_send_raw_GPS_status;
  adcs->adcs_send_raw_GPS_time = &adcs_send_raw_GPS_time;
  adcs->adcs_send_raw_GPS_x = &adcs_send_raw_GPS_x;
  adcs->adcs_send_raw_GPS_y = &adcs_send_raw_GPS_y;
  adcs->adcs_send_raw_GPS_z = &adcs_send_raw_GPS_z;
  adcs->adcs_send_estimation_data = &adcs_send_estimation_data;
  adcs->adcs_send_IGRF_modelled_magnetic_field_vector = &adcs_send_IGRF_modelled_magnetic_field_vector;
  adcs->adcs_send_modelled_sun_vector = &adcs_send_modelled_sun_vector;
  adcs->adcs_send_estimated_gyro_bias = &adcs_send_estimated_gyro_bias;
  adcs->adcs_send_estimated_innovation_vector = &adcs_send_estimated_innovation_vector;
  adcs->adcs_send_quaternion_error_vector = &adcs_send_quaternion_error_vector;
  adcs->adcs_send_quaternion_covariance = &adcs_send_quaternion_covariance;
  adcs->adcs_send_angular_rate_covariance = &adcs_send_angular_rate_covariance;
  adcs->adcs_send_ACP_execution_state = &adcs_send_ACP_execution_state;
  adcs->adcs_send_ACP_execution_times = &adcs_send_ACP_execution_times;
  adcs->adcs_send_sgp4_orbit_parameters = &adcs_send_sgp4_orbit_parameters;
  adcs->adcs_send_curr_config = &adcs_send_curr_config;
  adcs->adcs_send_EDAC_and_latchup_counters = &adcs_send_EDAC_and_latchup_counters;
  adcs->adcs_send_start_up_mode = &adcs_send_start_up_mode;
  adcs->adcs_send_image_capture_and_save_operations_statuses = &adcs_send_image_capture_and_save_operations_statuses;
  adcs->adcs_send_file_info = &adcs_send_file_info;
  adcs->adcs_send_file_block_CRC = &adcs_send_file_block_CRC;
  adcs->adcs_send_data_block = &adcs_send_data_block;
  adcs->adcs_send_uploaded_program_status = &adcs_send_uploaded_program_status;
  adcs->adcs_send_flash_program_list = &adcs_send_flash_program_list;
  adcs->telecommand_processed = 0;
  adcs->last_telecommand_id = 0;
  adcs->telecommands_received = 0;
  adcs->telemetry_requests_received = 0;
  adcs->run_mode = RUN_MODE_OFF;
  adcs->cube_control_signal_power_selection = POWER_DEPENDS_ON_CURRENT_CONTROL_MODE;
  adcs->cube_control_motor_power_selection = POWER_DEPENDS_ON_CURRENT_CONTROL_MODE;
  adcs->cube_sense_power_selection = POWER_DEPENDS_ON_CURRENT_CONTROL_MODE;
  adcs->cube_motor_power_selection = POWER_DEPENDS_ON_CURRENT_CONTROL_MODE;
  adcs->gps_lna_power_selection = POWER_DEPENDS_ON_CURRENT_CONTROL_MODE;
  adcs->boom_timeout = 0;
  adcs->attitude_estimation_mode = ATTITUDE_ESTIMATION_NO_ESTIMATION;
  adcs->control_mode = ATTITUDE_CONTROL_NO_CONTROL; // this one
  adcs->control_mode_override = 1; // actually is bool
  adcs->control_mode_timeout = 0;
  adcs->commanded_attitude_angles_roll = 0;
  adcs->commanded_attitude_angles_pitch = 0;
  adcs->commanded_attitude_angles_yaw = 0;
  adcs->wheel_x_speed = 0;
  adcs->wheel_y_speed = 0;
  adcs->wheel_z_speed = 0;
  adcs->magnetorquer_x_duty_cycle = 0;
  adcs->magnetorquer_y_duty_cycle = 0;
  adcs->magnetorquer_z_duty_cycle = 0;
  adcs->sgp4_inclination = 0.0;
  adcs->sgp4_eccentricity = 0.0;
  adcs->sgp4_right_ascension = 0.0;
  adcs->sgp4_perigee = 0.0;
  adcs->sgp4_b_star_drag_term = 0.0;
  adcs->sgp4_mean_motion = 0.0;
  adcs->sgp4_mean_anomaly = 0.0;
  adcs->sgp4_epoch = 0.0;
  adcs->magnetorquer_1_configuration = MAGNETORQUER_AXIS_NOT_USED;
  adcs->magnetorquer_2_configuration = MAGNETORQUER_AXIS_NOT_USED;
  adcs->magnetorquer_3_configuration = MAGNETORQUER_AXIS_NOT_USED;
  adcs->magnetorquer_max_x_dipole_moment = 0;
  adcs->magnetorquer_max_y_dipole_moment = 0;
  adcs->magnetorquer_max_z_dipole_moment = 0;
  adcs->magnetorquer_max_on_time = 0;
  adcs->magnetorquer_on_time_resolution = 0;
  adcs->magnetic_control_selection = MAGNETIC_CONTROL_SELECTION_SIGNAL_BOTH;
  adcs->wheel_transform_angle_alpha = 0;
  adcs->wheel_transform_angle_gamma = 0;
  adcs->wheel_polarity = ROTATION_POLARITY_INCREASE_Y_MOMENTUM;
  adcs->wheel_inertia = 0;
  adcs->wheel_max_torque = 0;
  adcs->wheel_max_speed = 0;
  adcs->wheel_control_gain = 0;
  adcs->wheel_backup_control_mode = 0; // actually is bool
  adcs->css1_config = MAGNETORQUER_AXIS_NOT_USED;
  adcs->css2_config = MAGNETORQUER_AXIS_NOT_USED;
  adcs->css3_config = MAGNETORQUER_AXIS_NOT_USED;
  adcs->css4_config = MAGNETORQUER_AXIS_NOT_USED;
  adcs->css5_config = MAGNETORQUER_AXIS_NOT_USED;
  adcs->css6_config = MAGNETORQUER_AXIS_NOT_USED;
  adcs->css1_rel_scale = 0;
  adcs->css2_rel_scale = 0;
  adcs->css3_rel_scale = 0;
  adcs->css4_rel_scale = 0;
  adcs->css5_rel_scale = 0;
  adcs->css6_rel_scale = 0;
  adcs->css_autofill_missing_facets = 0; // actually is bool
  adcs->css_threshold = 0;
  adcs->sun_sensor_transform_angle_alpha = 0;
  adcs->sun_sensor_transform_angle_beta = 0;
  adcs->sun_sensor_transform_angle_gamma = 0;
  adcs->sun_detection_threshold = 0;
  adcs->sun_sensor_auto_adjust_mode = 0; // actually is bool
  adcs->sun_sensor_exposure_time = 0;
  adcs->sun_sensor_AGC = 0;
  adcs->sun_blue_gain = 0;
  adcs->sun_red_gain = 0;
  adcs->sun_boresight_x = 0;
  adcs->sun_boresight_y = 0;
  adcs->sun_shift = 0; // actually is bool
  adcs->nadir_sensor_transform_angle_alpha = 0;
  adcs->nadir_sensor_transform_angle_beta = 0;
  adcs->nadir_sensor_transform_angle_gamma = 0;
  adcs->nadir_detection_threshold = 0;
  adcs->nadir_sensor_auto_adjust = 0; // actually is bool
  adcs->nadir_sensor_exposure_time = 0;
  adcs->nadir_sensor_AGC = 0;
  adcs->nadir_blue_gain = 0;
  adcs->nadir_red_gain = 0;
  adcs->nadir_boresight_x = 0;
  adcs->nadir_boresight_y = 0;
  adcs->nadir_shift = 0; // actually is bool
  adcs->min_x_of_area_1 = 0;
  adcs->max_x_of_area_1 = 0;
  adcs->min_y_of_area_1 = 0;
  adcs->max_y_of_area_1 = 0;
  adcs->min_x_of_area_2 = 0;
  adcs->max_x_of_area_2 = 0;
  adcs->min_y_of_area_2 = 0;
  adcs->max_y_of_area_2 = 0;
  adcs->min_x_of_area_3 = 0;
  adcs->max_x_of_area_3 = 0;
  adcs->min_y_of_area_3 = 0;
  adcs->max_y_of_area_3 = 0;
  adcs->min_x_of_area_4 = 0;
  adcs->max_x_of_area_4 = 0;
  adcs->min_y_of_area_4 = 0;
  adcs->max_y_of_area_4 = 0;
  adcs->min_x_of_area_5 = 0;
  adcs->max_x_of_area_5 = 0;
  adcs->min_y_of_area_5 = 0;
  adcs->max_y_of_area_5 = 0;
  adcs->magnetometer_transform_angle_alpha = 0;
  adcs->magnetometer_transform_angle_beta = 0;
  adcs->magnetometer_transform_angle_gamma = 0;
  adcs->magnetometer_channel1_offset = 0;
  adcs->magnetometer_channel2_offset = 0;
  adcs->magnetometer_channel3_offset = 0;
  adcs->magnetometer_sens_mat_s11 = 0;
  adcs->magnetometer_sens_mat_s12 = 0;
  adcs->magnetometer_sens_mat_s13 = 0;
  adcs->magnetometer_sens_mat_s21 = 0;
  adcs->magnetometer_sens_mat_s22 = 0;
  adcs->magnetometer_sens_mat_s23 = 0;
  adcs->magnetometer_sens_mat_s31 = 0;
  adcs->magnetometer_sens_mat_s32 = 0;
  adcs->magnetometer_sens_mat_s33 = 0;
  adcs->y_rate_sensor_offset = 0;
  adcs->y_rate_sensor_transform_angle_alpha = 0;
  adcs->y_rate_sensor_transform_angle_gamma = 0;
  adcs->detumbling_spin_gain = 0.0;
  adcs->detumbling_damping_gain = 0.0;
  adcs->ref_spin_rate = 0;
  adcs->y_momentum_control_gain = 0.0;
  adcs->y_momentum_damping_gain = 0.0;
  adcs->y_momentum_proportional_gain = 0.0;
  adcs->y_momentum_derivative_gain = 0.0;
  adcs->ref_wheel_momentum = 0.0;
  adcs->ref_wheel_momentum_init_pitch = 0.0;
  adcs->ref_wheel_torque_init_pitch = 0;
  adcs->moment_of_inertia_xx = 0.0;
  adcs->moment_of_inertia_yy = 0.0;
  adcs->moment_of_inertia_zz = 0.0;
  adcs->moment_of_inertia_xy = 0.0;
  adcs->moment_of_inertia_xz = 0.0;
  adcs->moment_of_inertia_yz = 0.0;
  adcs->magnetometer_rate_filter_system_noise = 0.0;
  adcs->EKF_system_noise = 0.0;
  adcs->css_meas_noise = 0.0;
  adcs->sun_sensor_meas_noise = 0.0;
  adcs->nadir_sensor_meas_noise = 0.0;
  adcs->magnetometer_sensor_meas_noise = 0.0;
  adcs->mask_senors_usage = 0;
  adcs->sun_and_nadir_sampling_period = 0;
  adcs->start_up_mode = START_UP_MODE_OFF;
  adcs->boot_index = 0;
}

adcs_t adcs;
uint8_t command_id;
uint8_t* recv_struct;

const int buttonPin1 = 2;
const int buttonPin2 = 3;
const int buttonPin3 = 4;
const int buttonPin4 = 5;

const int ledPinGreen = 6;
const int ledPinRed = 7;

const int rheostat1 = 0;
const int rheostat2 = 1;

const int RGBLedRed = 10;
const int RGBLedGreen = 9;
const int RGBLedBlue = 8;

const int joyHPin = 15;
const int joyVPin = 14;
const int joySel = 11;

const int photoRes1 = 11;
const int photoRes2 = 10;

void setup() {
  Serial.begin(9600);

  pinMode(buttonPin1, INPUT);
  pinMode(buttonPin2, INPUT);
  pinMode(buttonPin3, INPUT);
  pinMode(buttonPin4, INPUT);

  pinMode(rheostat1, INPUT);
  pinMode(rheostat2, INPUT);

  pinMode(RGBLedRed, OUTPUT);
  pinMode(RGBLedGreen, OUTPUT);
  pinMode(RGBLedBlue, OUTPUT);

  pinMode(ledPinGreen, OUTPUT);
  pinMode(ledPinRed, OUTPUT);

  pinMode(joySel, INPUT);
  pinMode(joyHPin, INPUT);
  pinMode(joyVPin, INPUT);
  
  pinMode(photoRes1, INPUT);
  pinMode(photoRes2, INPUT);

  tlm_size = 0;
  recv_struct = (uint8_t*) malloc(259); // the largest struct for telecommand structure
  initialize_adcs_lpc_local( &adcs );
  //adcs_unix_time_t set_time;
  //set_time.unix_time = 12345678;
  //set_time.millis = 12345;
  //adcs_handle_command_id(&adcs, 2, (void*) &set_time);
  Wire.begin(4);                // join i2c bus with address #4
  Wire.onReceive(receiveEvent); // register event
  Wire.onRequest(requestEvent); // register event

}

void loop() {
  buttonState1 = digitalRead(buttonPin1);
  buttonState2 = digitalRead(buttonPin2);
  buttonState3 = digitalRead(buttonPin3);
  buttonState4 = digitalRead(buttonPin4);

  rheostatValue1 = analogRead(rheostat1);
  rheostatValue2 = analogRead(rheostat2);

  if (buttonState1 == HIGH) {
    adcs.control_mode = ATTITUDE_CONTROL_NO_CONTROL;
    digitalWrite(ledPinGreen, LOW);
    digitalWrite(ledPinRed, LOW);
    Serial.print("ADCS Mode: ");
    Serial.println(adcs.mode(&adcs));
  }
  if (buttonState2 == HIGH) {
    adcs.control_mode = ATTITUDE_CONTROL_DETUMBLING_CONTROL;
    digitalWrite(ledPinGreen, LOW);
    digitalWrite(ledPinRed, HIGH);
    Serial.print("ADCS Mode: ");
    Serial.println(adcs.mode(&adcs));
  }
  if (buttonState3 == HIGH) {
    adcs.control_mode = ATTITUDE_CONTROL_INIT_PITCH;
    digitalWrite(ledPinRed, LOW);
    digitalWrite(ledPinGreen, HIGH);
    Serial.print("ADCS Mode: ");
    Serial.println(adcs.mode(&adcs));
  }
  if (buttonState4 == HIGH) {
    adcs.control_mode = ATTITUDE_CONTROL_STEADY_STATE;
    digitalWrite(ledPinRed, LOW);
    digitalWrite(ledPinGreen, HIGH);
    Serial.print("ADCS Mode: ");
    Serial.println(adcs.mode(&adcs));
  }

  joyH = analogRead(joyHPin);
  joyV = analogRead(joyVPin);
  joyButton = digitalRead(joySel);
  
  photoResValue1 = analogRead(photoRes1);
  photoResValue2 = analogRead(photoRes2);

  int tlm_size_temp = tlm_size;

  adcs_power_and_temperature_meas_t* power_and_temperature_meas = adcs.adcs_send_power_and_temperature_meas(&adcs);

  tlm_size = tlm_size_temp;

  Serial.print("Cubesense 3V3 current: ");
  Serial.println(power_and_temperature_meas->cubesense_3V3_current);
  
  Serial.print("Cubesense nadir sram current: ");
  Serial.println(power_and_temperature_meas->cubesense_nadir_sram_current);
  
  Serial.print("Cubesense sun sram current: ");
  Serial.println(power_and_temperature_meas->cubesense_sun_sram_current);
  
  
  Serial.print("Cubecontrol 3V3 current: ");
  Serial.println(power_and_temperature_meas->cubecontrol_3V3_current);
  
  Serial.print("Cubecontrol 5V current: ");
  Serial.println(power_and_temperature_meas->cubecontrol_5V_current);
  
  Serial.print("Cubecontrol Vbat current: ");
  Serial.println(power_and_temperature_meas->cubecontrol_Vbat_current);
  
  
  Serial.print("Periphery magnetorquer current: ");
  Serial.println(power_and_temperature_meas->magnetorquer_current);
  
  Serial.print("Periphery momentum wheel current: ");
  Serial.println(power_and_temperature_meas->momentum_wheel_current);
  
  Serial.print("Periphery rate sensor temperature: ");
  Serial.println(power_and_temperature_meas->rate_sensor_temperature);
  
  Serial.print("Periphery ARM CPU temperature: ");
  Serial.println(power_and_temperature_meas->ARM_CPU_temperature);

  Serial.println();
  Serial.println();
  Serial.println();

  free(power_and_temperature_meas);

  delay(100);
}

void receiveEvent(int howMany)
{
  int i = 0;

  command_id = Wire.read();
  Serial.println(command_id);
  if (command_id > 127)
  {
    return;
  }
  else
  {
    while ( Wire.available())
    {
      *(recv_struct + i) = Wire.read();
      i++;
    }
    //adcs_unix_time_t* time = (adcs_unix_time_t*) recv_struct;
    //Serial.print("unix_time: ");
    //Serial.println(time->unix_time);
    //Serial.print("millis: ");
    //Serial.println(time->millis);
    adcs_handle_command_id(&adcs, command_id, (void*)recv_struct);
  }

  /*
  }
  if (command_id < 128)
  {
    adcs_handle_command_id(&adcs, command_id, (void*)recv_struct);
  }
  */
}
// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent()
{
  if (command_id > 127)
  {
    uint8_t* ret_struct = (uint8_t*)adcs_handle_command_id(&adcs, command_id, NULL);
    Wire.write(ret_struct, tlm_size);
    free(ret_struct);
  }
}
