/*
 * Copyright (C) 2015  Brendan Bruner, Oleg Oleynikov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file adcs
 * @author Brendan Bruner
 * @author Oleg Oleynikov
 * @date Mar 02, 2015
 */
#ifndef INCLUDE_ADCS_H_
#define INCLUDE_ADCS_H_

#include <stdint.h>

/* Required typedefs */
typedef struct adcs_t adcs_t;


typedef unsigned char adcs_mode_t;
/* Possible return values of adcs_t::mode */
#define ADCS_MODE_STABILIZED	((adcs_mode_t) 0)
#define ADCS_MODE_DETUMBLE		((adcs_mode_t) 1)
#define ADCS_MODE_IDLE			((adcs_mode_t) 2)


typedef uint8_t adcs_reset_t;
/* Reset Types */
#define RESET_MCU_PERIPHERALS ((adcs_reset_t) 1)
#define RESET_MCU ((adcs_reset_t) 2)
#define RESET_ADCS_PROCESSING_LIBRARY ((adcs_reset_t) 3)
#define RESET_ADCS_NODES ((adcs_reset_t) 4)


typedef struct adcs_unix_time_t adcs_unix_time_t;


typedef uint8_t adcs_run_mode_t;
/* State of ADCS execution loop as returned by the ADCS */
#define RUN_MODE_OFF ((adcs_run_mode_t) 0)
#define RUN_MODE_ENABLED ((adcs_run_mode_t) 1)
#define RUN_MODE_TRIGGERED ((adcs_run_mode_t) 2)


typedef struct adcs_logged_data_t adcs_logged_data_t;


typedef struct adcs_power_control_t adcs_power_control_t;
typedef uint8_t adcs_element_power_control_t;
/* Power control values */
#define POWER_PERMANENTLY_OFF ((adcs_element_power_control_t) 0)
#define POWER_PERMANENTLY_ON ((adcs_element_power_control_t) 1)
#define POWER_DEPENDS_ON_CURRENT_CONTROL_MODE ((adcs_element_power_control_t) 2)
#define POWER_SIM_AUTO_MODE ((adcs_element_power_control_t) 3)


typedef uint8_t adcs_deploy_magnetometer_boom_timeout_t;


typedef uint8_t adcs_attitude_estimation_mode_t;
/* Attitude estimation modes */
#define ATTITUDE_ESTIMATION_NO_ESTIMATION ((adcs_attitude_estimation_mode_t) 0)
#define ATTITUDE_ESTIMATION_MEMS_RATE_SENSING ((adcs_attitude_estimation_mode_t) 1)
#define ATTITUDE_ESTIMATION_MAGNETOMETER_RATE_FILTER ((adcs_attitude_estimation_mode_t) 2)
#define ATTITUDE_ESTIMATION_MAGNETOMETER_RATE_FILTER_PITCH_ESTIMATION ((adcs_attitude_estimation_mode_t) 3)
#define ATTITUDE_ESTIMATION_FULL_STATE_EKF ((adcs_attitude_estimation_mode_t) 4)
#define ATTITUDE_ESTIMATION_MAGNETOMETER_AND_FINE_SUN_TRIAD ((adcs_attitude_estimation_mode_t) 5)


typedef struct adcs_attitude_control_mode_t adcs_attitude_control_mode_t;
typedef uint8_t adcs_attitude_control_mode_value_t;
/* Attitude control mode */
#define ATTITUDE_CONTROL_NO_CONTROL ((adcs_attitude_control_mode_value_t) 0)
#define ATTITUDE_CONTROL_DETUMBLING_CONTROL ((adcs_attitude_control_mode_value_t) 1)
#define ATTITUDE_CONTROL_INIT_PITCH ((adcs_attitude_control_mode_value_t) 2)
#define ATTITUDE_CONTROL_STEADY_STATE ((adcs_attitude_control_mode_value_t) 3)


typedef struct adcs_commanded_attitude_angles_t adcs_commanded_attitude_angles_t;


typedef struct adcs_wheel_speed_t adcs_wheel_speed_t;


typedef struct adcs_magnetorquer_output_t adcs_magnetorquer_output_t;


typedef struct adcs_sgp4_orbit_parameters_t adcs_sgp4_orbit_parameters_t;


typedef struct adcs_curr_config_t adcs_curr_config_t;
typedef uint8_t adcs_magnetorquer_magnetic_control_value_t;
/* Magnetorquer axis config */
#define MAGNETORQUER_AXIS_POSITIVE_X ((adcs_magnetorquer_magnetic_control_value_t) 0)
#define MAGNETORQUER_AXIS_NEGATIVE_X ((adcs_magnetorquer_magnetic_control_value_t) 1)
#define MAGNETORQUER_AXIS_POSITIVE_Y ((adcs_magnetorquer_magnetic_control_value_t) 2)
#define MAGNETORQUER_AXIS_NEGATIVE_Y ((adcs_magnetorquer_magnetic_control_value_t) 3)
#define MAGNETORQUER_AXIS_POSITIVE_Z ((adcs_magnetorquer_magnetic_control_value_t) 4)
#define MAGNETORQUER_AXIS_NEGATIVE_Z ((adcs_magnetorquer_magnetic_control_value_t) 5)
#define MAGNETORQUER_AXIS_NOT_USED ((adcs_magnetorquer_magnetic_control_value_t) 6)

typedef uint8_t adcs_magnetic_control_value_t;
/* Magnetic control selection */
#define MAGNETIC_CONTROL_SELECTION_SIGNAL_BOTH ((adcs_magnetic_control_value_t) 0)
#define MAGNETIC_CONTROL_SELECTION_MOTOR_MEAS_SIGNAL_CONTROL ((adcs_magnetic_control_value_t) 1)
#define MAGNETIC_CONTROL_SELECTION_MOTOR_BOTH ((adcs_magnetic_control_value_t) 2)

typedef uint8_t adcs_rotation_polarity_value_t;
/* Rotation polarity */
#define ROTATION_POLARITY_INCREASE_Y_MOMENTUM ((adcs_rotation_polarity_value_t) 0)
#define ROTATION_POLARITY_DECREASE_Y_MOMENTUM ((adcs_rotation_polarity_value_t) 1)

typedef uint8_t adcs_start_up_mode_t;
/* Startup mode values */
#define START_UP_MODE_OFF ((adcs_start_up_mode_t) 0)
#define START_UP_MODE_AUTO_DETUMBLE ((adcs_start_up_mode_t) 1)


typedef struct adcs_magnetorquer_config_t adcs_magnetorquer_config_t;
typedef struct adcs_wheel_config_t adcs_wheel_config_t;
typedef struct adcs_css_config_t adcs_css_config_t;
typedef struct adcs_sun_sensor_config_t adcs_sun_sensor_config_t;
typedef struct adcs_nadir_sensor_config_t adcs_nadir_sensor_config_t;
typedef struct adcs_magnetometer_config_t adcs_magnetometer_config_t;
typedef struct adcs_rate_sensor_config_t adcs_rate_sensor_config_t;
typedef struct adcs_detumbling_control_t adcs_detumbling_control_t;
typedef struct adcs_Y_momentum_control_t adcs_Y_momentum_control_t;
typedef struct adcs_moment_of_inertia_t adcs_moment_of_inertia_t;
typedef struct adcs_estimation_parameters_t adcs_estimation_parameters_t;


typedef struct adcs_capture_and_save_image_t adcs_capture_and_save_image_t;
typedef uint8_t adcs_camera_t;
/* Camera selection */
#define CAMERA_NADIR ((adcs_camera_t) 0)
#define CAMERA_SUN ((adcs_camera_t) 1)


//typedef struct adcs_init_file_download_t {char filename[13];} adcs_init_file_download_t;
typedef uint16_t adcs_next_block_num_to_read_t;


typedef uint8_t adcs_boot_index_t;


typedef struct adcs_upload_program_block_t adcs_upload_program_block_t;


typedef struct adcs_finalize_program_upload_t adcs_finalize_program_upload_t;




typedef struct adcs_identification_t adcs_identification_t;


typedef struct adcs_comm_status_t adcs_comm_status_t;


typedef struct adcs_ack_t adcs_ack_t;


typedef uint8_t adcs_reset_cause_t;
/* Reset cause */
#define RESET_CAUSE_POWER_ON_RESET ((adcs_reset_cause_t) 0)
#define RESET_CAUSE_BROWN_OUT_ON_REGULATED ((adcs_reset_cause_t) 1)
#define RESET_CAUSE_BROWN_OUT_ON_UNREGULATED ((adcs_reset_cause_t) 2)
#define RESET_CAUSE_EXTERNAL_WATCHDOG ((adcs_reset_cause_t) 3)
#define RESET_CAUSE_EXTERNAL ((adcs_reset_cause_t) 4)
#define RESET_CAUSE_WATCHDOG ((adcs_reset_cause_t) 5)
#define RESET_CAUSE_LOCKUP_SYSTEM ((adcs_reset_cause_t) 6)
#define RESET_CAUSE_LOCKUP ((adcs_reset_cause_t) 7)
#define RESET_CAUSE_SYSTEM_REQUEST ((adcs_reset_cause_t) 8)
#define RESET_CAUSE_UNKNOWN_CAUSE ((adcs_reset_cause_t) 9)


typedef struct adcs_power_and_temperature_meas_t adcs_power_and_temperature_meas_t;


typedef struct adcs_cubesense_current_measurements_t adcs_cubesense_current_measurements_t;
typedef struct adcs_cubecontrol_current_measurements_t adcs_cubecontrol_current_measurements_t;
typedef struct adcs_peripheral_current_and_temperature_meas_t adcs_peripheral_current_and_temperature_meas_t;


typedef struct adcs_state_t adcs_state_t;

typedef struct adcs_curr_time_t adcs_curr_time_t;
typedef struct adcs_curr_state_t adcs_curr_state_t;
typedef struct adcs_estimated_attitude_angles_t adcs_estimated_attitude_angles_t;
typedef struct adcs_estimated_angular_rates_t adcs_estimated_angular_rates_t;
typedef struct adcs_satellite_velocity_t adcs_satellite_velocity_t;
typedef struct adcs_satellite_position_t adcs_satellite_position_t;

typedef struct adcs_measurements_t adcs_measurements_t;

typedef struct adcs_magnetic_field_vector_t adcs_magnetic_field_vector_t;
typedef struct adcs_coarse_sun_vector_t adcs_coarse_sun_vector_t;
typedef struct adcs_fine_sun_vector_t adcs_fine_sun_vector_t;
typedef struct adcs_nadir_vector_t adcs_nadir_vector_t;
typedef struct adcs_rate_sensor_rates_t adcs_rate_sensor_rates_t;
typedef struct adcs_wheel_speed_meas_t adcs_wheel_speed_meas_t;


typedef struct adcs_actuator_command_t adcs_actuator_command_t;

typedef struct adcs_magnetorquer_command_t adcs_magnetorquer_command_t;
typedef struct adcs_wheel_speed_command_t adcs_wheel_speed_command_t;


typedef struct adcs_raw_sensor_measurements_t adcs_raw_sensor_measurements_t;
typedef uint8_t adcs_busy_status_t;
/* Nadir and sun busy status */
#define BUSY_STATUS_IDLE ((adcs_busy_status_t) 0)
#define BUSY_STATUS_WAITING_TO_CAPTURE ((adcs_busy_status_t) 1)
#define BUSY_STATUS_CAPTURING ((adcs_busy_status_t) 2)
#define BUSY_STATUS_DETECTING ((adcs_busy_status_t) 3)

typedef uint8_t adcs_nadir_detection_result_t;
/* Nadir detection result */
#define NADIR_RESULT_NO_ERROR ((adcs_nadir_detection_result_t) 0)
#define NADIR_RESULT_CAMERA_TIMEOUT ((adcs_nadir_detection_result_t) 1)
#define NADIR_RESULT_SRAM_OVERCURRENT_DETECTED ((adcs_nadir_detection_result_t) 4)
#define NADIR_RESULT_TOO_MANY_EDGES ((adcs_nadir_detection_result_t) 10)
#define NADIR_RESULT_NOT_ENOUGH_EDGES ((adcs_nadir_detection_result_t) 11)
#define NADIR_RESULT_MATRIX_INVERSION_ERROR ((adcs_nadir_detection_result_t) 12)
#define NADIR_RESULT_BAD_HORIZON_FIT ((adcs_nadir_detection_result_t) 13)
#define NADIR_RESULT_NO_DETECTION_PERFORMED ((adcs_nadir_detection_result_t) 255)

typedef uint8_t adcs_sun_detection_result_t;
/* Sun detection result */
#define SUN_RESULT_NO_ERROR ((adcs_sun_detection_result_t) 0)
#define SUN_RESULT_CAMERA_TIMEOUT ((adcs_sun_detection_result_t) 1)
#define SUN_RESULT_SRAM_OVERCURRENT_DETECTED ((adcs_sun_detection_result_t) 4)
#define SUN_RESULT_SUN_NOT_FOUND ((adcs_sun_detection_result_t) 20)
#define SUN_RESULT_NO_DETECTION_PERFORMED ((adcs_sun_detection_result_t) 255)

typedef uint8_t adcs_GPS_solution_status_t;
/* GPS solution status */
#define GPS_SOLUTION_SOLUTION_COMPUTED ((adcs_GPS_solution_status_t) 0)
#define GPS_SOLUTION_INSUFFICIENT_OBSERVATIONS ((adcs_GPS_solution_status_t) 1)
#define GPS_SOLUTION_NO_CONVERGENCE ((adcs_GPS_solution_status_t) 2)
#define GPS_SOLUTION_SINGULARITY_AT_PARAMETERS_MATRIX ((adcs_GPS_solution_status_t) 3)
#define GPS_SOLUTION_COVARIANCE_TRACE_EXCEEDS_MAXIMUM ((adcs_GPS_solution_status_t) 4)
#define GPS_SOLUTION_NOT_YET_CONVERGED_FROM_COLD_START ((adcs_GPS_solution_status_t) 5)
#define GPS_SOLUTION_HEIGHT_OR_VELOCITY_LIMITS_EXCEEDED ((adcs_GPS_solution_status_t) 6)
#define GPS_SOLUTION_VARIANCE_EXCEEDS_LIMITS ((adcs_GPS_solution_status_t) 7)
#define GPS_SOLUTION_LARGE_RESIDUALS_MAKE_POSITION_UNRELIABLE ((adcs_GPS_solution_status_t) 8)
#define GPS_SOLUTION_CALCULATING_COMPARISON_TO_USER_PROVIDED ((adcs_GPS_solution_status_t) 9)
#define GPS_SOLUTION_THE_FIXED_POSITION_S_INVALID ((adcs_GPS_solution_status_t) 10)
#define GPS_SOLUTION_POSITION_TYPE_IS_UNAUTHORIZED ((adcs_GPS_solution_status_t) 11)

typedef struct adcs_raw_nadir_sensor_t adcs_raw_nadir_sensor_t;
typedef struct adcs_raw_sun_sensor_t adcs_raw_sun_sensor_t;
typedef struct adcs_raw_css_t adcs_raw_css_t;
typedef struct adcs_raw_magnetometer_t adcs_raw_magnetometer_t;
typedef struct adcs_raw_GPS_status_t adcs_raw_GPS_status_t;
typedef struct adcs_raw_GPS_time_t adcs_raw_GPS_time_t;
typedef struct adcs_raw_GPS_x_t adcs_raw_GPS_x_t;
typedef struct adcs_raw_GPS_y_t adcs_raw_GPS_y_t;
typedef struct adcs_raw_GPS_z_t adcs_raw_GPS_z_t;


typedef struct adcs_estimation_data_t adcs_estimation_data_t;

typedef struct adcs_IGRF_modelled_magnetic_field_vector_t adcs_IGRF_modelled_magnetic_field_vector_t;
typedef struct adcs_modelled_sun_vector_t adcs_modelled_sun_vector_t;
typedef struct adcs_estimated_gyro_bias_t adcs_estimated_gyro_bias_t;
typedef struct adcs_estimated_innovation_vector_t adcs_estimated_innovation_vector_t;
typedef struct adcs_quaternion_error_vector_t adcs_quaternion_error_vector_t;
typedef struct adcs_quaternion_covariance_t adcs_quaternion_covariance_t;
typedef struct adcs_angular_rate_covariance_t adcs_angular_rate_covariance_t;


typedef struct adcs_ACP_execution_state_t adcs_ACP_execution_state_t;
typedef uint8_t adcs_current_execution_point_t;
/* ACP current execution point */
#define ACP_CUR_EXEC_POINT_INIT ((adcs_current_execution_point_t) 0)
#define ACP_CUR_EXEC_POINT_IDLE ((adcs_current_execution_point_t) 1)
#define ACP_CUR_EXEC_POINT_SENSOR_OR_ACTUATOR_COMMS ((adcs_current_execution_point_t) 2)
#define ACP_CUR_EXEC_POINT_ADCS_UPDATE ((adcs_current_execution_point_t) 3)
#define ACP_CUR_EXEC_POINT_PERIPHERAL_POWER_COMMANDS ((adcs_current_execution_point_t) 4)
#define ACP_CUR_EXEC_POINT_CPU_TEMP_SAMPLING ((adcs_current_execution_point_t) 5)
#define ACP_CUR_EXEC_POINT_IMAGE_DOWNLOAD ((adcs_current_execution_point_t) 6)
#define ACP_CUR_EXEC_POINT_IMAGE_COMPRESSION ((adcs_current_execution_point_t) 7)
#define ACP_CUR_EXEC_POINT_SAVING_IMAGE_TO_SD_CARD ((adcs_current_execution_point_t) 8)
#define ACP_CUR_EXEC_POINT_LOGGING ((adcs_current_execution_point_t) 9)
#define ACP_CUR_EXEC_POINT_LOG_FILE_COMPRESSION ((adcs_current_execution_point_t) 10)
#define ACP_CUR_EXEC_POINT_SAVING_LOG_TO_SD_CARD ((adcs_current_execution_point_t) 11)
#define ACP_CUR_EXEC_POINT_WRITING_TO_FLASH ((adcs_current_execution_point_t) 12)


typedef struct adcs_ACP_execution_times_t adcs_ACP_execution_times_t;


typedef struct adcs_EDAC_and_latchup_counters_t adcs_EDAC_and_latchup_counters_t;


typedef struct adcs_image_capture_and_save_operations_statuses_t adcs_image_capture_and_save_operations_statuses_t;
typedef uint8_t adcs_image_save_status_t;
/* Image save status */
#define IMAGE_SAVE_STATUS_NO_ERROR ((adcs_image_save_status_t) 0)
#define IMAGE_SAVE_STATUS_TIMEOUT_WAITING_FOR_SENSOR_TO_BECOME_AVAILABLE ((adcs_image_save_status_t) 1)
#define IMAGE_SAVE_STATUS_TIMEOUT_WAITING_FOR_NEXT_FRAME_TO_BECOME_READY ((adcs_image_save_status_t) 2)
#define IMAGE_SAVE_STATUS_CHECKSUM_MISMATCH_BETWEEN_DOWNLOADED_AND_CUBESENSE_FRAMES ((adcs_image_save_status_t) 3)
#define IMAGE_SAVE_STATUS_ERROR_WRITING_TO_SD_CARD ((adcs_image_save_status_t) 4)


typedef struct adcs_file_info_t adcs_file_info_t;


typedef struct adcs_file_block_CRC_t adcs_file_block_CRC_t;


typedef struct adcs_uploaded_program_status_t adcs_uploaded_program_status_t;


typedef struct adcs_flash_program_list_t adcs_flash_program_list_t;



/**
 * @struct adcs_t
 * @brief
 * 		Structure used with all API for the ADCS subsystem.
 *
 * @details
 * 		Structure used with all API for the ADCS subsystem.
 * 		This structured must be initialized before use. For example,
 * 			@code
 * 			initialize_adcs_lpc_csp( &adcs );
 * 			@endcode
 * 		Where the variable "adcs" is of type adcs_t. This is one of several
 * 		possible initialization functions.
 *
 * @attention
 * 		Initializing multiple adcs_t structures to do IO with the same
 * 		board will result in undefined behavior.
 *
 * @var adcs_t::mode
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_mode_t mode( adcs_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it.
 * 		The return value is one of:
 * 			@code
 * 			ADCS_MODE_STABILIZED
 * 			ADCS_MODE_DETUMBLE
 *			ADCS_MODE_IDLE
 * 			@endcode
 * 		the returned value indicates the mode of the adcs.
 *
 * @var adcs_t::reset_count
 * 		Keeps track of how many times the reset command has
 * 		been sent to ADCS
 *
 * @var adcs_t::adcs_reset
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_reset( adcs_t *, adcs_reset_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_reset_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_unix_time
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_set_unix_time( adcs_t *, adcs_unix_time_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_unix_time_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_set_run_mode
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_set_run_mode( adcs_t *, adcs_run_mode_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t adcs_run_mode_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_select_logged_data
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_select_logged_data( adcs_t *, adcs_logged_data_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_logged_data_t structure which contains the relevant telecommand information.

 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_select_element_power_control
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_select_element_power_control( adcs_t *, adcs_power_control_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_power_control_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_deploy_magnetometer_boom
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_deploy_magnetometer_boom( adcs_t *, adcs_deploy_magnetometer_boom_timeout_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_deploy_magnetometer_boom_timeout_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_trigger_adcs_loop
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_trigger_adcs_loop( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it.
 *
 * 		The returned value is a message 'ADCS loop triggered'.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_set_attitude_estimation_mode
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_set_attitude_estimation_mode( adcs_t *, adcs_attitude_estimation_mode_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_attitude_estimation_mode_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_set_attitude_control_mode
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_set_attitude_control_mode( adcs_t *, adcs_attitude_control_mode_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_attitude_control_mode_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_set_commanded_attitude_angles
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_set_commanded_attitude_angles( adcs_t *, adcs_commanded_attitude_angles_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_commanded_attitude_angles_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_set_wheel_speed
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_set_wheel_speed( adcs_t *, adcs_wheel_speed_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_wheel_speed_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_set_magnetorquer_output
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_set_magnetorquer_output( adcs_t *, adcs_magnetorquer_output_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_magnetorquer_output_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 *
 * @var adcs_t::adcs_set_sgp4_orbit_parameters
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_set_sgp4_orbit_parameters( adcs_t *, adcs_sgp4_orbit_parameters_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_sgp4_orbit_parameters_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_set_curr_config
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_set_curr_config( adcs_t *, adcs_curr_config_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_curr_config_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_save_config
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_save_config( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it.
 *
 * 		The returned value is the message 'Config saved'.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_save_orbit_params
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_save_orbit_params( adcs_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it.
 *
 * 		The returned value is the message 'Orbit parameters saved'.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_set_start_up_mode
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_set_start_up_mode( adcs_t *, adcs_start_up_mode_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_start_up_mode_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_capture_and_save_image
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_capture_and_save_image( adcs_t *, adcs_capture_and_save_image_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_capture_and_save_image_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_reset_file_list
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_reset_file_list( adcs_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it.
 *
 * 		The returned value is the message 'File list reset'.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_advance_file_list_index
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_advance_file_list_index( adcs_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it.

 * 		The returned value is the message 'File list index advanced'.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_init_download
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_init_download( adcs_t *, char*, int );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it, a character array and an integer.
 *
 *		The array is the file name of the file that has to be downloaded, and int is length of the name, which has to be 13
 * 		The returned value is the file name.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_advance_file_read_pointer
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_advance_file_read_pointer( adcs_t *, adcs_next_block_num_to_read_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and the adcs_next_block_num_to_read_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_erase_file
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_erase_file( adcs_t *, char *, int );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it a character array and an integer
 *
 * 		The array is the file name and the integer is the length of the name, which has to be 13
 * 		The returned value is the file name.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_erase_all_files
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_erase_all_files( adcs_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is the message 'Erased all files'.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_set_boot_index
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_set_boot_index( adcs_t *, adcs_boot_index_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_boot_index_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_erase_program
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_erase_program( adcs_t *, adcs_boot_index_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_boot_index_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_upload_program_block
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_upload_program_block( adcs_t *, adcs_upload_program_block_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_upload_program_block_t structure which contains the relevant telecommand information.
 *
 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_finalize_program_upload
 * 		A function pointer with prototype:
 * 			@code
 * 			char* adcs_finalize_program_upload( adcs_t *, adcs_finalize_program_upload_t * );
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it and adcs_finalize_program_upload_t structure which contains the relevant telecommand information.

 * 		The returned value is the ascii representation of the second input structure.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_send_identification
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_identification_t adcs_send_identification( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_send_comm_status
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_comm_status_t adcs_send_comm_status( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_send_ack
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_ack_t adcs_send_ack( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * 	@var adcs_t::adcs_send_reset_cause
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_reset_cause_t adcs_send_reset_cause( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * 	@var adcs_t::adcs_send_selected_element_power_control
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_power_control_t adcs_send_selected_element_power_control( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * 	@var adcs_t::adcs_send_power_and_temperature_meas
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_power_and_temperature_meas_t adcs_send_power_and_temperature_meas( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * 	@var adcs_t::adcs_send_state
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_state_t adcs_send_state( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * 	@var adcs_t::adcs_send_measurements
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_measurements_t adcs_send_measurements( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * 	@var adcs_t::adcs_send_actuator_command
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_actuator_command_t adcs_send_actuator_command( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * 	@var adcs_t::adcs_send_raw_sensor_measurements
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_raw_sensor_measurements_t adcs_send_raw_sensor_measurements( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * 	@var adcs_t::adcs_send_estimation_data
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_estimation_data_t adcs_send_estimation_data( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * 	@var adcs_t::adcs_send_ACP_execution_state
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_ACP_execution_state_t adcs_send_ACP_execution_state( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * 	@var adcs_t::adcs_send_ACP_execution_times
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_ACP_execution_times_t adcs_send_ACP_execution_times( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * 	@var adcs_t::adcs_send_sgp4_orbit_parameters
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_sgp4_orbit_parameters_t adcs_send_sgp4_orbit_parameters( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * 	@var adcs_t::adcs_send_curr_config
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_curr_config_t adcs_send_curr_config( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * 	@var adcs_t::adcs_send_EDAC_and_latchup_counters
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_EDAC_and_latchup_counters_t adcs_send_EDAC_and_latchup_counters( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_send_start_up_mode
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_start_up_mode_t adcs_send_start_up_mode( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_send_image_capture_and_save_operations_statuses
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_image_capture_and_save_operations_statuses_t adcs_send_image_capture_and_save_operations_statuses( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_send_file_info
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_file_info_t adcs_send_file_info( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_send_file_block_CRC
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_file_block_CRC_t adcs_send_file_block_CRC( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_send_data_block
 * 		A function pointer with prototype:
 * 			@code
 * 			void* adcs_send_data_block( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a pointer to random data, which is 2048 bits long since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_send_uploaded_program_status
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_uploaded_program_status_t adcs_send_uploaded_program_status( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 *
 * @var adcs_t::adcs_send_flash_program_list
 * 		A function pointer with prototype:
 * 			@code
 * 			adcs_flash_program_list_t adcs_send_flash_program_list( adcs_t *);
 * 			@endcode
 * 		This method takes a pointer to the adcs_t structure used to access it
 * 		The returned value is a random structure since the prime purpose, at this stage, is to demonstrate functionality.
 * 		This functionality is just a placeholder designed to demonstrate
 * 		and test communication. Later, this will be changed to proper functionality
 */
struct adcs_t
{
	void *data;
	unsigned int reset_count;
	adcs_mode_t (*mode)( adcs_t * );
	/* Telecommands */
	void* (*adcs_handle_command_id)(adcs_t, int, void*);
	void (*adcs_reset)( adcs_t *, adcs_reset_t * );
	void (*adcs_set_unix_time)( adcs_t *, adcs_unix_time_t * );
	void (*adcs_set_run_mode)( adcs_t *, adcs_run_mode_t * );
	void (*adcs_select_logged_data) ( adcs_t*, adcs_logged_data_t * );
	void (*adcs_select_element_power_control) (adcs_t*, adcs_power_control_t *);
	void (*adcs_deploy_magnetometer_boom) (adcs_t*, adcs_deploy_magnetometer_boom_timeout_t *);
	void (*adcs_trigger_adcs_loop) (adcs_t *);
	void (*adcs_set_attitude_estimation_mode) (adcs_t *, adcs_attitude_estimation_mode_t *);
	void (*adcs_set_attitude_control_mode) (adcs_t *, adcs_attitude_control_mode_t *);
	void (*adcs_set_commanded_attitude_angles) (adcs_t *, adcs_commanded_attitude_angles_t *);
	void (*adcs_set_wheel_speed) (adcs_t *, adcs_wheel_speed_t *);
	void (*adcs_set_magnetorquer_output) (adcs_t *, adcs_magnetorquer_output_t *);
	void (*adcs_set_sgp4_orbit_parameters) (adcs_t *, adcs_sgp4_orbit_parameters_t *);
	void (*adcs_set_curr_config) (adcs_t *, adcs_curr_config_t *);
	void (*adcs_set_magnetorquer_config)(adcs_t *self, adcs_magnetorquer_config_t *);
	void (*adcs_set_wheel_config)(adcs_t *self, adcs_wheel_config_t *);
	void (*adcs_set_css_config)(adcs_t *self, adcs_css_config_t *);
	void (*adcs_set_sun_sensor_config)(adcs_t *self, adcs_sun_sensor_config_t *);
	void (*adcs_set_nadir_sensor_config)(adcs_t *self, adcs_nadir_sensor_config_t *);
	void (*adcs_set_magnetometer_config)(adcs_t *self, adcs_magnetometer_config_t *);
	void (*adcs_set_rate_sensor_config)(adcs_t *self, adcs_rate_sensor_config_t *);
	void (*adcs_set_detumbling_control)(adcs_t *self, adcs_detumbling_control_t *);
	void (*adcs_set_Y_momentum_control)(adcs_t *self, adcs_Y_momentum_control_t *);
	void (*adcs_set_moment_of_inertia)(adcs_t *self, adcs_moment_of_inertia_t *);
	void (*adcs_set_estimation_parameters)(adcs_t *self, adcs_estimation_parameters_t *);
	void (*adcs_save_config) (adcs_t *);
	void (*adcs_save_orbit_params) (adcs_t *);
	void (*adcs_set_start_up_mode) (adcs_t *, adcs_start_up_mode_t *);
	void (*adcs_capture_and_save_image) (adcs_t *, adcs_capture_and_save_image_t *);
	void (*adcs_reset_file_list) (adcs_t *);
	void (*adcs_advance_file_list_index) (adcs_t *);
	void (*adcs_init_download) (adcs_t *, char*, int);
	void (*adcs_advance_file_read_pointer) (adcs_t *, adcs_next_block_num_to_read_t *);
	void (*adcs_erase_file) (adcs_t *, char*, int);
	void (*adcs_erase_all_files) (adcs_t *);
	void (*adcs_set_boot_index) (adcs_t *, adcs_boot_index_t *);
	void (*adcs_erase_program) (adcs_t *, adcs_boot_index_t *);
	void (*adcs_upload_program_block) (adcs_t *, adcs_upload_program_block_t *);
	void (*adcs_finalize_program_upload) (adcs_t *, adcs_finalize_program_upload_t *);
	/* Telemetry requests */
	adcs_identification_t* (*adcs_send_identification) (adcs_t *);
	adcs_comm_status_t* (*adcs_send_comm_status) (adcs_t *);
	adcs_ack_t* (*adcs_send_ack) (adcs_t *);
	adcs_reset_cause_t* (*adcs_send_reset_cause) (adcs_t *);
	adcs_power_control_t* (*adcs_send_selected_element_power_control) (adcs_t *);
	adcs_power_and_temperature_meas_t* (*adcs_send_power_and_temperature_meas) (adcs_t *);
	adcs_cubesense_current_measurements_t* (*adcs_send_cubesense_current_measurement)(adcs_t *);
	adcs_cubecontrol_current_measurements_t* (*adcs_send_cubecontrol_current_measurements)(adcs_t *);
	adcs_peripheral_current_and_temperature_meas_t* (*adcs_send_peripheral_current_and_temperature_meas)(adcs_t *);
	adcs_state_t* (*adcs_send_state) (adcs_t *);
	adcs_unix_time_t* (*adcs_send_curr_time)(adcs_t *);
	adcs_curr_state_t* (*adcs_send_curr_state)(adcs_t *);
	adcs_estimated_attitude_angles_t* (*adcs_send_estimated_attitude_angles)(adcs_t *);
	adcs_estimated_angular_rates_t* (*adcs_send_estimated_angular_rates)(adcs_t *);
	adcs_satellite_velocity_t* (*adcs_send_satellite_velocity)(adcs_t *);
	adcs_satellite_position_t* (*adcs_send_satellite_position)(adcs_t *);
	adcs_measurements_t* (*adcs_send_measurements) (adcs_t *);
	adcs_magnetic_field_vector_t* (*adcs_send_magnetic_field_vector)(adcs_t *);
	adcs_coarse_sun_vector_t* (*adcs_send_coarse_sun_vector)(adcs_t *);
	adcs_fine_sun_vector_t* (*adcs_send_fine_sun_vector)(adcs_t *);
	adcs_nadir_vector_t* (*adcs_send_nadir_vector)(adcs_t *);
	adcs_rate_sensor_rates_t* (*adcs_send_rate_sensor_rates)(adcs_t *);
	adcs_wheel_speed_meas_t* (*adcs_send_wheel_speed_meas)(adcs_t *);
	adcs_actuator_command_t* (*adcs_send_actuator_command) (adcs_t *);
	adcs_magnetorquer_command_t* (*adcs_send_magnetorquer_command)(adcs_t *);
	adcs_wheel_speed_command_t* (*adcs_send_wheel_speed_command)(adcs_t *);
	adcs_raw_sensor_measurements_t* (*adcs_send_raw_sensor_measurements) (adcs_t *);
	adcs_raw_nadir_sensor_t* (*adcs_send_raw_nadir_sensor)(adcs_t *);
	adcs_raw_sun_sensor_t* (*adcs_send_raw_sun_sensor)(adcs_t *);
	adcs_raw_css_t* (*adcs_send_raw_css)(adcs_t *);
	adcs_raw_magnetometer_t* (*adcs_send_raw_magnetometer)(adcs_t *);
	adcs_raw_GPS_status_t* (*adcs_send_raw_GPS_status)(adcs_t *);
	adcs_raw_GPS_time_t* (*adcs_send_raw_GPS_time)(adcs_t *);
	adcs_raw_GPS_x_t* (*adcs_send_raw_GPS_x)(adcs_t *);
	adcs_raw_GPS_y_t* (*adcs_send_raw_GPS_y)(adcs_t *);
	adcs_raw_GPS_z_t* (*adcs_send_raw_GPS_z)(adcs_t *);
	adcs_estimation_data_t* (*adcs_send_estimation_data) (adcs_t *);
	adcs_IGRF_modelled_magnetic_field_vector_t* (*adcs_send_IGRF_modelled_magnetic_field_vector)(adcs_t *);
	adcs_modelled_sun_vector_t* (*adcs_send_modelled_sun_vector)(adcs_t *);
	adcs_estimated_gyro_bias_t* (*adcs_send_estimated_gyro_bias)(adcs_t *);
	adcs_estimated_innovation_vector_t* (*adcs_send_estimated_innovation_vector)(adcs_t *);
	adcs_quaternion_error_vector_t* (*adcs_send_quaternion_error_vector)(adcs_t *);
	adcs_quaternion_covariance_t* (*adcs_send_quaternion_covariance)(adcs_t *);
	adcs_angular_rate_covariance_t* (*adcs_send_angular_rate_covariance)(adcs_t *);
	adcs_ACP_execution_state_t* (*adcs_send_ACP_execution_state) (adcs_t *);
	adcs_ACP_execution_times_t* (*adcs_send_ACP_execution_times) (adcs_t *);
	adcs_sgp4_orbit_parameters_t* (*adcs_send_sgp4_orbit_parameters) (adcs_t *);
	adcs_curr_config_t* (*adcs_send_curr_config) (adcs_t *);
	adcs_EDAC_and_latchup_counters_t* (*adcs_send_EDAC_and_latchup_counters) (adcs_t *);
	adcs_start_up_mode_t* (*adcs_send_start_up_mode) (adcs_t *);
	adcs_image_capture_and_save_operations_statuses_t* (*adcs_send_image_capture_and_save_operations_statuses) (adcs_t *);
	adcs_file_info_t* (*adcs_send_file_info) (adcs_t *);
	adcs_file_block_CRC_t* (*adcs_send_file_block_CRC) (adcs_t *);
	void* (*adcs_send_data_block) (adcs_t *);
	adcs_uploaded_program_status_t* (*adcs_send_uploaded_program_status) (adcs_t *);
	adcs_flash_program_list_t* (*adcs_send_flash_program_list) (adcs_t *);
        /* Data stored on the unit */
	uint8_t last_telecommand_id;
	uint8_t telecommand_processed;
	uint16_t telecommands_received;
	uint16_t telemetry_requests_received;
	uint32_t unix_time;
	uint16_t millis;
	adcs_run_mode_t run_mode;
	adcs_element_power_control_t cube_control_signal_power_selection;
	adcs_element_power_control_t cube_control_motor_power_selection;
	adcs_element_power_control_t cube_sense_power_selection;
	adcs_element_power_control_t cube_motor_power_selection;
	adcs_element_power_control_t gps_lna_power_selection;
	adcs_deploy_magnetometer_boom_timeout_t boom_timeout;
	adcs_attitude_estimation_mode_t attitude_estimation_mode;
	adcs_attitude_control_mode_value_t control_mode;
	uint8_t control_mode_override; // actually is bool
	uint16_t control_mode_timeout;
	int16_t commanded_attitude_angles_roll;
	int16_t commanded_attitude_angles_pitch;
	int16_t commanded_attitude_angles_yaw;
	int16_t wheel_x_speed;
	int16_t wheel_y_speed;
	int16_t wheel_z_speed;
	int16_t magnetorquer_x_duty_cycle;
	int16_t magnetorquer_y_duty_cycle;
	int16_t magnetorquer_z_duty_cycle;
	double sgp4_inclination;
	double sgp4_eccentricity;
	double sgp4_right_ascension;
	double sgp4_perigee;
	double sgp4_b_star_drag_term;
	double sgp4_mean_motion;
	double sgp4_mean_anomaly;
	double sgp4_epoch;
	adcs_magnetorquer_magnetic_control_value_t magnetorquer_1_configuration;
	adcs_magnetorquer_magnetic_control_value_t magnetorquer_2_configuration;
	adcs_magnetorquer_magnetic_control_value_t magnetorquer_3_configuration;
	uint16_t magnetorquer_max_x_dipole_moment;
	uint16_t magnetorquer_max_y_dipole_moment;
	uint16_t magnetorquer_max_z_dipole_moment;
	uint8_t magnetorquer_max_on_time;
	uint16_t magnetorquer_on_time_resolution;
	adcs_magnetic_control_value_t magnetic_control_selection;
	int16_t wheel_transform_angle_alpha;
	int16_t wheel_transform_angle_gamma;
	adcs_rotation_polarity_value_t wheel_polarity;
	uint16_t wheel_inertia;
	uint16_t wheel_max_torque;
	uint16_t wheel_max_speed;
	uint8_t wheel_control_gain;
	uint8_t wheel_backup_control_mode; // actually is bool
	adcs_magnetorquer_magnetic_control_value_t css1_config;
	adcs_magnetorquer_magnetic_control_value_t css2_config;
	adcs_magnetorquer_magnetic_control_value_t css3_config;
	adcs_magnetorquer_magnetic_control_value_t css4_config;
	adcs_magnetorquer_magnetic_control_value_t css5_config;
	adcs_magnetorquer_magnetic_control_value_t css6_config;
	uint8_t css1_rel_scale;
	uint8_t css2_rel_scale;
	uint8_t css3_rel_scale;
	uint8_t css4_rel_scale;
	uint8_t css5_rel_scale;
	uint8_t css6_rel_scale;
	uint8_t css_autofill_missing_facets; // actually is bool
	uint8_t css_threshold;
	int16_t sun_sensor_transform_angle_alpha;
	int16_t sun_sensor_transform_angle_beta;
	int16_t sun_sensor_transform_angle_gamma;
	uint8_t sun_detection_threshold;
	uint8_t sun_sensor_auto_adjust_mode; // actually is bool
	uint8_t sun_sensor_exposure_time;
	uint8_t sun_sensor_AGC;
	uint8_t sun_blue_gain;
	uint8_t sun_red_gain;
	uint16_t sun_boresight_x;
	uint16_t sun_boresight_y;
	uint8_t sun_shift; // actually is bool
	int16_t nadir_sensor_transform_angle_alpha;
	int16_t nadir_sensor_transform_angle_beta;
	int16_t nadir_sensor_transform_angle_gamma;
	uint8_t nadir_detection_threshold;
	uint8_t nadir_sensor_auto_adjust; // actually is bool
	uint8_t nadir_sensor_exposure_time;
	uint8_t nadir_sensor_AGC;
	uint8_t nadir_blue_gain;
	uint8_t nadir_red_gain;
	uint16_t nadir_boresight_x;
	uint16_t nadir_boresight_y;
	uint8_t nadir_shift; // actually is bool
	uint16_t min_x_of_area_1;
	uint16_t max_x_of_area_1;
	uint16_t min_y_of_area_1;
	uint16_t max_y_of_area_1;
	uint16_t min_x_of_area_2;
	uint16_t max_x_of_area_2;
	uint16_t min_y_of_area_2;
	uint16_t max_y_of_area_2;
	uint16_t min_x_of_area_3;
	uint16_t max_x_of_area_3;
	uint16_t min_y_of_area_3;
	uint16_t max_y_of_area_3;
	uint16_t min_x_of_area_4;
	uint16_t max_x_of_area_4;
	uint16_t min_y_of_area_4;
	uint16_t max_y_of_area_4;
	uint16_t min_x_of_area_5;
	uint16_t max_x_of_area_5;
	uint16_t min_y_of_area_5;
	uint16_t max_y_of_area_5;
	int16_t magnetometer_transform_angle_alpha;
	int16_t magnetometer_transform_angle_beta;
	int16_t magnetometer_transform_angle_gamma;
	int16_t magnetometer_channel1_offset;
	int16_t magnetometer_channel2_offset;
	int16_t magnetometer_channel3_offset;
	int16_t magnetometer_sens_mat_s11;
	int16_t magnetometer_sens_mat_s12;
	int16_t magnetometer_sens_mat_s13;
	int16_t magnetometer_sens_mat_s21;
	int16_t magnetometer_sens_mat_s22;
	int16_t magnetometer_sens_mat_s23;
	int16_t magnetometer_sens_mat_s31;
	int16_t magnetometer_sens_mat_s32;
	int16_t magnetometer_sens_mat_s33;
	int16_t y_rate_sensor_offset;
	int16_t y_rate_sensor_transform_angle_alpha;
	int16_t y_rate_sensor_transform_angle_gamma;
	float detumbling_spin_gain;
	float detumbling_damping_gain;
	int16_t ref_spin_rate;
	float y_momentum_control_gain;
	float y_momentum_damping_gain;
	float y_momentum_proportional_gain;
	float y_momentum_derivative_gain;
	float ref_wheel_momentum;
	float ref_wheel_momentum_init_pitch;
	uint16_t ref_wheel_torque_init_pitch;
	float moment_of_inertia_xx;
	float moment_of_inertia_yy;
	float moment_of_inertia_zz;
	float moment_of_inertia_xy;
	float moment_of_inertia_xz;
	float moment_of_inertia_yz;
	float magnetometer_rate_filter_system_noise;
	float EKF_system_noise;
	float css_meas_noise;
	float sun_sensor_meas_noise;
	float nadir_sensor_meas_noise;
	float magnetometer_sensor_meas_noise;
	uint8_t mask_senors_usage;
	uint8_t sun_and_nadir_sampling_period;
	adcs_start_up_mode_t start_up_mode;
	adcs_boot_index_t boot_index;

};

 /**
  * @struct adcs_unix_time_t
  * @brief
  * 		Structure that defines the unix time of the subsystem
  *
  * @details
  * 		Structure that defines the time that ADCS thinks is now
  *		Can be set with
  *			@code
  *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
  *			@endcode
  *		Also, this is the format of timestamps in telemetry log files.
  *		Essentially identical to adcs_curr_time_t structure, which can be requested through
  *			@code
  *			adcs_send_curr_time(adcs_t*)
  *			@endcode
  *
  * @var adcs_unix_time_t::unix_time
  * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
  * @var adcs_unix_time_t::millis
  * 		Current milliseconds count of the subsystem
  */
struct adcs_unix_time_t
{
	uint32_t unix_time;
	uint16_t millis;
};

/**
 * @struct adcs_logged_data_t
 * @brief
 * 		Structure that defines what kind of data should be logged and how
 *
 * @details
 * 		Lets ADCS know what telemetry data to log, into which file and how often.
 * 		Logging is initiated through
 * 			@code
 * 			adcs_select_logged_data(adcs_t*, adcs_logged_data*)
 * 			@endcode
 *
 * @var adcs_logged_data::log_selection
 * 		32 flags that define which telemetry data should be logged.
 * 		IDs of telemetry that can be logged are from 144 to 175.
 * 		Refer to the Reference Manual for which ID means which telemetry
 * @var adcs_logged_data_t::log_period
 * 		How often telemetry should be logged; period is in seconds
 * @var adcs_logged_data_t::compress
 * 		Boolean flag that specifies whether the telemetry file(s) should be ZIP-compressed
 * @var adcs_logged_data_t::log_identifier
 * 		The title of the log file to be created.
 * 		The actual name of the file is the identifier+00.TLM for the first created file
 */

struct adcs_logged_data_t
{
	uint32_t log_selection;
	uint16_t log_period;
	uint8_t compress; // actually is bool
	char log_identifier[6];
};

/**
 * @struct adcs_power_control_t
 * @brief
 * 		Structure that defines selection of element power control
 *
 * @details
 * 		Structure that controls power mode of CubeControl signal and motor, CubeSense,
 * 		Cube motor and GPS LNA.
 *
 * 		The values for power control are
 * 			@code
 * 			POWER_PERMANENTLY_OFF
 *			POWER_PERMANENTLY_ON
 *			POWER_DEPENDS_ON_CURRENT_CONTROL_MODE
 *			POWER_SIM_AUTO_MODE
 *			@endcode
 *
 *		Can be set with
 *			@code
 *			adcs_select_element_power_control( adcs_t *, adcs_power_control_t * )
 *			@endcode
 *
 * @var adcs_element_power_control_t::cube_control_signal_power_selection
 * 		Set power control mode for CubeControl signal
 * @var adcs_element_power_control_t::cube_control_motor_power_selection
 * 		Set power control mode for CubeControl motor
 * @var adcs_element_power_control_t::cube_sense_power_selection
 * 		Set power control mode for CubeSense
 * @var adcs_element_power_control_t::cube_motor_power_selection
 * 		Set power control mode for Cube motor
 * @var adcs_element_power_control_t::gps_lna_power_selection
 * 		Set power control mode for GPS LNA
 */

struct adcs_power_control_t
{
	adcs_element_power_control_t cube_control_signal_power_selection;
	adcs_element_power_control_t cube_control_motor_power_selection;
	adcs_element_power_control_t cube_sense_power_selection;
	adcs_element_power_control_t cube_motor_power_selection;
	adcs_element_power_control_t gps_lna_power_selection;
};
// TODO: everything from here
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */

struct adcs_attitude_control_mode_t
{
	adcs_attitude_control_mode_value_t control_mode;
	uint8_t override; // actually is bool
	uint16_t timeout;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */

struct adcs_commanded_attitude_angles_t
{
	int16_t roll;
	int16_t pitch;
	int16_t yaw;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */

struct adcs_wheel_speed_t
{
	int16_t x_speed;
	int16_t y_speed;
	int16_t z_speed;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */

struct adcs_magnetorquer_output_t
{
	int16_t x_duty_cycle;
	int16_t y_duty_cycle;
	int16_t z_duty_cycle;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */

struct adcs_sgp4_orbit_parameters_t
{
	double inclination;
	double eccentricity;
	double right_ascension;
	double perigee;
	double b_star_drag_term;
	double mean_motion;
	double mean_anomaly;
	double epoch;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */

struct adcs_curr_config_t
{
	adcs_magnetorquer_magnetic_control_value_t magnetorquer_1_configuration;
	adcs_magnetorquer_magnetic_control_value_t magnetorquer_2_configuration;
	adcs_magnetorquer_magnetic_control_value_t magnetorquer_3_configuration;
	uint16_t magnetorquer_max_x_dipole_moment;
	uint16_t magnetorquer_max_y_dipole_moment;
	uint16_t magnetorquer_max_z_dipole_moment;
	uint8_t magnetorquer_max_on_time;
	uint16_t magnetorquer_on_time_resolution;
	adcs_magnetic_control_value_t magnetic_control_selection;
	int16_t wheel_transform_angle_alpha;
	int16_t wheel_transform_angle_gamma;
	adcs_rotation_polarity_value_t wheel_polarity;
	uint16_t wheel_inertia;
	uint16_t wheel_max_torque;
	uint16_t wheel_max_speed;
	uint8_t wheel_control_gain;
	uint8_t wheel_backup_control_mode; // actually is bool
	adcs_magnetorquer_magnetic_control_value_t css1_config;
	adcs_magnetorquer_magnetic_control_value_t css2_config;
	adcs_magnetorquer_magnetic_control_value_t css3_config;
	adcs_magnetorquer_magnetic_control_value_t css4_config;
	adcs_magnetorquer_magnetic_control_value_t css5_config;
	adcs_magnetorquer_magnetic_control_value_t css6_config;
	uint8_t css1_rel_scale;
	uint8_t css2_rel_scale;
	uint8_t css3_rel_scale;
	uint8_t css4_rel_scale;
	uint8_t css5_rel_scale;
	uint8_t css6_rel_scale;
	uint8_t css_autofill_missing_facets; // actually is bool
	uint8_t css_threshold;
	int16_t sun_sensor_transform_angle_alpha;
	int16_t sun_sensor_transform_angle_beta;
	int16_t sun_sensor_transform_angle_gamma;
	uint8_t sun_detection_threshold;
	uint8_t sun_sensor_auto_adjust_mode; // actually is bool
	uint8_t sun_sensor_exposure_time;
	uint8_t sun_sensor_AGC;
	uint8_t sun_blue_gain;
	uint8_t sun_red_gain;
	uint16_t sun_boresight_x;
	uint16_t sun_boresight_y;
	uint8_t sun_shift; // actually is bool
	int16_t nadir_sensor_transform_angle_alpha;
	int16_t nadir_sensor_transform_angle_beta;
	int16_t nadir_sensor_transform_angle_gamma;
	uint8_t nadir_detection_threshold;
	uint8_t nadir_sensor_auto_adjust; // actually is bool
	uint8_t nadir_sensor_exposure_time;
	uint8_t nadir_sensor_AGC;
	uint8_t nadir_blue_gain;
	uint8_t nadir_red_gain;
	uint16_t nadir_boresight_x;
	uint16_t nadir_boresight_y;
	uint8_t nadir_shift; // actually is bool
	uint16_t min_x_of_area_1;
	uint16_t max_x_of_area_1;
	uint16_t min_y_of_area_1;
	uint16_t max_y_of_area_1;
	uint16_t min_x_of_area_2;
	uint16_t max_x_of_area_2;
	uint16_t min_y_of_area_2;
	uint16_t max_y_of_area_2;
	uint16_t min_x_of_area_3;
	uint16_t max_x_of_area_3;
	uint16_t min_y_of_area_3;
	uint16_t max_y_of_area_3;
	uint16_t min_x_of_area_4;
	uint16_t max_x_of_area_4;
	uint16_t min_y_of_area_4;
	uint16_t max_y_of_area_4;
	uint16_t min_x_of_area_5;
	uint16_t max_x_of_area_5;
	uint16_t min_y_of_area_5;
	uint16_t max_y_of_area_5;
	int16_t magnetometer_transform_angle_alpha;
	int16_t magnetometer_transform_angle_beta;
	int16_t magnetometer_transform_angle_gamma;
	int16_t magnetometer_channel1_offset;
	int16_t magnetometer_channel2_offset;
	int16_t magnetometer_channel3_offset;
	int16_t magnetometer_sens_mat_s11;
	int16_t magnetometer_sens_mat_s12;
	int16_t magnetometer_sens_mat_s13;
	int16_t magnetometer_sens_mat_s21;
	int16_t magnetometer_sens_mat_s22;
	int16_t magnetometer_sens_mat_s23;
	int16_t magnetometer_sens_mat_s31;
	int16_t magnetometer_sens_mat_s32;
	int16_t magnetometer_sens_mat_s33;
	int16_t y_rate_sensor_offset;
	int16_t y_rate_sensor_transform_angle_alpha;
	int16_t y_rate_sensor_transform_angle_gamma;
	float detumbling_spin_gain;
	float detumbling_damping_gain;
	int16_t ref_spin_rate;
	float y_momentum_control_gain;
	float y_momentum_damping_gain;
	float y_momentum_proportional_gain;
	float y_momentum_derivative_gain;
	float ref_wheel_momentum;
	float ref_wheel_momentum_init_pitch;
	uint16_t ref_wheel_torque_init_pitch;
	float moment_of_inertia_xx;
	float moment_of_inertia_yy;
	float moment_of_inertia_zz;
	float moment_of_inertia_xy;
	float moment_of_inertia_xz;
	float moment_of_inertia_yz;
	float magnetometer_rate_filter_system_noise;
	float EKF_system_noise;
	float css_meas_noise;
	float sun_sensor_meas_noise;
	float nadir_sensor_meas_noise;
	float magnetometer_sensor_meas_noise;

	/*
	 * Mask sensors usage contains 4 bool values. From least struct offset to most:
	 * 1. sun sensor
	 * 2. nadir sensor
	 * 3. coarse sun sensor
	 * 4. rate sensor
	 */
	uint8_t mask_senors_usage;
	/*uint8_t mask_sun_sensor; // actually is bool
	uint8_t mask_nadir_sensor; // actually is bool
	uint8_t mask_css; // actually is bool
	uint8_t mask_rate_sensor; // actually is bool*/

	uint8_t sun_and_nadir_sampling_period;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */

struct adcs_magnetorquer_config_t
{
	adcs_magnetorquer_magnetic_control_value_t magnetorquer_1_configuration;
	adcs_magnetorquer_magnetic_control_value_t magnetorquer_2_configuration;
	adcs_magnetorquer_magnetic_control_value_t magnetorquer_3_configuration;
	uint16_t magnetorquer_max_x_dipole_moment;
	uint16_t magnetorquer_max_y_dipole_moment;
	uint16_t magnetorquer_max_z_dipole_moment;
	uint8_t magnetorquer_max_on_time;
	uint16_t magnetorquer_on_time_resolution;
	adcs_magnetic_control_value_t magnetic_control_selection;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_wheel_config_t
{
	int16_t wheel_transform_angle_alpha;
	int16_t wheel_transform_angle_gamma;
	adcs_rotation_polarity_value_t wheel_polarity;
	uint16_t wheel_inertia;
	uint16_t wheel_max_torque;
	uint16_t wheel_max_speed;
	uint8_t wheel_control_gain;
	uint8_t wheel_backup_control_mode; // actually is bool
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_css_config_t
{
	adcs_magnetorquer_magnetic_control_value_t css1_config;
	adcs_magnetorquer_magnetic_control_value_t css2_config;
	adcs_magnetorquer_magnetic_control_value_t css3_config;
	adcs_magnetorquer_magnetic_control_value_t css4_config;
	adcs_magnetorquer_magnetic_control_value_t css5_config;
	adcs_magnetorquer_magnetic_control_value_t css6_config;
	uint8_t css1_rel_scale;
	uint8_t css2_rel_scale;
	uint8_t css3_rel_scale;
	uint8_t css4_rel_scale;
	uint8_t css5_rel_scale;
	uint8_t css6_rel_scale;
	uint8_t css_autofill_missing_facets; // actually is bool
	uint8_t css_threshold;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_sun_sensor_config_t
{
	int16_t sun_sensor_transform_angle_alpha;
	int16_t sun_sensor_transform_angle_beta;
	int16_t sun_sensor_transform_angle_gamma;
	uint8_t sun_detection_threshold;
	uint8_t sun_sensor_auto_adjust_mode; // actually is bool
	uint8_t sun_sensor_exposure_time;
	uint8_t sun_sensor_AGC;
	uint8_t sun_blue_gain;
	uint8_t sun_red_gain;
	uint16_t sun_boresight_x;
	uint16_t sun_boresight_y;
	uint8_t sun_shift; // actually is bool
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_nadir_sensor_config_t
{
	int16_t nadir_sensor_transform_angle_alpha;
	int16_t nadir_sensor_transform_angle_beta;
	int16_t nadir_sensor_transform_angle_gamma;
	uint8_t nadir_detection_threshold;
	uint8_t nadir_sensor_auto_adjust; // actually is bool
	uint8_t nadir_sensor_exposure_time;
	uint8_t nadir_sensor_AGC;
	uint8_t nadir_blue_gain;
	uint8_t nadir_red_gain;
	uint16_t nadir_boresight_x;
	uint16_t nadir_boresight_y;
	uint8_t nadir_shift; // actually is bool
	uint16_t min_x_of_area_1;
	uint16_t max_x_of_area_1;
	uint16_t min_y_of_area_1;
	uint16_t max_y_of_area_1;
	uint16_t min_x_of_area_2;
	uint16_t max_x_of_area_2;
	uint16_t min_y_of_area_2;
	uint16_t max_y_of_area_2;
	uint16_t min_x_of_area_3;
	uint16_t max_x_of_area_3;
	uint16_t min_y_of_area_3;
	uint16_t max_y_of_area_3;
	uint16_t min_x_of_area_4;
	uint16_t max_x_of_area_4;
	uint16_t min_y_of_area_4;
	uint16_t max_y_of_area_4;
	uint16_t min_x_of_area_5;
	uint16_t max_x_of_area_5;
	uint16_t min_y_of_area_5;
	uint16_t max_y_of_area_5;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_magnetometer_config_t
{
	int16_t magnetometer_transform_angle_alpha;
	int16_t magnetometer_transform_angle_beta;
	int16_t magnetometer_transform_angle_gamma;
	int16_t magnetometer_channel1_offset;
	int16_t magnetometer_channel2_offset;
	int16_t magnetometer_channel3_offset;
	int16_t magnetometer_sens_mat_s11;
	int16_t magnetometer_sens_mat_s12;
	int16_t magnetometer_sens_mat_s13;
	int16_t magnetometer_sens_mat_s21;
	int16_t magnetometer_sens_mat_s22;
	int16_t magnetometer_sens_mat_s23;
	int16_t magnetometer_sens_mat_s31;
	int16_t magnetometer_sens_mat_s32;
	int16_t magnetometer_sens_mat_s33;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_rate_sensor_config_t
{
	int16_t y_rate_sensor_offset;
	int16_t y_rate_sensor_transform_angle_alpha;
	int16_t y_rate_sensor_transform_angle_gamma;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_detumbling_control_t
{
	float detumbling_spin_gain;
	float detumbling_damping_gain;
	int16_t ref_spin_rate;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_Y_momentum_control_t
{
	float y_momentum_control_gain;
	float y_momentum_damping_gain;
	float y_momentum_proportional_gain;
	float y_momentum_derivative_gain;
	float ref_wheel_momentum;
	float ref_wheel_momentum_init_pitch;
	uint16_t ref_wheel_torque_init_pitch;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_moment_of_inertia_t
{
	float moment_of_inertia_xx;
	float moment_of_inertia_yy;
	float moment_of_inertia_zz;
	float moment_of_inertia_xy;
	float moment_of_inertia_xz;
	float moment_of_inertia_yz;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_estimation_parameters_t
{
	float magnetometer_rate_filter_system_noise;
	float EKF_system_noise;
	float css_meas_noise;
	float sun_sensor_meas_noise;
	float nadir_sensor_meas_noise;
	float magnetometer_sensor_meas_noise;

	/*
	 * Mask sensors usage contains 4 bool values. From least struct offset to most:
	 * 1. sun sensor
	 * 2. nadir sensor
	 * 3. coarse sun sensor
	 * 4. rate sensor
	 */
	uint8_t mask_senors_usage;
	/*uint8_t mask_sun_sensor; // actually is bool
	uint8_t mask_nadir_sensor; // actually is bool
	uint8_t mask_css; // actually is bool
	uint8_t mask_rate_sensor; // actually is bool*/

	uint8_t sun_and_nadir_sampling_period;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_capture_and_save_image_t
{
	adcs_camera_t camera;
	/*
	 * capture and compress contains two bool values. From least struct offset to most:
	 * 1. capture new image (if false, return contents of CubeSense SRAM)
	 * 2. compress through JPEG
	 */
	uint8_t capture_and_compress;
	/*uint8_t capture; // actually is bool
	uint8_t compress; // actually is bool*/
	char image_identifier[8];
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_upload_program_block_t
{
	uint8_t program_index;
	uint16_t block_number;
	void *data; // has to be 2048 bits (or 256 bytes)
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_finalize_program_upload_t
{
	uint8_t boot_index;
	uint32_t program_length;
	uint16_t checksum;
	char prgram_description[64];
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_identification_t
{
	uint8_t node_type;
	uint8_t interface_version; // should have value of 1
	uint8_t firmvare_version_major;
	uint8_t firmvare_version_minor;
	uint16_t runtime_seconds;
	uint16_t runtime_millis;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_comm_status_t
{
	uint16_t telecommand_counter;
	uint16_t telemetry_request_counter;

	/*
	 * comm_status_booleans contains 4 bool values. From least struct offset to most:
	 * 1. if telecommand buffer was overrun while receiving a telecommand
	 * 2. i2c telemetry file read error, if occurred
	 * 3. UART protocol error if occured
	 * 4. if there has been an incomplete UART message received (start-of-message id received without preceding end-of-message-id)
	 */
	uint8_t comm_status_booleans;
	/*uint8_t telecommand_buffer_overrun; // actually is bool, also one bit long
	uint8_t i2c_TLM_read_error; // actually is bool, also one bit long
	uint8_t UART_protocol_error; // actually is bool, also one bit long
	uint8_t UART_incomplete_message; // actually is bool, also one bit long*/
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_ack_t
{
	uint8_t last_telecommand_id;
	uint8_t processed; // actually is bool
	uint8_t telecommand_error_status;
	uint8_t telecommand_parameter_error_index;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_power_and_temperature_meas_t
{
	uint16_t cubesense_3V3_current;
	uint16_t cubesense_nadir_sram_current;
	uint16_t cubesense_sun_sram_current;
	uint16_t cubecontrol_3V3_current;
	uint16_t cubecontrol_5V_current;
	uint16_t cubecontrol_Vbat_current;
	uint16_t magnetorquer_current;
	uint16_t momentum_wheel_current;
	int8_t rate_sensor_temperature;
	int8_t ARM_CPU_temperature;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_cubesense_current_measurements_t
{
	uint16_t cubesense_3V3_current;
	uint16_t cubesense_nadir_sram_current;
	uint16_t cubesense_sun_sram_current;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_cubecontrol_current_measurements_t
{
	uint16_t cubecontrol_3V3_current;
	uint16_t cubecontrol_5V_current;
	uint16_t cubecontrol_Vbat_current;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_peripheral_current_and_temperature_meas_t
{
	uint16_t magnetorquer_current;
	uint16_t momentum_wheel_current;
	int8_t rate_sensor_temperature;
	int8_t ARM_CPU_temperature;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_state_t
{
	uint32_t current_unix_time;
	uint32_t millis;

	/*
	 * adcs modes contains three values in it. From least struct offset to most:
	 * 1. adcs loop run mode (adcs_run_mode_t, 2 bits)
	 * 2. estimation mode (adcs_attitude_estimation_mode_t, 3 bits)
	 * 3. adcs control mode (adcs_attitude_control_mode_value_t, 3 bits)
	 */
	uint8_t adcs_modes;
	/*adcs_run_mode_t adcs_state; //2 bits long instead of 8
	adcs_attitude_estimation_mode_t attitude_estimation_mode; //3 bits long instead of 8
	adcs_attitude_control_mode_value_t attitude_control_mode; //3s bits long instead of 8*/

	/*
	 * adcs_state_bool_flags_set1 has 8 bool error flags in it. From least struct offset to most:
	 * 1. cube control signal enabled
	 * 2. cube control motor enabled
	 * 3. cube sense enabled
	 * 4. gps receiver enabled
	 * 5. gps LNA enabled
	 * 6. motor driver enabled
	 * 7. magnetometer deployment enabled
	 * 8. config load error
	 */
	uint8_t adcs_state_bool_flags_set1;
	/*uint8_t cubecontrol_signal_enabled; // actually is bool, and is 1 bit long
	uint8_t cubecontrol_motor_enabled; // actually is bool, and is 1 bit long
	uint8_t cubesense_enabled; // actually is bool, and is 1 bit long
	uint8_t gps_reciever_enabled; // actually is bool, and is 1 bit long
	uint8_t gps_lna_power_enabled; // actually is bool, and is 1 bit long
	uint8_t motor_driver_enabled; // actually is bool, and is 1 bit long
	uint8_t magnetometer_deployment_enabled; // actually is bool, and is 1 bit long
	uint8_t config_load_error; // actually is bool, and is 1 bit long*/

	/*
	 * adcs_state_bool_flags_set2 contains 8 boolean flags. From least struct offset to most:
	 * 1. orbit parameters load error
	 * 2. cube sense communication error
	 * 3. cube control signal communication error
	 * 4. cube control motor communication error
	 * 5. cube sense node communication error
	 * 6. cube control signal identification error
	 * 7. cube control motor identification error
	 * 8. magnetometer deployment error
	 */
	uint8_t adcs_state_bool_flags_set2;
	/*uint8_t orbit_params_load_error; // actually is bool, and is 1 bit long
	uint8_t cubesense_comm_error; // actually is bool, and is 1 bit long
	uint8_t cube_control_signal_comm_error; // actually is bool, and is 1 bit long
	uint8_t cubecontrol_motor_comm_error; // actually is bool, and is 1 bit long
	uint8_t cubesense_node_comm_error; // actually is bool, and is 1 bit long
	uint8_t cubecontrol_signal_identification_error; // actually is bool, and is 1 bit long
	uint8_t cubecontrol_motor_identification_error; // actually is bool, and is 1 bit long
	uint8_t magnetometer_deployment_error; // actually is bool, and is 1 bit long*/

	/*
	 * adcs_state_bool_flags_set3 contains 8 boolean flags. From least struct offset to most:
	 * 1. control loop time exceeded
	 * 2. magnetometer range error
	 * 3. sun sensor overcurrent detected
	 * 4. sun sensor busy error
	 * 5. sun sensor detection error
	 * 6. sun sensor range error
	 * 7. nadir sensor overcurrent detected
	 * 8. nadir sensor budy error
	 */
	uint8_t adcs_state_bool_flags_set3;
	/*uint8_t control_loop_time_exceeded; // actually is bool, and is 1 bit long
	uint8_t magnetometer_range_error; // actually is bool, and is 1 bit long
	uint8_t sun_sensor_overcurrent_detected; // actually is bool, and is 1 bit long
	uint8_t sun_sensor_busy_error; // actually is bool, and is 1 bit long
	uint8_t sun_sensor_detection_error; // actually is bool, and is 1 bit long
	uint8_t sun_sensor_range_error; // actually is bool, and is 1 bit long
	uint8_t nadir_sensor_overcurrent_detected; // actually is bool, and is 1 bit long
	uint8_t nadir_sensor_busy_error; // actually is bool, and is 1 bit long*/

	/*
	 * adcs_state_bool_flags_set4 contains 8 boolean flags. From least struct offset to most:
	 * 1. nadir sensor detection error
	 * 2. nadir sensor range error
	 * 3. rate sensor range error
	 * 4. wheel speed range error
	 * 5. coarse sun sensor error
	 * 6. orbit parameters invalid
	 * 7. config invalid
	 * 8. control mode change is not allowed
	 */
	uint8_t adcs_state_bool_flags_set4;
	/*uint8_t nadir_sensor_detection_error; // actually is bool, and is 1 bit long
	uint8_t nadir_sensor_range_error; // actually is bool, and is 1 bit long
	uint8_t rate_sensor_range_error; // actually is bool, and is 1 bit long
	uint8_t wheel_speed_range_error; // actually is bool, and is 1 bit long
	uint8_t coarse_sun_sensor_error; // actually is bool, and is 1 bit long
	uint8_t orbit_parameters_invalid; // actually is bool, and is 1 bit long
	uint8_t config_invalid; // actually is bool, and is 1 bit long
	uint8_t control_mode_change_not_allowed; // actually is bool, and is 1 bit long*/

	/*
	 * adcs_state_bool_flags_set5 contains 8 boolean flags. From least struct offset to most:
	 * 1. estimator change is not allowed
	 * 2. modelled and measured magnetic fields differ in size
	 * 3. SRAM latchup error
	 * 4. SD card error
	 * 5. node recovery error
	 * 6. steady state Y-Thompson deteceted
	 * 7. TRIAD and EKF report same attitude
	 * 8. sun is above local horizon
	 */
	uint8_t adcs_state_bool_flags_set5;
	/*uint8_t estimator_change_not_allowed; // actually is bool, and is 1 bit long
	uint8_t modelled_and_measured_magnetic_fields_differ_in_size; // actually is bool, and is 1 bit long
	uint8_t SRAM_latchup_error; // actually is bool, and is 1 bit long
	uint8_t SD_card_error; // actually is bool, and is 1 bit long
	uint8_t node_recovery_error; // actually is bool, and is 1 bit long
	uint8_t steady_state_Y_thompson_detected; // actually is bool, and is 1 bit long
	uint8_t TRIAD_and_EKF_report_same_attitude; // actually is bool, and is 1 bit long
	uint8_t sun_above_local_horizon; // actually is bool, and is 1 bit long*/

	int16_t commanded_roll_angle;
	int16_t commanded_pitch_angle;
	int16_t commanded_yaw_angle;
	int16_t estimated_roll_angle;
	int16_t estimated_pitch_angle;
	int16_t estimated_yaw_angle;
	int16_t estimated_X_angular_rate;
	int16_t estimated_Y_angular_rate;
	int16_t estimated_Z_angular_rate;
	int16_t X_position;
	int16_t Y_position;
	int16_t Z_position;
	int16_t X_velocity;
	int16_t Y_velocity;
	int16_t Z_velocity;
	int16_t latitude;
	int16_t longitude;
	uint16_t altitude;
};


/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_curr_state_t
{
	uint8_t adcs_modes;
	/*adcs_run_mode_t adcs_state; //2 bits long instead of 8
	adcs_attitude_estimation_mode_t attitude_estimation_mode; //3 bits long instead of 8
	adcs_attitude_control_mode_value_t attitude_control_mode; //3s bits long instead of 8*/

	/*
	 * adcs_state_bool_flags_set1 has 8 bool error flags in it. From least struct offset to most:
	 * 1. cube control signal enabled
	 * 2. cube control motor enabled
	 * 3. cube sense enabled
	 * 4. gps receiver enabled
	 * 5. gps LNA enabled
	 * 6. motor driver enabled
	 * 7. magnetometer deployment enabled
	 * 8. config load error
	 */
	uint8_t adcs_state_bool_flags_set1;
	/*uint8_t cubecontrol_signal_enabled; // actually is bool, and is 1 bit long
	uint8_t cubecontrol_motor_enabled; // actually is bool, and is 1 bit long
	uint8_t cubesense_enabled; // actually is bool, and is 1 bit long
	uint8_t gps_reciever_enabled; // actually is bool, and is 1 bit long
	uint8_t gps_lna_power_enabled; // actually is bool, and is 1 bit long
	uint8_t motor_driver_enabled; // actually is bool, and is 1 bit long
	uint8_t magnetometer_deployment_enabled; // actually is bool, and is 1 bit long
	uint8_t config_load_error; // actually is bool, and is 1 bit long*/

	/*
	 * adcs_state_bool_flags_set2 contains 8 boolean flags. From least struct offset to most:
	 * 1. orbit parameters load error
	 * 2. cube sense communication error
	 * 3. cube control signal communication error
	 * 4. cube control motor communication error
	 * 5. cube sense node communication error
	 * 6. cube control signal identification error
	 * 7. cube control motor identification error
	 * 8. magnetometer deployment error
	 */
	uint8_t adcs_state_bool_flags_set2;
	/*uint8_t orbit_params_load_error; // actually is bool, and is 1 bit long
	uint8_t cubesense_comm_error; // actually is bool, and is 1 bit long
	uint8_t cube_control_signal_comm_error; // actually is bool, and is 1 bit long
	uint8_t cubecontrol_motor_comm_error; // actually is bool, and is 1 bit long
	uint8_t cubesense_node_comm_error; // actually is bool, and is 1 bit long
	uint8_t cubecontrol_signal_identification_error; // actually is bool, and is 1 bit long
	uint8_t cubecontrol_motor_identification_error; // actually is bool, and is 1 bit long
	uint8_t magnetometer_deployment_error; // actually is bool, and is 1 bit long*/

	/*
	 * adcs_state_bool_flags_set3 contains 8 boolean flags. From least struct offset to most:
	 * 1. control loop time exceeded
	 * 2. magnetometer range error
	 * 3. sun sensor overcurrent detected
	 * 4. sun sensor busy error
	 * 5. sun sensor detection error
	 * 6. sun sensor range error
	 * 7. nadir sensor overcurrent detected
	 * 8. nadir sensor budy error
	 */
	uint8_t adcs_state_bool_flags_set3;
	/*uint8_t control_loop_time_exceeded; // actually is bool, and is 1 bit long
	uint8_t magnetometer_range_error; // actually is bool, and is 1 bit long
	uint8_t sun_sensor_overcurrent_detected; // actually is bool, and is 1 bit long
	uint8_t sun_sensor_busy_error; // actually is bool, and is 1 bit long
	uint8_t sun_sensor_detection_error; // actually is bool, and is 1 bit long
	uint8_t sun_sensor_range_error; // actually is bool, and is 1 bit long
	uint8_t nadir_sensor_overcurrent_detected; // actually is bool, and is 1 bit long
	uint8_t nadir_sensor_busy_error; // actually is bool, and is 1 bit long*/

	/*
	 * adcs_state_bool_flags_set4 contains 8 boolean flags. From least struct offset to most:
	 * 1. nadir sensor detection error
	 * 2. nadir sensor range error
	 * 3. rate sensor range error
	 * 4. wheel speed range error
	 * 5. coarse sun sensor error
	 * 6. orbit parameters invalid
	 * 7. config invalid
	 * 8. control mode change is not allowed
	 */
	uint8_t adcs_state_bool_flags_set4;
	/*uint8_t nadir_sensor_detection_error; // actually is bool, and is 1 bit long
	uint8_t nadir_sensor_range_error; // actually is bool, and is 1 bit long
	uint8_t rate_sensor_range_error; // actually is bool, and is 1 bit long
	uint8_t wheel_speed_range_error; // actually is bool, and is 1 bit long
	uint8_t coarse_sun_sensor_error; // actually is bool, and is 1 bit long
	uint8_t orbit_parameters_invalid; // actually is bool, and is 1 bit long
	uint8_t config_invalid; // actually is bool, and is 1 bit long
	uint8_t control_mode_change_not_allowed; // actually is bool, and is 1 bit long*/

	/*
	 * adcs_state_bool_flags_set5 contains 8 boolean flags. From least struct offset to most:
	 * 1. estimator change is not allowed
	 * 2. modelled and measured magnetic fields differ in size
	 * 3. SRAM latchup error
	 * 4. SD card error
	 * 5. node recovery error
	 * 6. steady state Y-Thompson deteceted
	 * 7. TRIAD and EKF report same attitude
	 * 8. sun is above local horizon
	 */
	uint8_t adcs_state_bool_flags_set5;
	/*uint8_t estimator_change_not_allowed; // actually is bool, and is 1 bit long
	uint8_t modelled_and_measured_magnetic_fields_differ_in_size; // actually is bool, and is 1 bit long
	uint8_t SRAM_latchup_error; // actually is bool, and is 1 bit long
	uint8_t SD_card_error; // actually is bool, and is 1 bit long
	uint8_t node_recovery_error; // actually is bool, and is 1 bit long
	uint8_t steady_state_Y_thompson_detected; // actually is bool, and is 1 bit long
	uint8_t TRIAD_and_EKF_report_same_attitude; // actually is bool, and is 1 bit long
	uint8_t sun_above_local_horizon; // actually is bool, and is 1 bit long*/
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_estimated_attitude_angles_t
{
	int16_t estimated_roll_angle;
	int16_t estimated_pitch_angle;
	int16_t estimated_yaw_angle;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_estimated_angular_rates_t
{
	int16_t estimated_X_angular_rate;
	int16_t estimated_Y_angular_rate;
	int16_t estimated_Z_angular_rate;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_satellite_velocity_t
{
	int16_t X_velocity;
	int16_t Y_velocity;
	int16_t Z_velocity;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_satellite_position_t
{
	int16_t latitude;
	int16_t longitude;
	uint16_t altitude;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_measurements_t
{
	int16_t magnetic_field_x;
	int16_t magnetic_field_y;
	int16_t magnetic_field_z;
	int16_t coarse_sun_x;
	int16_t coarse_sun_y;
	int16_t coarse_sun_z;
	int16_t sun_x;
	int16_t sun_y;
	int16_t sun_z;
	int16_t nadir_x;
	int16_t nadir_y;
	int16_t nadir_z;
	int16_t angular_rate_x;
	int16_t angular_rate_y;
	int16_t angular_rate_z;
	int16_t wheel_speed_x;
	int16_t wheel_speed_y;
	int16_t wheel_speed_z;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_magnetic_field_vector_t
{
	int16_t magnetic_field_x;
	int16_t magnetic_field_y;
	int16_t magnetic_field_z;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_coarse_sun_vector_t
{
	int16_t coarse_sun_x;
	int16_t coarse_sun_y;
	int16_t coarse_sun_z;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_fine_sun_vector_t
{
	int16_t sun_x;
	int16_t sun_y;
	int16_t sun_z;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_nadir_vector_t
{
	int16_t nadir_x;
	int16_t nadir_y;
	int16_t nadir_z;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_rate_sensor_rates_t
{
	int16_t angular_rate_x;
	int16_t angular_rate_y;
	int16_t angular_rate_z;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_wheel_speed_meas_t
{
	int16_t wheel_speed_x;
	int16_t wheel_speed_y;
	int16_t wheel_speed_z;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_actuator_command_t
{
	int16_t magnetorquer_command_x;
	int16_t magnetorquer_command_y;
	int16_t magnetorquer_command_z;
	int16_t wheel_speed_x;
	int16_t wheel_speed_y;
	int16_t wheel_speed_z;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_magnetorquer_command_t
{
	int16_t magnetorquer_command_x;
	int16_t magnetorquer_command_y;
	int16_t magnetorquer_command_z;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_wheel_speed_command_t
{
	int16_t wheel_speed_x;
	int16_t wheel_speed_y;
	int16_t wheel_speed_z;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_raw_sensor_measurements_t
{
	int16_t nadir_centroid_x;
	int16_t nadir_centroid_y;
	adcs_busy_status_t nadir_busy_status;
	adcs_nadir_detection_result_t nadir_detection_result;
	int16_t sun_centroid_x;
	int16_t sun_centroid_y;
	adcs_busy_status_t sun_busy_status;
	adcs_sun_detection_result_t sun_detection_result;
	uint8_t css1;
	uint8_t css2;
	uint8_t css3;
	uint8_t css4;
	uint8_t css5;
	uint8_t css6;
	int16_t magx;
	int16_t magy;
	int16_t magz;
	adcs_GPS_solution_status_t GPS_solution_status;
	uint8_t number_of_tracked_GPS_satellites;
	uint8_t number_of_GPS_satellites_used_in_solution;
	uint8_t counter_for_XYZ_log_from_GPS;
	uint8_t counter_for_RANGE_log_from_GPS;
	uint8_t response_message_for_GPS_log_setup;
	uint16_t GPS_reference_week;
	uint32_t GPS_time_millis;
	int32_t ECEF_position_x;
	int16_t ECEF_velocity_x;
	int32_t ECEF_position_y;
	int16_t ECEF_velocity_y;
	int32_t ECEF_position_z;
	int16_t ECEF_velocity_z;
	uint8_t x_pos_stddev;
	uint8_t y_pos_stddev;
	uint8_t z_pos_stddev;
	uint8_t x_vel_stddev;
	uint8_t y_vel_stddev;
	uint8_t z_vel_stddev;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_raw_nadir_sensor_t
{
	int16_t nadir_centroid_x;
	int16_t nadir_centroid_y;
	adcs_busy_status_t nadir_busy_status;
	adcs_nadir_detection_result_t nadir_detection_result;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_raw_sun_sensor_t
{
	int16_t sun_centroid_x;
	int16_t sun_centroid_y;
	adcs_busy_status_t sun_busy_status;
	adcs_sun_detection_result_t sun_detection_result;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_raw_css_t
{
	uint8_t css1;
	uint8_t css2;
	uint8_t css3;
	uint8_t css4;
	uint8_t css5;
	uint8_t css6;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_raw_magnetometer_t
{
	int16_t magx;
	int16_t magy;
	int16_t magz;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_raw_GPS_status_t
{
	adcs_GPS_solution_status_t GPS_solution_status;
	uint8_t number_of_tracked_GPS_satellites;
	uint8_t number_of_GPS_satellites_used_in_solution;
	uint8_t counter_for_XYZ_log_from_GPS;
	uint8_t counter_for_RANGE_log_from_GPS;
	uint8_t response_message_for_GPS_log_setup;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_raw_GPS_time_t
{
	uint16_t GPS_reference_week;
	uint32_t GPS_time_millis;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_raw_GPS_x_t
{
	int32_t ECEF_position_x;
	int16_t ECEF_velocity_x;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_raw_GPS_y_t
{
	int32_t ECEF_position_y;
	int16_t ECEF_velocity_y;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_raw_GPS_z_t
{
	int32_t ECEF_position_z;
	int16_t ECEF_velocity_z;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_estimation_data_t
{
	int16_t IGRF_modelled_mag_field_x;
	int16_t IGRF_modelled_mag_field_y;
	int16_t IGRF_modelled_mag_field_z;
	int16_t modelled_sun_vector_x;
	int16_t modelled_sun_vector_y;
	int16_t modelled_sun_vector_z;
	int16_t estimated_x_gyro_bias;
	int16_t estimated_y_gyro_bias;
	int16_t estimated_z_gyro_bias;
	int16_t innovation_vector_x;
	int16_t innovation_vector_y;
	int16_t innovation_vector_z;
	int16_t quaternion1_error;
	int16_t quaternion2_error;
	int16_t quaternion3_error;
	int16_t quaternion1_rms_covariance;
	int16_t quaternion2_rms_covariance;
	int16_t quaternion3_rms_covariance;
	int16_t angular_rate_covariance_x;
	int16_t angular_rate_covariance_y;
	int16_t angular_rate_covariance_z;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_IGRF_modelled_magnetic_field_vector_t
{
	int16_t IGRF_modelled_mag_field_x;
	int16_t IGRF_modelled_mag_field_y;
	int16_t IGRF_modelled_mag_field_z;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_modelled_sun_vector_t
{
	int16_t modelled_sun_vector_x;
	int16_t modelled_sun_vector_y;
	int16_t modelled_sun_vector_z;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_estimated_gyro_bias_t
{
	int16_t estimated_x_gyro_bias;
	int16_t estimated_y_gyro_bias;
	int16_t estimated_z_gyro_bias;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_estimated_innovation_vector_t
{
	int16_t innovation_vector_x;
	int16_t innovation_vector_y;
	int16_t innovation_vector_z;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_quaternion_error_vector_t
{
	int16_t quaternion1_error;
	int16_t quaternion2_error;
	int16_t quaternion3_error;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_quaternion_covariance_t
{
	int16_t quaternion1_rms_covariance;
	int16_t quaternion2_rms_covariance;
	int16_t quaternion3_rms_covariance;
};
/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_angular_rate_covariance_t
{
	int16_t angular_rate_covariance_x;
	int16_t angular_rate_covariance_y;
	int16_t angular_rate_covariance_z;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_ACP_execution_state_t
{
	uint16_t time_since_iteration_start;
	adcs_current_execution_point_t current_execution_point;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_ACP_execution_times_t
{
	uint16_t time_to_perform_ADCS_update;
	uint16_t time_to_perform_sensor_or_actuator_comms;
	uint16_t time_to_execute_SGP4_propagator;
	uint16_t time_to_execute_IGRF_model;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_EDAC_and_latchup_counters_t
{
	uint16_t single_SRAM_upsets;
	uint16_t double_SRAM_upsets;
	uint16_t multiple_SRAM_upsets;
	uint16_t SRAM1_latchups;
	uint16_t SRAM2_latchups;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_image_capture_and_save_operations_statuses_t
{
	uint8_t percentage_complete;
	adcs_image_save_status_t status;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_file_info_t
{
	/*
	 * file_info_flags contains 2 bools. From least struct offset to most:
	 * 1. processing flag (this file entry only allowed if this flag is true)
	 * 2. end reached flag
	 */
	uint8_t file_info_flags;
	/*uint8_t processing_flag; // actually is bool, and is 1 bit long
	uint8_t end_reached_flag; // actually is bool, and is 1 bit long, but the two together are 8 bits long*/

	char filename[13];
	uint32_t filesize;
	uint16_t date;
	uint16_t time;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_file_block_CRC_t
{
	uint16_t block_number;
	uint8_t block_length;

	/*
	 * file_block_CRC_flags contains 4 bool flags. From least struct offset to most:
	 * 1. processing flag (busy reading file into telemetry buffer)
	 * 2. end reached flag
	 * 3. file not found
	 * read error
	 */
	uint8_t file_block_CRC_flags;
	/*uint8_t processing_flag; // actually is bool, and is 1 bit long
	uint8_t end_reached; // actually is bool, and is 1 bit long
	uint8_t file_not_found; // actually is bool, and is 1 bit long
	uint8_t read_error; // actually is bool, and is 1 bit long; the 4 together are 8 bits long*/

	uint16_t CRC16_checksum;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_uploaded_program_status_t
{
	uint8_t programming_completed; // actually is bool, and is 8 bits long
	uint16_t CRC16_checksum;
};

/**
 * @struct adcs_unix_time_t
 * @brief
 * 		Structure that defines the unix time of the subsystem
 *
 * @details
 * 		Structure that defines the time that ADCS thinks is now
 *		Can be set with
 *			@code
 *			adcs_set_unix_time(adcs_t*, adcs_unix_time_t*)
 *			@endcode
 *		Also, this is the format of timestamps in telemetry log files
 *
 * @var adcs_unix_time_t::unix_time
 * 		Unix time, a 32-bit variable representing seconds since January 1, 1970
 * @var adcs_unix_time_t::millis
 * 		Current milliseconds count of the subsystem
 */
struct adcs_flash_program_list_t
{
	uint32_t program1_length;
	uint16_t program1_CRC16;
	uint8_t program1_valid;
	char program1_description[64];
	uint32_t program2_length;
	uint16_t program2_CRC16;
	uint8_t program2_valid;
	char program2_description[64];
	uint32_t program3_length;
	uint16_t program3_CRC16;
	uint8_t program3_valid;
	char program3_description[64];
	uint32_t program4_length;
	uint16_t program4_CRC16;
	uint8_t program4_valid;
	char program4_description[64];
	uint32_t program5_length;
	uint16_t program5_CRC16;
	uint8_t program5_valid;
	char program5_description[64];
	uint32_t program6_length;
	uint16_t program6_CRC16;
	uint8_t program6_valid;
	char program6_description[64];
	uint32_t program7_length;
	uint16_t program7_CRC16;
	uint8_t program7_valid;
	char program7_description[64];
};

/**
 * @brief
 * 		Initializes an adcs_t structure to act as a mock up of the ADCS.
 *
 * @details
 * 		Initializes an adcs_t structure to act as a mock up of the ADCS.
 * 		Multiple adcs_t instances can be initialized safely with this method.
 *
 * 		In this mock up, the adcs_t::mode method will simulate a real adcs.
 * 		Initially, it will return ADCS_MODE_IDLE, then eventually ADCS_MODE_DETUMBLE,
 * 		and eventually ADCS_MODE_STABILIZED. After a while it will drop back down to
 * 		ADCS_MODE_DETUMBLE, then back up to ADCS_MODE_STABILIZED, and finally, rince
 * 		and repeat.
 *
 * @param adcs
 * 		A pointer to the adcs_t structure to initialize.
 */
void initialize_adcs_lpc_local( adcs_t *adcs );

#endif /* INCLUDE_ADCS_H_ */

