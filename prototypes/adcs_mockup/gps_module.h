#include "adcs.h"
#include <stdlib.h>

adcs_raw_GPS_status_t* gps_status();
adcs_raw_GPS_time_t* gps_time(long time_since_start_up);
adcs_raw_GPS_x_t* gps_x();
adcs_raw_GPS_y_t* gps_y();
adcs_raw_GPS_z_t* gps_z();
