#include "gps_module.h"
#include "adcs.h"

adcs_raw_GPS_status_t* gps_status()
{
  adcs_raw_GPS_status_t* raw_status = (adcs_raw_GPS_status_t*) malloc(6);
  
  // everything is ideal with this mockup
  raw_status->GPS_solution_status = GPS_SOLUTION_SOLUTION_COMPUTED;
  raw_status->number_of_tracked_GPS_satellites = 1;
  raw_status->number_of_GPS_satellites_used_in_solution = 1;
  raw_status->counter_for_XYZ_log_from_GPS = 0;
  raw_status->counter_for_RANGE_log_from_GPS = 0;
  raw_status->response_message_for_GPS_log_setup = 1;
  
  return raw_status;
}

adcs_raw_GPS_time_t* gps_time(long time_since_start_up)
{
  adcs_raw_GPS_time_t* raw_time = (adcs_raw_GPS_time_t*) malloc(6);
  
  // the number of the week of presentation
  raw_time->GPS_reference_week = 1840;
  // approximately Wednesday, 18:15 plus however long the program has been running
  raw_time->GPS_time_millis = 238500000 + time_since_start_up;//millis();
  
  return raw_time;
}

adcs_raw_GPS_x_t* gps_x()
{
  adcs_raw_GPS_x_t* raw_x = (adcs_raw_GPS_x_t*) malloc(6);

  int pos_x = 1000*cos(millis());

  raw_x->ECEF_position_x = pos_x;
  raw_x->ECEF_velocity_x = 1000;
  
  return raw_x;
}

adcs_raw_GPS_y_t* gps_y()
{
  adcs_raw_GPS_y_t* raw_y = (adcs_raw_GPS_y_t*) malloc(6);

  int pos_y = 1000*sin(millis());

  raw_y->ECEF_position_y = pos_y;
  raw_y->ECEF_velocity_y = 1000;

  return raw_y;
}

adcs_raw_GPS_z_t* gps_z()
{
  adcs_raw_GPS_z_t* raw_z = (adcs_raw_GPS_z_t*) malloc(6);

  int pos_z = 1000/cos(3.14/4.0*sin(millis()));

  raw_z->ECEF_position_z = pos_z;
  raw_z->ECEF_velocity_z = 1000;

  return raw_z;
}

