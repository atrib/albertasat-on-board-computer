/*
===============================================================================
 Name        : Stefan-I2C_Prototype.c
 Author      : Stefan Damkjar
 Version     :
 Copyright   : 2015
 Description : Simple one byte I2C TX loop
===============================================================================
*/

/*********************************************************************//**
 * Required Includes
 **********************************************************************/
#include <cr_section_macros.h>
#include <stdio.h>

/* FreeRTOS.org includes. */
#include "FreeRTOS.h"
#include "task.h"

/* LPC17xx includes. */
#include "lpc17xx_i2c.h"
#include "lpc17xx_clkpwr.h"
#include "lpc17xx_pinsel.h"

/*********************************************************************//**
 * Required Defines
 **********************************************************************/

/* Defines for Example Behavior. */
#define I2C_MASTER           0
#define TRANSMIT			 0
#define RECIEVE				 1
/* If 1, SDA is used as a gpio and blinks with freq of 1Hz */
#define TESTER				 0

/* Defines for I2C Transaction. */
#define I2C_BUS            	 (LPC_I2C1)
#define I2C_CLK_RATE         (100000)
#define I2C_SLAVE_ADDR       (0x04)
#define I2C_TX_COUNT         (0)
#define I2C_TX_RETRY         (10)
#define I2C_RX_COUNT         (0)
#define I2C_RX_RETRY         (0)

/* Defines for Task Create. */
#define STACK_SIZE_FOR_TASK  (configMINIMAL_STACK_SIZE + 100)
#define TASK_PRIORITY		 (tskIDLE_PRIORITY + 1)

/* Buffers. */
char transmitBuffer[] =      "hello\n";
#define TX_BUFFERSIZE        (sizeof(transmitBuffer) / sizeof(char))
#define RX_BUFFERSIZE        (6)
char receiveBuffer[RX_BUFFERSIZE];

/*********************************************************************//**
 * The Task Prototypes
 **********************************************************************/

void setupI2C( void *pvParameters );
void I2C_master_tx( void *pvParameters );
void I2C_master_rx( void *pvParameters );
void I2C_slave_tx( void *pvParameters );
void I2C_slave_rx( void *pvParameters );
void tester_task( void * );
void setup_blink( void );
void blink( void );

/*********************************************************************//**
 * Queue for Pending I2C Transactions
 **********************************************************************/

/*********************************************************************//**
 * Main and Task Scheduling
 **********************************************************************/

int main(void) {
	/* Init the semi-hosting. */
	printf( "\n" );
	setupI2C(NULL);
#if (I2C_MASTER == 1)
#if (TRANSMIT == 1)
	xTaskCreate( I2C_master_tx, "I2C_master_tx", STACK_SIZE_FOR_TASK, NULL, (1 + tskIDLE_PRIORITY), NULL );
#endif
#if (RECIEVE == 1)
	xTaskCreate( I2C_master_rx, "I2C_master_rx", STACK_SIZE_FOR_TASK, NULL, 1, NULL );
#endif
#endif
#if (I2C_MASTER == 0)
#if (TRANSMIT == 1)
	xTaskCreate( I2C_slave_tx, "I2C_slave_tx", STACK_SIZE_FOR_TASK, NULL, TASK_PRIORITY, NULL );
#endif
#if (RECIEVE == 1)
	xTaskCreate( I2C_slave_rx, "I2C_slave_rx", STACK_SIZE_FOR_TASK, NULL, TASK_PRIORITY, NULL );
#endif
#endif
#if (TESTER == 1)
	xTaskCreate( tester_task, "tester task", STACK_SIZE_FOR_TASK, NULL, (1 + tskIDLE_PRIORITY), NULL );
#endif
	/* Start the scheduler so our tasks start executing. */
	vTaskStartScheduler();

	/* If all is well we will never reach here as the scheduler will now be
	running.  If we do reach here then it is likely that there was insufficient
	heap available for the idle task to be created. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

void setup_blink( void )
{
    PINSEL_CFG_Type PinCfg;
    PinCfg.Portnum   = 2;
    PinCfg.Funcnum   = PINSEL_FUNC_0;
    PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
    PinCfg.Pinmode   = PINSEL_PINMODE_PULLUP;

    PinCfg.Pinnum    = 13;
    PINSEL_ConfigPin(&PinCfg);

    GPIO_SetDir( 2, (1 << 13), 1);
}

void blink( void )
{
	static char blink_stat = 0;

	if( blink_stat == 0 )
	{
		GPIO_SetValue( 2, (1 << 13) );
		blink_stat = 1;
	}
	else
	{
		GPIO_ClearValue( 2, (1 << 13) );
		blink_stat = 0;
	}

}

void tester_task( void *param )
{
    setup_blink( );

    for( ;; )
    {
    	blink( );
    	vTaskDelay( 500 / portTICK_RATE_MS );
    }
}

/*********************************************************************//**
 * I2C Initialization
 **********************************************************************/
__IO FlagStatus complete_M;
__IO FlagStatus complete_S;
#if (I2C_MASTER == 1)
void I2C1_IRQHandler() {
	I2C_MasterHandler(I2C_BUS);
	if(I2C_MasterTransferComplete(I2C_BUS)) {
		complete_M = SET;
	}
}
#else
void I2C1_IRQHandler() {
	I2C_SlaveHandler(I2C_BUS);
	if(I2C_SlaveTransferComplete(I2C_BUS))
	{
		complete_S = SET;
	}
}
#endif

void setupI2C( void *pvParameters ) {

    PINSEL_CFG_Type PinCfg;
    PinCfg.Portnum   = 0;
    PinCfg.Funcnum   = PINSEL_FUNC_3;
    PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
    PinCfg.Pinmode   = PINSEL_PINMODE_PULLUP;

    PinCfg.Pinnum    = 0;
    PINSEL_ConfigPin(&PinCfg);
    PinCfg.Pinnum    = 1;
    PINSEL_ConfigPin(&PinCfg);

#if (I2C_MASTER == DISABLE)
    I2C_OWNSLAVEADDR_CFG_Type SlaveAddrCfg;
    SlaveAddrCfg.SlaveAddrChannel   = 0;
    SlaveAddrCfg.SlaveAddr_7bit     = I2C_SLAVE_ADDR;
    SlaveAddrCfg.GeneralCallState   = DISABLE;
    SlaveAddrCfg.SlaveAddrMaskValue = I2C_I2ADR_BITMASK;

    I2C_SetOwnSlaveAddr(I2C_BUS, &SlaveAddrCfg);
#endif

    I2C_Init(I2C_BUS,I2C_CLK_RATE);

	NVIC_DisableIRQ(I2C1_IRQn);
	NVIC_SetPriority(I2C1_IRQn, ((0x00<<3)|0x01));

    I2C_Cmd(I2C_BUS, TRUE);
}
/*-----------------------------------------------------------*/

/*********************************************************************//**
 * I2C Transaction Tasks
 **********************************************************************/

void I2C_master_tx( void *pvParameters ) {

	for( ;; ){

    	I2C_M_SETUP_Type master_tx = { 	.sl_addr7bit         = I2C_SLAVE_ADDR,
										.tx_data             = transmitBuffer,
										.tx_length           = TX_BUFFERSIZE,
										.tx_count            = I2C_TX_COUNT,
										.retransmissions_max = I2C_TX_RETRY };
    	complete_M = RESET;
        I2C_MasterTransferData(I2C_BUS, &master_tx, I2C_TRANSFER_INTERRUPT);
        while( complete_M == RESET )
        {
        	vTaskDelay( 1000 / portTICK_RATE_MS );
        }
    	vTaskDelay( 1000 / portTICK_RATE_MS );
    }
}
/*-----------------------------------------------------------*/

void I2C_master_rx( void *pvParameters ) {

	for( ;; ){

		I2C_M_SETUP_Type master_rx = { 	.sl_addr7bit         = I2C_SLAVE_ADDR,
		            					.rx_data             = receiveBuffer,
		            					.rx_length           = RX_BUFFERSIZE,
		            					.rx_count            = I2C_RX_COUNT,
		            					.retransmissions_max = I2C_RX_RETRY };

		I2C_MasterTransferData(I2C_BUS, &master_rx, I2C_TRANSFER_POLLING);

		printf( receiveBuffer );

        vTaskDelay( 1000 / portTICK_RATE_MS );
    }
}
/*-----------------------------------------------------------*/

void I2C_slave_tx( void *pvParameters ) {

	for( ;; ){

		I2C_S_SETUP_Type slave_tx =  {  .tx_data             = transmitBuffer,
										.tx_length           = TX_BUFFERSIZE,
										.tx_count            = I2C_TX_COUNT };

		I2C_SlaveTransferData(I2C_BUS, &slave_tx, I2C_TRANSFER_POLLING);

        vTaskDelay( 1000 / portTICK_RATE_MS );
    }
}
/*-----------------------------------------------------------*/

void I2C_slave_rx( void *pvParameters ) {

	for( ;; ){

		printf( "starting rec\n" );
		I2C_S_SETUP_Type slave_rx =  {  .rx_data             = receiveBuffer,
										.rx_length           = RX_BUFFERSIZE,
										.rx_count			 = I2C_RX_COUNT };

		complete_S = RESET;
		I2C_SlaveTransferData(I2C_BUS, &slave_rx, I2C_TRANSFER_INTERRUPT);
		while(complete_S == RESET )
		{
			vTaskDelay( 100 / portTICK_RATE_MS );
		}
		printf( "Finished rec\n" );
		printf( receiveBuffer );

		vTaskDelay( 1000 / portTICK_RATE_MS );
    }
}
/*-----------------------------------------------------------*/

void vApplicationMallocFailedHook( void )
{
	/* This function will only be called if an API call to create a task, queue
	or semaphore fails because there is too little heap RAM remaining. */
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed char *pcTaskName )
{
	/* This function will only be called if a task overflows its stack.  Note
	that stack overflow checking does slow down the context switch
	implementation. */
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook( void )
{
	/* This example does not use the idle hook to perform any processing. */
}
/*-----------------------------------------------------------*/

void vApplicationTickHook( void )
{
	/* This example does not use the tick hook to perform any processing. */
}


