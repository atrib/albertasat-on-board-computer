/*
 * Copyright (C) 2015  Stefan Damkjar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file nanopower.c
 * @author Stefan Damkjar
 * @date Feb 14, 2015
 */

#include "nanopower.h"

static int v_counter = 0;
static int i_counter = 0;
static uint16_t voltages[] = { 1500, 16200, 16400, 16200, 16800,
							  16400, 16000, 13500, 12000, 12000,
							  14000, 16000, 16400, 16000, 16300,
							  13000, 890};
#define SIZE_OF_VOLTAGES (sizeof(voltages)/sizeof(uint16_t))
static uint16_t currents[] = {1111, 2222, 3333, 4444, 5555, 6666, 7777, 8888, 9999};
#define SIZE_OF_CURRENTS (sizeof(currents)/sizeof(uint16_t))

int eps_hk_get( eps_hk_t * eps_hk )
{
	eps_hk->vbatt = voltages[v_counter++ % SIZE_OF_VOLTAGES];
	eps_hk->curin[0] = currents[i_counter++ % SIZE_OF_CURRENTS];
	eps_hk->curin[1] = currents[i_counter++ % SIZE_OF_CURRENTS];
	eps_hk->curin[2] = currents[i_counter++ % SIZE_OF_CURRENTS];

	return SUCCESS;
}
