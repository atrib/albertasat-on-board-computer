/*
 * Copyright (C) 2015  Brendan Bruner, Stefan Damkjar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file eps_lpc_local.c
 * @author Brendan Bruner
 * @author Stefan Damkjar
 * @date Feb 2, 2015
 */

#include "eps.h"
#include <stdlib.h>
#include <stdint.h>
#include <Wire.h>

int eps_hk_get(eps_hk_t * hk);
uint8_t tlm_size;
static int v_counter = 0;
static int i_counter = 0;
static uint16_t voltages[] = { 1500, 16200, 16400, 16200, 16800,
							  16400, 16000, 13500, 12000, 12000,
							  14000, 16000, 16400, 16000, 16300,
							  13000, 890};
#define SIZE_OF_VOLTAGES (sizeof(voltages)/sizeof(uint16_t))
static uint16_t currents[] = {1111, 2222, 3333, 4444, 5555, 6666, 7777, 8888, 9999};
#define SIZE_OF_CURRENTS (sizeof(currents)/sizeof(uint16_t))

int eps_hk_get( eps_hk_t * eps_hk )
{
	eps_hk->vbatt = voltages[v_counter++ % SIZE_OF_VOLTAGES];
	eps_hk->curin[0] = currents[i_counter++ % SIZE_OF_CURRENTS];
	eps_hk->curin[1] = currents[i_counter++ % SIZE_OF_CURRENTS];
	eps_hk->curin[2] = currents[i_counter++ % SIZE_OF_CURRENTS];

	return SUCCESS;
}

/**
 * @brief
 *		Calls EPS CSP level drivers to refresh hk table
 * @details
 * 		This function makes a call to the hardware level transaction driver for
 * 		the EPS to update its housekeeping table. This returns a code whose value
 * 		should correspond to one of the following error code as described in csp_error.h:
 *
 *            Macro     |   Value   |       Description
 * 		----------------|-----------|-------------------------
 * 		CSP_ERR_NONE	|	 0 		| No error
 *		CSP_ERR_NOMEM	|	-1 		| Not enough memory
 *		CSP_ERR_INVAL	|	-2		| Invalid argument
 *		CSP_ERR_TIMEDOUT|	-3		| Operation timed out
 *		CSP_ERR_USED	|	-4 		| Resource already in use
 *		CSP_ERR_NOTSUP	|	-5 		| Operation not supported
 *		CSP_ERR_BUSY	|	-6 		| Device or resource busy
 *		CSP_ERR_ALREADY	|	-7		| Connection already in progress
 *		CSP_ERR_RESET	|	-8 		| Connection reset
 *		CSP_ERR_NOBUFS	|	-9 		| No more buffer space available
 *		CSP_ERR_TX		|	-10 	| Transmission failed
 *		CSP_ERR_DRIVER	|	-11		| Error in driver layer
 *		CSP_ERR_AGAIN	|	-12		| Resource temporarily unavailable
 *		CSP_ERR_HMAC	|	-100 	| HMAC failed
 *		CSP_ERR_XTEA	|	-101	| XTEA failed
 *		CSP_ERR_CRC32	|	-102	| CRC32 failed
 *	@param eps
 * 		A pointer to the eps_t structure whose hk structure should be refreshed.
 *	@return
 *		See error code descriptions in details
 */
static int eps_hk_refresh( eps_t *eps )
{

	return eps_hk_get( &eps->hk );
}

/**
 * @brief
 * 		Retrieves a specific housekeeping item from EPS housekeeping struct
 * @details
 * 		This method uses a function pointer to retrieve a specific housekeeping item
 * 		from the EPS housekeeping table.
 *
 *            Macro             |Value|       Description
 * 		------------------------|-----|-------------------------
 * 		EPS_VBOOST              |  0  |   Voltage of boost converters [mV] [PV1, PV2, PV3]
 * 		EPS_VBATT               |  1  |   Voltage of battery [mV]
 * 		EPS_CURIN               |  2  |   Current in [mA]
 * 		EPS_CURSUN              |  3  |	  Current from boost converters [mA]
 * 		EPS_CURSYS              |  4  |   Current out of battery [mA]
 * 		EPS_RESERVED            |  5  |   Reserved for future use
 * 		EPS_CUROUT              |  6  |   Current out (switchable outputs) [mA]
 * 		EPS_OUTPUT              |  7  |   Status of outputs ([6] is BP4 heater, [7] is BP4 switch)
 * 		EPS_OUTPUT_ON_DELTA     |  8  |   Time till power on ([6] is BP4 heater, [7] is BP4 switch)
 * 		EPS_OUTPUT_OFF_DELTA    |  9  |   Time till power off ([6] is BP4 heater, [7] is BP4 switch)
 * 		EPS_LATCHUP             | 10  |   Number of latch-ups
 * 		EPS_WDT_I2C_TIME_LEFT   | 11  |   Time left on I2C wdt [s]
 * 		EPS_WDT_GND_TIME_LEFT   | 12  |   Time left on I2C wdt [s]
 * 		EPS_WDT_CSP_PINGS_LEFT  | 13  |   Pings left on CSP wdt
 * 		EPS_COUNTER_WDT_I2C     | 14  |   Number of WDT I2C reboots
 * 		EPS_COUNTER_WDT_GND     | 15  |   Number of WDT GND reboots
 * 		EPS_COUNTER_WDT_CSP     | 16  |   Number of WDT CSP reboots
 * 		EPS_COUNTER_BOOT        | 17  |   Number of EPS reboots
 * 		EPS_TEMP                | 18  |   Temperatures [degC] [0 = TEMP1,TEMP2,TEMP3,TEMP4,BP4a, BP4b]
 * 		EPS_BOOTCAUSE           | 19  |   Cause of last EPS reboot
 * 		EPS_BATTMODE            | 20  |   Mode for battery [0 = initial,1 = undervoltage,2 = nominal, 3 = batteryfull]
 * 		EPS_PPTMODE             | 21  |   Mode of PPT tracker [1=MPPT, 2=FIXED
 * 		EPS_RESERVED2           | 22  |   Reserved for future use
 *	@param eps
 * 		A pointer to the eps_t structure whose hk structure is being accessed.
 *  @param tlm_id
 *		A macro corresponding to the desired housekeeping value
 *  @return
 *  	Returns a struct with the size (in bytes) and address of the requested housekeeping item
 *
 */
static hk_handler_t eps_hk_get_tlm( eps_t *eps, eps_tlm_id_t tlm_id )
{

	/* Initialize structure for housekeeping item and pad with zeros */
	hk_handler_t hk_itm = {0};

	/* Retrieve the size of, and a pointer to the request housekeeping item */
	switch(tlm_id) {
	case EPS_VBOOST:
			hk_itm.size = sizeof(eps->hk.vboost);
			hk_itm.ptr  = &eps->hk.vboost;
			break;
	case EPS_VBATT:
			hk_itm.size = sizeof(eps->hk.vbatt);
			hk_itm.ptr  = &eps->hk.vbatt;
			break;
	case EPS_CURIN:
			hk_itm.size = sizeof(eps->hk.curin);
			hk_itm.ptr  = &eps->hk.curin;
			break;
	case EPS_CURSUN:
			hk_itm.size = sizeof(eps->hk.cursun);
			hk_itm.ptr  = &eps->hk.cursun;
			break;
	case EPS_CURSYS:
			hk_itm.size = sizeof(eps->hk.cursys);
			hk_itm.ptr  = &eps->hk.cursys;
			break;
	case EPS_RESERVED:
			hk_itm.size = sizeof(eps->hk.reserved);
			hk_itm.ptr  = &eps->hk.reserved;
			break;
	case EPS_CUROUT:
			hk_itm.size = sizeof(eps->hk.curout);
			hk_itm.ptr  = &eps->hk.curout;
			break;
	case EPS_OUTPUT:
			hk_itm.size = sizeof(eps->hk.output);
			hk_itm.ptr  = &eps->hk.output;
			break;
	case EPS_OUTPUT_ON_DELTA:
			hk_itm.size = sizeof(eps->hk.output_on_delta);
			hk_itm.ptr  = &eps->hk.output_on_delta;
			break;
	case EPS_OUTPUT_OFF_DELTA:
			hk_itm.size = sizeof(eps->hk.output_off_delta);
			hk_itm.ptr  = &eps->hk.output_off_delta;
			break;
	case EPS_LATCHUP:
			hk_itm.size = sizeof(eps->hk.latchup);
			hk_itm.ptr  = &eps->hk.latchup;
			break;
	case EPS_WDT_I2C_TIME_LEFT:
			hk_itm.size = sizeof(eps->hk.wdt_i2c_time_left);
			hk_itm.ptr  = &eps->hk.wdt_i2c_time_left;
			break;
	case EPS_WDT_GND_TIME_LEFT:
			hk_itm.size = sizeof(eps->hk.wdt_gnd_time_left);
			hk_itm.ptr  = &eps->hk.wdt_gnd_time_left;
			break;
	case EPS_WDT_CSP_PINGS_LEFT:
			hk_itm.size = sizeof(eps->hk.wdt_csp_pings_left);
			hk_itm.ptr  = &eps->hk.wdt_csp_pings_left;
			break;
	case EPS_COUNTER_WDT_I2C:
			hk_itm.size = sizeof(eps->hk.counter_wdt_i2c);
			hk_itm.ptr  = &eps->hk.counter_wdt_i2c;
			break;
	case EPS_COUNTER_WDT_GND:
			hk_itm.size = sizeof(eps->hk.counter_wdt_gnd);
			hk_itm.ptr  = &eps->hk.counter_wdt_gnd;
			break;
	case EPS_COUNTER_WDT_CSP:
			hk_itm.size = sizeof(eps->hk.counter_wdt_csp);
			hk_itm.ptr  = &eps->hk.counter_wdt_csp;
			break;
	case EPS_COUNTER_BOOT:
			hk_itm.size = sizeof(eps->hk.counter_boot);
			hk_itm.ptr  = &eps->hk.counter_boot;
			break;
	case EPS_TEMP:
			hk_itm.size = sizeof(eps->hk.temp);
			hk_itm.ptr  = &eps->hk.temp;
			break;
	case EPS_BOOTCAUSE:
			hk_itm.size = sizeof(eps->hk.bootcause);
			hk_itm.ptr  = &eps->hk.bootcause;
			break;
	case EPS_BATTMODE:
			hk_itm.size = sizeof(eps->hk.battmode);
			hk_itm.ptr  = &eps->hk.battmode;
			break;
	case EPS_PPTMODE:
			hk_itm.size = sizeof(eps->hk.pptmode);
			hk_itm.ptr  = &eps->hk.pptmode;
			break;
	case EPS_RESERVED2:
			hk_itm.size = sizeof(eps->hk.reserved2);
			hk_itm.ptr  = &eps->hk.reserved2;
			break;
	default:
			return TLM_ERR_INVAL;
	}

	return hk_itm;

}

static eps_vbatt_t eps_get_vbatt( eps_t *eps )
{
	return eps->hk.vbatt;
}

/**
 * @breif
 * 		Determine status of power supply from battery voltage
 * @details
 * 		Uses preset threshold battery voltage values to evaluate the status of
 * 		the power supply (i.e., optimal, normal, power safe, or critical). The
 * 		ranges of battery voltage are described here:
 *
 * 		EPS_MODE_OPTIMAL
 * 			   OPTIMAL_LOWER_THRESHOLD <= (battery voltage)
 *
 * 		EPS_MODE_NORMAL
 * 			    NORMAL_LOWER_THRESHOLD <= (battery voltage) < OPTIMAL_LOWER_THRESHOLD
 *
 * 		EPS_MODE_POWER_SAFE
 *			POWER_SAFE_LOWER_THRESHOLD <= (battery voltage) < NORMAL_LOWER_THRESHOLD
 *
 *		EPS_MODE_CRITICAL
 *										  (battery voltage) < POWER_SAFE_LOWER_THRESHOLD
 *
 * @param eps
 * 		A pointer to the eps_t structure whose hk structure should be refreshed.
 * @return
 *		Returns a macro corresponding to the determined state
 */
static eps_mode_t eps_mode( eps_t *eps )
{
  	eps_vbatt_t current_voltage= *(eps_vbatt_t*)eps->hk_get(eps,EPS_VBATT).ptr;

	if( current_voltage >= OPTIMAL_LOWER_THRESHOLD )
	{
		return EPS_MODE_OPTIMAL;
	}
	else if( current_voltage < OPTIMAL_LOWER_THRESHOLD &&
			 current_voltage >= NORMAL_LOWER_THRESHOLD )
	{
		return EPS_MODE_NORMAL;
	}
	else if( current_voltage < NORMAL_LOWER_THRESHOLD &&
			 current_voltage >= POWER_SAFE_LOWER_THRESHOLD )
	{
		return EPS_MODE_POWER_SAFE;
	}
	else
	{
		return EPS_MODE_CRITICAL;
	}
}

void initialize_eps_lpc_local( eps_t *eps )
{
	/* Initialize function pointers to their corresponding functions */
	eps->hk_get     = &eps_hk_get_tlm;
	eps->hk_refresh = &eps_hk_refresh;
	eps->mode       = &eps_mode;

	/* Initialize Telemetery Struct */
	eps->hk = (eps_hk_t){0};
}

eps_t eps;
uint8_t command_id;
uint8_t* recv_struct;

static void* eps_handle_command_id(eps_t *self, int command_id, eps_tlm_id_t *data)
{
  void* ret;
  eps_mode_t m;
  hk_handler_t h;
  int res;
  switch (command_id)
  {
    case 0:
            m = self->mode(self);
            tlm_size = sizeof(eps_mode_t);
//            ret = malloc(tlm_size);
            ret = &m;
            return ret;
            break;
    case 1:
            h = self->hk_get( self, *data );
            tlm_size = sizeof(hk_handler_t);
//            void* ret = malloc(tlm_size);
            ret = &h;
            return ret;
            break;
    case 2:
            res = self->hk_refresh(self);
            tlm_size = sizeof(int);
//            void* ret = malloc(tlm_size);
            ret = &res;
            return ret;
            break;
  }
}

void setup() {
	Serial.begin(9600);
	initialize_eps_lpc_local( &eps );
        tlm_size = 0;
        recv_struct = (uint8_t*) malloc(sizeof(eps_tlm_id_t)); // the largest struct for telecommand structure
        Wire.begin(4);                // join i2c bus with address #4
        Wire.onReceive(receiveEvent); // register event
        Wire.onRequest(requestEvent); // register event

}

void loop() {
        delay(100);
}

void receiveEvent(int howMany)
{
  int i = 0;

  command_id = Wire.read();
  Serial.println(command_id);
  if (command_id != 1)
  {
     return;
  }
  else
  {
    while( Wire.available()) 
    {
      *(recv_struct + i) = Wire.read();
      i++;
    }
  }
}
// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent()
{
  uint8_t* ret_struct;
  if (command_id != 1)
  {
    ret_struct = (uint8_t*)eps_handle_command_id(&eps, command_id, NULL);
  }
  else
  {
    ret_struct = (uint8_t*)eps_handle_command_id(&eps, command_id, (eps_tlm_id_t*)recv_struct);
  }
  Wire.write(ret_struct, tlm_size);
  free(ret_struct);
}
