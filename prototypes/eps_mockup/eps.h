/*
 * Copyright (C) 2015 Collin Cupido, Brendan Bruner, Stefan Damkjar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file eps.h
 * @author Collin Cupido
 * @author Brendan Bruner
 * @author Stefan Damkjar
 * @date 2014-12-28
 */

#ifndef EPS_H_
#define EPS_H_

#include <stdint.h>
#include "nanopower.h"

// TODO include "csp_error.h"

// TODO DELETE FOR FLIGHT CODE! This is a placeholder until libcsp is included
/**
 * Used under the terms of the GNU Lesser General Public License as stated here:
 *
 *   Cubesat Space Protocol - A small network-layer protocol designed for Cubesats
 *
 *   Copyright (C) 2012 GomSpace ApS (http://www.gomspace.com)
 *	 Copyright (C) 2012 AAUSAT3 Project (http://aausat3.space.aau.dk)
 *
 * 	 This library is free software; you can redistribute it and/or
 * 	 modify it under the terms of the GNU Lesser General Public
 * 	 License as published by the Free Software Foundation; either
 *	 version 2.1 of the License, or (at your option) any later version.
 *
 *	 This library is distributed in the hope that it will be useful,
 *	 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *	 Lesser General Public License for more details.
 *
 *	 You should have received a copy of the GNU Lesser General Public
 *	 License along with this library; if not, write to the Free Software
 *	 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
#define CSP_ERR_INVAL		-2		/* Invalid argument                 */
#define CSP_ERR_NONE		 0 		/* No error                         */
#define CSP_ERR_NOMEM		-1 		/* Not enough memory                */
#define CSP_ERR_INVAL		-2		/* Invalid argument                 */
#define CSP_ERR_TIMEDOUT	-3		/* Operation timed out              */
#define CSP_ERR_USED		-4 		/* Resource already in use          */
#define CSP_ERR_NOTSUP		-5 		/* Operation not supported          */
#define CSP_ERR_BUSY		-6 		/* Device or resource busy          */
#define CSP_ERR_ALREADY		-7		/* Connection already in progress   */
#define CSP_ERR_RESET		-8 		/* Connection reset                 */
#define CSP_ERR_NOBUFS		-9 		/* No more buffer space available   */
#define CSP_ERR_TX			-10 	/* Transmission failed              */
#define CSP_ERR_DRIVER		-11		/* Error in driver layer            */
#define CSP_ERR_AGAIN		-12		/* Resource temporarily unavailable */
#define CSP_ERR_HMAC		-100 	/* HMAC failed                      */
#define CSP_ERR_XTEA		-101	/* XTEA failed                      */
#define CSP_ERR_CRC32		-102	/* CRC32 failed                     */

/* Required typedefs */
typedef struct                eps_t eps_t;
typedef struct         hk_handler_t hk_handler_t;
typedef uint8_t                     eps_tlm_id_t;
typedef uint8_t                     eps_mode_t;

/**
* @struct hk_itm_t
* @brief
*		Describes a housekeeping item or housekeeping table with a pointer to the
*		item or table and its size in bytes
* @details
* 		This structure type allows a housekeeping item or housekeeping table of
* 		arbitrary type or size to be passed or returned.
*
* 		An invalid housekeeping item request is indicated by a size of zero
* @var size
* 		Indicated the size of the corresponding housekeeping item or table in bytes
* @var ptr
* 		Indicated the physical memory location of the housekeeping item or table
*/
struct hk_handler_t
{
	uint16_t size;
	void*   ptr;
};

/* Telemetry Item Data Types */
typedef uint16_t                    eps_vboost_t[];
typedef uint16_t                    eps_vbatt_t;
typedef uint16_t                    eps_curin_t[];
typedef uint16_t                    eps_cursun_t;
typedef uint16_t                    eps_cursys_t;
typedef uint16_t                    eps_reserved_t;
typedef uint16_t                    eps_curout_t[];
typedef uint8_t                     eps_output_t[];
typedef uint16_t                    eps_output_on_delta_t[];
typedef uint16_t                    eps_output_off_delta_t[];
typedef uint16_t                    eps_latchup_t[];
typedef uint32_t                    eps_wdt_i2c_time_left_t;
typedef uint32_t                    eps_wdt_gnd_time_left_t;
typedef uint8_t                     eps_wdt_csp_pings_left_t[];
typedef uint32_t                    eps_counter_wdt_i2c_t;
typedef uint32_t                    eps_counter_wdt_gnd_t;
typedef uint32_t                    eps_counter_wdt_csp_t[];
typedef uint32_t                    eps_counter_boot_t;
typedef uint16_t                    eps_temp_t[];
typedef uint8_t                     eps_bootcause_t;
typedef uint8_t                     eps_battmode_t;
typedef uint8_t                     eps_pptmode_t;
typedef uint16_t                    eps_reserved2_t;

/* Telemetry Request Error Code Define Statements */
#define TLM_ERR_INVAL               ((hk_handler_t){0,0})

/* EPS Mode ID Define Statements */
#define EPS_MODE_CRITICAL 		    ((eps_mode_t) 0)
#define EPS_MODE_POWER_SAFE		    ((eps_mode_t) 1)
#define EPS_MODE_NORMAL			    ((eps_mode_t) 2)
#define EPS_MODE_OPTIMAL		    ((eps_mode_t) 3)

/* EPS Mode Threshold Define Statements */
#define OPTIMAL_LOWER_THRESHOLD		((eps_vbatt_t) 16200)
#define NORMAL_LOWER_THRESHOLD		((eps_vbatt_t) 15800)
#define POWER_SAFE_LOWER_THRESHOLD	((eps_vbatt_t) 15500)

/* Telemetry ID Define Statements */
#define EPS_VBOOST                  ((eps_tlm_id_t)  0)
#define EPS_VBATT                   ((eps_tlm_id_t)  1)
#define EPS_CURIN                   ((eps_tlm_id_t)  2)
#define EPS_CURSUN                  ((eps_tlm_id_t)  3)
#define EPS_CURSYS                  ((eps_tlm_id_t)  4)
#define EPS_RESERVED                ((eps_tlm_id_t)  5)
#define EPS_CUROUT                  ((eps_tlm_id_t)  6)
#define EPS_OUTPUT                  ((eps_tlm_id_t)  7)
#define EPS_OUTPUT_ON_DELTA         ((eps_tlm_id_t)  8)
#define EPS_OUTPUT_OFF_DELTA        ((eps_tlm_id_t)  9)
#define EPS_LATCHUP                 ((eps_tlm_id_t) 10)
#define EPS_WDT_I2C_TIME_LEFT       ((eps_tlm_id_t) 11)
#define EPS_WDT_GND_TIME_LEFT       ((eps_tlm_id_t) 12)
#define EPS_WDT_CSP_PINGS_LEFT      ((eps_tlm_id_t) 13)
#define EPS_COUNTER_WDT_I2C         ((eps_tlm_id_t) 14)
#define EPS_COUNTER_WDT_GND         ((eps_tlm_id_t) 15)
#define EPS_COUNTER_WDT_CSP         ((eps_tlm_id_t) 16)
#define EPS_COUNTER_BOOT            ((eps_tlm_id_t) 17)
#define EPS_TEMP                    ((eps_tlm_id_t) 18)
#define EPS_BOOTCAUSE               ((eps_tlm_id_t) 19)
#define EPS_BATTMODE                ((eps_tlm_id_t) 20)
#define EPS_PPTMODE                 ((eps_tlm_id_t) 21)
#define EPS_RESERVED2               ((eps_tlm_id_t) 22)

/**
 * @struct eps_t
 * @brief
 *		A structure used for communicating with the electronic power supply.
 *
 * @details
 * 		A structure used for communicating with the electronic power supply.
 * 		Each instance of this structure can support IO with exactly one
 * 		power board and must be properly initialized.
 *
 * 		Multiple instance of this structure must not be initialized to do
 * 		IO with the same power board. This will result in undefined behavior.
 *
 * 		All variables inside the eps_t structure must be treated as private.
 * 		They must never be accessed directly.
 *
 * @attention
 * 		Multiple instance of this structure must not be initialized to do
 * 		IO with the same power board. This will result in undefined behavior.
 *
 * @var eps_t::hk
 * 		A table containing housekeeping data for the eps_t structure where
 * 		the table is located
 *
 * @var eps_t::hk_get
 * 		This method takes a pointer to the eps_t structure where the method
 * 		pointer is located and a macro corresponding to a specific housekeeping
 * 		value. The method returns a structure with a pointer and size (in bytes)
 * 		for the corresponding value.
 *
 * 		An invalid housekeeping item request is indicated by a size of zero
 *
 * 		The reason this method is defined as a function pointer in the eps_t
 * 		structure is to support multiple implementations of the hk_get method.
 *	    By making this a function pointer we can swap out how the eps_t
 *	    structure is initialized and the rest of the code will work the same.
 *
 * @var eps_t::mode
 * 		This method takes a pointer to the eps_t structure where the method
 * 		pointer is located and returns the mode (or status) of the power supply
 * 		based on the current battery voltage and preset threshold values
 *
 * @var eps_t::hk_refresh
 * 		This method takes a pointer to the eps_t structure where the method
 * 		pointer is located and interacts with the device drivers for the EPS to
 * 		refresh the hk housekeeping table for the eps_t structure where the
 *      table is located
 *
 *      This method returns an int type code indicating the success or reason for
 *      failure of the device driver side transaction. For CSP, see csp_error.h
 */
struct eps_t
{
	eps_hk_t hk;

	hk_handler_t (*hk_get)(eps_t *, eps_tlm_id_t);

	eps_vbatt_t (*get_vbatt)(eps_t *);

	eps_mode_t (*mode)(eps_t *);

	int (*hk_refresh)(eps_t *);
};

/**
 * @brief
 * 		Initialize an eps_t structure to mock up an eps on the lpc board.
 *
 * @details
 * 	 	Initialize an eps_t structure to mock up an eps on the lpc board.
 * 		This mock up exists on the same board as the executing
 * 		software - there is no physical IO.
 *
 * 		The virtual method
 * 			@code
 * 			eps_t::battery_voltage
 * 			@endcode
 * 		will return numbers between 15000mV and 17000mV where the returned
 * 		v(t) ~= -(t - a)^2 + 17000
 *
 * @param eps
 * 		A pointer to the eps_t structure to initialize.
 *
 * @memberof eps_t
 */
void initialize_eps_lpc_local( eps_t *eps );

#endif /* EPS_H_ */
