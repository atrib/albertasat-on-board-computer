#include "parser.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv )
{
  if( argc > 1 )
  {
	  printf( "input: %s\n", argv[1] );
	  parseScript( NULL, argv[1], strlen( argv[1] ) );
  }
  else
  {
	  char* script = "COMMAND( \"hello\", \"world\");";
	  parseScript( NULL, script, strlen( script ) );
  }
  return 0;
}
