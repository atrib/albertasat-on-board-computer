#include "parser.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>

static Symbol symbol;
static char const* inputStream;
static int streamLocation;
static int streamLength;
static char const* currentSymbol;

#define MAX_SYMBOL_LENGTH 15
#define SCRIPT_CHARACTER_ERROR( got, expected ) \
	printf( "Error, %d: expected \"%c\", but got \"%c\"\n", (int)__LINE__, expected, got )
#define SCRIPT_EOS_ERROR( character ) \
	printf( "Error, %d: expected \"%c\", but go EOS\n", (int)__LINE__, character )
#define SCRIPT_STRING_ERROR( ) \
	printf( "Error, %d: expected string, but got something else\n", (int)__LINE__ )
#define SCRIPT_INVALID_SYMBOL_ERROR( ) \
	printf( "Error, %d: symbol not recognized\n", (int)__LINE__)

static unsigned long hash(char *str)
{
    unsigned long hash = 5381;
    int c;

    while ((c = *str++))
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}

/* Consume empty space, stops when a new symbol is reached. */
static void consumeSpace( )
{
	while( *currentSymbol == ' ' || *currentSymbol == '\n' || *currentSymbol == '\r' )
	{
		++streamLocation;
		++currentSymbol;
		if( streamLocation >= streamLength )
		{
			/* Script end reached. */
			return;
		}
	}
}

/* Consume characters until end of symbol or end of script is reached. */
static void consumeSymbol( )
{
	#define ASCII_LETTER( ch )		( ((ch) >= 'A' && (ch) <= 'Z') || ((ch) >= 'a' && (ch) <= 'z') )
	#define ASCII_UNDERSCORE( ch ) 	( (ch) == '_' )

	while( ASCII_LETTER( *currentSymbol ) || ASCII_UNDERSCORE( *currentSymbol ) )
	{
		++currentSymbol;
		++streamLocation;
		if( streamLocation >= streamLength )
		{
			/* Script end reached. */
			return;
		}
	}
}

/* assert the next valid character. consumes empty space before asserting the next character. */
static char acceptCharacter( char character )
{
	consumeSpace( );
	if( streamLocation >= streamLength )
	{
		SCRIPT_EOS_ERROR( character );
		return 0;
	}
	if( *currentSymbol != character )
	{
		return 0;
	}
	++currentSymbol;
	++streamLocation;
	return 1;
}

static char extractString( char const** string, int* length )
{
	int stringStart;
	int stringEnd;

	stringStart = 0;
	stringEnd = 0;

	/* Look for string start. */
	if( !acceptCharacter( '"' ) )
	{
		SCRIPT_STRING_ERROR( );
		return 0;
	}

	/* Saved string start. */
	stringStart = streamLocation;
	/* Go to string end. */
	consumeSymbol( );
	/* Save string end. */
	stringEnd = streamLocation;

	/* String should end with '"', otherwise the string has invalid characters. */
	if( !acceptCharacter( '"' ) )
	{
		SCRIPT_STRING_ERROR( );
		return 0;
	}

	/* Extract the string. */
	*length = stringEnd - stringStart;
	*string = inputStream + stringStart;

	return 1;
}

/* Look for the next symbol and put it into the symbolBuffer argument. */
static void extractSymbol( char* symbolBuffer )
{
	int symbolStart;
	int symbolEnd;

	symbolStart = 0;
	symbolEnd = 0;

	consumeSpace( );
	if( streamLocation >= streamLength )
	{
		/* At script end. */
		strcpy( symbolBuffer, SCRIPT_END );
		return;
	}

	/* Look for symbol end. */
	symbolStart = streamLocation;
	consumeSymbol( );
	symbolEnd = streamLocation;

	/* Extract the symbol. */
	strncpy( symbolBuffer, inputStream + symbolStart, symbolEnd - symbolStart );
	symbolBuffer[symbolEnd-symbolStart] = '\0';
}

static void nextSymbol( )
{
	unsigned long symbolHash;
	char symbolBuffer[MAX_SYMBOL_LENGTH];

	extractSymbol( symbolBuffer );
	symbolHash = hash( symbolBuffer );

	if(	  	 symbolHash == hash( COMMAND ) )			{ symbol = commandSym; }
	else if( symbolHash == hash( SCRIPT_END ) )			{ symbol = scriptEndSym; }
	else
	{
		symbol = invalidSym;
		SCRIPT_INVALID_SYMBOL_ERROR( );
	}
}

char acceptSymbol( Symbol sym )
{
	if( symbol == sym ){ return 1; }
	return 0;
}

static char command( )
{
	char const* commandName;
	int commandNameLength;
	char const* argumentName;
	int argumentNameLength;

	/* Look for opening bracked. */
	if( !acceptCharacter( '(' ) )
	{
		SCRIPT_CHARACTER_ERROR( *currentSymbol, '(' );
		return 0;
	}

	/* extract command name. */
	if( !extractString( &commandName, &commandNameLength ) )
	{
		SCRIPT_STRING_ERROR( );
		return 0;
	}

	/* Check if optional argument is included. */
	if( acceptCharacter( ',' ) )
	{
		/* Extract optional argument. */
		if( !extractString( &argumentName, &argumentNameLength ) )
		{
			SCRIPT_STRING_ERROR( );
			return 0;
		}
	}
	else
	{
		argumentName = NULL;
	}

	/* Look for closing bracket. */
	if( !acceptCharacter( ')' ) )
	{
		SCRIPT_CHARACTER_ERROR( *currentSymbol, ')' );
		return 0;
	}

	/* Look for symbol finisher. */
	if( !acceptCharacter( ';' ) )
	{
		SCRIPT_CHARACTER_ERROR( *currentSymbol, ';' );
		return 0;
	}

	if( argumentName == NULL )
	{
		printf( "COMMAND( \"%.*s\" );\n", commandNameLength, commandName );
	}
	else
	{
		printf( "COMMAND( \"%.*s\", \"%.*s\" );\n", commandNameLength, commandName, argumentNameLength, argumentName );
	}

	return 1;
}

static char program( )
{

	/* Expecting a command or blank script. */
	nextSymbol( );
	if( acceptSymbol( commandSym ) )
	{
		if( !command( ) ){ return 0; }
	}
	else if( acceptSymbol( scriptEndSym ) )
	{
		return 1;
	}
	else
	{
		return 0;
	}

	/* Expecting EOS. */
	nextSymbol( );
	if( acceptSymbol( scriptEndSym ) )
	{
		return 1;
	}

	/* Should not get here. */
	return 0;
}

char parseScript( char* rules, char const* script, int scriptLength )
{
	if( scriptLength <= 0 ){ return 0; }
	streamLocation = 0;
	streamLength = scriptLength;
	inputStream = script;
	currentSymbol = script;

	if( !program( ) )
	{
		printf( "Parser failed\n" );
		return 0;
	}
	return 1;
}


#ifdef BIG_COMMENT_BLOCK
#define MAX_SYMBOL_LENGTH 15
#define MAX_INDENTIFIER_LENGTH 25

static Symbol symbol;
static char const* inputStream;
static int streamLocation;
static int streamLength;

static unsigned long hash(char *str)
{
    unsigned long hash = 5381;
    int c;

    while ((c = *str++))
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}

static void extractIdentifier( char* symbolBuffer )
{
	char const* currentSymbol;
	int symbolStart;
	int symbolEnd;

	symbolStart = 0;
	symbolEnd = 0;
	currentSymbol = inputStream + streamLocation;

	/* Look for a symbol start. */
	while( *currentSymbol == ' ' || *currentSymbol == '\n' || *currentSymbol == '\r' )
	{
		++streamLocation;
		currentSymbol = (inputStream + streamLocation);
		if( streamLocation >= streamLength )
		{
			printf( "Error: bad script, %d\n", (int) __LINE__ );
			for( ;; );
		}
	}

	/* Look for symbol end. */
	symbolStart = streamLocation;
	while( *currentSymbol != ' ' && *currentSymbol != '\n' && *currentSymbol != '\r' && *currentSymbol != ';' )
	{
		++streamLocation;
		currentSymbol = inputStream + streamLocation;
		if( streamLocation >= streamLength )
		{
			printf( "Error: bad script, %d\n", (int) __LINE__ );
			for( ;; );
		}
	}
	if( *currentSymbol != ';' )
	{
		printf( "Error: missing ;, %d\n", (int) __LINE__ );
		for( ;; );
	}
	symbolEnd = streamLocation;
	++streamLocation;

	/* Extract the symbol. */
	strncpy( symbolBuffer, inputStream + symbolStart, symbolEnd - symbolStart );
	symbolBuffer[symbolEnd-symbolStart+1] = '\0';
}

static void extractSymbolString( char* symbolBuffer )
{
	char const* currentSymbol;
	int symbolStart;
	int symbolEnd;

	symbolStart = 0;
	symbolEnd = 0;
	currentSymbol = inputStream + streamLocation;

	/* Look for a symbol start. */
	while( *currentSymbol == ' ' || *currentSymbol == '\n' || *currentSymbol == '\r' || *currentSymbol == '\0' )
	{
		++streamLocation;
		currentSymbol = (inputStream + streamLocation);
		if( streamLocation >= streamLength )
		{
			/* Script end reached. */
			strcpy( symbolBuffer, "SCRIPT END" );
			return;
		}
	}

	/* Look for symbol end. */
	symbolStart = streamLocation;
	while( *currentSymbol != ' ' && *currentSymbol != '\n' && *currentSymbol != '\r' )
	{
		++streamLocation;
		currentSymbol = inputStream + streamLocation;
		if( streamLocation >= streamLength )
		{
			printf( "Error: expected symbol, but got EOS, %d\n", (int) __LINE__ );
			for( ;; );
		}
	}
	symbolEnd = streamLocation;

	/* Extract the symbol. */
	strncpy( symbolBuffer, inputStream + symbolStart, symbolEnd - symbolStart );
	symbolBuffer[symbolEnd-symbolStart] = '\0';
}

static void nextSymbol( )
{
	unsigned long symbolHash;
	char symbolBuffer[MAX_SYMBOL_LENGTH];
  
	extractSymbolString( symbolBuffer );
	symbolHash = hash( symbolBuffer );

	if(	  	 symbolHash == hash( "VAR" ) )			{ symbol = varSym; }
	else if( symbolHash == hash( ":=" ) )			{ symbol = assignSym; }
	else if( symbolHash == hash( "WHILE" ) )		{ symbol = whileSym; }
	else if( symbolHash == hash( "DO" ) )			{ symbol = doSym; }
	else if( symbolHash == hash( "END" ) )			{ symbol = endSym; }
	else if( symbolHash == hash( "==" ) )			{ symbol = eqlSym; }
	else if( symbolHash == hash( "SCRIPT END" ) )	{ symbol = scriptEndSym; }
	else
	{
		printf( "Error: bad script: symbol not recognized, %d\n", (int) __LINE__ );
		for( ;; );
	}
}

char accept( Symbol sym )
{
	if( symbol == sym ){ return 1; }
	return 0;
}

static void statement( )
{

}

static void program( )
{
	char identifier[MAX_INDENTIFIER_LENGTH];

	/* Register all identifiers, "VAR time; VAR counter;", etc. */
	while( accept( varSym ) )
	{
		extractIdentifier( identifier );
		printf( "%s\n", identifier ); /* Register identifier with identifier stack. */
		nextSymbol( );
	}

	/* Dive into the outer most statement. */
	statement( );

	/* Expecting end of script. */
	if( accept( scriptEndSym ) )
	{
		return;
	}
	else
	{
		printf( "Error: expected EOS, %d", (int) __LINE__ );
		for( ;; );
	}
}

char parseScript( char* rules, char const* script, int scriptLength )
{
  streamLocation = 0;
  streamLength = scriptLength;
  inputStream = script;
  nextSymbol( );
  program( );
  return 1;
}
#endif
