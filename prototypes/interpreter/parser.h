typedef enum
{
  commandSym,
  scriptEndSym,
  invalidSym
} Symbol;


#define COMMAND "COMMAND"
#define SCRIPT_END "SCRIPT_END"


#ifdef BIG_COMMENT_BLOCK
typedef enum
{
  varSym,
  assignSym,
  whileSym,
  doSym,
  endSym,
  eqlSym,
  scriptEndSym
} Symbol;

typedef enum
{
  varRule,
  statementRule,
  whileRule,
  conditionRule,
  expressionRule,
  termRule,
  assignmentRule
} Rules;
#endif

char parseScript( char*, char const*, int );
