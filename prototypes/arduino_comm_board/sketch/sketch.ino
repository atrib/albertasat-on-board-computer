#include <Wire.h>
#include <stdint.h>
#include <stdio.h>

#define HK_SIZE  10
#define NUM_HK    HK_ARRAY_SIZE
#define COMMANDS_SIZE 100
#define ZERO_BYTES 0
#define NO_TELECOMMAND		0
#define INCOMING_TELECOMMAND	1
#define HK_ARRAY_SIZE		8
#define BEACON_REQUEST	0
#define COMM_HK_REQUEST	1
#define COMM_TELEMETRY_PUSH 5
#define COMM_TC_REQUEST	2
#define COMM_IS_BEACONED 0
#define COMM_NOT_BEACONED 1
#define led 11
#define GREEN_ON 0
#define RED_ON 1
#define TELEMETRY_PACKET_SIZE 40
#define TELEMETRY_BUFFER_SIZE 20

typedef struct __attribute__((packed))
{
  uint8_t 	priority_and_option;
  uint8_t 	payload_bytes;
  uint32_t 	time_of_creation;
  uint32_t 	ms_to_execution;
  uint16_t	telecommand_code;
  uint32_t	unique_id;
  uint8_t       data;
} telecommand_packet_t;

typedef struct __attribute__((packed))
{
  uint8_t telemetry[TELEMETRY_PACKET_SIZE];
} telemetry_packet_t;

int sync = 0;
int led_mode;
uint8_t hk[NUM_HK][HK_SIZE];
telecommand_packet_t commands[COMMANDS_SIZE];
telemetry_packet_t telemetry[TELEMETRY_BUFFER_SIZE];
uint8_t head, tail;
uint8_t tel_head, tel_tail;
char *beacon_command = "beacon";
static uint8_t count;
static telecommand_packet_t empty_command;
static telemetry_packet_t empty_telemetry;

static uint8_t beacon_mock( );
static void refresh_hk( );
static void toggle_beacon( );
static void toggle_led( );

void setup() {
  head = 0;
  tail = head;
  tel_head = 0;
  tel_tail = tel_head;
  count = 0;
  Serial.begin(9600);
  Serial1.begin(115200);
  pinMode(led, OUTPUT); 
  digitalWrite(led, HIGH);
  led_mode = RED_ON;
  randomSeed( analogRead( 0 ) );
  for( int i = 0; i < sizeof( telecommand_packet_t ); ++i )
  {
    *(((uint8_t *) &empty_command) + i) = 0x00;
  }
  for( int i = 0; i < TELEMETRY_PACKET_SIZE; ++i )
  {
    *(((uint8_t *) &empty_telemetry) + i) = 0x00;
  }
}



void loop() {
  
  if( Serial.available() > ZERO_BYTES )
  {
    char inc_command = Serial.peek( );
    if( inc_command == 0x02 )
    {
      Serial.read( );
      toggle_led( );
      toggle_beacon( );
    }
    else if( inc_command == 0x03 )
    {
      Serial.read( ); 
      if( tel_tail == tel_head )
      {
        uint8_t *noc = (uint8_t *) &empty_telemetry;
        Serial.write( noc, TELEMETRY_PACKET_SIZE );
      }
      else
      {
        uint8_t *whole_command = (uint8_t *) &telemetry[tel_tail];
        Serial.write( whole_command, TELEMETRY_PACKET_SIZE );
        ++tel_tail;
        tel_tail %= TELEMETRY_BUFFER_SIZE;
      }
    }
    else
    {
      delay( 50 );
      char whole_command[ sizeof( telecommand_packet_t ) + 1 ];
      uint8_t index = 0;
      while( Serial.available( ) > ZERO_BYTES )
      {
        whole_command[index] = Serial.read( );
        ++index;
      }
      uint8_t *write_loc = (uint8_t *) &commands[head];
      ++head;
      head %= COMMANDS_SIZE;
      *write_loc = ( whole_command[1] << 1 ) | ( whole_command[0] & 0x01 );
      for( int i = 1; i < sizeof( telecommand_packet_t ); ++i )
      {
        *((uint8_t *) (write_loc + i)) = (uint8_t) whole_command[i+1];
      }
    }  
  }
  
  if( Serial1.available() > ZERO_BYTES )
  {
    char inc_byte = Serial1.read( );
    
    if( inc_byte == BEACON_REQUEST )
    {
      Serial1.flush( );
      uint8_t bec = beacon_mock( );
      Serial1.write( bec );
    }
    else if( inc_byte == COMM_HK_REQUEST )
    {
      Serial1.flush( );
      refresh_hk( );
      Serial1.write( (uint8_t *) hk, NUM_HK * HK_SIZE );
    }
    else if( inc_byte == COMM_TELEMETRY_PUSH )
    { 
      Serial1.flush( );
      delay( 50 );
      char whole_command[ TELEMETRY_PACKET_SIZE ];
      uint8_t index = 0;
      while( Serial1.available( ) > ZERO_BYTES )
      {
        whole_command[index] = Serial1.read( );
        ++index;
      }
      Serial1.write( 10 );
      uint8_t *write_loc = (uint8_t *) &telemetry[tel_head];
      ++tel_head;
      tel_head %= TELEMETRY_BUFFER_SIZE;
      for( int i = 0; i < TELEMETRY_PACKET_SIZE; ++i )
      {
        *((uint8_t *) (write_loc + i)) = (uint8_t) whole_command[i];
      }
    }
    else if( inc_byte == COMM_TC_REQUEST )
    {
      Serial1.flush( );
      if( tail == head )
      {
        uint8_t *noc = (uint8_t *) &empty_command;
        Serial1.write( noc, sizeof( telecommand_packet_t ) );
      }
      else
      {
        uint8_t *whole_command = (uint8_t *) &commands[tail];
        Serial1.write( whole_command, sizeof( telecommand_packet_t ) );
        ++tail;
        tail %= COMMANDS_SIZE;
      }
    }
  }
  
  delay( 50 );
}

static void toggle_led( )
{
  if( led_mode == RED_ON )
  {
    digitalWrite(led, LOW);
    led_mode = GREEN_ON;
  }
  else
  {
    digitalWrite(led, HIGH);
    led_mode = RED_ON;
  }
}

static void toggle_beacon( )
{
  if( count == 0 )
  {
    count = 1;
  }
  else
  {
    count = 0;
  }
}

static uint8_t beacon_mock( )
{
  if( count == 0 )
  {
    return COMM_NOT_BEACONED;
  }
  else
  {
    return COMM_IS_BEACONED;
  }
}

static void refresh_hk( )
{
  for( int i = 0; i < NUM_HK; ++i )
  {
    for( int j = 0; j < HK_SIZE; ++j )
    {
      hk[i][j] = random(0, 255);
    }
  }
}
