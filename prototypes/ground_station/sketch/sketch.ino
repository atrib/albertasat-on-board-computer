#include <Wire.h>

#define led 11

void setup() {
  Serial.begin(9600);
  pinMode(led, OUTPUT);
}

void loop() {
  digitalWrite(led, LOW);
  while( Serial.available() > 0 )
  {
    char inc_command = Serial.read( );
    Serial.write( inc_command );
  }
  delay( 500 );
  digitalWrite(led, HIGH);
  delay( 500 );
}
