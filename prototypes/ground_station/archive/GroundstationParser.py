 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #

import TelecommandParser, BeaconParser, ArduinoInterface

GS_COMMANDS = "Usage: program [options] file...\n" \
              "Programs:\n" \
              "\thelp\t\tDisplay this help information\n" \
              "\tversion\t\tDisplay version information\n" \
              "\tbeacon\t\tSend beacons. Type beacon --help for help\n" \
              "\ttc\t\ttelecommand to open serial port. Type tc --help\n" \
              "\t\t\tfor telecommanding help."

GS_FATAL_ERROR = "gs: fatal error: "
NO_SATELLITE = "No satellite in range"
GS_TERMINATION = "\ngs terminated"

BEACON_PACKET = [2]

COMMAND_PROMPT = "ground_station >> "
START_MESSAGE = "\nAlbertaSat ground station. ( version 1.0 )\n" \
                "copyright (C) 2015 Brendan Bruner. GNU GPL V2\n" \
                "type 'help' to recieve help\n"
VERSION = "ground_station 1.0\n" \
          "copyright (C) 2015 Brendan Bruner\n" \
          "This is free software; see the source for copying conditions.  There is NO\n" \
          "warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."

HELP_MESSAGE = "type 'help' to receive help"
UNKOWN_COMMAND = "unkown command"

class GroundstationParser:

    def __init__( self, interface ):
        self.tcp = TelecommandParser.TelcommandParser( )
        self.bp = BeaconParser.BeaconParser( )
        self.ai = interface
        
    def parse( self, command ):
        if command != '':
            command = command.split( )
            if command[0] == "tc":
                self.tcp.parse( command[1:] )
                result = self.tcp.error( )
                if result == TelecommandParser.TC_GOOD_COMMAND
                    if self.bp.status( ) == BEACONING:
                        packet = self.tcp.bitStream( )
                        self.ai.write( packet )
                    else:
                        print GS_FATAL_ERROR + NO_SATELLITE + GS_TERMINATION
                else:
                    print result
            elif command[0] == "help":
                print GS_COMMANDS
            elif command[0] == "version":
                print VERSION
            elif command[0] == "beacon":
                self.bp.parse( command[1:] )
                if self.bp.error( ) == BCN_NO_ERROR:
                    packet_format = struct.Struct('! B')
                    serial_data = packet_format.pack( *BEACON_PACKET )
                    self.ai.write( serial_data )
            else:
                print GS_FATAL_ERROR + UNKOWN_COMMAND
                print HELP_MESSAGE
