 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #

import getopt, sys

BCN_USAGE = "Usage: beacon [options] ...\n" \
            "Options:\n" \
            "  --help\tDisplay this information\n" \
            "  --start\tSend beacons periodically\n" \
            "  --stop\tStop sending beacons\n" \
            "\n" \
            "No beacon is generated until the --start option is used\n" \
            "Please beacon responsibly"

VERSION = "ground_station 1.0\n" \
          "copyright (C) 2015 Brendan Bruner\n" \
          "This is free software; see the source for copying conditions.  There is NO\n" \
          "warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."

BCN_OPTIONS = "h"
BCN_LONG_OPTIONS = ["help","start","stop"]

BCN_FATAL_ERROR = "beacon: fatal error: "
BCN_TERMINATION = "\nbeacon terminated"
BCN_BAD_OPTIONS = "bad options, use beacon --help"
BCN_STARTED = "ground_station is successfully beaconing"
BCN_STOPPED = "ground_station successfully stopped beaconing"
BCN_NO_ERROR = "valid beacon"

BEACONING = 1
NOT_BEACONING = 0

class BeaconParser:
    def __init__( self ):
        self._beaconStatus = NOT_BEACONING
        self._errorCode = BCN_NO_ERROR

    def error( self ):
        return self._errorCode
        
    def status( self ):
        return self._beaconStatus

    def parse( self, command ):
        self._errorCode = self._parseCommand( command )

    def _parseCommand( self, command ):
        try:
            all_options, all_arguments = getopt.getopt( command, BCN_OPTIONS, BCN_LONG_OPTIONS )
        except getopt.GetoptError:
            return BCN_FATAL_ERROR + BCN_BAD_OPTIONS + BCN_TERMINATION

        for option, argument in all_options:
            errorCode = self._parseOptions( option, argument )
            if errorCode != BCN_NO_ERROR:
                return errorCode
       
        return BCN_NO_ERROR

    def _parseOptions( self, option, argument ):
        if option in ("-h", "--help"):
            return BCN_USAGE

        elif option == "--start":
            if self._beaconStatus == NOT_BEACONING:
                self._beaconStatus = BEACONING

        elif option == "--stop":
            if self._beaconStatus == BEACONING:
                self._beaconStatus = NOT_BEACONING

        return BCN_NO_ERROR


