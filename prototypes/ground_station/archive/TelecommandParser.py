 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #

import getopt, sys

MIN_PRIORITY = 0
MAX_PRIORITY = 4

MIN_COMMAND = 0
MAX_COMMAND = 0

# Do not reorder these indexes
EXTRA_BOOL = 0
PRIORITY = 1
EXTRA_SIZE = 2
CREATION_TIME = 3
DELAY_TIME = 4
COMMAND_ID = 5
UNIQUE_ID = 6
EXTRAS = 7

EXTRAS_EXIST = 1
NO_EXTRAS = 0

TC_USAGE = "Usage: tc [options] file...\n" \
           "Options:\n" \
           "  --help\t\tDisplay this information.\n" \
           "  --telecommands\tDisplay all available telecommands and their code.\n" \
           "  --priority\t\tDisplay valid priorty levels and their meaning.\n" \
           "  -p <priority>\t\tSpecify command priority. Must be a positive integer.\n" \
           "  -c <command>\t\tSpecify the command\n" \
           "  -f <file>\t\tSpecify an optional file to be appended to the command.\n" \
           "  -d <delay>\t\tDelay telecommand execution by <delay> milliseconds\n" \
           "\t\t\tfrom the time this command line function runs. Assumed\n" \
           "\t\t\tto be zero if not specified\n" \
           "\n" \
           "Please command responsibly."

TC_COMMANDS_LIST = "telecommands:\n" \
                   "  0\tInvoke an action. The OBC will perform some action to\n" \
                   "\tdemonstrate it is still operational. Valid only for \n" \
                   "\tflat sat.\n"

TC_PRIORITY_LIST = "priorities:\n" \
                   "  0\tDiagnostics priority\n" \
                   "  1\tVery high priority\n" \
                   "  2\tHigh priority\n" \
                   "  3\tLow priority\n" \
                   "  4\tVery low priority\n"

TC_OPTIONS = "hp:c:f:d:"
TC_LONG_OPTIONS = ["help","telecommands","priority"]

TC_FATAL_ERROR = "tc: fatal error: "
TC_TERMINATION = "\ntc terminated"
TC_BAD_PRIORITY = "priority must be a positive integer between " \
                  + str( MIN_PRIORITY ) + " and " + str( MAX_PRIORITY )
TC_BAD_COMMAND = "telecommand must be a positive integer between " \
                 + str( MIN_COMMAND ) + " and " + str( MAX_COMMAND )
TC_BAD_DELAY = "delay must be a positive integer"
TC_BAD_PACKET = "telecommand and/or priority not specified"
TC_BAD_OPTIONS = "bad options, use beacon --help"
TC_NO_COMMAND = "no command in memory"
TC_GOOD_COMMAND = "valid command"

class TelecommandParser:
    def __init__( self ):
        self._telecommand = [0] * 8
        self._errorCode = TC_NO_COMMAND

    def parse( self, command ):
        self._errorCode = self._parseCommand( command )

    def error( self ):
        return self._errorCode

    def _parseCommand( self, command ):
        self._telecommand = [0] * 8
        self._telecommand[PRIORITY] = MAX_PRIORITY+1
        self._telecommand[COMMAND_ID] = MAX_COMMAND+1
        try:
            all_options, all_arguments = getopt.getopt( command, 
                                                        TC_OPTIONS,
                                                        TC_LONG_OPTIONS )
        except getopt.GetoptError:
            return TC_FATAL_ERROR + TC_BAD_OPTIONS + TC_TERMINATION

        for option, argument in all_options:
            errorCode = self._parseOptions( option, argument )
            if errorCode != TC_GOOD_COMMAND:
                return errorCode

        self._telecommand[EXTRA_BOOL] = NO_EXTRAS
        self._telecommand[EXTRA_SIZE] = 0
        self._telecommand[EXTRAS] = 0
        self._telecommand[UNIQUE_ID] = 0
        self._telecommand[CREATION_TIME] = 1005

        if self._telecommand[COMMAND_ID] == MAX_COMMAND+1:
            return TC_FATAL_ERROR + TC_BAD_PACKET + TC_TERMINATION
        if self._telecommand[PRIORITY] == MAX_PRIORITY+1:
            return TC_FATAL_ERROR + TC_BAD_PACKET + TC_TERMINATION

        return TC_GOOD_COMMAND

    def _parseOptions( self, option, argument ):
        if option in ("-h", "--help"):
            return TC_USAGE

        elif option == "--telecommands":
            return TC_COMMANDS_LIST

        elif option == "--priority":
            return  TC_PRIORITY_LIST

        elif option == "-p":
            if argument.isdigit( ) and ( int( argument ) in range( MIN_PRIORITY, MAX_PRIORITY+1 ) ):
                self._telecommand[PRIORITY] = int( argument )
            else:
                return  TC_FATAL_ERROR + TC_BAD_PRIORITY + TC_TERMINATION

        elif option == "-c":
            if argument.isdigit( ) and ( int( argument ) in range( MIN_COMMAND, MAX_COMMAND+1 ) ):
                self._telecommand[COMMAND_ID] = int( argument )
            else:
                return TC_FATAL_ERROR + TC_BAD_COMMAND + TC_TERMINATION

        elif option == "-d":
            if argument.isdigit( ):
                self._telecommand[DELAY_TIME] = int( argument )
            else:
                return TC_FATAL_ERROR + TC_BAD_DELAY + TC_TERMINATION

        elif option == "-f":
            self._telecommand[EXTRA_BOOL] = NO_EXTRAS
            self._telecommand[EXTRA_SIZE] = 0
            self._telecommand[EXTRAS] = 0
            return TC_FATAL_ERROR + "File extension not supported in v1.0" + TC_TERMINATION
        else:
            return TC_FATAL_ERROR + TC_BAD_OPTION + TC_TERMINATION

        return TC_GOOD_COMMAND

    def bitStream( self ):
        packetFormat = struct.Struct('! 3B 2I H I B')
        serialData = packetFormat.pack( *self._telecommand )
        return serialData
