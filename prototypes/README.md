#Prototypes

All prototypes require their own folder. Within the protype folder should be a mocks, src, and tests folder. Consider the different prototype methodologies when designing your prototype

***Throw Away***

A prototype for demonstrations purposes. The code used for this prototype should not be used in the project.

***Evolutionary***

This kind of prototype implements all of its features together. Each feature grows as the prototype evolves.

***Incremental***

Feature are implemented one at a time. A new feature is not created until the previous feature is finished and functioning correctly.