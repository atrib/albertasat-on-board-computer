/*
 * test_ADCS.cpp
 *
 *  Created on: 2014-11-24
 *      Author: brendan
 */
/*
extern "C" {
#include "ADCS.h"
}

#include "CppUTest/TestHarness.h"


TEST_GROUP(EPS)
{
	ADCS adcs;

	void setup()
	{
		newADCS(&adcs);
	}
};

/*
 * Page 30 of the ADCS reference manual describes the process for sending a
 * telecommand. There are 3 steps. Testing this procedure should be done in
 * a mock up of the ADCS.
 */

/*
 * Is ADCS on board telemetry logging being used? see page 30 of ADCS reference
 * manual.
 */
