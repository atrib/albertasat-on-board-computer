/**
 * @file StateMockUp.c
 * @author Brendan Bruner
 * @date 2014-12-23
 *
 */
#include "StateMockUp.h"
#include "Satellite.h"

/*****************************************************************************/
/* Methods																	 */
/*****************************************************************************/
static void collectPayloadData(State *obj, Satellite *sat)
{
	++((StateMockUp *) obj)->data._payloadCount;
}
static int collectPayloadDataCount(StateMockUp *obj)
{
	return obj->data._payloadCount;
}

static void collectWOD(State *obj, Satellite *sat)
{
	++((StateMockUp *) obj)->data._WODCount;
}
static int collectWODCount(StateMockUp *obj)
{
	return obj->data._WODCount;
}

static void transmitData(State *obj, Satellite *sat)
{
	++((StateMockUp *) obj)->data._transmitCount;
}
static int transmitDataCount(StateMockUp *obj)
{
	return obj->data._transmitCount;
}

static void executeSoftTelecommand(State *obj, Satellite *sat, Command *com)
{
	++((StateMockUp *) obj)->data._softCount;
}
static int executeSoftTelecommandCount(StateMockUp *obj)
{
	return obj->data._softCount;
}

static void executeHardTelecommand(State *obj, Satellite *sat, Command *com)
{
	++((StateMockUp *) obj)->data._hardCount;
}
static int executeHardTelecommandCount(StateMockUp *obj)
{
	return obj->data._hardCount;
}

/*****************************************************************************/
/* Constructor																 */
/*****************************************************************************/
Constructor(StateMockUp)
{
	Super(State);

	OverrideMethod(State, collectPayloadData);
	LinkMethod(collectPayloadDataCount);

	OverrideMethod(State, collectWOD);
	LinkMethod(collectWODCount);

	OverrideMethod(State, transmitData);
	LinkMethod(transmitDataCount);

	OverrideMethod(State, executeSoftTelecommand);
	LinkMethod(executeSoftTelecommandCount);

	OverrideMethod(State, executeHardTelecommand);
	LinkMethod(executeHardTelecommandCount);

	obj->data._WODCount = 0;
	obj->data._hardCount = 0;
	obj->data._payloadCount = 0;
	obj->data._softCount = 0;
	obj->data._transmitCount = 0;
}
