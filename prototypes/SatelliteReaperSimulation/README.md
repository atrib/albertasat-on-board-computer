#Satelltie Simulation

* The src folder was renamed to Application.
* The DependencyInjection folder Contains C files used for dependency injection in the test suite.
* The doc folder contains documentation made by doxygen. Use a web browser and
open up doc/html/index.html to view the documentation.

###Branches

Make a branch from SatelliteSimulation to work on a feature. Current branches are:

* SatelliteSimulation-CSP: Attempt at integrating a CSP echo server into satellite. Active branch.
* SatelliteSimulation-Collin: Collin's work on implementing the EPS driver and State pattern to have a demonstraction. Feature has been merged into SatelliteSimulation. This is a dead branch now.
* SatelliteSimulation-WODSupervisors: Brendan's work on the Supervisor and WODSupervisor class. Feature has been merged into SatelliteSimulation. This is a dead branch now.

##Objectives and Description

The purpose of this prototype is to implement the firmware architecture using mockups of hardware and device drivers.

##Results

As of 2014-12-31 The WODSupervisor invokes the collection of WOD data as a function of the state of the software.

##Recommendation

* the Application, tests, and DependencyInjection folders contain eclipse projects. Import them into eclipse for easy editing.
* Read the details about the firmware architecture in the wiki.
* Contact Brendan for questions about the software architecture.
* Contact Collin for questions about the requirements of sub systems, pay loads, and states.