/*
 * Copyright (C) 2015 Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file State.c
 * @author Brendan Bruner
 * @date 2014-12-23
 */

#include "Satellite.h"
#include "State.h"

/*****************************************************************************/
/* Methods																	 */
/*****************************************************************************/
/**
 * @brief
 * 		Does nothing
 * @remark
 * 		This method is implemented to prevent NULL function pointers
 * 		from hanging around.
 * @public @memberof State
 */
static void collectPayloadData(State *obj, Satellite *sat)
{
	return;
}

/**
 * @brief
 * 		Does nothing
 * @remark
 * 		This method is implemented to prevent NULL function pointers
 * 		from hanging around.
 * @public @memberof State
 */
static void collectWOD(State *obj, Satellite *sat)
{
	return;
}

/**
 * @brief
 * 		Does nothing
 * @remark
 * 		This method is implemented to prevent NULL function pointers
 * 		from hanging around.
 * @public @memberof State
 */
static void transmitData(State *obj, Satellite *sat)
{
	return;
}

/**
 * @brief
 * 		Does nothing
 * @remark
 * 		This method is implemented to prevent NULL function pointers
 * 		from hanging around.
 * @public @memberof State
 */
static void executeSoftTelecommand(State *obj, Satellite *sat, Command *command)
{
	return;
}

/**
 * @brief
 * 		Does nothing
 * @remark
 * 		This method is implemented to prevent NULL function pointers
 * 		from hanging around.
 * @public @memberof State
 */
static void executeHardTelecommand(State *obj, Satellite *sat, Command *command)
{
	return;
}

/*****************************************************************************/
/* Constructor 																 */
/*****************************************************************************/
/**
 * @brief
 * 		Constructor for State class
 * @public @memberof State
 */
Constructor(State)
{
	LinkMethod(collectPayloadData);
	LinkMethod(collectWOD);
	LinkMethod(transmitData);
	LinkMethod(executeSoftTelecommand);
	LinkMethod(executeHardTelecommand);
}
