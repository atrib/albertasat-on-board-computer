/*
 * Copyright (C) 2015 Collin Cupido, Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file PowerSafe.c
 * @author Collin Cupido
 * @author Brendan Bruner
 * @date 2014-12-28
 *
 * Implements methods which override methods inherited from State class.
 */
#include <stdio.h>

#include "Satellite.h"
#include "PowerSafe.h"
#include "EPS.h"

/*****************************************************************************/
/* Methods																	 */
/*****************************************************************************/
/**
 * @brief
 * 		Collects the WOD as required by PowerSafe mode.
 * @param obj
 * 		A pointer to the object invoking this method.
 * @param sat
 * 		The satellite object maintaining state information.
 * @remark
 * 		Thread safe.
 * 		Non reentrant.
 * @sa State::collectWOD
 * @public @memberof PowerSafe
 */
static void collectWOD(State *obj, Satellite *sat){

	int voltage, threshold;
	EPS *eps;

	eps = sat->getEPSDriver(sat);
	voltage = eps->getHouseKeeping(eps);
	threshold = ((PowerSafe *) obj)->getSwitchingVoltage((PowerSafe *) obj);

	printf("%i mV\n",voltage);

	if( voltage >= threshold )
	{
		sat->setCurrentState(sat, sat->getScienceState(sat));
		printf("Battery Voltage Rising: Going to Science Mode.\n");
	}
	else
	{
		printf("Battery Voltage Low: Continuing PowerSafe Mode.\n");
	}
}

/**
 * @brief
 * 		Returns the voltage at which this state (PowerSafe) can be safely
 * 		left (ie, can safety change to a different state).
 * @param obj
 * 		A pointer to the PowerSafe object invoking this method
 * @return
 * 		The voltage level required to safety change out of PowerSafe state.
 * @public @memberof PowerSafe
 */
static int getSwitchingVoltage(PowerSafe *obj)
{
	return obj->data._switchingVoltage;
}
/**
 * @brief
 * 		Executes a hard telecommand as required by PowerSafe mode.
 * @param obj
 * 		A pointer to the object invoking this method.
 * @param sat
 * 		A pointer to the Satellite object maintaining state information.
 * @remark
 * 		Thread safe.
 * 		Non reentrant.
 * @sa State::executeHardTelecommand
 * @public @memberof PowerSafe
 */
static void executeHardTelecommand(State *obj, Satellite *sat, Command *command)
{
	printf("Hard telecommand executed by PowerSafe mode. \n");
	command->execute(command, sat);
}

/*****************************************************************************/
/* Constructor																 */
/*****************************************************************************/
Constructor(PowerSafe)
{
	Super(State);
	LinkMethod(getSwitchingVoltage);
	OverrideMethod(State, collectWOD);
	OverrideMethod(State, executeHardTelecommand);

	((State *) obj)->data._testID = POWER_SAFE_TEST_ID;
	obj->data._switchingVoltage = 16300;
}
