/*
 * Copyright (C) 2015 Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file Supervisor.h
 * @author Brendan Bruner
 * @date 2014-12-31
 *
 * Prototypes the abstract class Supervisor.
 */

#ifndef SUPERVISOR_H_
#define SUPERVISOR_H_

#include "Class.h"
#include "Satellite.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#define vSupervisorTaskDelay(period) vTaskDelay((period) / portTICK_RATE_MS)
#define TASK_CREATED 0
#define TASK_NOT_CREATED 1

/**
 * @struct Supervisor
 * @brief
 * 		Abstract class for all supervisors
 * @details
 * 		Defines a FreeRTOS task for calling methods of the Satellite
 * 		class. Classes which inherit will have to override the
 * 		supervisorTask() method to define what the task does.
 * @remark
 * 		Subclasses override supervisorTask() method.
 */
typedef unsigned int period_t;
Class(Supervisor)
	Data
		period_t _period;
		xTaskHandle _taskHandle;
		Satellite *_sat;
		xSemaphoreHandle _setMutex, _createdMutex;
		int _isTaskCreated;
	Methods
		void (*setPeriod)(Supervisor *, period_t);
		void (*setSatellite)(Supervisor *, Satellite *);
		Satellite *(*getSatellite)(Supervisor *);
		xTaskHandle *(*getTaskHandle)(Supervisor *);
		void (*start)(Supervisor *);
		void (*stop)(Supervisor *);
		void (*initSupervisorTask)(Supervisor *,
									char const * const,
									unsigned short,
									unsigned portBASE_TYPE);
		void (*_supervisorTask)(Supervisor *);
EndClass;

#endif /* SUPERVISOR_H_ */
