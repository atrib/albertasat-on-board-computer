/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file Science.h
 * @author Brendan Bruner
 * @date 2015-01-07
 */

#ifndef SCIENCE_H_
#define SCIENCE_H_

#include "Class.h"
#include "State.h"

/**
 * @struct Science
 * @brief
 * 		Behavior when satellite is operating as highest level.
 * @extends State
 */
Class(Science) Extends(State)
	Data
		int _switchingVoltage;
	Methods
		int (*getSwitchingVoltage)(Science *);
EndClass;

#endif /* SCIENCE_H_ */
