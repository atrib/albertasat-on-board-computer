/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file MacroCommand.h
 * @author Brendan Bruner
 * @date 2015-01-10
 */

#ifndef MACROCOMMAND_H_
#define MACROCOMMAND_H_

#include "Class.h"
#include "Command.h"
#include "FreeRTOS.h"
#include "semphr.h"

#define COMMAND_BUFFER_LENGTH 100

/**
 * @struct MacroCommand
 * @brief
 * 		A buffer for Command objects with a simply mechanism for macro execution.
 * @details
 * 		Buffers pointers to Command object. This allows batch execution of
 * 		several commands or to buffer commands until a point in time when they
 * 		can be executed.
 * @extends Command
 */
typedef unsigned int command_buffer_length_t;
Class(MacroCommand) Extends(Command)
	Data
		Command *_commandBuffer[COMMAND_BUFFER_LENGTH];
		command_buffer_length_t _numBufferedCommands;
		xSemaphoreHandle _bufferMutex;
	Methods
		void (*addCommand)(MacroCommand *, Command *);
EndClass;

#endif /* MACROCOMMAND_H_ */
