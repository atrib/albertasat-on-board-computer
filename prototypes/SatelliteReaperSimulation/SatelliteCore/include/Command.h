/*
 * Copyright (C) 2015 Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file Detumble.c
 * @author Brendan Bruner
 * @date 2014-12-28
 */

#ifndef COMMAND_H_
#define COMMAND_H_

#include "Class.h"

/* Forward declare Satellite class. */
Forward(Satellite);

/**
 * @struct Command
 * @brief
 * 		Abstract class for commands.
 * @attention
 * 		*** IMPORTANT ***
 * 		All subclass must NOT have extra methods or variables. subclasses
 * 		must ONLY override the execute() method and nothing else. The only
 * 		exception is the MacroCommand class.
 *
 * 		Please see
 * 		https://bitbucket.org/bbruner0/albertasat-on-board-computer/wiki/2.%20Architecture/2.1.%20Architecture%20Specification
 * 		for more details.
 */
Class(Command)
	Data
	Methods
		void (*execute)(Command *, Satellite *);
EndClass;

#endif /* COMMAND_H_ */
