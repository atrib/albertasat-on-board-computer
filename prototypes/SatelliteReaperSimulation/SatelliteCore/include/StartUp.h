/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file StartUp.h
 * @author Brendan Bruner
 * @date 2015-01-07
 */

#ifndef STARTUP_H_
#define STARTUP_H_

#include "Class.h"
#include "State.h"

/**
 * @struct StartUp
 * @brief
 * 		Describes the behavior of system initialization.
 * @extends State
 */
Class(StartUp) Extends(State)
	Data
	Methods
EndClass;

#endif /* STARTUP_H_ */
