/*
 * Copyright (C) 2015 Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file WODSupervisor.c
 * @author Brendan Bruner
 * @date 2014-12-31
 *
 * Implements methods of WODSupervisor class.
 */

#include "WODSupervisor.h"
#include <stdio.h>

/*****************************************************************************/
/* Methods																	 */
/*****************************************************************************/
/**
 * @brief
 * 		Invokes the Satellite objects Satellite::collectWOD() method at ms period
 * 		set with this objects setPeriod method.
 * @remarks
 * 		This is a protected method - it must not be called from the application.
 * @protected @memberof WODSupervisor
 */
static void _supervisorTask(Supervisor *obj)
{
	Satellite *sat = obj->getSatellite(obj);
	for(;;)
	{
		sat->collectWOD(sat);
		vSupervisorTaskDelay(obj->data._period);
	}
}

/*****************************************************************************/
/* Constructor																 */
/*****************************************************************************/
/**
 * @public @memberof WODSupervisor
 */
Constructor(WODSupervisor)
{
	Super(Supervisor);
	OverrideMethod(Supervisor, _supervisorTask);
}
