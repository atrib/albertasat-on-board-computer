/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file MacroCommand.c
 * @author Brendan Bruner
 * @date 2015-01-10
 */

#include "MacroCommand.h"
#include "Satellite.h"

/*****************************************************************************/
/* Methods																	 */
/*****************************************************************************/
/**
 * @brief
 * 		Executes all commands the have been buffered.
 * @details
 * 		All buffered commands are executed. After execution, the buffered
 * 		commands cannot be executed again.
 * @param obj
 *		A pointer to the Command object invoking the method
 * @param sat
 * 		A pointer to the Satellite object which the commands use to do their
 * 		work.
 * @attention
 * 		Dynamically allocated Commands buffered in the MacroCommand are not
 * 		freed after their execution.
 * @public @memberof MacroCommand
 */
static void execute(Command *obj, Satellite *sat)
{
	MacroCommand *derived;

	derived = (MacroCommand *) obj;
	xSemaphoreTake(derived->data._bufferMutex, portMAX_DELAY);

	for(; derived->data._numBufferedCommands > 0; --derived->data._numBufferedCommands)
	{
		Command *curCommand;

		curCommand = derived->data._commandBuffer[derived->data._numBufferedCommands - 1];
		curCommand->execute(curCommand, sat);
	}
	xSemaphoreGive(derived->data._bufferMutex);
}

/**p
 * @brief
 * 		Add a Command to the buffer.
 * @details
 * 		Adds a Command to the buffer. A command will only be added if there is no
 * 		task/thread currently executing the Command::execute() method of this
 * 		MacroCommand object. If there is a task/thread executing that method then
 * 		a blocking wait will be done until the task/thread finishes. The command
 * 		will be added after the task/thread finishes execution of the
 * 		Command::execute() method.
 *
 * 		The MacroCommand buffer only saves pointers to Command objects. It does not
 * 		make copies of them. As such, the programmer must make sure the Command
 * 		objects pointed to will still exist  when the MacroCommand::execute() method
 * 		is  called.
 *
 * 		For example,
 * 			void someFunction(MacroCommand *mc)
 * 			{
 * 				CommandDoSomething command;
 * 				newCommandDoSomething(&command);
 * 				mc->addCommand(mc, (Command *) &command);
 * 			}
 * 		This is bad. The memory allocated to the CommandDoSomething object will
 * 		be undefined when the someFunction() method exits. This means the
 * 		MacroCommand pointed to by the function argument has a pointer to
 * 		undefined data in its internal buffer.
 * @param obj
 * 		A pointer to the MacroCommand object invoking this method.
 * @param command
 * 		A pointer to the Command object which will be added to the buffer.
 * @public @memberof MacroCommand
 */
static void addCommand(MacroCommand *obj, Command *command)
{
	xSemaphoreTake(obj->data._bufferMutex, portMAX_DELAY);
	obj->data._commandBuffer[obj->data._numBufferedCommands++] = command;
	xSemaphoreGive(obj->data._bufferMutex);
}

/*****************************************************************************/
/* Constructor																 */
/*****************************************************************************/
Constructor(MacroCommand)
{
	Super(Command);

	OverrideMethod(Command, execute);
	LinkMethod(addCommand);

	obj->data._numBufferedCommands = 0;
	obj->data._bufferMutex = xSemaphoreCreateMutex();
	if(obj->data._bufferMutex == NULL)
	{
		/* TODO: Handle failed mutex creation. */
	}
	else
	{
		xSemaphoreGive(obj->data._bufferMutex);
	}
}
