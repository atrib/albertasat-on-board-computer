/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file TelecommandPacket.h
 * @author Brendan Bruner
 * @date 2015-01-12
 *
 * Defines the packet format for a telecommand from gound station.
 */

#ifndef TELECOMMANDPACKET_H_
#define TELECOMMANDPACKET_H_

#define TELECOMMAND_PACKET_LENGTH 128

/**
 * @struct telecommand_packet_t
 * @brief
 * 		Packet format of telecommands.
 * @var priority
 * 		 The priority the telecommand has. A higher priority indicates it is
 * 		 more important the telecommand is executed without delay.
 * @var telecommand
 * 		An array of chars which represent the command being issued.
 */
typedef unsigned short telecommand_priority_t;
struct telecommand_packet_t
{
	telecommand_priority_t priority;
	char telecommand[TELECOMMAND_PACKET_LENGTH];
};

#endif /* TELECOMMANDPACKET_H_ */
