/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file COMM.c
 * @author Brendan Bruner
 * @date 2015-01-12
 */
#include <stdio.h>
#include <string.h>
#include "COMM.h"
#include "FreeRTOS.h"
#include "task.h"
#include "TelecommandPacket.h"
#include "TelemetryPacket.h"
#include "PosixMessageQueueIPC.h"
#include "mqueue.h"

#define COMM_GATEWAY_PERIOD_MS	100 / portTICK_RATE_MS
#define COMM_GATEWAY_PRIORITY	(tskIDLE_PRIORITY + 1)
#define COMM_GATEWAY_STACK		5
#define COMM_PIPE_NAME 			"/pipetosat"

struct telecommand_packet_t fullcommand;
struct telecommand_packet_t emptycommand;

/*****************************************************************************/
/* Methods																	 */
/*****************************************************************************/
/**
 * @public @memberof COMM
 */
static struct telecommand_packet_t *blockOnTelecommand(COMM *obj)
{
	struct telecommand_packet_t *packet = &obj->data.telecommandPacket;

	return packet;
}

/**
 * @details
 * 		Gets teleCommand packet from CSP network.
 * @param obj
 * 		A pointer to the COMM object.
 * @remark
 * 		Thread safe.
 * 		Non reentrant.
 * @public @memberof COMM
 */
static struct telecommand_packet_t getCommand(COMM *obj)
{
	/* Name of posix pipe. */
	char *pipename = COMM_PIPE_NAME;

	/* Open the pipe with this program before running echoclient */
	mqd_t pipe = xPosixIPCOpen(pipename, NULL, NULL);

	/* Check with non-blocking call for message.
	* If there is a message, copy it to a packet
	* and clear it.
	* Otherwise, just continue.
	*/
	xMessageObject message;
	lPosixIPCReceiveMessage(pipe, &message);

	if(message.cMessageBytes[0] != 0)
	{
		strcpy(fullcommand.telecommand, message.cMessageBytes);
		message.cMessageBytes[512] = 0;
		return fullcommand;
	}
	else
	{
		strcpy(emptycommand.telecommand, "none");
		return emptycommand;
	}
}

/*****************************************************************************/
/* Constructor																 */
/*****************************************************************************/
Constructor(COMM)
{
	LinkMethod(blockOnTelecommand);
	LinkMethod(getCommand);
}
