 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #

import time, binascii
from serial import Serial, SerialException
from threading import Lock

class ArduinoInterface:
    _ports = {}

    def __init__( self, port, baud ):
        self._serial = None
        self._myPort = None
        self._mutex = Lock( )
        self.initSerial( port, baud )

    def __init__( self ):
        self._serial = None
        self._myPort = None
        self._mutex = Lock( )

    def initSerial( self, port, baud ):
        # If the port isn't open in a seperate ArduinoInterface instance, open it here.
        if port not in ArduinoInterface._ports:
            try:
                self._serial = Serial( port, baud )
                self._myPort = port
                ArduinoInterface._ports[ port ] = port
                time.sleep( 2 )
            except SerialException:
                self._serial = None
                self._myPort = None

    def deInit( self ):
        if self._myPort in ArduinoInterface._ports:
            del ArduinoInterface._ports[ self._myPort ]

    def portOpen( self ):
        if self._serial == None:
            return False
        else:
            return True

    def write( self, data ):
        if self.portOpen:
            self._mutex.acquire( )
            written = self._serial.write( data )
            time.sleep( 1.5 )
            self._mutex.release( )
            return written
        else:
            return 0

    def read( self, size=1 ):
        if self.portOpen:
            read = ''
            self._mutex.acquire( )
            read += self._serial.read( size )
            self._mutex.release( )
            return read
        else:
            return 0

    def inWaiting( self ):
        if self.portOpen:
            return self._serial.inWaiting( )
        else:
            return 0
