 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #
import threading, time, binascii

# Types are a copy of the #defines in
# /liba/Subsystem/include/telemetry_packet.h
PACKET_TYPE_WOD = 0
PACKET_TYPE_DFGM = 1
PACKET_TYPE_MNLP = 2
PACKET_TYPE_CMND_STATUS = 3
PACKET_TYPE_STATE = 4
packet_map_type_to_log = ["wod_log.txt", "dfgm_log.txt", "mnlp_log.txt", 
                          "cmnd_status_log.txt", "state_log.txt"]
packet_map_type_to_size = [260, 32, 32, 32, 150]

class DownLinker:

    def __init__( self, serialInterface ):
        self._serialInterface = serialInterface
        self._isRun = False

    def _downLinkThread( self ):
        read = ""
        while 1:
            if self._serialInterface.inWaiting( ) > 0:
                # read data out
                read += self._serialInterface.read( self._serialInterface.inWaiting( ) )
                # Incoming data is in network order (big endian).
                packetType = int( binascii.hexlify(read[0]), 16 )

                # Check if we have the full packet yet.
                try:
                    if len(read) < (packet_map_type_to_size[packetType] + 5):
                        continue
                except IndexError:
                    # corrupt packet, reset.
                    read = ""
                    continue
                    
                # Get name of log file based on packet type
                try:
                    logFile = packet_map_type_to_log[packetType]
                except IndexError:
                    # corrupt packet, reset.
                    read = ""
                    continue

                # open the log file and write the telemetry to it
                fileObj = open( "downlinked_telemetry/" + logFile, "a" )
                fileObj.write( binascii.hexlify(read) )
                fileObj.close( )
                read = ""

            time.sleep( 0.1 )


    def run( self ):
        try:
            if not self._serialInterface.portOpen:
                return False
        except AttributeError:
            return False
        if not self._isRun:
            threading.Thread( target=self._downLinkThread ).start( )
            self._isRun = True
        return True
