 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #
import threading, time

class UpLinker:

    def __init__( self, serialI ):
        self._serialI = serialI
        self._mutex = threading.Lock( )
        self._bPeriod = 0
        self._isRun = False


    def write( self, packet ):
        self._serialI.write( packet )


    def setBeaconPeriod( self, period ):
        self._mutex.acquire( )
        self._bPeriod = period
        self._mutex.release( )


    def run( self ):
        try:
            if not self._serialI.portOpen:
                return False
        except AttributeError:
            return False
        if not self._isRun:
            threading.Thread( target=self._beaconTask ).start( )
            self._isRun = True
        return True

        
    def _beaconTask( self ):
        while 1:
            self._mutex.acquire( )
            bPeriod = self._bPeriod
            self._mutex.release( )
            if bPeriod != 0:
                self._serialI.write( "beacon" )
                time.sleep( bPeriod )
            else:
                time.sleep( 0.1 )
        
