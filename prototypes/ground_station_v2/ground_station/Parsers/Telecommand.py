 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #
import getopt, struct

# Possible options when invoking the telecommand program
TELECOMMAND_OPTIONS = "hp:c:d:"
TELECOMMAND_LONG_OPTIONS = ["help","telecommands","version","priority","payload="]
TELECOMMAND_AUTO_COMPLETES = TELECOMMAND_LONG_OPTIONS

# Bounding values on some option arguments
MIN_PRIORITY = 0
MAX_PRIORITY = 4
MIN_COMMAND = 0
MAX_COMMAND = 5

# Message that acompany the options above
TELECOMMAND_FATAL_ERROR = "telecommand: fatal error: "
TELECOMMAND_TERMINATION = "\ntelecommand terminated"
TELECOMMAND_BAD_PRIORITY = "priority must be a positive integer between " + str( MIN_PRIORITY ) + " and " + str( MAX_PRIORITY )
TELECOMMAND_BAD_COMMAND = "telecommand must be a positive integer between " + str( MIN_COMMAND ) + " and " + str( MAX_COMMAND )
TELECOMMAND_BAD_DELAY = "delay must be a positive integer"
TELECOMMAND_BAD_PACKET = "telecommand and/or priority not specified"
TELECOMMAND_BAD_OPTIONS = "bad options, use beacon --help"
TELECOMMAND_NO_OPTIONS = "no options"
TELECOMMAND_USAGE = "Usage: telecommand [options] file...\n" \
                    "Options:\n" \
                    "  --help\t\tDisplay this information.\n" \
                    "  --telecommands\tDisplay all available telecommands and their code.\n" \
                    "  --priority\t\tDisplay valid priorty levels and their meaning.\n" \
                    "  --version\t\tDisplay current version.\n" \
                    "  --payload=\t\tSpecify an optional file of payload data to append to\n\t\t\tthe command.\n"\
                    "  -p <priority>\t\tSpecify command priority. Must be a positive integer.\n" \
                    "  -c <command>\t\tSpecify the command\n" \
                    "  -d <delay>\t\tDelay telecommand execution by <delay> milliseconds\n" \
                    "\t\t\tfrom the time this command line function runs. Assumed\n" \
                    "\t\t\tto be zero if not specified\n" \
                    "\n" \
                    "Please command responsibly."
VERSION = "telecommand v2.0\n" \
          "copyright (C) 2015 Brendan Bruner\n" \
          "This is free software; see the source for copying conditions.  There is NO\n" \
          "warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."
TELECOMMAND_COMMANDS_LIST = "telecommands:\n" \
                            "  0\tBlink an led\n" \
                            "  1\tChange state to power on\n" \
                            "  2\tChange state to post ejection\n" \
                            "  3\tChange state to pre charge\n" \
                            "  4\tChange state to start up\n" \
                            "  5\tChange state to power safe\n" \
                            "  6\tChange state to detumble\n" \
                            "  7\tChange state to science\n" \
                            "  8\tQueue a change to power safe upper threshold\n" \
                            "  9\tConfirm command 8\n" \
                            "  10\tQueue a change to power safe lower threshold\n" \
                            "  11\tConfirm command 10\n" \
                            "  12\tQueue a change to pre charge upper threshold\n" \
                            "  13\tConfirm command 12\n" \
                            "  14\tQueue a change to pre charge lower theshold\n" \
                            "  15\tConfirm command 14\n" \
                            "  16\tForward ADCS command and downlink telemetry,\n" \
                            "\t\trequires payload.\n" \
                            "  17\tSave a payload script to a specific slot,\n" \
                            "\t\trequires payload.\n" \
                            "  18\tDelete a file from memory and downlink confirmation,\n" \
                            "\t\trequires payload.\n" \
                            "  19\tQuery the size of a file in memory and downlink size,\n" \
                            "\t\trequires payload.\n" \
                            "  20\tDownlink contents of a file in memory,\n"\
                            "\t\trequires payload.\n" \
                            "  21\tQueue a new boot image into OBC,\n"\
                            "\t\trequires payload.\n" \
                            "  22\tConfirm command 21.\n" \
                            "  23\tQueue a reboot of OBC,\n" \
                            "\t\trequies payload.\n" \
                            "  24\tConfirm command 23.\n" \
                            "  25\tQueue an update to the time of the OBC's RTC,\n" \
                            "\t\trequies payload.\n" \
                            "  26\tConfirm command 25.\n" \
                            "  27\tSpecify order for autonomously downlinked telemetry,\n" \
                            "\t\trequires payload.\n" \
                            "  28\tQueue a command which confirms received telemetry,\n" \
                            "\t\trequires payload.\n" \
                            "  29\tConfirm command 28.\n" \
                            "  30\tQueue a command which will schedule downlinking of telemetry,\n" \
                            "\t\trequires payload.\n" \
                            "  31\tConfirm command 30.\n" \
                            "  32\tQueue a configuration file for the EPS,\n" \
                            "\t\trequires payload.\n" \
                            "  33\tConfirm command 32.\n" \
                            "  34\tQueue a configuration file for the ADCS,\n" \
                            "\t\trequires payload.\n" \
                            "  35\tConfirm command 34.\n" \
                            "  36\tQueue a configuration file for the COMM,\n" \
                            "\t\trequires payload.\n" \
                            "  37\tConfirm command 36.\n"

TELECOMMAND_PRIORITY_LIST = "priorities:\n" \
                            "  0\tDiagnostics priority\n" \
                            "  1\tVery high priority\n" \
                            "  2\tHigh priority\n" \
                            "  3\tLow priority\n" \
                            "  4\tVery low priority\n"

# Indices into command packet before being packed
PACKET_SIZE = 7
PRIORITY = 0
EXTRA_SIZE = 1
CREATION_TIME = 2
DELAY_TIME = 3
COMMAND_ID = 4
UNIQUE_ID = 5
EXTRAS = 6

# Return codes
TELECOMMAND_PARSE_FAIL = 0
TELECOMMAND_PARSE_SUCCESS = 1


class Telecommand:
    name = "telecommand"

    def __init__( self ):
        return

    def usage( self ):
        print TELECOMMAND_USAGE

    def parse( self, command=None ):
        if command == None:
            return (TELECOMMAND_PARSE_FAIL, 0)

        (result, rawPacket) = self._parseText( command )
        if( result == TELECOMMAND_PARSE_SUCCESS ):
            packet = self._serializeNoPayload( rawPacket )
            return ( result, packet )
        else:
            return ( result, 0 )


    def _parseText( self, command ):
        telecommand_packet = [0] * PACKET_SIZE
        telecommand_packet[PRIORITY] = MAX_PRIORITY+1
        telecommand_packet[COMMAND_ID] = MAX_COMMAND+1

        try:
            all_options, all_arguments = getopt.getopt( command, TELECOMMAND_OPTIONS, TELECOMMAND_LONG_OPTIONS )
        except getopt.GetoptError:
            print TELECOMMAND_FATAL_ERROR + TELECOMMAND_BAD_OPTIONS + TELECOMMAND_TERMINATION
            return TELECOMMAND_PARSE_FAIL

        if not all_options:
            print TELECOMMAND_FATAL_ERROR + TELECOMMAND_NO_OPTIONS
            print TELECOMMAND_USAGE

        for option, argument in all_options:
            if option in ("-h", "--help"):
                print TELECOMMAND_USAGE
                return ( TELECOMMAND_PARSE_FAIL, 0 )

            elif option in ("--version"):
                print VERSION
                return ( TELECOMMAND_PARSE_FAIL, 0 )

            elif option == "--telecommands":
                print TELECOMMAND_COMMANDS_LIST
                return ( TELECOMMAND_PARSE_FAIL, 0 )

            elif option == "--priority":
                print TELECOMMAND_PRIORITY_LIST
                return ( TELECOMMAND_PARSE_FAIL, 0 )

            elif option == "-p":
                if argument.isdigit( ) and ( int( argument ) in range( MIN_PRIORITY, MAX_PRIORITY+1 ) ):
                    telecommand_packet[PRIORITY] = int( argument )
                else:
                    print TELECOMMAND_FATAL_ERROR + TELECOMMAND_BAD_PRIORITY + TELECOMMAND_TERMINATION
                    return ( TELECOMMAND_PARSE_FAIL, 0 )

            elif option == "-c":
                if argument.isdigit( ) and ( int( argument ) in range( MIN_COMMAND, MAX_COMMAND+1 ) ):
                    telecommand_packet[COMMAND_ID] = int( argument )
                else:
                    print TELECOMMAND_FATAL_ERROR + TELECOMMAND_BAD_COMMAND + TELECOMMAND_TERMINATION
                    return ( TELECOMMAND_PARSE_FAIL, 0 )

            elif option == "-d":
                if argument.isdigit( ):
                    telecommand_packet[DELAY_TIME] = int( argument )
                else:
                    print TELECOMMAND_FATAL_ERROR + TELECOMMAND_BAD_DELAY + TELECOMMAND_TERMINATION
                return ( TELECOMMAND_PARSE_FAIL, 0 )

            elif option == "--payload":
                # Argument will be file name in this case
                telecommand_packet[EXTRA_SIZE] = 0
                telecommand_packet[EXTRAS] = 0

        telecommand_packet[EXTRA_SIZE] = 0
        telecommand_packet[EXTRAS] = 0

        telecommand_packet[UNIQUE_ID] = 0
        telecommand_packet[CREATION_TIME] = 1005

        if telecommand_packet[COMMAND_ID] == MAX_COMMAND+1 or telecommand_packet[PRIORITY] == MAX_PRIORITY+1:
            print TELECOMMAND_FATAL_ERROR + TELECOMMAND_BAD_PACKET + TELECOMMAND_TERMINATION
            return ( TELECOMMAND_PARSE_FAIL, 0 )

        return ( TELECOMMAND_PARSE_SUCCESS, telecommand_packet ) 


    def _serializeNoPayload( self, rawPacket ):
        packet_format = struct.Struct('! B H 2I H I B')
        serial_data = packet_format.pack( *rawPacket )
        return serial_data

    def _serializeWithPayload( self, rawPacket, payloadSize ):
        return
