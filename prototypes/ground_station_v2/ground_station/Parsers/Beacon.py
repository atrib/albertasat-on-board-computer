 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #
import getopt

# Options when using beacon program
BEACON_OPTIONS = "h"
BEACON_LONG_OPTIONS = ["help","version","period=","status"]
BEACON_AUTO_COMPLETES = BEACON_LONG_OPTIONS
# Messages which acompany options above
BEACON_FATAL_ERROR = "beacon: fatal error: "
BEACON_TERMINATION = "\nbeacon terminated"
BEACON_BAD_OPTIONS = "bad options, use beacon --help"
BEACON_NO_OPTIONS = "no options"
BEACON_PERIOD = "beacon: period: "
BEACON_USAGE = "Usage: beacon [options] ...\n" \
               "Options:\n" \
               "  --help\tDisplay this information\n" \
               "  --period=\tSet the period of beaconing in seconds. A value\n" \
               "\t\tof zero turns the beacon off.\n"\
               "  --status\tPeriod of the beacon\n" \
               "\n" \
               "Beacon is set with a period of zero at start up.\n" \
               "Please beacon responsibly"
BEACON_VERSION = "beacon v2.0\n" \
                 "copyright (C) 2015 Brendan Bruner\n" \
                 "This is free software; see the source for copying conditions.  There is NO\n" \
                 "warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."

# Return codes
BEACON_FAILED = 0
BEACON_DONE = 1 
BEACON_NEW_PERIOD = 2

class Beacon:
    name = "beacon"

    def __init__( self ):
        self._period = 0

    def usage( self ):
        print BEACON_USAGE

    def parse( self, command=None ):
        if command == None:
            return ( BEACON_DONE, 0 )
      
        try:
            all_options, all_arguments = getopt.getopt( command, BEACON_OPTIONS, BEACON_LONG_OPTIONS )
        except getopt.GetoptError:
            print BEACON_FATAL_ERROR + BEACON_BAD_OPTIONS + BEACON_TERMINATION
            return ( BEACON_FAILED, 0 )
        
        if not all_options:
            print BEACON_FATAL_ERROR + BEACON_NO_OPTIONS
            print BEACON_USAGE
            return ( BEACON_DONE, 0 )
        
        for option, argument in all_options:
            if option in ("-h", "--help"):
                print BEACON_USAGE
                return ( BEACON_DONE, 0 )

            elif option == "--version":
                print BEACON_VERSION
                return ( BEACON_DONE, 0 )

            elif option == "--period":
                if argument.isdigit( ):
                    self._period = int( argument )
                    print BEACON_PERIOD + str( self._period ) + " seconds"
                    return ( BEACON_NEW_PERIOD, self._period )

            elif option == "--status":
                print BEACON_PERIOD + str( self._period ) + " seconds"
                return ( BEACON_DONE, 0 )

        print BEACON_FATAL_ERROR + BEACON_NO_OPTIONS + BEACON_USAGE
        return ( BEACON_DONE, 0 )
