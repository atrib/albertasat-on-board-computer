 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #

#!/usr/bin/env python

import ArduinoInterface, serial, sys, getopt, time, struct, binascii, thread

MIN_PRIORITY = 0
MAX_PRIORITY = 4

MIN_COMMAND = 0
MAX_COMMAND = 5

# Do not reorder these indexes
EXTRA_BOOL = 0
PRIORITY = 1
EXTRA_SIZE = 2
CREATION_TIME = 3
DELAY_TIME = 4
COMMAND_ID = 5
UNIQUE_ID = 6
EXTRAS = 7
FIELDS = ["",  ]

EXTRAS_EXIST = 1
NO_EXTRAS = 0

GS_COMMANDS = "Usage: program [options] file...\n" \
              "Programs:\n" \
              "\thelp\t\tDisplay this help information\n" \
              "\tversion\t\tDisplay version information\n" \
              "\tbeacon\t\tSend beacons. Type beacon --help for help\n" \
              "\ttc\t\ttelecommand to open serial port. Type tc --help\n" \
              "\t\t\tfor telecommanding help."
GS_FATAL_ERROR = "gs: fatal error: "
COMMAND_PROMPT = "ground_station >> "
DEFAULT_PORT = "/dev/ttyACM1"
START_MESSAGE = "\nAlbertaSat ground station. ( version 1.0 )\n" \
                "copyright (C) 2015 Brendan Bruner. GNU GPL V2\n" \
                "type 'help' to recieve help\n"

BCN_USAGE = "Usage: beacon [options] ...\n" \
            "Options:\n" \
            "  --help\tDisplay this information\n" \
            "  --start\tSend beacons periodically\n" \
            "  --stop\tStop sending beacons\n" \
            "  --status\tStatus of beacon\n" \
            "\n" \
            "No beacon is generated until the --start option is used\n" \
            "Please beacon responsibly"

TC_USAGE = "Usage: tc [options] file...\n" \
        "Options:\n" \
        "  --help\t\tDisplay this information.\n" \
        "  --telecommands\tDisplay all available telecommands and their code.\n" \
        "  --priority\t\tDisplay valid priorty levels and their meaning.\n" \
        "  --version\t\tDisplay current version.\n" \
        "  -p <priority>\t\tSpecify command priority. Must be a positive integer.\n" \
        "  -c <command>\t\tSpecify the command\n" \
        "  -f <file>\t\tSpecify an optional file to be appended to the command.\n" \
        "  -d <delay>\t\tDelay telecommand execution by <delay> milliseconds\n" \
        "\t\t\tfrom the time this command line function runs. Assumed\n" \
        "\t\t\tto be zero if not specified\n" \
        "\n" \
        "Please command responsibly."
TC_COMMANDS_LIST = "telecommands:\n" \
                "  0\tInvoke an action. The OBC will perform some action to\n" \
                "\tdemonstrate it is still operational. Valid only for \n" \
                "\tflat sat\n" \
                "  1\tOBC prints it's current state over jtag\n"
VERSION = "ground_station 1.0\n" \
          "copyright (C) 2015 Brendan Bruner\n" \
          "This is free software; see the source for copying conditions.  There is NO\n" \
          "warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."
TC_PRIORITY_LIST = "priorities:\n" \
                "  0\tDiagnostics priority\n" \
                "  1\tVery high priority\n" \
                "  2\tHigh priority\n" \
                "  3\tLow priority\n" \
                "  4\tVery low priority\n"

TC_OPTIONS = "hp:c:f:d:"
BCN_OPTIONS = "h"

TC_LONG_OPTIONS = ["help","telecommands","version","priority"]
BCN_LONG_OPTIONS = ["help","start","stop","status"]

TC_FATAL_ERROR = "tc: fatal error: "
TC_TERMINATION = "\ntc terminated"
TC_BAD_PRIORITY = "priority must be a positive integer between " + str( MIN_PRIORITY ) + " and " + str( MAX_PRIORITY )
TC_BAD_COMMAND = "telecommand must be a positive integer between " + str( MIN_COMMAND ) + " and " + str( MAX_COMMAND )
TC_BAD_DELAY = "delay must be a positive integer"
TC_BAD_PACKET = "telecommand and/or priority not specified"
TC_BAD_OPTIONS = "bad options, use beacon --help"

NO_SATELLITE = "No satellite in range"

BCN_FATAL_ERROR = "beacon: fatal error: "
BCN_TERMINATION = "\nbeacon terminated"
BCN_BAD_OPTIONS = "bad options, use beacon --help"
BCN_STARTED = "ground_station is successfully beaconing"
BCN_STOPPED = "ground_station successfully stopped beaconing"

HELP_MESSAGE = "type 'help' to receive help"
UNKOWN_COMMAND = "unkown command"

NO_SERIAL_PORT = "serial port not found"
PORT_INITIALIZED = 1
PORT_NOT_INITIALIZED = 0

TC_PARSE_SUCCESS = 1
TC_PARSE_FAIL = 0

TELEMETRY_PACKET = [3]
BEACON_PACKET = [2]

BEACONING = 1
NOT_BEACONING = 0

global arduino
global beacon_status

def bcn_parse_command( command ):
    global beacon_status

    try:
        all_options, all_arguments = getopt.getopt( command, BCN_OPTIONS, BCN_LONG_OPTIONS )
    except getopt.GetoptError:
        print BCN_FATAL_ERROR + BCN_BAD_OPTIONS + BCN_TERMINATION

    for option, argument in all_options:
        if option in ("-h", "--help"):
            print BCN_USAGE

        elif option == "--start":
            if beacon_status == NOT_BEACONING:
                beacon_status = BEACONING
                send_beacon( )
                print BCN_STARTED

        elif option == "--stop":
            if beacon_status == BEACONING:
                beacon_status = NOT_BEACONING
                send_beacon( )
                print BCN_STOPPED

        elif option == "--status":
            if beacon_status == BEACONING:
                print "beacon: status: beaconing"
            else:
                print "beacon: status: not beaconing"

def tc_parse_command( command ):
    telecommand_packet = [0] * 8
    telecommand_packet[PRIORITY] = MAX_PRIORITY+1
    telecommand_packet[COMMAND_ID] = MAX_COMMAND+1
    try:
        all_options, all_arguments = getopt.getopt( command, TC_OPTIONS, TC_LONG_OPTIONS )
    except getopt.GetoptError:
        print TC_FATAL_ERROR + TC_BAD_OPTIONS + TC_TERMINATION
        return TC_PARSE_FAIL

    for option, argument in all_options:
        if option in ("-h", "--help"):
            print TC_USAGE
            return ( TC_PARSE_FAIL, 0 )

        elif option in ("--version"):
            print VERSION
            return ( TC_PARSE_FAIL, 0 )

        elif option == "--telecommands":
            print TC_COMMANDS_LIST
            return ( TC_PARSE_FAIL, 0 )

        elif option == "--priority":
            print TC_PRIORITY_LIST
            return ( TC_PARSE_FAIL, 0 )

        elif option == "-p":
            if argument.isdigit( ) and ( int( argument ) in range( MIN_PRIORITY, MAX_PRIORITY+1 ) ):
                telecommand_packet[PRIORITY] = int( argument )
            else:
                print TC_FATAL_ERROR + TC_BAD_PRIORITY + TC_TERMINATION
                return ( TC_PARSE_FAIL, 0 )

        elif option == "-c":
            if argument.isdigit( ) and ( int( argument ) in range( MIN_COMMAND, MAX_COMMAND+1 ) ):
                telecommand_packet[COMMAND_ID] = int( argument )
            else:
                print TC_FATAL_ERROR + TC_BAD_COMMAND + TC_TERMINATION
                return ( TC_PARSE_FAIL, 0 )

        elif option == "-d":
            if argument.isdigit( ):
                telecommand_packet[DELAY_TIME] = int( argument )
            else:
                print TC_FATAL_ERROR + TC_BAD_DELAY + TC_TERMINATION
                return ( TC_PARSE_FAIL, 0 )

        elif option == "-f":
            telecommand_packet[EXTRA_BOOL] = NO_EXTRAS
            telecommand_packet[EXTRA_SIZE] = 0
            telecommand_packet[EXTRAS] = 0
            print FATAL_ERROR + "File extension not supported in v1.0" + TERMINATION
            return ( TC_PARSE_FAIL, 0 )

    telecommand_packet[EXTRA_BOOL] = NO_EXTRAS
    telecommand_packet[EXTRA_SIZE] = 0
    telecommand_packet[EXTRAS] = 0

    telecommand_packet[UNIQUE_ID] = 0
    telecommand_packet[CREATION_TIME] = 1005

    if telecommand_packet[COMMAND_ID] == MAX_COMMAND+1 or telecommand_packet[PRIORITY] == MAX_PRIORITY+1:
        print TC_FATAL_ERROR + TC_BAD_PACKET + TC_TERMINATION
        return ( TC_PARSE_FAIL, 0 )

    return ( TC_PARSE_SUCCESS, telecommand_packet ) 

def send_beacon( ):
    global arduino
    
    packet_format = struct.Struct('! B')
    serial_data = packet_format.pack( *BEACON_PACKET )

    written = arduino.write( serial_data )

def send_packet( packet ):
    global arduino
    print "id " + str( packet[COMMAND_ID] )
    packet_format = struct.Struct('! 3B 2I H I B')
    serial_data = packet_format.pack( *packet )

    print( "\ttelecommanding.." )
    written = arduino.write( serial_data )
    print "\t\t" + str( written ) + " bytes written" 
    #print( "\tConfirming.." )
    #count = 0
    #while arduino.inWaiting( ):
    #    read = arduino.readline( written - 1 )
    #    print "bytes echoed: " + binascii.hexlify( read )
    #    count += len(read)
    #print "\t\t" + str( count ) + " bytes were written" 

def ground_station_initialize( port ):
    global arduino
    arduino = ArduinoInterface.ArduinoInterface( port, 9600 )
    if arduino.portOpen( ):
        return 1
    else:
        print GS_FATAL_ERROR + NO_SERIAL_PORT
        return 0

def telemetry_thread( ):
    global arduino
    global beacon_status

    packet_format = struct.Struct('! B')
    serial_data = packet_format.pack( *TELEMETRY_PACKET )

    while 1:
        if beacon_status == BEACONING:
            written = arduino.write( serial_data )
            #print "wrote: " + str( written )
            result = arduino.read( 40 )
            #print result # binascii.hexlify( resu\t )
            if( result != "\0"*40 ):
                if result[0] == '\x00':
                    fo = open( "telemetry/eps_telemetry.txt", "a" )
                elif result[0] == '\x01':
                    fo = open( "telemetry/adcs_telemetry.txt", "a" )
                elif result[0] == '\x02':
                    fo = open( "telemetry/comm_telemetry.txt", "a" )
                elif result[0] == '\x03':
                    fo = open( "telemetry/state_telemetry.txt", "a" )
                else:
                    fo = open( "telemetry/telemetry_unkown.txt", "a" )
                fo.write( result[1:] )
                fo.close( )
        time.sleep( 0.4 )

if __name__=="__main__":
    global beacon_status

    beacon_status = NOT_BEACONING
    print START_MESSAGE

    port_initialized = 0
    port = raw_input( "Please enter serial port. Default is " + DEFAULT_PORT + "\n" \
                      "if nothing is entered.\n" + COMMAND_PROMPT )
    while 1:
        if( port == '' ):
            port_initialized = ground_station_initialize( DEFAULT_PORT )
        else:
            port_initialized = ground_station_initialize( port )
        if port_initialized == 1:
            break
        port = raw_input( COMMAND_PROMPT )
    print "port connected"

    thread.start_new_thread( telemetry_thread, () )
    while 1:
        command = raw_input( COMMAND_PROMPT )
        if command != '':
            command = command.split( )
            if command[0] == "tc":
                (result, packet) = tc_parse_command( command[1:] )
                if result == TC_PARSE_SUCCESS:
                    if beacon_status == BEACONING:
                        send_packet( packet )
                    else:
                        print TC_FATAL_ERROR + NO_SATELLITE + TC_TERMINATION
            elif command[0] == "help":
                print GS_COMMANDS
            elif command[0] == "version":
                print VERSION
            elif command[0] == "beacon":
                bcn_parse_command( command[1:] )
            else:
                print GS_FATAL_ERROR + UNKOWN_COMMAND
                print HELP_MESSAGE

