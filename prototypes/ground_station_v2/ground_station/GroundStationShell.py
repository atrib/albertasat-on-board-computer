 #
 # Copyright (C) 2015  Brendan Bruner
 #
 # This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # bbruner@ualberta.ca
 #

import sys
sys.path.insert( 0, 'Interfaces/' )
sys.path.insert( 0, 'Parsers/' )

import readline
from GSCompleter import GSCompleter
from DownLinker import DownLinker
from UpLinker import UpLinker
from ArduinoInterface import ArduinoInterface
from Telecommand import Telecommand
import Telecommand as TC
from Beacon import Beacon
import Beacon as BC

# Start up message
GROUND_STATION_START_MESSAGE = "\nAlbertaSat ground station. ( version 2.0 )\n" \
                               "copyright (C) 2015 Brendan Bruner. GNU GPL V2\n"\
                               "type 'help' to recieve help\n"

# Command Prompt
GROUND_STATION_PROMPT = "@ ground station >> "

# Commands that can be run
GROUND_STATION_VERSION = "version"
GROUND_STATION_HELP = "help"
GROUND_STATION_PROGRAMS = [Telecommand.name, Beacon.name, GROUND_STATION_HELP, GROUND_STATION_VERSION]

# Messages that accompany the commands above
GROUND_STATION_FATAL = "gs: fatal error: " 
GROUND_STATION_HINT_MSG = "type 'help' to receive help"
GROUND_STATION_USAGE = "Usage: program [options] file...\n" \
                       "Programs:\n" \
                       "\thelp\t\tDisplay this help information\n" \
                       "\tversion\t\tDisplay version information\n" \
                       "\tbeacon\t\tSend beacons. Type beacon --help for help\n" \
                       "\ttelecommand\ttelecommand satellite. Type tc --help\n" \
                       "\t\t\tfor telecommanding help."
GROUND_STATION_VERSION_MSG = "ground_station v2.0\n" \
                             "copyright (C) 2015 Brendan Bruner\n" \
                             "This is free software; see the source for copying conditions.  There is NO\n" \
                             "warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."
GROUND_STATION_UNKOWN_COMMAND = "unkown command"
GROUND_STATION_NO_SERIAL_PORT = "No serial port found. Try again."

# Constants used when initializing serial interface
GROUND_STATION_BAUD = 9600
GROUND_STATION_DEFAULT_PORT = "/dev/ttyACM0"

class GroundStationShell:


    def __init__( self ):
        # Get an instance of each suport class
        self._shellCompleter = GSCompleter( GROUND_STATION_PROGRAMS,
                                            TC.TELECOMMAND_AUTO_COMPLETES,
                                            BC.BEACON_AUTO_COMPLETES )
        self._tc = Telecommand( )
        self._beacon = Beacon( )
        self._arduino = ArduinoInterface( )
        self._downLinker = DownLinker( self._arduino )
        self._upLinker = UpLinker( self._arduino )
        
        # Run the set
        self._setUp( )


    def _setUp( self ):        
        print GROUND_STATION_START_MESSAGE

        # Set auto complete function for entered commands
        readline.set_completer( self._shellCompleter.complete )
        readline.parse_and_bind( 'tab: complete' )
        
        # Set port and baud in self._arduino
        port = raw_input( "Please enter serial port. Default is " + GROUND_STATION_DEFAULT_PORT + "\n" \
                          "if nothing is entered.\n" + GROUND_STATION_PROMPT )
        while 1:
            if( port == '' ):
                self._arduino.initSerial( GROUND_STATION_DEFAULT_PORT, GROUND_STATION_BAUD )
            else:
                self._arduino.initSerial( port, GROUND_STATION_BAUD )

            if self._arduino.portOpen( ):
                break
            print GROUND_STATION_FATAL + GROUND_STATION_NO_SERIAL_PORT
            port = raw_input( GROUND_STATION_PROMPT )
        print "port connected"
        
        # Start running the up/down linker. It will fail to run if the ArduinoInterface does not have
        # an open serial port
        self._downLinker.run( )
        self._upLinker.run( )


    def run( self ):
        while 1:
            command = raw_input( GROUND_STATION_PROMPT )

            if command != '':
                command = command.split( )

                if command[0] == Telecommand.name:
                    (result, packet) = self._tc.parse( command[1:] )
                    if result == TC.TELECOMMAND_PARSE_SUCCESS:
                        self._upLinker.write( packet )

                elif command[0] == Beacon.name:
                    (result, period) = self._beacon.parse( command[1:] )
                    if result == BC.BEACON_NEW_PERIOD:
                        self._upLinker.setBeaconPeriod( period )

                elif command[0] == GROUND_STATION_HELP:
                    print GROUND_STATION_USAGE

                elif command[0] == GROUND_STATION_VERSION:
                    print GROUND_STATION_VERSION_MSG

                else:
                    print GROUND_STATION_FATAL + GROUND_STATION_UNKOWN_COMMAND
                    print GROUND_STATION_HINT_MSG


