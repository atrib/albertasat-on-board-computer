Install Python	

Install PySerial
       sudo apt-get install python-serial

Give bash script execution priv
     chmod +x ground_station

Add "ground_station" to bashrc
    echo "alias ground_station='./ground_station'" >> ~/.bashrc

Reset your terminal (close and open)

In this directory type:
   ground_station
to run the ground station application
