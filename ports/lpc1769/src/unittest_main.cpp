/*
===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

// TODO: insert other include files here
#include <CppUTest/CommandLineTestRunner.h>
// TODO: insert other definitions and declarations here

int main(void) {

#if defined (__USE_LPCOPEN)
#if !defined(NO_BOARD_LIB)
    // Read clock settings and update SystemCoreClock variable
    SystemCoreClockUpdate();
    // Set up and initialize all required blocks and
    // functions related to the board hardware
    Board_Init();
    // Set the LED to the state of "On"
    Board_LED_Set(0, true);
#endif
#endif

    // TODO: insert code here
    printf("initialize console output\n");
    //putchar('n');
	CommandLineTestRunner::RunAllTests(0, (const char**) NULL);
	printf("Done tests\n");


    // Force the counter to be placed into memory
    volatile static int i = 0 ;
    // Enter an infinite loop, writing stuff to the console.
    while(1) {
    	char c = i % 32 + 32;
    	printf("%c", c);
    	if (c == 63){
    		printf("\n");
    	}

    	i++ ;
    }
    return 0;
}

// Need to import test groups since we are linking in the test lib
//IMPORT_TEST_GROUP(TimeServiceTest);
IMPORT_TEST_GROUP(GpsInfoTest);
IMPORT_TEST_GROUP(GpsServiceTest);
IMPORT_TEST_GROUP(NmeaSerdeTest);
IMPORT_TEST_GROUP(GpsGatewayTest);



