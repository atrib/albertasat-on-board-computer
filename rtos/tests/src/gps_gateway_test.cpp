/*
 * gps_gateway_test.cpp
 *
  *
 * THIS IS A TEST USING AN INCOMPLETE DRIVER USED ONLY TO HELP CONFIRM BUILD SETTINGS ARE WORKING.
 * The intention is that this file will be replaced once other production code is in place.
 * In particular, this is not the tesing using the details of the GPS device we will be using for Albertasat.
 *
 * This file also demonstrates using a test double: FakeGpsDriver.  I used function pointer replacement to replace
 * calls to GpsDriver_ functions used by the GpsGateway.
 */
extern "C" {
#include "gps_gateway.h"
#include "gps_em406a.h"
#include "string.h"
}
#include <CppUTest/TestHarness.h>
#include <string>

/*
Note for testing:
FreeRTOS queues appear to work even when the task scheuduler has not
been started.  So we can test using a queue to tranfer
messages through the GpsGateway.
 */
using namespace std;
static char gpsBuffer[GPS_DRIVER_MAX_MESSAGE_SIZE];
static size_t bufferCount;
static uint32_t retFromInit;
// This fake copies gpsBuffer into buffer, and returns bufferCount
size_t FakeGpsDriver_getCharsImpl(GpsDriver* gpsDriver, char* buffer){
	memcpy(buffer, gpsBuffer, bufferCount);
	return bufferCount;
}
size_t (*FakeGpsDriver_getChars)(GpsDriver* gpsDriver, char* buffer) = FakeGpsDriver_getCharsImpl;

//This fake copies the msg to our test gps buffer
void FakeGpsDriver_putChars(GpsDriver* gpsDriver, char* msg, size_t len){
	memcpy(gpsBuffer + bufferCount, msg, len);
	bufferCount += len;
}

// copies msg into the vars set up for the fake gps driver
static void setupFakeGpsDriverBuffer(string msg){
	bufferCount = msg.length();
	memcpy(gpsBuffer, msg.c_str(),bufferCount);
}

//copies from our test gpsBuffer and resets it
static void getFakeGpsDriverBuffer(GpsMsgBuffer* buf){
	strncpy(buf->chars, gpsBuffer, GPS_DRIVER_MAX_MESSAGE_SIZE);
	buf->len = bufferCount;
	bufferCount = 0;
}

TEST_GROUP(GpsGatewayTest){
	void setup(){
		UT_PTR_SET(GpsDriver_getChars, FakeGpsDriver_getChars);
		UT_PTR_SET(GpsDriver_putChars, FakeGpsDriver_putChars);
		GpsDriver gpsDriver;
		retFromInit = GpsGateway_init(&gpsDriver);
	}
};


// Should initialize without an error
TEST(GpsGatewayTest, Init){
	CHECK_EQUAL(0, retFromInit); // 0 means no error
}

// Should set message buffer length to 0 on init
TEST(GpsGatewayTest, InitLen0){
    GpsMsgBuffer act;
    GpsGateway_fromGpsMsgBuffer(&act);
    CHECK_EQUAL(0, act.len);
}

//Should read a complete test message
TEST(GpsGatewayTest,QueueMessage){
	string msg = "A test message\r\n";
	// expect msg less \r\n
	string exp = msg.substr(0, msg.length() - 2);
	// setup vars for FakeGpsDriver
	setupFakeGpsDriverBuffer(msg);
	GpsGateway_queueFromGpsIsr();
	GpsMsgBuffer act;
	GpsGateway_receiveFromGps();
	GpsGateway_fromGpsMsgBuffer(&act);
	CHECK_EQUAL(exp.length(), act.len);
	STRCMP_EQUAL(exp.c_str(), act.chars);
}

//Should only read a message up to \r\n, and not the rest
TEST(GpsGatewayTest,MessageExtra){
	string exp = "Another test message";
	string msg = exp + "\r\nplus some more";
	// setup vars for FakeGpsDriver
	// setup vars for FakeGpsDriver
	setupFakeGpsDriverBuffer(msg);
	GpsGateway_queueFromGpsIsr();
	GpsMsgBuffer act;
	GpsGateway_receiveFromGps();
	GpsGateway_fromGpsMsgBuffer(&act);
	CHECK_EQUAL(exp.length(), act.len);
	STRCMP_EQUAL(exp.c_str(), act.chars);
}

//Should be able to read a split  message
TEST(GpsGatewayTest,SplitMessage){
	string exp = "Humpty Dumpty sat on a wall";
    string part1 = exp.substr(0,10);
    string part2 = exp.substr(10) + "\r\n";
	// setup FakeGpsDriver with the first part
	setupFakeGpsDriverBuffer(part1);
	GpsGateway_queueFromGpsIsr();
	// write the last part of message
	setupFakeGpsDriverBuffer(part2);
	GpsGateway_queueFromGpsIsr();
	GpsGateway_receiveFromGps();
	GpsMsgBuffer act;
	GpsGateway_fromGpsMsgBuffer(&act);
	// should get all of message 2
	CHECK_EQUAL(exp.length(), act.len);
	STRCMP_EQUAL(exp.c_str(), act.chars);
}

//Should be able to read a split second message
TEST(GpsGatewayTest,SplitSecondMessage){
	string exp = "The second message which was split";
	// Can't call _readGpsQueue when there isn't a full message ready to be read
	// since it blocks.  So prep with a message + part of the second message
    string part1 = exp.substr(0,10);
    string part2 = exp.substr(10) + "\r\n";
	string msg = "First message\r\n" + part1;
	// setup FakeGpsDriver with the first message + first part of message 2
	setupFakeGpsDriverBuffer(msg);
	GpsGateway_queueFromGpsIsr();
	GpsGateway_receiveFromGps(); // get the first message (and ignore for this test)
	// write the last part of message two
	setupFakeGpsDriverBuffer(part2);
	GpsGateway_queueFromGpsIsr();
	GpsGateway_receiveFromGps();
	GpsMsgBuffer act;
	GpsGateway_fromGpsMsgBuffer(&act);
	// should get all of message 2
	CHECK_EQUAL(exp.length(), act.len);
	STRCMP_EQUAL(exp.c_str(), act.chars);
}
// should handle message split between \r and \n
//Should be able to read a split second message
TEST(GpsGatewayTest,SplitAtCrLf){
	string exp = "Humpty Dumpty sat on a wall";
    string part1 = exp + "\r";
    string part2 = "\n";
	// setup FakeGpsDriver with the first part
	setupFakeGpsDriverBuffer(part1);
	GpsGateway_queueFromGpsIsr();
	// write the last part of message
	setupFakeGpsDriverBuffer(part2);
	GpsGateway_queueFromGpsIsr();
	GpsGateway_receiveFromGps();
	GpsMsgBuffer act;
	GpsGateway_fromGpsMsgBuffer(&act);
	// should get all of message 2
	CHECK_EQUAL(exp.length(), act.len);
	STRCMP_EQUAL(exp.c_str(), act.chars);
}

// should be able to send a complete message to the GPS, auto appended with \r\n
TEST(GpsGatewayTest, SendMessage){
	string msg = "A test message to a GPS";
	string exp = msg + "\r\n";
	GpsGateway_queueToGps(msg.c_str());
	GpsGateway_receiveToGpsIsr();
	GpsMsgBuffer act;
	getFakeGpsDriverBuffer(&act);
	CHECK_EQUAL(strlen(exp.c_str()), act.len);
	STRCMP_EQUAL(exp.c_str(), act.chars);
}
