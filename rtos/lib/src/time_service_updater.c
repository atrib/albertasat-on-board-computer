/*
 * time_service_updater.c
 *
 *  Created on: Oct 30, 2014
 *      Author: albertasat
 */
#include "time_service.h"
#include "rtosport.h"
#include "FreeRTOS.h"
#include "task.h"

void vTimeServiceUpdater( void *pvParameters )
{
const char *pcTaskName = "Time Service Updater is running\n";
uint32_t lastTime;

printf( pcTaskName );
TimeService_reset();
     for( ;; )
     {
		  lastTime = TimeService_timestamp();
		  vTaskDelay(1000 / portTICK_RATE_MS); //TODO should I divide?
		  printf("Updating timestamp\n");
		  TimeService_setTimestamp(lastTime + 1);
	  }

}



